﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace ByHeartSchool
{

	namespace Repositories
	{
		public class VideoRepository : IGetDownloadDeleteRepository<string>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public VideoRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}

			public async UniTask<string> Get(string id)
			{
				if (string.IsNullOrEmpty(id))
				{
					return null;
				}
				var save = _dataSaver.LoadVideo(id);
				if (!string.IsNullOrEmpty(save))
				{
					return save;
				}
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					return null;
				}
				var videoRef = _storageRef.RootReference.Child(id);
				try
				{
					Uri url = await videoRef.GetDownloadUrlAsync().AsUniTask();
					using var request = UnityWebRequest.Get(url);
					await request.SendWebRequest();
					var bytes = request.downloadHandler.data;
					_dataSaver.SaveVideoAsync(bytes, id);
					return request.result == UnityWebRequest.Result.Success
							? _dataSaver.LoadVideo(id)
							: null;
				}
				catch
				{
					return null;
				}
			}

			public bool IsDownload(string id)
			{
				return _dataSaver.IsDownload("video", id);
			}
			public async UniTask LocalDelete(string id)
			{
				await _dataSaver.DeleteData(Path.Combine("video", id));
			}

			public void LocalDeleteAll()
			{
				_dataSaver.DeleteDirectory(Path.Combine("video"));
			}

		}
	}
}
