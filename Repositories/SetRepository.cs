﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Firestore;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class SetRepository : IGetAllRepository<Set>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public SetRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}

			public async UniTask<List<Set>> GetAll()
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<List<Set>>("settings");
					return save;
				}
				var objects = new List<Set>();
				var quarry = await _dbRef.Collection("settings").GetSnapshotAsync().AsUniTask();
				foreach (var element in quarry)
				{
					objects.Add(element.ConvertTo<Set>());
				}
				_dataSaver.SaveDataAsync(objects, "settings");
				return objects;
			}
		}
	}
}