﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase;
using Firebase.Firestore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class PlaylistRepository : IChildRepository<Playlist>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public PlaylistRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public async UniTask<List<Playlist>> GetAllChild(string id)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<List<Playlist>>("playlist-" + id);
					var saveObjects = new List<Playlist>();
					foreach (var item in save)
					{
						object name;
						object phrase_languages;
						object learn_languages;
						if (item.name is string) name = item.name;
						else name = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(item.name));
						phrase_languages = JsonConvert.DeserializeObject<List<string>>(JsonConvert.SerializeObject(item.phrase_languages));
						learn_languages = JsonConvert.DeserializeObject<List<string>>(JsonConvert.SerializeObject(item.learn_languages));
						saveObjects.Add(new Playlist()
						{
							id = item.id,
							name = name,
							phrase_languages = (List<string>)phrase_languages,
							learn_languages = (List<string>)learn_languages,
							order = item.order,
						});
					}
					return saveObjects;
				}
				var objects = new List<Playlist>();
				var quarry = await _dbRef.Collection("courses").Document(id).Collection("playlists").GetSnapshotAsync().AsUniTask();
				foreach (var obj in quarry)
				{
					objects.Add(obj.ConvertTo<Playlist>());
				}
				_dataSaver.SaveDataAsync(objects, "playlist-" + id);
				return objects;
			}
		}
	}
}