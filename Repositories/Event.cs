﻿using Firebase.Firestore;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct Event
		{
			[FirestoreDocumentId]
			public string id { get; set; }
			[FirestoreProperty]
			public Dictionary<string, object> mp3 { get; set; }
			[FirestoreProperty]
			public int order { get; set; }
			[FirestoreProperty]
			public Dictionary<string, object> phrase { get; set; }
			[FirestoreProperty]
			public string type { get; set; }
			[FirestoreProperty]
			public string version { get; set; }
			[FirestoreProperty]
			public string voice { get; set; }
			[FirestoreProperty]
			public List<string> options { get; set; }
			[FirestoreProperty]
			public string placement { get; set; }
			[FirestoreProperty]
			public string media { get; set; }
		}
	}
}
