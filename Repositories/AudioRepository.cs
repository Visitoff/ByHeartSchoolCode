﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using System;
using System.IO;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Networking;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class AudioRepository: IExRepository<AudioClip>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			private readonly ILanguageService _languageService;
			public AudioRepository(IDataSaver dataSaver, UserManager userManager, ILanguageService languageService)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
				_languageService = languageService;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}
			public bool IsDownload(string id, string id2)
			{
				return _dataSaver.IsDownload("audio", id, id2.Contains(".mp3") ? id2 : id2 + ".mp3");
			}
			public async UniTask<AudioClip> GetChild(string id, string id2)
			{
				var save = _dataSaver.LoadClip(id , id2.Contains(".mp3") ? id2 : id2 + ".mp3");
				if (save != null)
				{
					return save;
				}
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					return null;
				}
				var audioRef = _storageRef.RootReference.Child("audio").Child(id).Child(id2.Contains(".mp3") ? id2 : id2 + ".mp3");
				try
				{
					Uri url = await audioRef.GetDownloadUrlAsync().AsUniTask();
					using var request = UnityWebRequestMultimedia.GetAudioClip(url,AudioType.MPEG);
					await request.SendWebRequest();
					var audio = DownloadHandlerAudioClip.GetContent(request);
					_dataSaver.SaveAudio(audio, id, id2.Contains(".mp3") ? id2 : id2 + ".mp3");
					return request.result == UnityWebRequest.Result.Success
							? _dataSaver.LoadClip(id, id2.Contains(".mp3") ? id2 : id2 + ".mp3")
							: null;
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			public async UniTask<AudioClip> Get(string id)
			{
				var save = _dataSaver.LoadClip("overrided", id.Contains(".mp3") ? id : id + ".mp3");
				return save;
			}
			public async UniTask Update(AudioClip clip)
			{
				_dataSaver.SaveAudio(clip, "overrided", clip.name.Contains(".mp3") ? clip.name : clip.name + ".mp3");
			}
			public async UniTask LocalDelete(string id)
			{
				await _dataSaver.DeleteData(Path.Combine("audio", id) + (!id.Contains(".mp3") ? ".mp3" : null));
			}

			public void LocalDeleteAll()
			{
				var langs = _languageService.GetAll();
				foreach (var lang in langs.Keys)
				{
					_dataSaver.DeleteDirectory(Path.Combine("audio", lang));
				}
			}
		}
	}
}
