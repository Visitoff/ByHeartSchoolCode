﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public interface IBaseRepository<T> : IGetRepository<T>, IGetAllRepository<T>, IUpdateRepository<T>
		{
		}
		public interface IBaseTaskRepository<T> : IGetTaskRepository<T>, IGetAllTaskRepository<T>, IUpdateTaskRepository<T>
		{
		}
		public interface IGetRepository<T>
		{
			UniTask<T> Get(string s);
		}
		public interface IGetTaskRepository<T>
		{
			Task<T> Get(string s);
		}
		public interface IGetChildRepository<T>
		{
			UniTask<T> GetChild(string s, string s2);
		}
		public interface IGetAllRepository<T>
		{
			UniTask<List<T>> GetAll();
		}
		public interface IGetAllTaskRepository<T>
		{
			Task<List<T>> GetAll();
		}
		public interface IUpdateRepository<T>
		{
			UniTask Update(T entity);
		}
		public interface IUpdateTaskRepository<T>
		{
			Task Update(T entity);
		}
		public interface IUpdateExRepository<T>
		{
			UniTask Update(T entity, string id);
		}
		public interface IDownloadDataRepository
		{
			bool IsDownload(string id);
		}
		public interface IDownloadChildDataRepository
		{
			bool IsDownload(string id, string id2);
		}
		public interface ILocalDeleteRepository: ILocalDeleteAllRepository, ILocalDeleteOneRepository
		{
		}
		public interface ILocalDeleteOneRepository
		{
			UniTask LocalDelete(string id);
		}
		public interface ILocalDeleteAllRepository
		{
			void LocalDeleteAll();

		}
		public interface IGetDownloadRepository<T> : IGetRepository<T>, IDownloadDataRepository { }
		public interface IGetDownloadDeleteRepository<T>: IGetRepository<T>, IDownloadDataRepository, ILocalDeleteRepository
		{
		}
		public interface IExRepository<T> : IGetRepository<T>, IGetChildRepository<T>, IDownloadChildDataRepository, ILocalDeleteRepository, IUpdateRepository<T>
		{
		}
		public interface IGetChildPath
		{
			UniTask<string> GetPath(string id, string id2);
		}
		public interface IChildRepository<T>
		{
			UniTask<List<T>> GetAllChild(string id);
		}
		public interface IChildChildRepository<T>
		{
			UniTask<List<T>> GetAllChild(string id, string id2);
		}
		public interface IChildChildDownloadDeleteRepository<T> : IChildChildRepository<T>, IDownloadDataRepository, ILocalDeleteRepository { }
		public interface IChildChildDeleteRepository<T> : IChildChildRepository<T>, ILocalDeleteRepository { }
		public interface IGetDeleteRepository<T> : IGetRepository<T> , ILocalDeleteRepository { }
		public interface IGetDeleteUpdateRepository<T> : IGetRepository<T>, ILocalDeleteRepository , IUpdateExRepository<T> { }
		public interface IGetUpdateRepository<T> : IGetRepository<T>, IUpdateExRepository<T> { }
	}

}
