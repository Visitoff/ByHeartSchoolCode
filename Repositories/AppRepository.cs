﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Firestore;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class AppRepository : IGetRepository<App>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public AppRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public async UniTask<App> Get(string id)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<App>("app_" + id);
					return save;
				}
				var quarry = await _dbRef.Collection("apps").Document(id).GetSnapshotAsync().AsUniTask();
				var element = quarry.ConvertTo<App>();
				_dataSaver.SaveDataAsync(element, "app_" + id);
				return element;
			}
		}
	}
}