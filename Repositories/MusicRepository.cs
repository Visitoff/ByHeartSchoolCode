﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class MusicRepository : IGetDownloadDeleteRepository<AudioClip>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			private readonly ILanguageService _languageService;
			public MusicRepository(IDataSaver dataSaver, UserManager userManager, ILanguageService languageService)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
				_languageService = languageService;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}
			public bool IsDownload(string id)
			{
				return _dataSaver.IsDownload("audio", "music", id.Contains(".mp3") ? id : id + ".mp3");
			}
			public async UniTask<AudioClip> Get(string id)
			{
				var save = _dataSaver.LoadClip("music", id.Contains(".mp3") ? id : id + ".mp3");
				if (save != null)
				{
					return save;
				}
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					return null;
				}
				var audioRef = _storageRef.RootReference.Child(id.Contains(".mp3") ? id : id + ".mp3");
				try
				{
					Uri url = await audioRef.GetDownloadUrlAsync().AsUniTask();
					using var request = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.MPEG);
					await request.SendWebRequest();
					var audio = ((DownloadHandlerAudioClip)request.downloadHandler).audioClip;
					_dataSaver.SaveAudio(audio, "music", id.Contains(".mp3") ? id : id + ".mp3");
					return request.result == UnityWebRequest.Result.Success
							? _dataSaver.LoadClip("music", id.Contains(".mp3") ? id : id + ".mp3")
							: null;
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			public async UniTask LocalDelete(string id)
			{
				await _dataSaver.DeleteData(Path.Combine("audio", "music", id) + (!id.Contains(".mp3") ? ".mp3" : null));
			}

			public void LocalDeleteAll()
			{
				var langs = _languageService.GetAll();
				foreach (var lang in langs.Keys)
				{
					_dataSaver.DeleteDirectory(Path.Combine("audio", lang));
				}
			}
		}
	}
}
