﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.Networking;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class MapRepository : IGetDeleteUpdateRepository<Dictionary<string, string>>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			private readonly ILanguageService _languageService;
			public MapRepository(IDataSaver dataSaver, UserManager userManager, ILanguageService languageService) 
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
				_languageService = languageService;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}
			public async UniTask<Dictionary<string, string>> Get(string id)
			{
				var save = await _dataSaver.LoadDataAsync<string>(id);
				if (save != null)
				{
					var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(save);
					return result; 
				}

				var mapRef = _storageRef.RootReference.Child("audio").Child(id + ".json");
				try
				{
					Uri url = await mapRef.GetDownloadUrlAsync().AsUniTask();
					using var request = UnityWebRequest.Get(url);
					await request.SendWebRequest();
					var json = ((DownloadHandler)request.downloadHandler).text;
					_dataSaver.SaveDataAsync(json, id);
					return request.result == UnityWebRequest.Result.Success
							? JsonConvert.DeserializeObject<Dictionary<string, string>>(json)
							: null;
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			public async UniTask Update(Dictionary<string, string> data, string id)
			{
				await _dataSaver.SaveDataAsync(data, id);
			}
			public async UniTask LocalDelete(string id)
			{ 
				await _dataSaver.DeleteData(id);
			}

			public void LocalDeleteAll()
			{
				var langs = _languageService.GetAll();
				foreach (var lang in langs.Keys)
				{
					_dataSaver.DeleteData(lang);
				}
			}
		}
	}
}
