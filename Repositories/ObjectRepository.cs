﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Firestore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class ObjectRepository : IBaseRepository<Object>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public ObjectRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public async UniTask<List<Object>> GetAll()
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<List<Object>>("objects");
					var saveObjects = new List<Object>();
					foreach (var item in save)
					{
						object body_text;
						object title_text;
						object specification_text;
						if (item.body_text is string) body_text = item.body_text;
						else body_text = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(item.body_text));
						if (item.title_text is string) title_text = item.title_text;
						else title_text = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(item.title_text));
						if (item.specification_text is string) specification_text = item.specification_text;
						else specification_text = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(item.specification_text));
						saveObjects.Add(new Object()
						{
							id = item.id,
							action = item.action,
							img = item.img,
							ratio = item.ratio,
							type = item.type,
							body_text = body_text,
							title_text = title_text,
							specification_text = specification_text,
							ss = item.ss,
							gallery = item.gallery,
							screen = item.screen,
						});
					}
					return saveObjects;
				}
				var objects = new List<Object>();
				var quarry = await _dbRef.Collection("menu").Document("objects").Collection("objects").GetSnapshotAsync().AsUniTask();
				var quarry2 = await _dbRef.Collection("menu").Document("objects").Collection("system").GetSnapshotAsync().AsUniTask();
				foreach (var obj in quarry)
				{
					var _obj = obj.ConvertTo<Object>();
					objects.Add(_obj);
				}
				foreach (var obj in quarry2)
				{
					var _obj = obj.ConvertTo<Object>();
					objects.Add(_obj);
				}
				_dataSaver.SaveDataAsync(objects, "objects");
				return objects;
			}
			public async UniTask<Object> Get(string id)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<Object>(id);
					return save;
				}
				var objects = new List<Object>();
				var quarry = await _dbRef.Collection("menu").Document("objects").Collection("objects").GetSnapshotAsync().AsUniTask();
				foreach (var _obj in quarry)
				{
					objects.Add(_obj.ConvertTo<Object>());
				}
				var obj = objects.FirstOrDefault(x => (x.id == id));
				_dataSaver.SaveDataAsync(obj, id);
				return obj;
			}
			public async UniTask Update(Object newObj)
			{
				UnityEngine.Debug.LogError("You can`t update this data.");
			}
		}
	}
}