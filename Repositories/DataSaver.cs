﻿using System;
using System.IO;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using Cysharp.Threading.Tasks;
using System.Threading.Tasks;

namespace ByHeartSchool
{

	namespace Repositories
	{
		public class DataSaver : IDataSaver
		{
			public async UniTask SaveDataAsync<T>(T dataToSave, string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, dataFileName + ".txt");

				JsonConvert.DefaultSettings = () => new JsonSerializerSettings();
				string jsonData = JsonConvert.SerializeObject(dataToSave, Formatting.Indented, new JsonSerializerSettings
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore
				});
				byte[] jsonByte = Encoding.UTF8.GetBytes(jsonData);


				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
				}

				try
				{
					await File.WriteAllBytesAsync(tempPath, jsonByte).AsUniTask();
					Debug.Log("Saved Data to: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To PlayerInfo Data to: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}
			}
			public static void SaveDataStreaming<T>(T dataToSave, string dataFileName)
			{
				string tempPath = Path.Combine(Application.streamingAssetsPath);
				tempPath = Path.Combine(tempPath, dataFileName + ".txt");

				JsonConvert.DefaultSettings = () => new JsonSerializerSettings();
				string jsonData = JsonConvert.SerializeObject(dataToSave, Formatting.Indented, new JsonSerializerSettings
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore
				});
				byte[] jsonByte = Encoding.UTF8.GetBytes(jsonData);


				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
				}

				try
				{
					File.WriteAllBytes(tempPath, jsonByte);
					Debug.Log("Saved Data to: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To PlayerInfo Data to: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}
			}
			public async UniTask SaveTextureAsync(Texture2D dataToSave, string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, dataFileName);
				byte[] bytes = ImageConversion.EncodeToPNG(dataToSave);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
				}
				try
				{
					await File.WriteAllBytesAsync(tempPath, bytes).AsUniTask();
					Debug.Log("Saved Data to: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To PlayerInfo Data to: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}
			}
			public void SaveAudio(AudioClip dataToSave, string langFolder, string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data" ,"audio", langFolder);
				tempPath = Path.Combine(tempPath, dataFileName);
				BinaryFormatter bf = new BinaryFormatter();
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
				}
				try
				{
					var file = File.Create(tempPath);
					AudioClipSample newSample = new AudioClipSample();
					newSample.sample = new float[dataToSave.samples * dataToSave.channels];
					newSample.frequency = dataToSave.frequency;
					newSample.samples = dataToSave.samples;
					newSample.channels = dataToSave.channels;
					dataToSave.GetData(newSample.sample, 0);
					bf.Serialize(file, newSample);
					file.Close();
					Debug.Log("Saved Data to: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To PlayerInfo Data to: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}
			}
			public bool IsDownloadTxt(string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, dataFileName + ".txt");
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return false;
				}
				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return false;
				}
				return true;
			}
			public bool IsDownload(string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, dataFileName);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return false;
				}
				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return false;
				}
				return true;
			}
			public bool IsDownload(string folder, string folder2,string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, folder, folder2, dataFileName);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return false;
				}
				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return false;
				}
				return true;
			}
			public bool IsDownload(string folder, string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, folder, dataFileName);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return false;
				}
				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return false;
				}
				return true;
			}
			public async UniTask<T> LoadDataAsync<T>(string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, dataFileName + ".txt");

				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return default(T);
				}

				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return default(T);
				}

				byte[] jsonByte = null;
				try
				{
					jsonByte = await File.ReadAllBytesAsync(tempPath).AsUniTask();
					Debug.Log("Loaded Data from: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Load Data from: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}

				string jsonData = Encoding.UTF8.GetString(jsonByte);

				object resultValue = JsonConvert.DeserializeObject<T>(jsonData);
				return (T)Convert.ChangeType(resultValue, typeof(T));
			}
			public static T LoadDataStreaming<T>(string dataFileName)
			{
				string tempPath = Path.Combine(Application.streamingAssetsPath);
				tempPath = Path.Combine(tempPath, dataFileName + ".txt");

				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return default(T);
				}

				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return default(T);
				}

				byte[] jsonByte = null;
				try
				{
					jsonByte = File.ReadAllBytes(tempPath);
					Debug.Log("Loaded Data from: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Load Data from: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}

				string jsonData = Encoding.UTF8.GetString(jsonByte);

				object resultValue = JsonConvert.DeserializeObject<T>(jsonData);
				return (T)Convert.ChangeType(resultValue, typeof(T));
			}
			public async UniTask<Texture2D> LoadPNGAsync(string dataFileName)
			{
				if (string.IsNullOrEmpty(dataFileName))
				{
					Debug.LogWarning("dataFileName is NULL");
					return null;
				}
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, dataFileName);

				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					return null;
				}

				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return null;
				}

				byte[] jsonByte = null;
				try
				{
					jsonByte = await File.ReadAllBytesAsync(tempPath).AsUniTask();
					Debug.Log("Loaded Data from: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Load Data from: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}

				Texture2D texture = new Texture2D(1, 1);
				ImageConversion.LoadImage(texture, jsonByte);
				return texture;
			}
			public AudioClip LoadClip(string langFolder, string dataFileName)
			{
				if (string.IsNullOrEmpty(dataFileName))
				{
					Debug.LogWarning("dataFileName is NULL");
					return null;
				}
				string tempPath = Path.Combine(Application.persistentDataPath, "data" , "audio", langFolder);
				tempPath = Path.Combine(tempPath, dataFileName);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					return null;
				}
				if (!File.Exists(tempPath))
				{
					return null;
				}
				byte[] jsonByte = null;
				try
				{
					BinaryFormatter bf = new BinaryFormatter();
					FileStream file = File.Open(tempPath, FileMode.Open);
					AudioClipSample clipSample = (AudioClipSample)bf.Deserialize(file);
					file.Close();

					AudioClip newClip = AudioClip.Create(dataFileName, clipSample.samples, clipSample.channels, clipSample.frequency, false);
					newClip.SetData(clipSample.sample, 0);
					Debug.Log("Loaded Data from: " + tempPath.Replace("/", "\\"));
					return newClip;
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Load Data from: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
					return null;
				}
			}
			public string GetClipPath(string langFolder, string dataFileName)
			{
				if (string.IsNullOrEmpty(dataFileName))
				{
					Debug.LogWarning("dataFileName is NULL");
					return null;
				}
				string tempPath = Path.Combine(Application.persistentDataPath, "data", "audio", langFolder);
				tempPath = Path.Combine(tempPath, dataFileName);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					return null;
				}
				if (!File.Exists(tempPath))
				{
					return null;
				}
				return tempPath;
			}
			public async UniTask<bool> DeleteData(string dataFilePath)
			{
				bool success = false;
				string tempPath = Application.persistentDataPath;
				tempPath = Path.Combine(tempPath, "data" , dataFilePath);

				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist" + tempPath);
					return false;
				}
				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist" + tempPath);
					return false;
				}
				try
				{
					var t = Task.Factory.StartNew(() => File.Delete(tempPath)).AsUniTask();
					await t;
					Debug.Log("Data deleted from: " + tempPath.Replace("/", "\\"));
					success = true;
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Delete Data: " + e.Message);
				}
				return success;
			}
			public bool DeleteDataRange(string searchOptions)
			{
				bool success = false;
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				var info = new DirectoryInfo(tempPath);
				var files =  info.GetFiles(searchOptions + "*");
				try
				{
					foreach (var file in files)
					{
						if (File.Exists(Path.Combine(tempPath, file.Name)))
						{
							File.Delete(Path.Combine(tempPath, file.Name));
						}
					}
					success = true;
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Delete Data: " + e.Message);
				}
				return success;
			}
			public bool DeleteDirectory(string directoryName)
			{
				bool success = false;
				string tempPath = Path.Combine(Application.persistentDataPath, "data");
				tempPath = Path.Combine(tempPath, directoryName);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return false;
				}
				try
				{
					Directory.Delete(tempPath, true);
					Debug.Log("Directory deleted from: " + tempPath.Replace("/", "\\"));
					success = true;
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To Delete Data: " + e.Message);
				}
				return success;
			}

			public string LoadVideo(string dataFileName)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data", "video");
				tempPath = Path.Combine(tempPath, dataFileName);

				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Debug.LogWarning("Directory does not exist");
					return null;
				}

				if (!File.Exists(tempPath))
				{
					Debug.Log("File does not exist");
					return null;
				}

				return tempPath;
			}

			public async UniTask SaveVideoAsync(byte[] bytes, string dataToSave)
			{
				string tempPath = Path.Combine(Application.persistentDataPath, "data", "video");
				tempPath = Path.Combine(tempPath, dataToSave);
				if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
				}

				try
				{
					await File.WriteAllBytesAsync(tempPath, bytes).AsUniTask();
					Debug.Log("Saved Data to: " + tempPath.Replace("/", "\\"));
				}
				catch (Exception e)
				{
					Debug.LogWarning("Failed To PlayerInfo Data to: " + tempPath.Replace("/", "\\"));
					Debug.LogWarning("Error: " + e.Message);
				}
			}
		}
		[Serializable]
		class AudioClipSample
		{
			public int frequency;
			public int samples;
			public int channels;
			public float[] sample;
		}
	}
}
