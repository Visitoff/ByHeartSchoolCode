﻿using Firebase.Firestore;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct App
		{
			[FirestoreDocumentId]
			public string id { get; set; }
			[FirestoreProperty]
			public Dictionary<string, string> roles { get; set; }
			[FirestoreProperty]
			public Dictionary<string, string> wallpapers { get; set; }
		}
	}
}
