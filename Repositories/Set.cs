﻿using Firebase.Firestore;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct Set
		{
			[FirestoreDocumentId]
			public string id { get; set; }
			[FirestoreProperty]
			public List<string> options { get; set; }
			[FirestoreProperty]
			public string name { get; set; }
			[FirestoreProperty]
			public bool shown { get; set; }
			[FirestoreProperty]
			public int order { get; set; }
			[FirestoreProperty]
			public int index { get; set; }
		}
	}
}
