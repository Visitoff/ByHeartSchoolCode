﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Firestore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class EventRepository : IChildChildDownloadDeleteRepository<Event>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public EventRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public bool IsDownload(string playlist)
			{
				return _dataSaver.IsDownloadTxt("events-" + playlist);
			}
			public async UniTask LocalDelete(string playlist)
			{
				await _dataSaver.DeleteData("events-" + playlist + ".txt");
			}
			public async UniTask<List<Event>> GetAllChild(string course, string playlist)
			{
				var save = await _dataSaver.LoadDataAsync<List<Event>>("events-" + playlist);
				var saveObjects = new List<Event>();
				if (save != null)
				{
					foreach (var item in save)
					{
						Dictionary<string, object> mp3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(item.mp3));
						saveObjects.Add(new Event()
						{
							mp3 = mp3,
							order = item.order,
							id = item.id,
							phrase = item.phrase,
							type = item.type,
							version = item.version,
							voice = item.voice,
							options = item.options,
							placement = item.placement,
							media = item.media,
						});
					}
					return saveObjects;
				}
				if (string.IsNullOrEmpty(course)) return null;
				var objects = new List<Event>();
				var quarry = await _dbRef.Collection("courses").Document(course).Collection("playlists").Document(playlist).Collection("events").GetSnapshotAsync().AsUniTask();
				foreach (var obj in quarry)
				{
					objects.Add(obj.ConvertTo<Event>());
				}
				_dataSaver.SaveDataAsync(objects, "events-" + playlist);
				return objects;
			}

			public void LocalDeleteAll()
			{
				_dataSaver.DeleteDataRange("events");
			}
		}
	}
}