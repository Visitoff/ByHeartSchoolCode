﻿using Firebase.Firestore;
using System;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct Course
		{
			[FirestoreDocumentId]
			public string id {  get; set; }
			[FirestoreProperty]
			public bool _shown { get; set; }
			[FirestoreProperty]
			public List<string> learn_languages { get; set; }
			[FirestoreProperty]
			public Dictionary<string, object> name { get; set; }
			[FirestoreProperty]
			public List<string> phrase_languages { get; set; }
			[FirestoreProperty]
			public DateTimeOffset last_update { get; set; }
		}
	}

}
