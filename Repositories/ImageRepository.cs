﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using System;
using UnityEngine;
using UnityEngine.Networking;

namespace ByHeartSchool
{

	namespace Repositories
	{

		public class ImageRepository : IGetDownloadRepository<Texture2D>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public ImageRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}
			public async UniTask<Texture2D> Get(string id)
			{
				try
				{
					if (string.IsNullOrEmpty(id))
					{
						return null;
					}
					var save = await _dataSaver.LoadPNGAsync(id.Contains('/') ? id.Substring(id.LastIndexOf('/') + 1) : id);
					if (save != null)
					{
						save = Compress(save);
						return save;
					}
					if (Application.internetReachability == NetworkReachability.NotReachable)
					{
						return null;
					}

					var imageRef = _storageRef.RootReference.Child(id);

					Uri url = await imageRef.GetDownloadUrlAsync().AsUniTask();
					using var request = UnityWebRequestTexture.GetTexture(url);
					await request.SendWebRequest();
					var texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
					_dataSaver.SaveTextureAsync(texture, id.Contains('/') ? id.Substring(id.LastIndexOf('/') + 1) : id);
					texture = Compress(texture);
					return request.result == UnityWebRequest.Result.Success
							? texture
							: null;
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			private Texture2D Compress(Texture2D texture)
			{
				if (texture != null && texture.width % 4 + texture.height % 4 == 0)
				{
					texture.Compress(false);
					texture.Apply(false, true);
					return texture;
				}
				texture.Apply(false, true);
				return texture;
			}
			public bool IsDownload(string id)
			{
				return _dataSaver.IsDownload(id.Contains('/') ? id.Substring(id.LastIndexOf('/') + 1) : id);
			}
		}
	}
}
