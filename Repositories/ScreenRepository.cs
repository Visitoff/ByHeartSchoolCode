﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Firestore;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class ScreenRepository : IBaseRepository<Screen>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public ScreenRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public async UniTask<List<Screen>> GetAll()
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var saveScreen = await _dataSaver.LoadDataAsync<List<Screen>>("screens");
					return saveScreen;
				}
				var screens = new List<Screen>();
				var quarry = await _dbRef.Collection("menu").Document("screens").Collection("screens").GetSnapshotAsync().AsUniTask();
				var quarry2 = await _dbRef.Collection("menu").Document("screens").Collection("system").GetSnapshotAsync().AsUniTask();
				foreach (var screen in quarry)
				{
					screens.Add(screen.ConvertTo<Screen>());
				}
				foreach (var screen in quarry2)
				{
					screens.Add(screen.ConvertTo<Screen>());
				}
				_dataSaver.SaveDataAsync(screens, "screens");
				return screens;
			}
			public async UniTask<Screen> Get(string id)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var saveScreen = await _dataSaver.LoadDataAsync<Screen>(id);
					return saveScreen;
				}
				var quarry = await _dbRef.Collection("menu").Document("screens").Collection("screens").Document(id).GetSnapshotAsync().AsUniTask();
				var screen = quarry.ConvertTo<Screen>();
				_dataSaver.SaveDataAsync(screen, id);
				return screen;
			}
			public async UniTask Update(Screen newScreen)
			{
				UnityEngine.Debug.LogError("You can`t update this data.");
			}
		}
	}
}