﻿using Zenject;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class RepositoriesInstaller : Installer<RepositoriesInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<UserRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<DataSaver>().AsSingle();
				Container.BindInterfacesAndSelfTo<ImageRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<AudioRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<CourseRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<ScreenRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<ObjectRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<PlaylistRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<EventRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<MusicRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<SetRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<VideoRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<MapDateTimeRepository>().AsSingle();
				Container.BindInterfacesAndSelfTo<AppRepository>().AsSingle();
			}
		}
	}

}
