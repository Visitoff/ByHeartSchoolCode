﻿using Cysharp.Threading.Tasks;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Video;

namespace ByHeartSchool
{

	namespace Repositories
	{
		public interface IDataSaver
		{
			UniTask SaveDataAsync<T>(T dataToSave, string dataFileName);
			UniTask SaveTextureAsync(Texture2D dataToSave, string dataFileName);
			bool IsDownloadTxt(string dataFileName);
			bool IsDownload(string dataFileName);
			bool IsDownload(string folder, string folder2, string dataFileName);
			void SaveAudio(AudioClip dataToSave, string langFolder,string dataFileName);
			UniTask<T> LoadDataAsync<T>(string dataFileName);
			UniTask<Texture2D> LoadPNGAsync(string dataFileName);
			UniTask<bool> DeleteData(string dataFileName);
			AudioClip LoadClip(string langFolder, string dataFileName);
			bool DeleteDirectory(string directoryName);
			bool DeleteDataRange(string searchOptions);
			bool IsDownload(string folder, string dataFileName);
			string LoadVideo(string dataFileName);
			UniTask SaveVideoAsync(byte[] bytes, string dataToSave);
			public string GetClipPath(string langFolder, string dataFileName);
		}
	}
}
