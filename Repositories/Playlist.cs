﻿using Firebase.Firestore;
using System;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct Playlist
		{
			[FirestoreDocumentId]
			public string id { get; set; }
			[FirestoreProperty]
			public List<string> learn_languages { get; set; }
			[FirestoreProperty]
			public object name { get; set; }
			[FirestoreProperty]
			public int order { get; set; }
			[FirestoreProperty]
			public List<string> phrase_languages { get; set; }
			[FirestoreProperty]
			public object flags { get; set; }
	}
	}
}
