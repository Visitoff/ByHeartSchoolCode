﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine.Networking;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class ListRepository : IChildChildDeleteRepository<string>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			private readonly ILanguageService _languageService;
			public ListRepository(IDataSaver dataSaver, UserManager userManager, ILanguageService languageService)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
				_languageService = languageService;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}

			public async UniTask<List<string>> GetAllChild(string id, string id2)
			{
				var save = await _dataSaver.LoadDataAsync<List<string>>("list-" + id2);
				if (save != null) return save;
				var listRef = _storageRef.RootReference.Child(id).Child(id2);
				try
				{
					Uri url = await listRef.GetDownloadUrlAsync();
					using var request = UnityWebRequest.Get(url);
					await request.SendWebRequest();
					var index = ((DownloadHandler)request.downloadHandler).text;
					var indexes = index.Split('\n').ToList();
					_dataSaver.SaveDataAsync(indexes, "list-" +id2);
					return request.result == UnityWebRequest.Result.Success
							? indexes
							: null;
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			public async UniTask LocalDelete(string id)
			{
				await _dataSaver.DeleteData("list-" + id);
			}

			public void LocalDeleteAll()
			{
				var langs = _languageService.GetAll();
				foreach (var lang in langs.Keys)
				{
					_dataSaver.DeleteData("list-" + lang);
				}
			}
		}
	}
}
