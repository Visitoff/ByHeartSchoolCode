﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Firestore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class CourseRepository : IBaseRepository<Course>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			public CourseRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public async UniTask<List<Course>> GetAll()
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<List<Course>>("courses");
					return save;
				}
				var courses = new List<Course>();
				var quarry = await _dbRef.Collection("courses").GetSnapshotAsync().AsUniTask();
				foreach (var course in quarry)
				{
					courses.Add(course.ConvertTo<Course>());
				}
				_dataSaver.SaveDataAsync(courses, "courses");
				return courses;
			}
			public async UniTask<Course> Get(string id)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<Course>(id);
					return save;
				}
				var courses = new List<Course>();
				var quarry = await _dbRef.Collection("courses").GetSnapshotAsync().AsUniTask();
				foreach (var _course in quarry)
				{
					courses.Add(_course.ConvertTo<Course>());
				}
				var course = courses.FirstOrDefault(x => (x.id == id));
				_dataSaver.SaveDataAsync(course, id);
				return course;
			}
			public async UniTask Update(Course newCourse)
			{
				UnityEngine.Debug.LogError("You can`t update this data.");
			}
		}

	}

}