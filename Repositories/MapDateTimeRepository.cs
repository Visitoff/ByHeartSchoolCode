﻿using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using Firebase.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class MapDateTimeRepository : IGetUpdateRepository<Dictionary<string, DateTimeOffset>>
		{
			private FirebaseStorage _storageRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			private readonly ILanguageService _languageService;
			public MapDateTimeRepository(IDataSaver dataSaver, UserManager userManager, ILanguageService languageService)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
				_languageService = languageService;
			}
			public void Init()
			{
				_storageRef = FirebaseStorage.GetInstance("gs://heart-school-europe");
			}
			public async UniTask<Dictionary<string, DateTimeOffset>> Get(string id)
			{
				var save = await _dataSaver.LoadDataAsync<Dictionary<string, DateTimeOffset>>(id);
				if (save != null)
				{
					return save;
				}
				return new();
			}
			public async UniTask Update(Dictionary<string, DateTimeOffset> data, string id)
			{
				await _dataSaver.SaveDataAsync(data, id);
			}
		}
	}
}
