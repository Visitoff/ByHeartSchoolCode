﻿using Firebase.Firestore;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[Serializable]
		[FirestoreData]
		public struct Screen
		{
			[FirestoreDocumentId]
			public string id {  get; set; }
			[FirestoreProperty]
			public bool _shown { get; set; }
			[FirestoreProperty]
			public string app_bar_title { get; set; }
			[FirestoreProperty]
			public string background_color { get; set; }
			[FirestoreProperty]
			public List<string> objects { get; set; }
		}

	}
}
