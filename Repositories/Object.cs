﻿using Firebase.Firestore;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct Object
		{
			public int order;

			[FirestoreDocumentId]
			public string id { get; set; }
			[FirestoreProperty]
			public string action { get; set; }
			[FirestoreProperty]
			public object body_text { get; set; }
			[FirestoreProperty]
			public string img { get; set; }
			[FirestoreProperty]
			public float ratio { get; set; }
			[FirestoreProperty]
			public object specification_text { get; set; }
			[FirestoreProperty]
			public object title_text { get; set; }
			[FirestoreProperty]
			public string type { get; set; }
			[FirestoreProperty]
			public bool ss { get; set; }
			[FirestoreProperty]
			public List<string> learn_languages { get; set; }
			[FirestoreProperty]
			public Dictionary<string, string> gallery { get; set; }
			[FirestoreProperty]
			public string screen { get; set; }
		}

	}
}
