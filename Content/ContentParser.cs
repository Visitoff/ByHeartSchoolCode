using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UIElements;

public class ContentParser : MonoBehaviour
{

    [SerializeField] private VisualTreeAsset visualTreeAsset;
    [SerializeField] private TextAsset textAsset;
    [SerializeField] private Texture2D testImage;
    [SerializeField] private ContentSelector[] contentSelectors;
    [SerializeField] private string currentContentSelectorName;

    private string[] ALletters = {"-", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    private ContentSelector contentSelector;

    // Start is called before the first frame update
    void Start()
    {
        LoadContentSelector();
        CreateContent();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LoadContentSelector() {
        foreach (ContentSelector currentContentSelector in contentSelectors) {
            if (currentContentSelector.LanguageName.Equals(currentContentSelectorName)) {
                contentSelector = currentContentSelector;
                break;
            }
        }
    }

    private void CreateContent() {
        var panel = GetComponent<UIDocument>().rootVisualElement;
        ScrollView scrollView = panel.Q<ScrollView>("scroll-view-main");

        string content = textAsset.text;

        List<string[]> parsedContent = new List<string[]>();
        string[] lines = content.Split("\n");

        for (int j = 0; j < lines.Length; j++) {
            string currentLine = lines[j];
            string[] currentLineElements = Regex.Split(currentLine, ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            parsedContent.Add(currentLineElements);
        }

        int i = 1;
        int listIndex = 1;

        bool bResetListIndex = false;
        bool bContinueLoop = false;


        while (i < lines.Length) {
            bContinueLoop = false;

            string placement = parsedContent[i][1];
            string media = parsedContent[i][2];
            string type = parsedContent[i][3];
            string option = parsedContent[i][4];

            if (type.Equals("T")) {
                string textData = parsedContent[i][contentSelector.Number];

                // Headers
                if (option.Equals("H1")) {
                    Label label = new Label(textData) {
                        style = {
                            whiteSpace = WhiteSpace.Normal,
                            fontSize = 24,
                            paddingRight = 10,
                            paddingLeft = 7,
                            unityFontStyleAndWeight = FontStyle.Bold
                        }
                    };
                    scrollView.Add(label);
                }
                if (option.Equals("H2")) {
                    Label label = new Label(textData) {
                        style = {
                            whiteSpace = WhiteSpace.Normal,
                            fontSize = 20,
                            paddingRight = 10,
                            paddingLeft = 7,
                            unityFontStyleAndWeight = FontStyle.Bold
                        }
                    };
                    scrollView.Add(label);
                }
                if (option.Equals("H3")) {
                    Label label = new Label(textData) {
                        style = {
                            whiteSpace = WhiteSpace.Normal,
                            fontSize = 18,
                            paddingRight = 10,
                            paddingLeft = 7,
                            unityFontStyleAndWeight = FontStyle.Bold
                        }
                    };
                    scrollView.Add(label);
                }

                // Citate
                if (option.Equals("CITE")) {
                    Label label = new Label(textData) {
                        style = {
                            whiteSpace = WhiteSpace.Normal,
                            fontSize = 12,
                            paddingLeft = 25,
                            color = new Color(0.4f,0.4f,0.4f),
                        }
                    };
                    scrollView.Add(label);
                }

                // Normal text
                if (option.Equals("")) {
                    string finalText = textData;
                    int e = i;
                    while (true) {
                        if (e + 1 < lines.Length) {
                            string nextType = parsedContent[e+1][3];
                            string nextOption = parsedContent[e+1][4];
                            string nextTextData = parsedContent[e+1][contentSelector.Number];

                            if (nextType.Equals("T") && nextOption.Equals("")) {
                                finalText += " " + nextTextData;
                            }

                            if (nextType.Equals("P") && nextOption.Equals("")) {
                                finalText += "\n\n";
                            }

                            if (!nextOption.Equals("") || !nextType.Equals("T")) {
                                i = e + 1;
                                bContinueLoop = true;
                                break;
                            }
                        } else {
                            break;
                        }

                        e += 1;
                    }

                    Label label = new Label(finalText) {
                        style = {
                            whiteSpace = WhiteSpace.Normal,
                            fontSize = 15,
                            paddingRight = 10,
                            paddingLeft = 7
                        }
                    };
                    scrollView.Add(label);
                }

                // Lists
                // no list (or end of list)
                if (!option.Contains("UL") && !option.Contains("NL") && !option.Contains("AL") && !option.Contains("BULLET")) {
                    bResetListIndex = true;
                }

                // UL
                if (option.Contains("UL,") || option.Equals("UL")) {
                    if (option.Contains("BULLET")) {
                        Label label = new Label("- " + textData) {
                            style = {
                                whiteSpace = WhiteSpace.Normal,
                                fontSize = 15,
                                paddingLeft = 20,
                                unityFontStyleAndWeight = FontStyle.Bold
                            }
                        };
                        scrollView.Add(label);

                        listIndex += 1;
                    } else {
                        Label label = new Label(textData) {
                            style = {
                                whiteSpace = WhiteSpace.Normal,
                                fontSize = 15,
                                paddingLeft = 20
                            }
                        };
                        scrollView.Add(label);
                    }
                }

                // NL
                if (option.Contains("NL,") || option.Equals("NL")) {
                    if (option.Contains("BULLET")) {
                        Label label = new Label(listIndex.ToString() + ". " + textData) {
                            style = {
                                whiteSpace = WhiteSpace.Normal,
                                fontSize = 15,
                                paddingLeft = 20,
                                unityFontStyleAndWeight = FontStyle.Bold
                            }
                        };
                        scrollView.Add(label);

                        listIndex += 1;
                    } else {
                        Label label = new Label(textData) {
                            style = {
                                whiteSpace = WhiteSpace.Normal,
                                fontSize = 15,
                                paddingLeft = 20
                            }
                        };
                        scrollView.Add(label);
                    }
                }

                // AL
                if (option.Contains("AL,") || option.Equals("AL")) {
                    if (option.Contains("BULLET")) {
                        Label label = new Label(ALletters[listIndex] + ". " + textData) {
                            style = {
                                whiteSpace = WhiteSpace.Normal,
                                fontSize = 15,
                                paddingRight = 5,
                                paddingLeft = 10,
                                unityFontStyleAndWeight = FontStyle.Bold
                            }
                        };
                        scrollView.Add(label);

                        listIndex += 1;
                    } else {
                        Label label = new Label(textData) {
                            style = {
                                whiteSpace = WhiteSpace.Normal,
                                fontSize = 15,
                                paddingRight = 5,
                                paddingLeft = 10
                            }
                        };
                        scrollView.Add(label);
                    }
                }
            }

            if (type.Equals("IMG")) {
                if (option.Equals("")) {
                    Image image = new Image();
                    image.image = testImage;
                    
                    scrollView.Add(image);
                }
                
                if (option.Equals("WIDE")) {
                    Image image = new Image();
                    image.image = testImage;

                    scrollView.Add(image);
                }
            }

            if (type.Equals("PB")) {
                // TODO: later
            }

            if (type.Equals("VIDEO")) {
                // TODO: later
            }

            if (type.Equals("MAP")) {
                // TODO: later
            }

            if (type.Equals("UNITY")) {
                // TODO: later
            }

            if (type.Contains("T") && (type.Contains("1") || type.Contains("2") || type.Contains("3") || type.Contains("4") || type.Contains("5") || type.Contains("6") || type.Contains("7") || type.Contains("8") || type.Contains("9"))) {
                int order = int.Parse(type.Replace("T", ""));
                int e = i;

                string textData = parsedContent[i][contentSelector.Number];
                string finalText = textData + "\n";

                while (true) {
                    if (e + 1 < lines.Length) {
                        string nextType = parsedContent[e+1][3];
                        string nextOption = parsedContent[e+1][4];
                        string nextTextData = parsedContent[e+1][contentSelector.Number];

                        if (nextType.Equals(type)) {
                            finalText += nextTextData + "\n";
                        }

                        if (!nextType.Equals(type)) {
                            i = e + 1;
                            bContinueLoop = true;
                            break;
                        }
                    } else {
                        break;
                    }

                    e += 1;
                }

                if (order % 2 == 0) {
                    
                } else {
                    
                }

                Label label = new Label(finalText) {
                    style = {
                        whiteSpace = WhiteSpace.Normal,
                        fontSize = 13,
                        borderBottomLeftRadius = 30,
                        borderTopRightRadius = 30,
                        borderBottomRightRadius = 30,
                        borderTopLeftRadius = 30,
                        backgroundColor = new Color(100, 100, 100),
                        borderBottomColor = new Color(0,0,0),
                        borderRightColor = new Color(0,0,0),
                        borderTopColor = new Color(0,0,0),
                        borderLeftColor = new Color(0,0,0),
                        borderBottomWidth = 2,
                        borderLeftWidth = 2,
                        borderRightWidth = 2,
                        borderTopWidth = 2,
                        paddingLeft = 20
                    }
                };

                scrollView.Add(label);
            }

            if (type.Equals("A") || type.Equals("B")) {
                string textData = parsedContent[i][contentSelector.Number];

                if (type.Equals("A")) {

                }
                if (type.Equals("B")) {

                }

                Label label = new Label(textData) {
                    style = {
                        whiteSpace = WhiteSpace.Normal,
                        fontSize = 13,
                        borderBottomLeftRadius = 30,
                        borderTopRightRadius = 30,
                        borderBottomRightRadius = 30,
                        borderTopLeftRadius = 30,
                        backgroundColor = new Color(100, 100, 100),
                        borderBottomColor = new Color(0,0,0),
                        borderRightColor = new Color(0,0,0),
                        borderTopColor = new Color(0,0,0),
                        borderLeftColor = new Color(0,0,0),
                        borderBottomWidth = 2,
                        borderLeftWidth = 2,
                        borderRightWidth = 2,
                        borderTopWidth = 2
                    }
                };

                scrollView.Add(label);
            }

            // end of WHILE
            if (bResetListIndex) {
                listIndex = 1;
                bResetListIndex = false;
            }
            if (bContinueLoop) {
                continue;
            }

            i += 1;
        }
    }
}

[System.Serializable]
public class ContentSelector
{
    public string LanguageName;
    public int Number;
}
