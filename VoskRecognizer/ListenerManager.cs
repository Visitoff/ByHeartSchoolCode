﻿using Recognize;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Recognizer
{
    public class ListenerManager : MonoBehaviour
    {
        [SerializeField] private Server _speechRecognizer;
        [SerializeField] private RecognizerManager _recognizerManager;
        private string[] _phrases;
        private List<string> _phrasesForRecognize = new List<string>();
        public bool IsPause => !_speechRecognizer.Working;
        public UnityAction OnStartProcessing { set { _speechRecognizer.Mircrophone.OnRecordingStart = () => value(); } }
        private UnityAction<string> OnResult { get; set; }

		public void Init(SystemLanguage language, string[] phrases)
        {
            _speechRecognizer.SetNewLanguageByCode(language);
			_phrases = phrases;
		}
		public void UpdateLanguages(SystemLanguage language)
		{
		    _speechRecognizer.SetNewLanguageByCode(language);
		}
		public int GetPhrasesForRecogniseCount()
        {
            return _phrasesForRecognize.Count;
        }
        public void CreatePhrase(int index, bool hardMode)
        {
            Debug.Log(_phrases[index]);
			_phrasesForRecognize.Clear();
            string[] array = _phrases[index].Split(' ', '-', '.', ',', ':', '。', '、', '！', '？', '…', '?', '!', '，', '一', '؜');
            foreach (string token in array)
            {
                string filteredToken = token;
                foreach (var c in filteredToken)
                {
                    if (char.IsPunctuation(c)) filteredToken = filteredToken.Replace(c.ToString(), "");
                }
                if (!string.IsNullOrEmpty(filteredToken))
                {
                    _phrasesForRecognize.Add(filteredToken.ToLower());
					Debug.Log(filteredToken);
				}
            }
            _speechRecognizer.SetVocabulary(_phrasesForRecognize.ToArray());
            _recognizerManager.Init(_phrasesForRecognize, hardMode);
            OnResult = _recognizerManager.Recognize;
            StartCoroutine(ResetVosk());
        }
        public void Update()
        {
            if (IsPause) return;
            string result = _speechRecognizer.GetPartialResult();
            if (string.IsNullOrEmpty(result)) return;
            OnResult?.Invoke(result);
        }
        private IEnumerator ResetVosk()
        {
			yield return new WaitForSeconds(10f);
			_speechRecognizer.Reset();
			StartCoroutine(ResetVosk());
			yield return null;
        }
        public void Stop()
		{
            OnResult = null;
			_speechRecognizer.CleanupAll();
            StopAllCoroutines();
		}
	}
}
