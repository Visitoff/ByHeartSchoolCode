using System.Collections.Concurrent;
using System.Threading.Tasks;
using Utils.Observables;
using Unity.Profiling;
using System.Linq;
using UnityEngine;
using Vosk;
using Cysharp.Threading.Tasks;

namespace Recognize
{
    [AddComponentMenu("Help/ Recognizing server")]
    public class Server : MonoBehaviour
    {
        public ObsString SelectedGrammar                        { get; private set; }
        public Observable<ServerStatus> ServerStatus    { get; private set; }
        
        [SerializeField] LanguageData[] LanguagesData;
        [SerializeField] VoiceProcessor VoiceProcessor;
        LanguageData SelectedLanguage;
        int SelectedGrammarID;
        Model langModel;
        VoskRecognizer recognizer;
        public VoiceProcessor Mircrophone => VoiceProcessor;

		readonly ConcurrentQueue<short[]> _threadedBufferQueue = new ConcurrentQueue<short[]>();
        static readonly ProfilerMarker voskRecognizerReadMarker = new ProfilerMarker("VoskRecognizer.AcceptWaveform");
        
        public bool Working => langModel != null && recognizer != null && VoiceProcessor.IsRecording;
        
        void Awake()
        {
            ServerStatus = new Observable<ServerStatus>(Recognize.ServerStatus.Idle);
            ResetGrammar();
            VoiceProcessor.OnFrameCaptured += VoiceProcessorOnOnFrameCaptured;
            
            void VoiceProcessorOnOnFrameCaptured(short[] samples)
            {
				if (!Working) return;
				_threadedBufferQueue.Enqueue(samples);
            }
        }
        
        public void SetNewLanguageByCode(SystemLanguage code)
        {
            var newLang = LanguagesData.FirstOrDefault(l=> l.LangCode.Equals(code));
            if (newLang == null)
            {
                Debug.LogError($"Recognize server. Language with code \"{code}\" not found. Changing language not processed");
                return;
            }
            if (SelectedLanguage == newLang)
            {
                ResetGrammar();
                //Debug.LogWarning("Recognize server. Language with code \"code\" already selected");
                return;
            }
            if (langModel != null)
            {
                langModel.Dispose();
                langModel = null;
                SelectedLanguage = null;
                ServerStatus.Value = Recognize.ServerStatus.Idle;
            }
            SelectedLanguage = newLang;
            ResetGrammar();
            ReloadModel();
        }
        
        public void SetNewLanguageByID(int langID)
        {
            if (SelectedLanguage != LanguagesData[langID])
            {
                SelectedLanguage = LanguagesData[langID];
            }
        }
        
        async void ReloadModel()
        {
            CleanupAll();
            ServerStatus.Value = Recognize.ServerStatus.Unpacking;
            await UniTask.Delay(32);
            var newPath = await Resolver.LoadFromUrl(SelectedLanguage.Url, SelectedLanguage.FolderName).Decompress();
            if (string.IsNullOrEmpty(newPath)) return;
            ServerStatus.Value = Recognize.ServerStatus.LoadingModel;
            await UniTask.Delay(32);
            langModel = new Model(newPath);
        }
        
        bool IsResetGrammarSuccess()
        {
            bool RequireReset = SelectedGrammarID != -1;
            if (RequireReset) ResetGrammar();
            return RequireReset;
        }
        
        void ResetGrammar()
        {
            SelectedGrammarID = -1;
            if (SelectedGrammar == null) SelectedGrammar = string.Empty;
            SelectedGrammar.Value = string.Empty;
        }
        
        public void SetVocabulary(string[] words)
        {
            SelectedGrammar.Value = ModifyArrayToGrammar(words);
            ReloadRecognizerAndListen();
        }
        
        string ModifyArrayToGrammar(string[] array)
        {
            var builder = new System.Text.StringBuilder();
            builder.Append("[\"");
            for(int i = 0; i < array.Length; i++)
            {
                builder.Append(array[i].ToLower());
                if (i < array.Length-1) builder.Append("\",\"");
            }
            builder.Append("\",\"[unk]\"]");
            return builder.ToString();
        }
        
        public void OverrideGrammarRules(string[] wordsArray)
        {
            bool HasUsefulInfo = wordsArray != null && wordsArray.Length > 0;
            if (string.IsNullOrEmpty(SelectedGrammar.Value) && !HasUsefulInfo) return;
            ResetGrammar();
            if (HasUsefulInfo) 
            {
                SelectedGrammar.Value = ModifyArrayToGrammar(wordsArray);
            }
            ReloadRecognizerAndListen();
        }
        
        void ReloadRecognizerAndListen()
        {
            TryResetRecognizer();

            ServerStatus.Value = Recognize.ServerStatus.CreateRecognizer;
            if (langModel == null)
            {
                CleanupAll();
                return;
            }
            if (string.IsNullOrEmpty(SelectedGrammar.Value))
            {
                recognizer = new VoskRecognizer(langModel, 16000.0f);
            }
            else
            {
                recognizer = new VoskRecognizer(langModel, 16000.0f, SelectedGrammar.Value);
            }
            //recognizer.SetMaxAlternatives(4);
            //recognizer.SetPartialWords(true);
            VoiceProcessor.StartRecording();
            ServerStatus.Value = Recognize.ServerStatus.Listening;
        }
        
        public void CleanupAll()
        {
            TryResetRecognizer();
            ServerStatus.Value = langModel == null? Recognize.ServerStatus.Idle : Recognize.ServerStatus.LoadingModel;
        }
        
        void TryResetRecognizer()
        {
            if (recognizer != null)
            {
                Reset();
                recognizer.Dispose();
                recognizer = null;
            }
            if (VoiceProcessor.IsRecording)
            {
                VoiceProcessor.StopRecording();
                _threadedBufferQueue.Clear();
            }
        }
        
        void Update()
        {
            if (recognizer == null) return;
            voskRecognizerReadMarker.Begin();
            //bool Changed = false;
            while (_threadedBufferQueue.TryDequeue(out short[] voiceResult))
            {
                //Changed = true;
                recognizer.AcceptWaveform(voiceResult, voiceResult.Length);
            }
            voskRecognizerReadMarker.End();
            //if (Changed) Partial = recognizer.PartialResult();
        }
        
        public void Reset()
        {
			_threadedBufferQueue.Clear();
			if (recognizer == null) return;
            recognizer.Reset();
        }
        
        public string GetPartialResult()
        {
            if (!Working) return string.Empty;
            var Result = recognizer.PartialResult();
            if (Result.Length < 20) return string.Empty;
            return Result.Substring(17, Result.Length - 20);
        }
    }
}