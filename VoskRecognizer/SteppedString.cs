using System;
using System.Collections.Generic;
using UnityEngine;

namespace Recognize
{
    [System.Serializable]
    public class SteppedString
    {
        public string FilledData { get; private set; }
        public bool Completed { get; private set; }
        [SerializeField] int achievedIndex;
        [SerializeField] List<string> parsingWords;
        [SerializeField] List<string> originalParts;
        [SerializeField] string[] array;
        
        public SteppedString(string original)
        {
            achievedIndex = 0;
            Completed = false;
            
            parsingWords = new();
            originalParts = new();
            
            bool wordEnded = false;
            int seekIndex = 0;
            while(seekIndex < original.Length && !char.IsLetter(original[seekIndex])) seekIndex++;
            int wordStartIndex = seekIndex;
            
            for(; seekIndex < original.Length; seekIndex ++)
            {
                if (!wordEnded)
                {
                    if (char.IsLetter(original[seekIndex])) continue;
                    wordEnded = true;
                    parsingWords.Add(original.Substring(wordStartIndex, seekIndex - wordStartIndex).ToLower());
                    continue;
                }
                else
                {
                    if (!char.IsLetter(original[seekIndex])) continue;
                    wordEnded = false;
                    originalParts.Add(original.Substring(wordStartIndex, seekIndex - wordStartIndex));
                    wordStartIndex = seekIndex;
                }
            }
            //seekIndex--;
            if (!wordEnded)
            {
                parsingWords.Add(original.Substring(wordStartIndex, seekIndex - wordStartIndex).ToLower());
            }
            originalParts.Add(original.Substring(wordStartIndex, seekIndex - wordStartIndex));
        }
        
        public void Reset()
        {
            achievedIndex = 0;
            Completed = false;
            FilledData = string.Empty;
        }
        
        public string[] GetParsedData() => parsingWords.ToArray();
        
        public bool IsSeekWordSuccess(string Seek)
        {
            const int WordCheckCount = 2;
            array??= new string[WordCheckCount];
            
            if (Completed) return false;
            if (Seek.Length == 0) return false;
            var Index = Seek.Length-1;
            var EndIndex = Seek.Length;
            for(int word = 0; word < WordCheckCount; word ++)
            {
                while(Index > 0 && char.IsLetter(Seek[Index])) Index--;
                var Word = Seek.Substring(Index, EndIndex - Index).Trim();
                EndIndex = Index;
                array[word] = Word;
                var IsSuccess = parsingWords[achievedIndex].Equals(Word);
                if (IsSuccess)
                {
                    FilledData += originalParts[achievedIndex];
                    achievedIndex++;
                    Completed = achievedIndex == parsingWords.Count;
                    return true;
                }
                while(Index > 0 && !char.IsLetter(Seek[Index])) Index--;
                //return IsSuccess;
            }
            return false;
        }
    }
}