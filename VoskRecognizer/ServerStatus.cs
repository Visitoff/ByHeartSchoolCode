namespace Recognize
{
    public enum ServerStatus
    {
        Idle, Downloading, Unpacking, LoadingModel, CreateRecognizer, Listening
    }

    public static class StatusHelp
    {
        public static string GetInfo(this ServerStatus status)
        {
            if (status == ServerStatus.Idle) return "Ожидание выбора модели";
            else if (status == ServerStatus.Downloading) return "Скачивание модели с сайта.";
            else if (status == ServerStatus.Unpacking) return "Распаковка модели распознавания. Ожидайте.";
            else if (status == ServerStatus.LoadingModel) return "Загрузка модели в память.";
            else if (status == ServerStatus.CreateRecognizer) return "Создание модуля распознавания.";
            else if (status == ServerStatus.Listening) return "Модуль работает. Слушаю пользователя.";
            else throw new System.NotImplementedException();
        }
    }
}