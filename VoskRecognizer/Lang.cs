using UnityEngine;
using System;

namespace Recognize
{
	[Serializable]
	public class LanguageData
	{
		[field: SerializeField] public SystemLanguage LangCode { get; private set; }
		[field: SerializeField] public string Url { get; private set; }
		[field: SerializeField] public string FolderName { get; private set; }
	}
}