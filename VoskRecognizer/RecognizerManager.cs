using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Recognizer
{
	public class RecognizerManager : MonoBehaviour
	{
		private List<string> _currentPhrase;
		private List<int> _activeWordsIndex;

		public Action<int> OnFastRecognize { get; set; }
		public Action<int> OnSlowRecognize { get; set; }
		private string _words;
		private bool _hardMode;
		public void Init(List<string> currentPhrase, bool hardMode)
		{
			_activeWordsIndex = new List<int>();
			_currentPhrase = currentPhrase;
			_hardMode = hardMode;
		}

		public void Recognize(string words)
		{
			if (words != "" && words != _words)
			{
				_words = words;
				List<string> currentWords = words.Split(' ').ToList();
				List<string> newCurrentWords = currentWords.Count > 1 ? new List<string> { currentWords[^2], currentWords.Last(), } : new List<string> { currentWords.Last(), };
				for (int i = 0; i < _currentPhrase.Count; i++)
				{
					if (_activeWordsIndex.Contains(i))
					{
						continue;
					}
					bool _return = false;
					for (int j = 0; j < newCurrentWords.Count; j++)
					{
						var word = newCurrentWords[j];
						if (_currentPhrase[i] == word || (!_hardMode && word == "[unk]"))
						{
							_activeWordsIndex.Add(i);
							OnSlowRecognize?.Invoke(i);
							newCurrentWords.RemoveRange(0, j + 1);
							_return = false;
							break;
						}
						else if (!_hardMode && i + 1 != _currentPhrase.Count && _currentPhrase[i + 1] == word)
						{

							_activeWordsIndex.Add(i);
							OnSlowRecognize?.Invoke(i);
							_return = false;
							break;
						}
						else
						{
							_return = true;
						}
					}
					if (_return) return;
				}
			}
		}
	}
}

