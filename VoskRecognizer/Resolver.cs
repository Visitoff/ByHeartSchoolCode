using System.IO;
using Cysharp.Threading.Tasks;
using Ionic.Zip;
using UnityEngine;
using UnityEngine.Networking;

namespace Recognize
{
    public class Resolver
    {
        string url;
        string filename;
        private Resolver() { }
        private const string languageNameFolder = "LanguageModels";
        public static Resolver LoadFromUrl(string url, string filename) 
        {
			return new Resolver()
			{
				url = url,
                filename = filename
			};
		}

		public async UniTask<string> Decompress()
        {
            string _decompressedModelPath = string.Empty;
            
            if (Directory.Exists(
                    Path.Combine(Application.persistentDataPath, languageNameFolder, filename)))
            {
                return Path.Combine(Application.persistentDataPath, languageNameFolder, filename);
            }

            string dataPath = url;

            Stream dataStream;

            UnityWebRequest www = UnityWebRequest.Get(dataPath);
            await www.SendWebRequest().ToUniTask();
            dataStream = new MemoryStream(www.downloadHandler.data);
            
            //Read the Zip File
            var zipFile = ZipFile.Read(dataStream);

            //Listen for the zip file to complete extraction
            zipFile.ExtractProgress += ZipFileOnExtractProgress;
            
            bool _isDecompressing = false;
            //Start Extraction
            zipFile.ExtractAll(Path.Combine(Application.persistentDataPath, languageNameFolder));

            //Wait until it's complete
            while (_isDecompressing == false) await UniTask.Yield();
            //Override path given in ZipFileOnExtractProgress to prevent crash
            _decompressedModelPath = Path.Combine(Application.persistentDataPath, languageNameFolder, filename);

            //Update status text
            //Wait a second in case we need to initialize another object.
            //yield return new WaitForSeconds(1);
            //Dispose the zipfile reader.
            zipFile.Dispose();
            return _decompressedModelPath;
            
            void ZipFileOnExtractProgress(object sender, ExtractProgressEventArgs e)
            {
                if (e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
                {
                    _isDecompressing = true;
                    zipFile.ExtractProgress -= ZipFileOnExtractProgress;
                    _decompressedModelPath = e.ExtractLocation;
                }
            }
            
        }
    }
}