using ByHeartSchool.Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ToMenuBtn : MonoBehaviour
{
	[SerializeField] Button backToMenuBtn;
	public void SetBackToMenu(ISceneNavigator sceneNavigator)
	{
		backToMenuBtn.onClick.AddListener(() => { sceneNavigator.SwitchScene("Menu"); backToMenuBtn.interactable = false; });
	}
}
