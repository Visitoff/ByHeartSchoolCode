using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckingOperationLevel : MonoBehaviour
{
    private int _currentId=1;
    private GameObject _itemView;

    public int CurrentId => _currentId;

    private void OnEnable()
    {
        ItemView.OnChooseItem += CheckIdItem;
    }

    private void OnDisable()
    {
        ItemView.OnChooseItem -= CheckIdItem;
    }


    public void CheckId(NeedObject currentObject)     // �������� ��������
    {
        print("1");
        if (_currentId == currentObject.Id)
        {
            print("2");
            currentObject.EnableSelectionAnimation();
            _currentId++;
        }

        if(_itemView != null)
        {
            Destroy(_itemView); 
        }
    }

    private void CheckIdItem(int id, ItemView itemView)
    {
        if (_currentId == id)
        {
            _currentId++;
             _itemView=itemView.gameObject;
        }
    }
}
