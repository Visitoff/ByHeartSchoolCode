using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreObjects : MonoBehaviour
{
    [SerializeField] private Shop _shop;
    private List<ModelPurchase> _modelObject = new List<ModelPurchase>();

    private int _numberModel=0;
    public ModelPurchase _currentModel;

    public bool CurrentModel => _currentModel !=null;

    private void Start()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            ModelPurchase modelPurchase = transform.GetChild(i).gameObject.GetComponent<ModelPurchase>();
            _modelObject.Add(modelPurchase);
            _modelObject[i].PointOutId(i);

            foreach (var number in _shop.numberList)
            {
                if(_modelObject[i].NumberProducts== number)
                {
                    _modelObject[i].gameObject.SetActive(true);
                    _modelObject[i].Buy();
                    _currentModel = _modelObject[i];
                }
            }
        }
    }
    public void ShowModel()
    {
        if (_numberModel >= _modelObject.Count)
        {
            _numberModel = 0;
        }

        for (int i = 0; i < _modelObject.Count; i++)
        {
            if (i == _numberModel)
                _modelObject[i].gameObject.SetActive(true);

            else
                _modelObject[i].gameObject.SetActive(false);
        }
        _numberModel++;
    }
    public void StartShowModel()
    { 
        if(_currentModel != null)
        {
            for (int i = 0; i < _modelObject.Count; i++)
            {
                if (i == _currentModel.Id)
                    _modelObject[i].gameObject.SetActive(true);

                else
                    _modelObject[i].gameObject.SetActive(false);
            }
        }
    }

    public void SwitchOffObjects()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;

        if (_currentModel !=null)
        {
            for (int i = 0; i < _modelObject.Count; i++)
            {
                if (i == _currentModel.Id)
                    _modelObject[i].gameObject.SetActive(true);

                else
                    _modelObject[i].gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < _modelObject.Count; i++)
            {
                _modelObject[i].gameObject.SetActive(false);
            }
        }
        _numberModel = 0;
    }

    public ModelPurchase GiveModelToBuy()
    {
        return _modelObject[_numberModel-1];
    }
    public void SetNumberPurchasedProduc(ModelPurchase modelPurchase)
    {
        _currentModel = modelPurchase;
    }
}
