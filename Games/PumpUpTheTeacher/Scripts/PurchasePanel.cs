using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class PurchasePanel : MonoBehaviour
{
    [SerializeField] private TMP_Text _price;
    [SerializeField] private Button _consentButton;
    [SerializeField] private GameObject _panelMoney;

    private StoreObjects _storeObjects;

    public  Action <bool> OnAgreeBuy;

    private void OnEnable()
    {
        _consentButton.onClick.AddListener(MakePurchase);
    }

    private void OnDisable()
    {
        _consentButton.onClick.RemoveListener(MakePurchase);
    }

    private void MakePurchase()
    {
        if (_storeObjects.GiveModelToBuy().IsBuy == false)
        {
            print("�������� ������");
            OnAgreeBuy?.Invoke(true);
        }

        else
            OnAgreeBuy?.Invoke(false);
    }

    public void ShowPrice(StoreObjects storeObjects)
    {
        _storeObjects = storeObjects;
        _panelMoney.gameObject.SetActive(true);

        if (_storeObjects.GiveModelToBuy().IsBuy)
            SwitchOffPrice();

        else
        {
            _price.gameObject.SetActive(true);
            _price.text = storeObjects.GiveModelToBuy().Price.ToString();
        }   
    }

    public void EnablePanelMoney()
    {
        _panelMoney.gameObject.SetActive(true);
    }

    public void SwitchOffPanelMoney()
    {
        _panelMoney.gameObject.SetActive(false);
    }

    public void SwitchOffPrice()
    {
        _price.gameObject.SetActive(false);
    }
}
