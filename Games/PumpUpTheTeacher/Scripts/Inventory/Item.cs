using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Item : MonoBehaviour
{
    [SerializeField] private Sprite _icon;
    [SerializeField] private int _id;
    [SerializeField] private HintPanel _hintPanel;

    public Sprite Icon => _icon;
    public int Id => _id;

    public void ShowHint()
    {
        if (_hintPanel != null)
        {
            _hintPanel.gameObject.SetActive(true);
            _hintPanel.TurnOnText();
        }
    }
}
