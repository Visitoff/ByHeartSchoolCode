using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private List<Item> _items;
    [SerializeField] private ItemView _template;
    [SerializeField] private GameObject _itemContainer;

    private void Start()
    {
        CallUpRender();
    }

    public void AddItemList(Item item)
    {
        _items.Add(item);
        CallUpRender();
    }

    private void CallUpRender()
    {
        if (_items.Count>0)
        {
            AddItemInView(_items[_items.Count-1]);
        }
    }

    private void AddItemInView(Item item)
    {
        var view = Instantiate(_template, _itemContainer.transform);
        view.Render(item);
    }
}

