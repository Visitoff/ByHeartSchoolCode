using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemView : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private Button _selectionButton;

    private Item _item;
    private int _id;

    public static Action<int, ItemView> OnChooseItem;    

    private void OnEnable()
    {
        _selectionButton.onClick.AddListener(OnButtonClick);
    }

    private void OnDisable()
    {
        _selectionButton.onClick.RemoveListener(OnButtonClick);
    }

    public void Render(Item item)
    {
        _item = item;
        _icon.sprite = item.Icon;
        _id = item.Id;
    }

    private void OnButtonClick()
    {
        OnChooseItem?.Invoke(_id,this);
    }
}
