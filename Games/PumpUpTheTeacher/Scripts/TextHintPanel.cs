using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class TextHintPanel : MonoBehaviour
{
    [SerializeField] private string _language;
    

    public string Language => _language;
}
