using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HintPanel : MonoBehaviour
{
    [SerializeField] private List<TextHintPanel> _textHintPanel;
    [SerializeField] private TranslateTheText _translateTheText;

    public void TurnOnText()
    {
        foreach (var text in _textHintPanel)
        {
            if(_translateTheText.Lang== text.Language)
            {
                text.gameObject.SetActive(true);
            }
        }
        //Destroy(gameObject, 12);
    }
}
