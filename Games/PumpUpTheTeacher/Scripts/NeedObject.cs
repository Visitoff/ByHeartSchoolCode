using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NeedObject : MonoBehaviour
{
    [SerializeField] private int _id;
    [SerializeField] private Animator _animator;
    [SerializeField] private TypeObject _typeObject;
    [SerializeField] private HintPanel _hintPanel;

    public int Id => _id;

    public void EnableSelectionAnimation()              // Название
    {
        print("3");
        _animator.SetTrigger("Selected");
    }
    public void ShowHint()
    {
        if(_hintPanel != null)
        {
            _hintPanel.gameObject.SetActive(true);
            _hintPanel.TurnOnText();
        }
    }
}
