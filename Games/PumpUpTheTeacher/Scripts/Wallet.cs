using ByHeartSchool.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

public class Wallet : MonoBehaviour
{
    private int _coins =1000;
    private IBankService _bankService;

    public Action<int> OnChangeNumberCoins;

    public int Coins => _coins;
	public async Task SetBankService(IBankService bankService)
	{
		_bankService = bankService;
		//_coins = await _bankService.GetBalance();
		//if (_coins <= 300)
		//{
		//	await _bankService.GetTransaction("Games", "Gift", 1000);
		//	_coins = await _bankService.GetBalance();
		//}
		OnChangeNumberCoins?.Invoke(_coins);
	}
	private void Start()
	{
        if (PlayerPrefs.HasKey("Coin"))
        {
            _coins = PlayerPrefs.GetInt("Coin");
            OnChangeNumberCoins?.Invoke(_coins);
        }
    }

	public void ReduceCount(int count)
    {
		_coins-=count;
		OnChangeNumberCoins?.Invoke(_coins);

        PlayerPrefs.SetInt("Coin", _coins);
        _bankService.BuyTransaction("Games", "Pump up the teacher", count);
    }
}
