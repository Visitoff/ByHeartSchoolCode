using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField] private List<StoreObjects> _storeObjects;
    [SerializeField] private PurchasePanel _purchasePanel;
    [SerializeField] private Wallet _wallet;

    private StoreObjects _storeObject;

    private bool _isShowObject;

    private string _numbersBuyObject;

    public List<int> numberList = new List<int>();


    private void OnEnable()
    {
        _purchasePanel.OnAgreeBuy += TryInstall;
    }

    private void OnDisable()
    {
        _purchasePanel.OnAgreeBuy -= TryInstall;
    }

    private void Awake()
    {
        GetNumbersBuyObjects();
    }
    //void Start()
    //{
    //    GetNumbersBuyObjects();
    //}

    private void GetNumbersBuyObjects()
    {
        if (PlayerPrefs.HasKey("BuyObjects"))
        {
            _numbersBuyObject = PlayerPrefs.GetString("BuyObjects");
            string[] numberStrings = _numbersBuyObject.Split('|');

            foreach (string numberString in numberStrings)
            {
                int number;
                if (int.TryParse(numberString, out number))
                {
                    numberList.Add(number);
                }
            }
            print(_numbersBuyObject);
        }
    }

    public void ShowObjects()
    {
        if (_isShowObject)
        {
            foreach (StoreObjects storeObject in _storeObjects)
            {
                storeObject.SwitchOffObjects();
                _purchasePanel.SwitchOffPanelMoney();
            }
            _isShowObject = false;
        }
        else
        {
            foreach (StoreObjects storeObject in _storeObjects)
            {
                if (storeObject.CurrentModel == false)
                {
                    storeObject.GetComponent<MeshRenderer>().enabled = true;
                }

                storeObject.StartShowModel();
            }
            _isShowObject = true;
        }
    }

    public void DefinePurchase(StoreObjects storeObject)
    {
        if (_isShowObject)
        {
            _storeObject = storeObject;
            storeObject.GetComponent<MeshRenderer>().enabled = false;
            _storeObject.ShowModel();

            _purchasePanel.ShowPrice(storeObject);
        }
    }

    private void TryInstall(bool isBuy)
    {
        
        if (isBuy)
        {
            if (FindOutEnoughMoney())
            {
                _wallet.ReduceCount(_storeObject.GiveModelToBuy().Price);
                MakePurchased();
            }
        }
        else
            ChooseProduct();
    }

    private bool FindOutEnoughMoney()
    {
        print(_storeObject.GiveModelToBuy().Price <= _wallet.Coins);
        return _storeObject.GiveModelToBuy().Price <= _wallet.Coins;

    }

    private void MakePurchased()
    {
        _storeObject.GiveModelToBuy().Buy();
        _storeObject.SetNumberPurchasedProduc(_storeObject.GiveModelToBuy());

        _purchasePanel.SwitchOffPrice();
        _numbersBuyObject += _storeObject.GiveModelToBuy().NumberProducts + "|";
        PlayerPrefs.SetString("BuyObjects", _numbersBuyObject);
    }

    private void ChooseProduct()
    {
        _storeObject.SetNumberPurchasedProduc(_storeObject.GiveModelToBuy());
    }
}
