using ByHeartSchool.Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateTheText : MonoBehaviour
{
    [HideInInspector] public ILanguageService languageService;

    private string _lang;

    public string Lang => _lang;
    private void Start()
    {
        Translate();
    }
    public void Translate()
    {
        _lang= languageService.GetCurrentLanguage();
    }
}
