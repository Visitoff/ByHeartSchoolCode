using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeacherInGame : MonoBehaviour
{
    [SerializeField] private HintPanel _hint;

    public void SpeakTeacher()
    {
        _hint.gameObject.SetActive(true);
        _hint.TurnOnText();
    }
}
