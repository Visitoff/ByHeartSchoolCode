using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseSelection : MonoBehaviour
{
    [SerializeField] private Shop _shop;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100f, Color.red, 100f);

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.GetComponent<StoreObjects>())
            {
                _shop.DefinePurchase(hit.collider.gameObject.GetComponent<StoreObjects>());
            }
        }
    }
}
