using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _coinsCountText;
    [SerializeField] private Wallet _wallet;

    private void OnEnable()
    {
        _wallet.OnChangeNumberCoins += ShowCountCoins;
    }

    private void OnDisable()
    {
        _wallet.OnChangeNumberCoins -= ShowCountCoins;
    }

    private void ShowCountCoins(int coinCount)
    {
        _coinsCountText.text = coinCount.ToString();
    }
}
