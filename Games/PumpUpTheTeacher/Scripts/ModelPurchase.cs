using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelPurchase : MonoBehaviour
{
    [SerializeField] private int _numberProducts;
    [SerializeField] private int _price;

    private bool _isBuy;
    private int _id;

    public int NumberProducts => _numberProducts;
    public int Price => _price;
    public bool IsBuy => _isBuy;
    public int Id => _id;

    public void Buy()
    {
        _isBuy = true;
    }

    public void PointOutId(int id)
    {
        _id = id;
    }


}
