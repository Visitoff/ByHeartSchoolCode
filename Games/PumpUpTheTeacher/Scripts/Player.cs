using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private CheckingOperationLevel _checkingOperationLevel;
    [SerializeField] private Inventory _inventory;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100f, Color.red, 100f);

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.GetComponent<Item>())
            {
                _inventory.AddItemList(hit.collider.gameObject.GetComponent<Item>());
                hit.collider.gameObject.GetComponent<Item>().ShowHint();
                Destroy(hit.collider.gameObject);
            }

            else if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.GetComponent<NeedObject>())
            {
                _checkingOperationLevel.CheckId(hit.collider.gameObject.GetComponent<NeedObject>());
                hit.collider.gameObject.GetComponent<NeedObject>().ShowHint();
            }

            else if(Physics.Raycast(ray, out hit) && hit.collider.gameObject.GetComponent<TeacherInGame>())
            {
                hit.collider.gameObject.GetComponent<TeacherInGame>().SpeakTeacher();
            }
        }
    }
}
