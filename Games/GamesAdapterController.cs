﻿using Zenject;
using ByHeartSchool.Services;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Games
	{
		public class GamesAdapterController: IInitializable
		{
			private readonly ISceneNavigator _sceneNavigator;
			private readonly IBankService _bankService;
			private readonly GamesAdapterView _view;
			private readonly ILanguageService _languageService;
            public GamesAdapterController(ISceneNavigator sceneNavigator, IBankService bankService, GamesAdapterView view, ILanguageService languageService)
            {
                _bankService = bankService;
				_sceneNavigator = sceneNavigator;
				_view = view;
				_languageService = languageService;
            }

			public async void Initialize()
			{
				Input.multiTouchEnabled = true;

				UnityEngine.Screen.autorotateToPortraitUpsideDown = false;
				UnityEngine.Screen.autorotateToPortrait = false;
				UnityEngine.Screen.autorotateToLandscapeLeft = true;
				UnityEngine.Screen.autorotateToLandscapeRight = true;
				Screen.orientation = ScreenOrientation.LandscapeLeft;
				Screen.orientation = ScreenOrientation.AutoRotation;
#if UNITY_ANDROID
				ApplicationChrome.statusBarState = ApplicationChrome.States.Hidden;
				ApplicationChrome.navigationBarState = ApplicationChrome.States.Hidden;
#endif
				if (_view.toMenuBtn != null) _view.toMenuBtn.SetBackToMenu(_sceneNavigator);
				if (_view.wallet) await _view.wallet.SetBankService(_bankService);
				if (_view.text) _view.text.languageService = _languageService;
			}
		}
	}
}
