﻿using Zenject;

namespace ByHeartSchool
{
	namespace Games
	{
		public class GamesAdapterInstaller: MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<GamesAdapterView>().FromInstance(GetComponent<GamesAdapterView>()).AsSingle();
				Container.BindExecutionOrder<GamesAdapterController>(1);
				Container.BindInterfacesAndSelfTo<GamesAdapterController>().AsSingle();

			}
		}
	}
}
