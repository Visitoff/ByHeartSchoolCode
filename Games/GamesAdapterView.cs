﻿using UnityEngine;
using Zenject;

namespace ByHeartSchool
{
	namespace Games
	{
		public class GamesAdapterView: MonoInstaller
		{
			[SerializeField] public Wallet wallet;
			[SerializeField] public ToMenuBtn toMenuBtn;
			[SerializeField] public TranslateTheText text;
		}
	}
}
