﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class LanguageServiceInstaller : Installer<LanguageServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<LanguageService>().AsSingle();
			}
		}
	}

}
