﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface ILanguageService
		{
			public void SetLanguage(string lang);
			public string GetCurrentLanguage();
			public int GetCurrentLanguageIndex();
			bool isChoised();
			public Dictionary<string, (string, string)> GetAll();
			public UniTask<List<string>> GetUserLanguages();
			bool IsRightToLeftLanguage(string language);
		}
	}
}
