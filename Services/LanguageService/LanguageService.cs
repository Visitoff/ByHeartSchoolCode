﻿using Zenject;
using UnityEngine;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ByHeartSchool.Repositories;
using System.Linq;
using UnityEngine.Analytics;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{


		public class LanguageService : IInitializable, ILanguageService
		{
			private string _lang;
			private bool _isChoised;
			private readonly Dictionary<string, (string, string)> _languages = new Dictionary<string, (string, string)>()
						{
						{     "nl" ,  ("Dutch", "Nederlands, Vlaams")},
						{     "en" ,  ("English", "English")},
						{  "fr" , ( "French",  "français, langue française")},
						{  "de" , ( "German",  "Deutsch")},
						{     "it" , ( "Italian",  "Italiano")},
						{     "ja" , ( "Japanese",  "日本語 (にほんご／にっぽんご)")},
						 {"pt" , ( "Portuguese",  "Português")},
						 			  {"ru" , ( "Russian",  "русский язык")},
									  			 { "es", ("Spanish", "español")},
							{ "ab" , ( "Abkhaz",  "аҧсуа") },
							{ "aa", ( "Afar",  "Afaraf")},
							{ "af", ( "Afrikaans",  "Afrikaans")},
							{ "ak", ( "Akan",  "Akan")},
							{ "sq", ( "Albanian",  "Shqip")},
				{  "am" , (  "Amharic",  "አማርኛ")},
				{ "ar" , (  "Arabic",  "العربية")},
				{  "an" ,  (  "Aragonese",  "Aragonés")},
		{     "hy" ,  (  "Armenian",  "Հայերեն")},
			 { "as" ,  ("Assamese", "অসমীয়া")},
			{  "av" ,  ("Avaric", "авар мацӀ, магӀарул мацӀ")},
			{  "ae" ,  ("Avestan", "avesta")},
			{  "ay" ,  ("Aymara", "aymar aru")},
			 { "az" ,  ("Azerbaijani", "azərbaycan dili")},
			{  "bm" ,  ("Bambara", "bamanankan")},
			{  "ba" ,  ("Bashkir", "башҡорт теле")},
			{  "eu" ,  ("Basque", "euskara, euskera")},
			{  "be" ,  ("Belarusian", "Беларуская")},
			{  "bn" ,  ("Bengali", "বাংলা")},
			{  "bh" ,  ("Bihari", "भोजपुरी")},
			{  "bi" ,  ("Bislama", "Bislama")},
			{  "bs" ,  ("Bosnian", "bosanski jezik")},
			{  "br" ,  ("Breton", "brezhoneg")},
			{  "bg" ,  ("Bulgarian", "български език")},
			{  "my" ,  ("Burmese", "ဗမာစာ")},
			{  "ca" ,  ("Catalan; Valencian", "Català")},
			{  "ch" ,  ("Chamorro", "Chamoru")},
			{  "ce" ,  ("Chechen", "нохчийн мотт")},
		{     "ny" ,  ("Chichewa; Chewa; Nyanja", "chiCheŵa, chinyanja")},
			{  "zh" ,  ("Chinese", "中文 (Zhōngwén), 汉语, 漢語")},
			{  "cv" ,  ("Chuvash", "чӑваш чӗлхи")},
		{     "kw" ,  ("Cornish", "Kernewek")},
		{     "co" ,  ("Corsican", "corsu, lingua corsa")},
			{  "cr" ,  ("Cree", "ᓀᐦᐃᔭᐍᐏᐣ")},
			{  "hr" ,  ("Croatian", "hrvatski")},
			{  "cs" ,  ("Czech", "česky, čeština")},
		{     "da" ,  ("Danish", "dansk")},
			{  "dv" ,  ("Divehi; Dhivehi; Maldivian;", "ދިވެހި")},
		{     "eo" ,  ("Esperanto", "Esperanto")},
		{     "et" ,  ("Estonian", "eesti, eesti keel")},
		{     "ee" ,  ("Ewe", "Eʋegbe")},
			{  "fo" ,  ("Faroese", "føroyskt")},
			{  "fj" ,  ("Fijian", "vosa Vakaviti")},
			{  "fi" ,  ("Finnish", "suomi, suomen kieli")},
		{     "ff" , ( "Fula; Fulah; Pulaar; Pular",  "Fulfulde, Pulaar, Pular")},
		{     "gl" , ( "Galician",  "Galego")},
			{  "ka" , ( "Georgian",  "ქართული")},
			{  "el" , ( "Greek, Modern",  "Ελληνικά")},
			{  "gn" , ( "Guaraní",  "Avañeẽ")},
		{     "gu" , ( "Gujarati",  "ગુજરાતી")},
		{     "ht" , ( "Haitian; Haitian Creole",  "Kreyòl ayisyen")},
		{     "ha" , ( "Hausa",  "Hausa, هَوُسَ")},
		{     "he" , ( "Hebrew",  "עברית")},
			{  "hz" , ( "Herero",  "Otjiherero")},
			{  "hi" , ( "Hindi",  "हिन्दी, हिंदी")},
			{  "ho" , ( "Hiri Motu",  "Hiri Motu")},
			{  "hu" , ( "Hungarian",  "Magyar")},
			{  "ia" , ( "Interlingua",  "Interlingua")},
		{  "id" , ( "Indonesian",  "Bahasa Indonesia")},
		{     "ie" , ( "Interlingue",  "Originally called Occidental; then Interlingue after WWII")},
			{  "ga" , ( "Irish",  "Gaeilge")},
			{  "ig" , ( "Igbo",  "Asụsụ Igbo")},
		{     "ik" , ( "Inupiaq",  "Iñupiaq, Iñupiatun")},
		{     "io" , ( "Ido",  "Ido")},
		{  "is" , ( "Icelandic",  "Íslenska")},
		{     "iu" , ( "Inuktitut",  "ᐃᓄᒃᑎᑐᑦ")},
			  {"jv" , ( "Javanese",  "basa Jawa")},
			  {"kl" , ( "Kalaallisut, Greenlandic",  "kalaallisut, kalaallit oqaasii")},
			  {"kn" , ( "Kannada",  "ಕನ್ನಡ")},
			  {"kr" , ( "Kanuri",  "Kanuri")},
			  {"ks" , ( "Kashmiri",  "कश्मीरी, كشميري‎")},
			  {"kk" , ( "Kazakh",  "Қазақ тілі")},
			  {"km" , ( "Khmer",  "ភាសាខ្មែរ")},
			  {"ki" , ( "Kikuyu, Gikuyu",  "Gĩkũyũ")},
			  {"rw" , ( "Kinyarwanda",  "Ikinyarwanda")},
			  {"ky" , ( "Kirghiz, Kyrgyz",  "кыргыз тили")},
			  {"kv" , ( "Komi",  "коми кыв")},
			  {"kg" , ( "Kongo",  "KiKongo")},
			  {"ko" , ( "Korean",  "한국어 (韓國語), 조선말 (朝鮮語)")},
			  {"ku" , ( "Kurdish",  "Kurdî, كوردی‎")},
			  {"kj" , ( "Kwanyama, Kuanyama",  "Kuanyama")},
			  {"la" , ( "Latin",  "latine, lingua latina")},
			  {"lb" , ( "Luxembourgish, Letzeburgesch",  "Lëtzebuergesch")},
			  {"lg" , ( "Luganda",  "Luganda")},
			  {"li" , ( "Limburgish, Limburgan, Limburger",  "Limburgs")},
			  {"ln" , ( "Lingala",  "Lingála")},
			  {"lo" , ( "Lao",  "ພາສາລາວ")},
			  {"lt" , ( "Lithuanian",  "lietuvių kalba")},
			  {"lu" , ( "Luba-Katanga",  "")},
			  {"lv" , ( "Latvian",  "latviešu valoda")},
			  {"gv" , ( "Manx",  "Gaelg, Gailck")},
			  {"mk" , ( "Macedonian",  "македонски јазик")},
			  {"mg" , ( "Malagasy",  "Malagasy fiteny")},
			  {"ms" , ( "Malay",  "bahasa Melayu, بهاس ملايو‎")},
			  {"ml" , ( "Malayalam",  "മലയാളം")},
			  {"mt" , ( "Maltese",  "Malti")},
			  {"mi" , ( "Māori",  "te reo Māori")},
			  {"mr" , ( "Marathi (Marāṭhī)",  "मराठी")},
			  {"mh" , ( "Marshallese",  "Kajin M̧ajeļ")},
			  {"mn" , ( "Mongolian",  "монгол")},
			  {"na" , ( "Nauru",  "Ekakairũ Naoero")},
			  {"nv" , ( "Navajo, Navaho",  "Diné bizaad, Dinékʼehǰí")},
			  {"nb" , ( "Norwegian Bokmål",  "Norsk bokmål")},
			  {"nd" , ( "North Ndebele",  "isiNdebele")},
			  {"ne" , ( "Nepali",  "नेपाली")},
			  {"ng" , ( "Ndonga",  "Owambo")},
			  {"nn" , ( "Norwegian Nynorsk",  "Norsk nynorsk")},
			  {"no" , ( "Norwegian",  "Norsk")},
			  {"ii" , ( "Nuosu",  "ꆈꌠ꒿ Nuosuhxop")},
			  {"nr" , ( "South Ndebele",  "isiNdebele")},
			  {"oc" , ( "Occitan",  "Occitan")},
			  {"oj" , ( "Ojibwe, Ojibwa",  "ᐊᓂᔑᓈᐯᒧᐎᓐ")},
			  {"cu" , (
	"Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic",
				 "ѩзыкъ словѣньскъ"
			  )},
				{  "om" , ( "Oromo",  "Afaan Oromoo")},
			  {"or" , ( "Oriya",  "ଓଡ଼ିଆ")},
			  {"os" , ( "Ossetian, Ossetic",  "ирон æвзаг")},
			  {"pa" , ( "Panjabi, Punjabi",  "ਪੰਜਾਬੀ, پنجابی‎")},
			  {"pi" , ( "Pāli",  "पाऴि")},
			  {"fa" , ( "Persian",  "فارسی")},
			  {"pl" , ( "Polish",  "polski")},
			  {"ps" , ( "Pashto, Pushto",  "پښتو")},
			  {"qu" , ( "Quechua",  "Runa Simi, Kichwa")},
			  {"rm" , ( "Romansh",  "rumantsch grischun")},
			  {"rn" , ( "Kirundi",  "kiRundi")},
			  {"ro" , ( "Romanian, Moldavian, Moldovan",  "română")},
			  {"sa" , ( "Sanskrit (Saṁskṛta)",  "संस्कृतम्")},
			  {"sc" , ( "Sardinian",  "sardu")},
			  {"sd" , ( "Sindhi",  "सिन्धी, سنڌي، سندھی‎")},
			  {"se" , ( "Northern Sami",  "Davvisámegiella")},
			 { "sm" , ( "Samoan",  "gagana faa Samoa")},
			 { "sg" , ( "Sango",  "yângâ tî sängö")},
			 { "sr" , ( "Serbian",  "српски језик")},
			 { "gd" , ( "Scottish Gaelic; Gaelic",  "Gàidhlig")},
			 { "sn" , ( "Shona",  "chiShona")},
			 { "si" , ( "Sinhala, Sinhalese",  "සිංහල")},
			 { "sk" , ( "Slovak",  "slovenčina")},
			 { "sl" , ( "Slovene",  "slovenščina")},
			 { "so" , ( "Somali",  "Soomaaliga, af Soomaali")},
			 { "st" , ( "Southern Sotho",  "Sesotho")},
			{  "su" , ( "Sundanese",  "Basa Sunda")},
			{  "sw" , ( "Swahili",  "Kiswahili")},
			{  "ss" , ( "Swati",  "SiSwati")},
			{  "sv" , ( "Swedish",  "svenska")},
			{  "ta" , ( "Tamil",  "தமிழ்")},
			{  "te" , ( "Telugu",  "తెలుగు")},
			{  "tg" , ( "Tajik",  "тоҷикӣ, toğikī, تاجیکی‎")},
			{  "th" , ( "Thai",  "ไทย")},
			{  "ti" , ( "Tigrinya",  "ትግርኛ")},
			{  "bo" , ( "Tibetan Standard, Tibetan, Central",  "བོད་ཡིག")},
			{  "tk" , ( "Turkmen",  "Türkmen, Түркмен")},
			{  "tl" , ( "Tagalog",  "Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔")},
			{  "tn" , ( "Tswana",  "Setswana")},
			{  "to" , ( "Tonga (Tonga Islands)",  "faka Tonga")},
			{  "tr" , ( "Turkish",  "Türkçe")},
			{  "ts" , ( "Tsonga",  "Xitsonga")},
			{  "tt" , ( "Tatar",  "татарча, tatarça, تاتارچا‎")},
			{  "tw" , ( "Twi",  "Twi")},
			{  "ty" , ( "Tahitian",  "Reo Tahiti")},
			{  "ug" , ( "Uighur, Uyghur",  "Uyƣurqə, ئۇيغۇرچە‎")},
			{  "uk" , ( "Ukrainian",  "українська")},
			{  "ur" , ( "Urdu",  "اردو")},
			{  "uz" , ( "Uzbek",  "zbek, Ўзбек, أۇزبېك‎")},
			{  "ve" , ( "Venda",  "Tshivenḓa")},
			{  "vi" , ( "Vietnamese",  "Tiếng Việt")},
			{  "vo" , ( "Volapük",  "Volapük")},
			{  "wa" , ( "Walloon",  "Walon")},
			{  "cy" , ( "Welsh",  "Cymraeg")},
			{  "wo" , ( "Wolof",  "Wollof")},
			{  "fy" , ( "Western Frisian",  "Frysk")},
			{  "xh" , ( "Xhosa",  "isiXhosa")},
			{  "yi" , ( "Yiddish",  "ייִדיש")},
			 { "yo" , ( "Yoruba",  "Yorùbá")},
			 { "za" , ( "Zhuang, Chuang",  "Saɯ cueŋƅ, Saw cuengh")}
					  };
			private Dictionary<int, string> _appLangList = new()
			{
				{ 10, "en"},
				{ 9, "nl"},
				{ 14, "fr"},
				{ 15, "de"},
				{ 21, "it"},
				{ 28, "pt"},
				{ 30, "ru"},
				{ 34, "es"},
				{ 22, "ja"},
				{ 6, "zh"},
				{ 35, "sv"},
				{ 42, "hi"},
				{ 1, "ar"},
				{ 37, "tr"}
			};
			private List<string> _rightToLeftLanguages = new()
			{
				"ar",
				"fa",
				"jv",
				"ks",
				"ku",
				"ps",
				"sd",
				"so",
				"ug",
				"ur",
				"he",
				"dv",
			};
			private readonly UserManager _userManager;
			private readonly IBaseRepository<User> _userRepository;

			public LanguageService(UserManager userManager, IBaseRepository<User> userRepository)
			{
				_userManager = userManager;
				_userRepository = userRepository;
			}
			public void Initialize()
			{
				if (PlayerPrefs.HasKey(nameof(_lang)))
				{
					_lang = PlayerPrefs.GetString(nameof(_lang));
					_isChoised = true;
				}
			}
			public bool IsRightToLeftLanguage(string language) => !string.IsNullOrEmpty(language) && language.Length >= 2 ? _rightToLeftLanguages.Contains(language.ToString()[0].ToString() + language.ToString()[1].ToString()) : false;
			public void SetLanguage(string lang)
			{
				_lang = lang;
				PlayerPrefs.SetString(nameof(_lang), lang);
				_isChoised = true;
			}
			public string GetCurrentLanguage() => _lang ?? "en";
			public bool isChoised() => _isChoised;
			public Dictionary<string, (string, string)> GetAll() => _languages;
			public async UniTask<List<string>> GetUserLanguages()
			{
				if (_userManager.Success)
				{
					var user = await _userRepository.Get(_userManager.User.UserId);
					var userLangs = new List<object>();
					var prefs = (Dictionary<string, object>)user.prefs;
					if (prefs == null) prefs = new Dictionary<string, object>();
					if (prefs.ContainsKey("user-languages"))
					{
						userLangs = (List<object>)prefs["user-languages"];
					}
					else
					{
						prefs.Add("user-languages", new List<object>());
						userLangs = (List<object>)prefs["user-languages"];
						user.prefs = prefs;
					}
					return userLangs.Select(x => x.ToString()[0].ToString() + x.ToString()[1].ToString()).ToList();
				}
				return new List<string> { _lang };
			}

			public int GetCurrentLanguageIndex()
			{
				foreach (var language in _appLangList.Keys)
				{
					if (_appLangList[language] == GetCurrentLanguage())
					{
						return language;
					}
				}
				return 0;
			}
		}
	}

}
