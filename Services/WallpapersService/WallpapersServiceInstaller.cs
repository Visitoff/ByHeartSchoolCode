﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class WallpapersServiceInstaller : Installer<WallpapersServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<WallpapersService>().AsSingle();
			}
		}
	}
}
