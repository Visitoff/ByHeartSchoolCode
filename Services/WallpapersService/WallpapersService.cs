﻿using ByHeartSchool.Repositories;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public class WallpapersService : IWallpapersService
		{
			private readonly IGetRepository<App> _appRepository;
			private readonly IGetDownloadRepository<Texture2D> _imageRepository;
			public WallpapersService(
				IGetRepository<App> appRepository,
				IGetDownloadRepository<Texture2D> imageRepository
				)
			{
				_appRepository = appRepository;
				_imageRepository = imageRepository;
			}

			public async UniTask<IBaseResponse<List<SpriteInfo>>> GetWallpapers()
			{
				try
				{
					var app = await _appRepository.Get(Configuration.FirestoreAppsId);
					if (app.Equals(default(App)) || app.wallpapers == null)
					{
						return new BaseResponse<List<SpriteInfo>>()
						{
							StatusCode = StatusCode.NotFound
						};
					}
					var newSprites = new List<SpriteInfo>();
					await UniTask.WhenAll(app.wallpapers.Keys.Select(async key =>
					{
						var texture = await _imageRepository.Get(app.wallpapers[key]);
						if (texture == null) return;
						var sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100);
						newSprites.Add(new SpriteInfo()
						{
							name = key,
							sprite = sprite
						});
					}));
					return new BaseResponse<List<SpriteInfo>>()
					{
						Data = newSprites,
						StatusCode = StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					UnityEngine.Debug.LogError($"[GetWallpapers] : {ex.Message}");
					return new BaseResponse<List<SpriteInfo>>()
					{
						Description = $"[GetWallpapers] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
		}
	}
}

