﻿using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IWallpapersService
		{
			UniTask<IBaseResponse<List<SpriteInfo>>> GetWallpapers();
		}
	}
}

