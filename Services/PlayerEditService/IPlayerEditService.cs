﻿using ByHeartSchool.Menu;
using Cysharp.Threading.Tasks;
using LeastSquares.Overtone;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IPlayerEditService
		{
			public UnityAction onDeselest { get; set; }
			public UnityAction onSelest { get; set; }
			Dictionary<int, AudioClip> OverridedClips { get; set; }
			ValueWrapper<bool> EditModeEnabled { get; set; }
			ValueWrapper<bool> VerticalPosChanging { get; set; }
			public void Edit(bool enable, ListAdapter adapter, List<InPlayerEvent> events, List<string> phrases, List<string> phrasesTranslate, int index);
			public void ClearOverrideClips();
			public void AddOverrideClip(int key, AudioClip clip, bool translate);
			public UniTask<AudioClip> GetOverrideClipAsync(int key, bool translate);
			public int GetSaveKey();
			public UniTask Synchronize(List<InPlayerEvent> events, List<string> phrases, bool translations);
			public void StopEditing(List<InPlayerEvent> events, List<string> phrases, List<string> phrasesTranslate, ListAdapter adapter, bool p_override = false);
			void SelectOvertoneLanguages(List<string> userLangs);
			UniTask<List<Dictionary<string, object>>> GetOverrideEvents();
			UniTask<bool?> IsA_ABBubleState(string id);
			void SetABBuble(string id, string type);
		}
	}
}
