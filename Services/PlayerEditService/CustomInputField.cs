﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

namespace ByHeartSchool
{
	namespace Services
	{
		public class CustomInputField : TMP_InputField
		{
			private TouchScreenKeyboard softKeyboard;

			public override void OnDeselect(BaseEventData eventData)
			{
				softKeyboard = m_SoftKeyboard;
				m_SoftKeyboard = null;
				base.OnDeselect(eventData);
				m_SoftKeyboard = softKeyboard;


				if (m_SoftKeyboard != null)
				{
					m_SoftKeyboard.active = false;
					m_SoftKeyboard = null;
				}
			}
		}
	}
}
