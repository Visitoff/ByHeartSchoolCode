﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayerEditServiceInstaller : Installer<PlayerEditServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<PlayerEditService>().AsSingle();
			}
		}
	}
}
