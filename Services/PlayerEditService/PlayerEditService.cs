﻿using ByHeartSchool.Menu;
using ByHeartSchool.Repositories;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
using LeastSquares.Overtone;
using System.Threading.Tasks;
using System.Linq;
using Zenject;
using Cysharp.Threading.Tasks;
using TMPro;
using UniRx;
using System;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayerEditService : IPlayerEditService, IInitializable, IDisposable
		{
			public ValueWrapper<bool> VerticalPosChanging { get; set; }
			public UnityAction onDeselest { get; set; }
			public UnityAction onSelest { get; set; }
			public Dictionary<int, AudioClip> OverridedClips { get; set; } = new();
			public Dictionary<int, AudioClip> OverridedClipsTranslations { get; set; } = new();
			public ValueWrapper<bool> EditModeEnabled { get; set; }
			private readonly IExRepository<AudioClip> _audioRepository;
			private readonly IEditEventsService _editEventsService;
			private readonly ILanguageService _languageService;
			private int _recordedPhrase = -1;
			private int _recordingPhrase = -1;
			private AudioClip _overridedAudioClip;
			private bool _isRecognizing;
			private TTSPlayer _overtonePlayer;
			private (ByHeartSchool.Menu.ItemView, int) _view;
			private float _startRecordingTime;
			private List<Dictionary<string, object>> _bubles;
			private TMP_Text _temp;
			private bool _tempIsTranslate;
			private (string, string) _languages;
			private List<IDisposable> _disposables = new();
			public PlayerEditService(IExRepository<AudioClip> audioRepository, IEditEventsService editEventsService, ILanguageService languageService)
			{
				_editEventsService = editEventsService;
				_audioRepository = audioRepository;
				_languageService = languageService;
			}
			public void Initialize()
			{
				
				var overtone = GameObject.Find("Overtone");
				_overtonePlayer = overtone?.GetComponent<TTSPlayer>();
				_bubles = null;
			}
			public void SetABBuble(string id, string type)
			{
				_editEventsService.OverrideBuble(_languageService.GetCurrentLanguage() + id, type);
			}
			public async UniTask<bool?> IsA_ABBubleState(string id)
			{
				if (_bubles == null) _bubles = await _editEventsService.GetOverrideBubles();
				var buble = _bubles.FindLast(x => x.ContainsKey(_languageService.GetCurrentLanguage() + id));
				if (buble != null && buble.Any())
				{
					return buble[_languageService.GetCurrentLanguage() + id].ToString() == "A";
				}
				return null;
			}
			public UniTask<List<Dictionary<string, object>>> GetOverrideEvents() => _editEventsService.GetOverrideEvents();
			public void SelectOvertoneLanguages(List<string> userLangs)
			{
				var languages = new List<string>();
				languages.AddRange(userLangs);
				if (!languages.Contains(_languageService.GetCurrentLanguage())) languages.Add(_languageService.GetCurrentLanguage());
				_overtonePlayer.SelectLanguages(languages);
				_languages.Item1 = _languageService.GetCurrentLanguage();
				if (languages.Any())
				{
					_languages.Item2 = languages.First();
				}
				else
				{
					_languages.Item2 = _languageService.GetCurrentLanguage();
				}
			}
			private async void OverrideAudio(int index, string text, ListAdapter adapter, string name, bool translate)
			{
				var view = adapter.GetItemView(index);
				view.ShowLoadingIndicator();
				var clip = await _overtonePlayer.Speak(text.Replace("*", ""));
				var clips = translate ? OverridedClipsTranslations : OverridedClips;
				if (!clips.ContainsKey(index))
				{
					clips.Add(index, clip);
				}
				else
				{
					clips[index] = clip;
				}
				clip.name = name;
				adapter.GetItemView(index).HideLoadingIndicator(true);
				await _audioRepository.Update(clip);
			}
			private async void OverrideAudio(int index, AudioClip clip, ListAdapter adapter, string name, bool translate)
			{
				var view = adapter.GetItemView(index);
				view.ShowLoadingIndicator();
				var clips = translate ? OverridedClipsTranslations : OverridedClips;
				if (!clips.ContainsKey(index))
				{
					clips.Add(index, clip);
				}
				else
				{
					clips[index] = clip;
				}
				clip.name = name;
				adapter.GetItemView(index).HideLoadingIndicator(true);
				await _audioRepository.Update(clip);
			}
			public void StopEditing(List<InPlayerEvent> events, List<string> phrases, List<string> phrasesTranslate, ListAdapter adapter, bool p_override = false)
			{
				if (_isRecognizing)
				{
					_isRecognizing = false;
					int j = _view.Item2;
					var device = "";
					UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
					Microphone.End(device);
					if (_view.Item2 == _recordedPhrase && p_override)
					{
						_recordedPhrase = -1;
						var newClip = AudioClip.Create(_overridedAudioClip.name, (int)((Time.time - _startRecordingTime) * _overridedAudioClip.frequency), _overridedAudioClip.channels, _overridedAudioClip.frequency, false);
						float[] data = new float[(int)((Time.time - _startRecordingTime) * _overridedAudioClip.frequency)];
						_overridedAudioClip.GetData(data, 0);
						newClip.SetData(data, 0);
						_overridedAudioClip = newClip;
						_view.Item1.maxWidth.ResetLayoutElement();
						LayoutRebuilder.ForceRebuildLayoutImmediate(adapter.content);
						//_editEventsService.Override(events[j].playlistId + "/" + events[j].id + (_tempIsTranslate ? _languages.Item2 : _languages.Item1), _tempIsTranslate ? phrasesTranslate[j] : phrases[j]);
						OverrideAudio(j, _overridedAudioClip, adapter, events[j].playlistId + "_" + events[j].id + (_tempIsTranslate ? _languages.Item2 : _languages.Item1), _tempIsTranslate);
					}
					else
					{
						_view.Item1.inputField.onDeselect?.Invoke("");
					}
				}
			}
			public void Edit(bool enable, ListAdapter adapter, List<InPlayerEvent> events, List<string> phrases, List<string> phrasesTranslate, int index)
			{
				if (!enable) StopEditing(events, phrases, phrasesTranslate, adapter);
				else Vibration.VibratePop();
				var views = adapter.GetItemViews();
				var view = views[index];
				var ev = events[index];
				if (view.inputField != null && view.edit)
				{
					view.buttons[0].onClick.RemoveAllListeners();
					view.buttons[0].onClick.AddListener(() =>
					{
						_tempIsTranslate = false;
						_temp = view.name;
						view.inputField.text = _temp.text.Replace("*", "");
						view.inputField.ActivateInputField();
						if (adapter.scrollRect.verticalNormalizedPosition >= 0) adapter.SnapTo(index, null, 0.3f);
						onSelest?.Invoke();
					});
					view.buttons[2].onClick.RemoveAllListeners();
					view.buttons[2].onClick.AddListener(() =>
					{
						_tempIsTranslate = true;
						_temp = view.description;
						view.inputField.text = _temp.text.Replace("*", "");
						view.inputField.ActivateInputField();
						if (adapter.scrollRect.verticalNormalizedPosition >= 0) adapter.SnapTo(index, null, 0.3f);
						onSelest?.Invoke();
					});
					view.buttons[1].onClick.RemoveAllListeners();
					view.buttons[1].onClick.AddListener(() =>
					{
						_tempIsTranslate = false;
						bool isRecognizing = _isRecognizing;
						_recordedPhrase = index;
						StopEditing(events, phrases, phrasesTranslate, adapter, true);
						_view.Item1 = view;
						_view.Item2 = index;
						VerticalPosChanging.Value = true;
						adapter.SnapTo(index, () => VerticalPosChanging.Value = false);
						onSelest?.Invoke();
						var device = "";
						int minFreq;
						int maxFreq;
						int freq = 44100;
						Microphone.GetDeviceCaps(device, out minFreq, out maxFreq);
						if (maxFreq < 44100 && maxFreq != 0)
							freq = maxFreq;
						if (!isRecognizing || _recordingPhrase != index)
						{
							_recordingPhrase = index;
							_startRecordingTime = Time.time;
							_overridedAudioClip = Microphone.Start(device, false, 600, freq);
							_isRecognizing = true;
						}
					});
					view.buttons[3].onClick.RemoveAllListeners();
					view.buttons[3].onClick.AddListener(() =>
					{
						_tempIsTranslate = true;
						bool isRecognizing = _isRecognizing;
						_recordedPhrase = index;
						StopEditing(events, phrases, phrasesTranslate, adapter, true);
						_view.Item1 = view;
						_view.Item2 = index;
						VerticalPosChanging.Value = true;
						adapter.SnapTo(index, () => VerticalPosChanging.Value = false);
						onSelest?.Invoke();
						var device = "";
						int minFreq;
						int maxFreq;
						int freq = 44100;
						Microphone.GetDeviceCaps(device, out minFreq, out maxFreq);
						if (maxFreq < 44100 && maxFreq != 0)
							freq = maxFreq;
						if (!isRecognizing || _recordingPhrase != index)
						{
							_recordingPhrase = index;
							_startRecordingTime = Time.time;
							view.buttons[1].GetComponent<Image>().DOColor(Color.red, 0.3f);
							_overridedAudioClip = Microphone.Start(device, false, 600, freq);
							_isRecognizing = true;
						}
					});
					view.inputField.onDeselect.RemoveAllListeners();
					view.inputField.onSubmit.RemoveAllListeners();
					view.inputField.onValidateInput = (text, charIndex, addedChar) =>
					{
						if (addedChar == '\n') addedChar = '\0';
						return addedChar;
					};
					view.inputField.onValueChanged.AddListener((s) =>
					{
						if (_temp.textInfo.lineCount <= 1)
						{
							view.maxWidth.ResetLayoutElement();
							view.description.GetComponent<MaxWidth>().ResetLayoutElement();
						}
						else
						{
							view.maxWidth.UpdateMaxWidth();
						}

						_temp.text = s;
						if (adapter.scrollRect.verticalNormalizedPosition >= 0) adapter.SnapTo(index, null, 0.3f);
					});
					view.inputField.onDeselect.AddListener((s) =>
					{
						view.name.text = phrases[index];
						view.description.text = phrasesTranslate[index];
						view.maxWidth.ResetLayoutElement();
						view.description.GetComponent<MaxWidth>().ResetLayoutElement();
						onDeselest?.Invoke();
						LayoutRebuilder.ForceRebuildLayoutImmediate(adapter.content);
						HideSoftKeyboard();
					});
					view.inputField.onSubmit.AddListener((s) =>
					{
						if (string.IsNullOrEmpty(s))
						{
							view.name.text = phrases[index];
							view.description.text = phrasesTranslate[index];
							view.maxWidth.ResetLayoutElement();
							view.description.GetComponent<MaxWidth>().ResetLayoutElement();
							Canvas.ForceUpdateCanvases();
							HideSoftKeyboard();
							return;
						}
						if (_tempIsTranslate) phrasesTranslate[index] = view.inputField.text;
						else phrases[index] = view.inputField.text;
						_temp.text = _tempIsTranslate ? phrasesTranslate[index] + '*' : phrases[index] + '*';
						view.maxWidth.ResetLayoutElement();
						view.description.GetComponent<MaxWidth>().ResetLayoutElement();
						LayoutRebuilder.ForceRebuildLayoutImmediate(adapter.content);
						_editEventsService.Override(events[index].playlistId + "/" + events[index].id + (_tempIsTranslate ? _languages.Item2 : _languages.Item1), _tempIsTranslate ? phrasesTranslate[index] : phrases[index]);
						OverrideAudio(index, _tempIsTranslate ? phrasesTranslate[index] : phrases[index], adapter, events[index].playlistId + "_" + events[index].id + (_tempIsTranslate ? _languages.Item2 : _languages.Item1), _tempIsTranslate);
						HideSoftKeyboard();
					});


				}
			}
			private void HideSoftKeyboard()
			{
#if UNITY_IOS
				var keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, true, true, false, false);
#endif
			}
			public void ClearOverrideClips()
			{
				OverridedClips = new();
				OverridedClipsTranslations = new();
			}

			public void AddOverrideClip(int key, AudioClip clip, bool translate)
			{
				var clips = translate ? OverridedClipsTranslations : OverridedClips;
				if (clips.ContainsKey(key))
				{
					clips[key] = clip;
				}
				else
				{
					clips.Add(key, clip);
				}
			}
			private int saveKey;
			public int GetSaveKey() => saveKey;
			public async UniTask<AudioClip> GetOverrideClipAsync(int key, bool translate)
			{
				AudioClip clip = null;
				var clips = translate ? OverridedClipsTranslations : OverridedClips;
				if (clips.ContainsKey(key))
				{
					saveKey = key;
					while (clips[key] == null)
					{
						await Task.Yield();
					}
					clip = clips[key];
				}
				return clip;
			}

			public async UniTask Synchronize(List<InPlayerEvent> events, List<string> phrases, bool translations)
			{
				var overrided = new Dictionary<int, AudioClip>();
				var clips = translations ? OverridedClipsTranslations : OverridedClips;
				foreach (var key in clips.Keys)
				{
					AudioClip clip;
					if (_audioRepository.IsDownload("overrided", events[key].playlistId + "_" + events[key].id + (translations ? _languages.Item2 : _languages.Item1)))
					{
						clip = await _audioRepository.Get(events[key].playlistId + "_" + events[key].id + (translations ? _languages.Item2 : _languages.Item1));
					}
					else
					{
						clip = await _overtonePlayer.Speak(phrases[key].Replace("*", ""));
						clip.name = events[key].playlistId + "_" + events[key].id + (translations ? _languages.Item2 : _languages.Item1);
						await _audioRepository.Update(clip);
					}
					overrided.Add(key, clip);
				}
				if (translations)
				{
					OverridedClipsTranslations = overrided;
				}
				else
				{
					OverridedClips = overrided;
				}
			}

			public void Dispose()
			{
				foreach (var disposable in _disposables) disposable.Dispose(); 
			}
		}
	}
}
