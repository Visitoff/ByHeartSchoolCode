﻿namespace ByHeartSchool
{
	namespace Services
	{
		public interface IFontService
		{
			float GetFontSizeMultiplyer();
			int GetDefaultFontSize();
			void SetFontSize(float size);
		}
	}
}

