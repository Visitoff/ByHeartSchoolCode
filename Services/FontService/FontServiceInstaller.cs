﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class FontServiceInstaller : Installer<FontServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<FontService>().AsSingle();
			}
		}
	}
}
