﻿using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public class FontService : IFontService
		{
			public int GetDefaultFontSize()
			{
				return 93;
			}

			public float GetFontSizeMultiplyer()
			{
				if (PlayerPrefs.HasKey("font"))
				{
					return PlayerPrefs.GetFloat("font");
				}
				return 1f;
			}

			public void SetFontSize(float size)
			{
				PlayerPrefs.SetFloat("font", size);
			}
		}
	}
}

