﻿using Cysharp.Threading.Tasks;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface ISceneNavigator
		{
			UnityAction OnSceneLoadingStart { get; set; }

			UnityAction<float> SceneLoadingProgress { get; set; }

			UnityAction OnSceneLoadingEnd { get; set; }

			UniTask SwitchScene(string sceneName);
			UniTask SwitchAddressableScene(string sceneName);

			void PreloadScene(string sceneName);

			void StartPreloadedScene();

			void ReloadScene();
		}
	}

}
