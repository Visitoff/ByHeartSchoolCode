﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class SceneNavigatorInstaller : Installer<SceneNavigatorInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<SceneNavigator>().AsSingle();
			}
		}
	}

}
