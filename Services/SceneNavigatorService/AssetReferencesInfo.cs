﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace ByHeartSchool
{
	namespace Services
	{
		[CreateAssetMenu(fileName = "AddressableInfo", menuName = "Info/New AddressableInfo")]
		public class AssetReferencesInfo: ScriptableObject
		{
			[SerializeField] private List<string> keys;
			[SerializeField] private List<AssetReference> assets;
			public Dictionary<string, AssetReference> AssetReferences { get { return keys.Zip(assets, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v); } }
		}
	}

}
