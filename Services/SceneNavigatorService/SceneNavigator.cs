﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

namespace ByHeartSchool
{
	namespace Services
	{
		public class SceneNavigator : ISceneNavigator, IDisposable
		{
			public UnityAction OnSceneLoadingStart { get; set; }
			public UnityAction<float> SceneLoadingProgress { get; set; }
			public UnityAction OnSceneLoadingEnd { get; set; }

			private const float DELAY_TIME = 1f;
			private AsyncOperation _loadingSceneProcess;
			private AsyncOperationHandle _loadingSceneProcessHandle;
			private readonly List<IDisposable> _disposables = new();
			private bool _isSceneLoading;

			private async UniTask SwitchSceneAsync(string sceneName, bool isPreload = false, bool isAddressable = false)
			{
				if (_isSceneLoading) return;
				_isSceneLoading = true;
				if (!isPreload) OnSceneLoadingStart?.Invoke();
				if (isAddressable)
				{
					var scenesInfo = Resources.Load<AssetReferencesInfo>("Info/ScenesInfo");
					if (scenesInfo != null)
					{
						AssetReference assetReference = null;
						if (scenesInfo.AssetReferences.TryGetValue(sceneName, out assetReference))
						{
							try
							{
								 await assetReference.LoadSceneAsync();
							}
							catch (Exception ex) { OnSceneLoadingEnd?.Invoke(); }
						}
					}
				}
				else
				{
					  await SceneManager.LoadSceneAsync(sceneName);
				}
				OnSceneLoadingEnd?.Invoke();
				_isSceneLoading = false;
			}
			public UniTask SwitchAddressableScene(string sceneName) => SwitchSceneAsync(sceneName, false, true);
			public UniTask SwitchScene(string sceneName) => SwitchSceneAsync(sceneName);

			public void PreloadScene(string sceneName) => SwitchSceneAsync(sceneName, true);

			public void StartPreloadedScene()
			{
				if (_loadingSceneProcess == null || _loadingSceneProcess.isDone)
					throw new Exception($"[{nameof(SceneNavigator)}] Scene already loaded!");
				OnSceneLoadingStart?.Invoke();
				_loadingSceneProcess.allowSceneActivation = true;
			}

			public void ReloadScene() => SwitchSceneAsync(SceneManager.GetActiveScene().name);

			private UniTask SetDelay(float delayTime)
			{
				var delay = UniTask.Delay(TimeSpan.FromSeconds(delayTime));
				return delay;
			}

			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}

}
