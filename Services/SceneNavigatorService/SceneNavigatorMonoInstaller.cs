﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class SceneNavigatorMonoInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<SceneNavigator>().AsSingle();
			}
		}
	}

}
