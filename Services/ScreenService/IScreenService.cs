﻿using System.Collections.Generic;
using UnityEngine.Events;
using ByHeartSchool.Response;
using System;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Menu
	{
		public interface IScreenService
		{
			UniTask<IBaseResponse<List<Item>>> GetScreen(string screenId, Dictionary<ScreenAction, UnityAction<string>> actions, bool translateToUserKnowledLanguages = false, bool recursion = false);
			UniTask<IBaseResponse<List<Item>>> GetCourse(string courseId, Dictionary<ScreenAction, UnityAction<string>> actions);
			UniTask<IBaseResponse<List<ByHeartSchool.Menu.Item>>> GetLibrary(Dictionary<ScreenAction, UnityAction<string>> actions);
			UniTask Update();
			Tuple<string, string> GetTextTuple(object obj, bool ss);
		}
	}
}
