﻿namespace ByHeartSchool
{
	namespace Menu
	{
		public enum ScreenAction
		{
			Courses,
			Screen,
			Trainings,
			Games,
			Playlist,
			Download,
			DownloadAndOpen,
			Language,
			Library,
			Delete,
			Settings
		}
	}
}
