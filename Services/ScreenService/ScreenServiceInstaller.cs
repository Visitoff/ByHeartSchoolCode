﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class ScreenServiceInstaller : Installer<ScreenServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<ScreenService>().AsSingle();
			}
		}
	}
}
