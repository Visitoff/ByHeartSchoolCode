﻿using ByHeartSchool.Repositories;
using ByHeartSchool.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using System;
using ByHeartSchool.Response;
using Unity.VisualScripting;
using System.Diagnostics;
using ByHeartSchool.Menu;
using UnityEngine.Device;
using Cysharp.Threading.Tasks;
using ByHeartSchool.PlayerReader;
using UnityEngine.UIElements;

namespace ByHeartSchool
{
	namespace Services
	{
		public class ScreenService : IScreenService, Zenject.IInitializable
		{
			private readonly IBaseRepository<Course> _courseRepository;
			private readonly IBaseRepository<Repositories.Screen> _screenRepository;
			private readonly IBaseRepository<Repositories.Object> _objectRepository;
			private readonly IGetRepository<Texture2D> _imageRepository;
			private readonly IChildRepository<Playlist> _playlistRepository;
			private readonly IPlaylistService _playlistService;
			private readonly ILanguageService _languageService;
			private readonly IBaseRepository<User> _userRepository;
			private readonly UserManager _userManager;
			private static Dictionary<string, Sprite> _sprites = new();
			private static Dictionary<string, Repositories.Screen> _screens = new Dictionary<string, Repositories.Screen>();
			private static Dictionary<string, List<Playlist>> _playlists = new Dictionary<string, List<Playlist>>();
			private static List<Course> _courses = new List<Course>();
			private static Dictionary<string, Repositories.Object> _objects = new();
			private static List<string> _downloadedPlaylist = new();
			private static List<string> _undownloadedPlaylist = new();
			private List<object> langs = new List<object>();
			private bool _langSuccesed;
			private readonly ISceneNavigator _sceneNavigator;
			private bool _isLoading = false;
			private Dictionary<string, int> _types;
			public ScreenService(
				IBaseRepository<Course> courseRepository,
				IBaseRepository<Repositories.Screen> screenRepository,
				IBaseRepository<Repositories.Object> objectRepository,
				ILanguageService languageService,
				IChildRepository<Playlist> playlistRepository,
				IGetRepository<Texture2D> imageRepository,
				IBaseRepository<User> userRepository,
				UserManager userManager,
				IPlaylistService playlistService,
				ISceneNavigator sceneNavigator
				)
			{
				_courseRepository = courseRepository;
				_objectRepository = objectRepository;
				_screenRepository = screenRepository;
				_languageService = languageService;
				_playlistRepository = playlistRepository;
				_imageRepository = imageRepository;
				_userRepository = userRepository;
				_userManager = userManager;
				_playlistService = playlistService;
				_sceneNavigator = sceneNavigator;
			}
			public void Initialize()
			{
				_types = Resources.Load<TypesInfo>("Info/ScreenTypesInfo").typesMap;
				_userManager.Initialized += async () => await Init();
				_userManager.LoginCompleted += async () => await Init();
			}
			private async UniTask Init()
			{
				if (_userManager.Success)
				{
					object newLangs;
					var user = await _userRepository.Get(_userManager.User.UserId);
					var prefs = (Dictionary<string, object>)user.prefs;
					if (prefs != null)
					{
						if (prefs.TryGetValue("user-languages", out newLangs))
						{
							langs = (List<object>)newLangs;
						}
					}
				}
			}
			private string GetLanguage(Dictionary<string, object> dictionary)
			{
				object name;
				var langs = _languageService.GetAll().Keys;
				var appLang = _languageService.GetCurrentLanguage();
				var defaultLang = "en";
				if (!dictionary.TryGetValue(appLang, out name))
				{
					if (!dictionary.TryGetValue(defaultLang, out name))
					{
						for (int k = 0; k < langs.Count; k++)
						{
							if (dictionary.TryGetValue(langs.ToList()[k], out name))
							{
								break;
							}
						}
					}
				}
				return name != null ? name.ToString() : "";
			}
			private string GetLanguageKey(Dictionary<string, object> dictionary)
			{
				var langs = _languageService.GetAll().Keys;
				var appLang = _languageService.GetCurrentLanguage();
				var defaultLang = "en";
				object key = string.Empty;
				if (!dictionary.ContainsKey(appLang))
				{
					if (!dictionary.ContainsKey(defaultLang))
					{
						for (int k = 0; k < langs.Count; k++)
						{
							if (dictionary.ContainsKey(langs.ToList()[k]))
							{
								key = langs.ToList()[k];
								break;
							}
						}
					}
					else
					{
						key = defaultLang;
					}
				}
				else
				{
					key = appLang;
				}
				return key.ToString();
			}
			private string GetUserKnowledLanguage(Dictionary<string, object> dictionary)
			{
				object name;
				if (langs.Count == 0)
				{
					return "";
				}
				else
				{
					foreach (var lang in langs)
					{
						string _lang = lang.ToString();
						_lang = _lang.Substring(0, 2);
						if (dictionary.Keys.Contains(_lang))
						{
							return dictionary[_lang].ToString();
						}
					}
					return "";
				}
			}
			private string GetUserKnowledLanguageKey(Dictionary<string, object> dictionary)
			{
				object key = string.Empty;
				if (langs.Count == 0)
				{
					return string.Empty;
				}
				else
				{
					foreach (var lang in langs)
					{
						string _lang = lang.ToString();
						_lang = _lang.Substring(0, 2);
						if (dictionary.ContainsKey(_lang))
						{
							return _lang.ToString();
						}
					}
					return string.Empty;
				}
			}
			private UnityAction CreateAction(string actionName, Dictionary<ScreenAction, UnityAction<string>> actions)
			{
				if (string.IsNullOrEmpty(actionName))
				{
					return null;
				}
				string objectId = actionName.Substring(actionName.IndexOf('/') + 1);
				if (actionName.Contains("courses"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Courses)) actions[ScreenAction.Courses]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("screen"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Screen)) actions[ScreenAction.Screen]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("trainings"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Trainings)) actions[ScreenAction.Trainings]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("games"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Games)) actions[ScreenAction.Games]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("playlist"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Playlist)) actions[ScreenAction.Playlist]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("_downloadAndOpen"))
				{
					return () =>
					{
						_downloadedPlaylist = new();
						_undownloadedPlaylist = new();
						if (actions.ContainsKey(ScreenAction.DownloadAndOpen)) actions[ScreenAction.DownloadAndOpen]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("_download"))
				{
					return () =>
					{
						_downloadedPlaylist = new();
						_undownloadedPlaylist = new();
						if (actions.ContainsKey(ScreenAction.Download)) actions[ScreenAction.Download]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("_delete"))
				{
					return () =>
					{
						_downloadedPlaylist = new();
						_undownloadedPlaylist = new();
						if (actions.ContainsKey(ScreenAction.Delete)) actions[ScreenAction.Delete]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("language"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Language)) actions[ScreenAction.Language]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("library"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Library)) actions[ScreenAction.Library]?.Invoke(objectId);
					};
				}
				else if (actionName.Contains("setting"))
				{
					return () =>
					{
						if (actions.ContainsKey(ScreenAction.Settings)) actions[ScreenAction.Settings]?.Invoke(objectId);
					};
				}
				return null;
			}
			public async UniTask<IBaseResponse<List<ByHeartSchool.Menu.Item>>> GetCourse(string courseId, Dictionary<ScreenAction, UnityAction<string>> actions)
			{
				if (!_langSuccesed)
				{
					await Init();
					_langSuccesed = true;
				}
				try
				{
					if (_objects.Count == 0)
					{
						var objectsList = await _objectRepository.GetAll();
						_objects = objectsList.ToDictionary(x => x.id, x => x);
					}
					if (_screens.Count == 0)
					{
						var screens = await _screenRepository.GetAll();
						_screens = new Dictionary<string, Repositories.Screen>();
						if (screens != null)
						{
							foreach (var _screen in screens)
							{
								_screens.Add(_screen.id, _screen);
							}
						}
						else
						{
							return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
							{
								Description = "Not found.",
								StatusCode = Response.StatusCode.NotFound,
							};
						}
					}
					List<Playlist> playlists;
					if (!_playlists.TryGetValue(courseId, out playlists))
					{
						playlists = await _playlistRepository.GetAllChild(courseId);
						_playlists.Add(courseId, playlists);
					}
					else
					{
						playlists = _playlists[courseId];
					}
					if (playlists == null)
					{
						return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
						{
							Description = "Not found.",
							StatusCode = Response.StatusCode.NotFound,
						};
					}
					var ordered = playlists.OrderBy(x => x.order).Where(x => x.learn_languages.Contains(_languageService.GetCurrentLanguage())).ToList();
					if (ordered.Count == 0)
					{
						return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
						{
							Description = "Not found.",
							StatusCode = Response.StatusCode.NotFound,
						};
					}
					var newList = new List<ByHeartSchool.Menu.Item>();
					await UniTask.WhenAll(
					ordered.Select(async x =>
					{
						var info = x;
						int j = ordered.IndexOf(x);
						var tuple = GetTextTuple(info.name, false);
						string name = tuple.Item1;
						string translate = tuple.Item2;
						bool playlistIsDownload = false;
						if (_downloadedPlaylist.Contains(info.id))
						{
							playlistIsDownload = true;
						}
						else if (_undownloadedPlaylist.Contains(info.id))
						{
							playlistIsDownload = false;
						}
						else
						{
							playlistIsDownload = await _playlistService.IsDownload(info.id, courseId);

							if (playlistIsDownload)
							{
								_downloadedPlaylist.Add(info.id);
							}
							else
							{
								_undownloadedPlaylist.Add(info.id);
							}
						}
						newList.Add(new ByHeartSchool.Menu.Item()
						{
							id = j,
							name = new Tuple<string, string>("<color=#808080>" + (j + 1).ToString() + ".</color>  " + name, "<color=#808080>" + (j + 1).ToString() + ".</color>  " + translate),
							contentTypeIndex = playlistIsDownload ? 0 : 1,
							onClick = playlistIsDownload ? CreateAction("playlist/" + courseId + "/" + info.id + "|" + j.ToString(), actions) : CreateAction("_downloadAndOpen/" + courseId + "/" + info.id + "|" + j.ToString(), actions),
							onClick2 = CreateAction("_download/" + courseId + "/" + info.id + "|" + j.ToString(), actions),
							onClick3 = CreateAction("_delete/" + courseId + "/" + info.id + "|" + j.ToString(), actions),
							isTargetLanguageRightToLeft = IsRightToLeft(info.name, false),
							isTranslationLanguageRightToLeft = IsRightToLeft(info.name, true),
						});
					}));
					newList = newList.OrderBy(x => x.id).ToList();
					return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = newList
					};
				}
				catch (Exception ex)
				{
					UnityEngine.Debug.LogError($"[GetCourse] : {ex.Message}");
					return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
					{
						Description = $"[GetCourse] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}


			}
			public async UniTask<IBaseResponse<List<ByHeartSchool.Menu.Item>>> GetLibrary(Dictionary<ScreenAction, UnityAction<string>> actions)
			{
				if (!_langSuccesed)
				{
					await Init();
					_langSuccesed = true;
				}
				try
				{
					var oldList = new List<ByHeartSchool.Menu.Item>();
					if (_courses.Count == 0)
					{
						_courses = await _courseRepository.GetAll();
					}
					var ordered = _courses.Where(x => x._shown).ToList();
					var newList = new List<ByHeartSchool.Menu.Item>();
					for (int i = 0; i < ordered.Count; i++)
					{
						var info = ordered[i];
						int j = i;
						bool playlistIsDownload = await _playlistService.IsDownload(info.id);
						newList.Add(new ByHeartSchool.Menu.Item()
						{
							name = GetTextTuple(info.name, false),
							contentTypeIndex = 2,
							onClick = CreateAction("courses/" + info.id, actions)
						});
					}
					return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = newList
					};
				}
				catch (Exception ex)
				{
					UnityEngine.Debug.LogError($"[GetLibrary] : {ex.Message}");
					return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
					{
						Description = $"[GetLibrary] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}

			}
			public async UniTask<IBaseResponse<List<ByHeartSchool.Menu.Item>>> GetScreen(string screenId, Dictionary<ScreenAction, UnityAction<string>> actions, bool translateToUserKnowledLanguages = false, bool recursion = false)
			{
				try
				{
					while (_isLoading)
					{
						await UniTask.Yield();
					}
					_isLoading = true;
					if (!_langSuccesed)
					{
						await Init();
						_langSuccesed = true;
					}
					Repositories.Screen screen;
					if (!_screens.TryGetValue(screenId, out screen))
					{
						var screens = await _screenRepository.GetAll();
						_screens = new Dictionary<string, Repositories.Screen>();
						if (screens != null)
						{
							_screens = screens.ToDictionary(x => x.id, x => x);
							if (_screens.ContainsKey(screenId)) screen = _screens[screenId];
						}
						else
						{
							_isLoading = false;
							return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
							{
								Description = "Not found.",
								StatusCode = Response.StatusCode.NotFound,
							};
						}
					}
					else
					{
						screen = _screens[screenId];
					}
					var objects = screen.objects;
					if (objects == null)
					{
						_isLoading = false;
						return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
						{
							Description = "Not found.",
							StatusCode = Response.StatusCode.NotFound,
						};
					}
					if (_objects.Count == 0)
					{
						var objectsList = await _objectRepository.GetAll();
						_objects = objectsList.ToDictionary(x => x.id, x => x);
					}
					_isLoading = false;
					var newList = new List<ByHeartSchool.Menu.Item>();
					var newObjects = new List<Repositories.Object>();
					for (int i = 0; i < objects.Count; i++)
					{
						if (!_objects.ContainsKey(objects[i])) continue;
						var info = _objects[objects[i]];
						info.order = i * 1000;
						newObjects.Add(info);
					}
					newObjects = newObjects.Where(x => x.learn_languages == null || x.learn_languages.Contains(_languageService.GetCurrentLanguage())).ToList();
					if (newObjects.Count == 0)
					{
						return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
						{
							Description = "Not found.",
							StatusCode = Response.StatusCode.NotFound,
						};
					}
					await UniTask.WhenAll(newObjects.Select(async info =>
					{
						if (string.IsNullOrEmpty(info.id))
						{
							return;
						}
						var sprite = await GetSprite(info.img);
						int type = _types.ContainsKey(info.type) ? _types[info.type] : 0;
						if ((type == 4 || type == 5) && info.gallery != null && !recursion)
						{
							string typeKey = type == 4 ? "img" : "imgMini";
							for (int k = 0; k < info.gallery.Count; k++)
							{
								var value = info.gallery.Values.ToList()[k];
								var key = info.gallery.Keys.ToList()[k];
								if (key.Contains("img"))
								{
									var gallerySprite = await GetSprite(value);
									int galleryType = _types.ContainsKey(typeKey) ? _types[typeKey] : 0;
									int l = k;
									newList.Add(new ByHeartSchool.Menu.Item()
									{
										mainImage = gallerySprite,
										contentTypeIndex = galleryType,
										onClick = actions != null ? CreateAction(info.action, actions) : null,
										toSpecialBlock = type == 4 ? 0 : 1,
										ratio = gallerySprite == null ? 0 : gallerySprite.bounds.size.y / gallerySprite.bounds.size.x,
										index = info.order + l,
										changeParentBlockHeight = true,
										raycastTargetMainImage = false
									});
								}
							}
							return;
						}
						if (type == 10 && !string.IsNullOrEmpty(info.screen) && !recursion)
						{
							var response = await GetScreen(info.screen, actions, translateToUserKnowledLanguages, true);
							if (response.StatusCode == StatusCode.OK)
							{
								for (int k = 0; k < response.Data.Count; k++)
								{
									var data = response.Data[k];
									data.toSpecialBlock = 1;
									data.changeParentBlockHeight = true;
									data.index = info.order + k;
								}
								newList.AddRange(response.Data);
								return;
							}
						}
						if (type == 9 && !string.IsNullOrEmpty(info.screen) && !recursion)
						{
							var response = await GetScreen(info.screen, actions, translateToUserKnowledLanguages, true);
							if (response.StatusCode == StatusCode.OK)
							{

								newList.Add(new()
								{
									name = GetTextTuple(info.title_text, info.ss),
									contentTypeIndex = _types.ContainsKey("type6") ? _types["type6"] : 0,
									onClick = actions != null ? CreateAction(info.action, actions) : null,
									toSpecialBlock = 2,
									index = info.order,
								});
								int l = 0;
								for (int k = 0; k < response.Data.Count; k++)
								{
									var data = response.Data[k];
									data.toSpecialBlock = 2;
									data.index = info.order + k + 1;
									data.contentTypeIndex = _types.ContainsKey("type3ShadowOff") ? _types["type3ShadowOff"] : 0;
									l = data.index;
								}
								newList.AddRange(response.Data);
								newList.Add(new()
								{
									name = GetTextTuple(info.specification_text, info.ss),
									contentTypeIndex = _types.ContainsKey("typeBtn") ? _types["typeBtn"] : 0,
									onClick = actions != null ? CreateAction(info.action, actions) : null,
									toSpecialBlock = 2,
									index = l,
								});
								return;
							}
						}
						newList.Add(new()
						{
							name = GetTextTuple(info.title_text, info.ss),
							specification = GetTextTuple(info.specification_text, info.ss),
							description = GetTextTuple(info.body_text, info.ss || translateToUserKnowledLanguages),
							mainImage = sprite,
							contentTypeIndex = type,
							onClick = actions != null ? CreateAction(info.action, actions) : null,
							ratio = info.ratio > 0 ? info.ratio : -1,
							index = info.order,
							isTargetLanguageRightToLeft = IsRightToLeft(info.title_text, false),
							isTranslationLanguageRightToLeft = IsRightToLeft(info.title_text, true),
							toSpecialBlock = -1

						});
					}));
					newList = newList.OrderBy(x => x.index).ToList();
					return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = newList
					};
				}
				catch (Exception ex)
				{
					_isLoading = false;
					UnityEngine.Debug.LogError($"[GetScreen] : {ex.Message}");
					return new BaseResponse<List<ByHeartSchool.Menu.Item>>()
					{
						Description = $"[GetScreen] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}

			}
			private async UniTask<Sprite> GetSprite(string id)
			{
				if (string.IsNullOrEmpty(id))
				{
					return null;
				}
				Sprite newSprite = null;
				if (_sprites.TryGetValue(id, out newSprite))
				{
					return newSprite;
				}
				Texture2D texture = await _imageRepository.Get(id);

				if (texture != null)
				{
					texture.name = id;
					newSprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100);
					if (!_sprites.ContainsKey(id)) _sprites.Add(id, newSprite);
				}
				else
				{
					if (!_sprites.ContainsKey(id)) _sprites.Add(id, newSprite);
				}
				return newSprite;
			}
			public Tuple<string, string> GetTextTuple(object obj, bool ss)
			{
				string text = "";
				string translated = "";
				if (obj is string)
				{
					string s = obj.ToString();
					text = s;
					translated = s;
				}
				else if (obj is Dictionary<string, object>)
				{
					var _obj = (Dictionary<string, object>)obj;
					text = GetLanguage(_obj);
					translated = GetUserKnowledLanguage(_obj);
				}
				else if (obj != null)
				{
					var _obj = obj.ConvertTo<Dictionary<string, object>>();
					text = GetLanguage(_obj);
					translated = GetUserKnowledLanguage(_obj);
				}
				return ss ? Tuple.Create(!string.IsNullOrEmpty(translated) ? translated : text, text) : Tuple.Create(text, !string.IsNullOrEmpty(translated) ? translated : text);
			}
			private bool IsRightToLeft(object obj, bool ss)
			{
				if (obj is string)
				{
					return false;
				}
				else if (obj is Dictionary<string, object>)
				{
					if (ss)
					{
						return _languageService.IsRightToLeftLanguage(GetUserKnowledLanguageKey((Dictionary<string, object>)obj));
					}
					else
					{
						return _languageService.IsRightToLeftLanguage(GetLanguageKey((Dictionary<string, object>)obj));
					}
				}
				else if (obj != null)
				{
					if (ss)
					{
						return _languageService.IsRightToLeftLanguage(GetUserKnowledLanguageKey(obj.ConvertTo<Dictionary<string, object>>()));
					}
					else
					{
						return _languageService.IsRightToLeftLanguage(GetLanguageKey(obj.ConvertTo<Dictionary<string, object>>()));
					}
				}
				return false;
			}
			public async UniTask Update()
			{
				_langSuccesed = false;
				_screens = new Dictionary<string, Repositories.Screen>();
				_playlists = new Dictionary<string, List<Playlist>>();
				_objects = new();
				_downloadedPlaylist = new();
				_undownloadedPlaylist = new();
			}
		}
	}
}


