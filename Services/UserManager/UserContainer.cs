﻿using UnityEngine;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public class UserContainer : MonoBehaviour
		{
			public UnityAction update;
			private void Update()
			{
				update?.Invoke();
			}
		}
	}
}
