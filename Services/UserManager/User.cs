﻿using Firebase.Firestore;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Repositories
	{
		[FirestoreData]
		public struct User
		{
			[FirestoreDocumentId]
			public string id { get; set; }
			[FirestoreProperty]
			public string _version { get; set; }
			[FirestoreProperty]
			public string nik { get; set; }
			[FirestoreProperty]
			public string phone { get; set; }
			[FirestoreProperty]
			public object prefs { get; set; }
			[FirestoreProperty]
			public object reg { get; set; }
			[FirestoreProperty]
			public object referral { get; set; }
			[FirestoreProperty]
			public object created { get; set; }
		}

	}
}
