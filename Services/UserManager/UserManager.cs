﻿using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Native;
using Firebase;
using Firebase.Auth;
using Firebase.Extensions;
using Firebase.Firestore;
#if UNITY_ANDROID
using Google;
#endif
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using System.Text;
using System.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Repositories
	{
	}
	namespace Services
	{
		public class UserManager
		{
			private readonly Transform _container;
			private readonly UserContainer _userContainer;
			private DependencyStatus _status;
			private FirebaseAuth _auth;
			private FirebaseUser _user;
			public FirebaseUser User => _user;
			private string _message;
			public string Message => _message;
			public bool Success => _auth != null && _auth.CurrentUser != null;
			public UnityAction CodeAutoRetrievalTimeOut;
			public UnityAction CodeSent;
			public UnityAction LoginCompleted;
			public UnityAction<string> LoginFailed;
			public UnityAction LogoutCompleted;
			public UnityAction RegistrationCompleted;
			public UnityAction<string> RegistrationFailed;
			public UnityAction Initialized;
			public UnityAction ToRegistration;
			private const string _googleWebApi = "680587170125-pibgk6vgjg17ffcdmreljc02vjihelfn.apps.googleusercontent.com";
			private const string _appleWebUrl = "https://heart-school.firebaseapp.com/__/auth/handler";
#if UNITY_ANDROID
			private readonly GoogleSignInConfiguration _googleSignInConfiguration;
#endif
			private IAppleAuthManager _appleAuthManager;
			public UserManager()
			{
				_container = new GameObject("UserManagerContainer").transform;
				_userContainer = _container.gameObject.AddComponent<UserContainer>();
				UnityEngine.Object.DontDestroyOnLoad(_container);
#if UNITY_ANDROID
				_googleSignInConfiguration = new GoogleSignInConfiguration()
				{
					WebClientId = _googleWebApi,
					RequestIdToken = true,
					RequestEmail = true,
				};
#endif
				if (AppleAuthManager.IsCurrentPlatformSupported)
				{
					var deserializer = new PayloadDeserializer();
					_appleAuthManager = new AppleAuthManager(deserializer);
					_userContainer.update = _appleAuthManager.Update;
				}
				_appleAuthManager?.SetCredentialsRevokedCallback(result =>
				 {
					 LoginFailed?.Invoke(result);
				 });
				_userContainer.StartCoroutine(CheckAndFixDependenciesAsync());
			}
			private IEnumerator CheckAndFixDependenciesAsync()
			{
				var task = FirebaseApp.CheckAndFixDependenciesAsync();
				yield return new WaitUntil(() => task.IsCompleted);
				_status = task.Result;
				if (_status == DependencyStatus.Available)
				{
					InitializeFirebase();
					yield return new WaitForEndOfFrame();
					 _userContainer.StartCoroutine(CheckAuthLogin());
				}
				else
				{
					UnityEngine.Debug.LogError("Dependecy status: " + _status);
				}
			}
			private IEnumerator CheckAuthLogin()
			{
				if (_user != null)
				{
					var task = _user.ReloadAsync();
					yield return new WaitUntil(() => task .IsCompleted);
					FirebaseFirestore.DefaultInstance.Settings.PersistenceEnabled = false;
				}
				Initialized?.Invoke();
			}
			private void InitializeFirebase()
			{
				_auth = FirebaseAuth.DefaultInstance;
				_auth.StateChanged += AuthStateChanged;
				AuthStateChanged(this, null);
			}
			private void AuthStateChanged(object sender, System.EventArgs eventArgs)
			{
				if (_auth.CurrentUser != _user)
				{
					bool signedIn = _user != _auth.CurrentUser && _auth.CurrentUser != null;

					if (!signedIn && _user != null)
					{
						UnityEngine.Debug.Log("Signed out " + _user.UserId);
					}

					_user = _auth.CurrentUser;


					if (signedIn)
					{
						UnityEngine.Debug.Log("Signed in " + _user.UserId);
					}
				}
			}
			public void Login()
			{
				_userContainer.StartCoroutine(LoginAsync());
			}
			public IEnumerator LoginAsync()
			{
				var loginTask = _auth.SignInAnonymouslyAsync();

				yield return new WaitUntil(() => loginTask.IsCompleted);

				if (loginTask.Exception != null)
				{
					UnityEngine.Debug.LogWarning(loginTask.Exception);
					FirebaseException firebaseException = loginTask.Exception.GetBaseException() as FirebaseException;
					AuthError authError = (AuthError)firebaseException.ErrorCode;

					_message = "Login failed! Because ";

					switch (authError)
					{
						case AuthError.InvalidEmail:
							_message += "email is invalid";
							break;
						case AuthError.WrongPassword:
							_message += "wrong password";
							break;
						case AuthError.MissingEmail:
							_message += "email is missing";
							break;
						default:
							_message += "login failed";
							break;
					}
					UnityEngine.Debug.Log(_message);
					LoginFailed?.Invoke(_message);
				}
				else
				{
					_message = "";
					_user = loginTask.Result.User;
					UnityEngine.Debug.LogFormat("{0} You are successfully logged in", _user.DisplayName);
					LoginCompleted?.Invoke();
				}
			}
			public void Login(string email, string password)
			{
				_userContainer.StartCoroutine(LoginAsync(email, password));
			}
			private IEnumerator LoginAsync(string email, string password)
			{
				var loginTask = _auth.SignInWithEmailAndPasswordAsync(email, password);

				yield return new WaitUntil(() => loginTask.IsCompleted);

				if (loginTask.Exception != null)
				{
					UnityEngine.Debug.Log(loginTask.Exception);
					FirebaseException firebaseException = loginTask.Exception.GetBaseException() as FirebaseException;
					AuthError authError = (AuthError) firebaseException.ErrorCode;

				    _message = "Login failed! Because ";

					switch (authError)
					{
						case AuthError.InvalidEmail:
							_message += "email is invalid";
							LoginFailed?.Invoke(_message);
							break;
						case AuthError.WrongPassword:
							_message += "wrong password";
							LoginFailed?.Invoke(_message);
							break;
						case AuthError.MissingEmail:
							_message += "email is missing";
							LoginFailed?.Invoke(_message);
							break;
						default:
							_message += "login failed";
							ToRegistration?.Invoke();
							break;
					}
					UnityEngine.Debug.Log(_message);
				}
				else
				{
					_user = loginTask.Result.User;
					UnityEngine.Debug.LogFormat("{0} You are successfully logged in", _user.DisplayName);
					LoginCompleted?.Invoke();
				}
			}
			public void Login(string phone)
			{
				LoginWithPhone(phone);
			}
			private void LoginWithPhone(string phone)
			{
				var provider = PhoneAuthProvider.GetInstance(_auth);
				provider.VerifyPhoneNumber(
					new Firebase.Auth.PhoneAuthOptions
					{
						PhoneNumber = phone,
						TimeoutInMilliseconds = 60000,
						ForceResendingToken = null
					},
					verificationCompleted: async (credential) => {
						UnityEngine.Debug.Log("comple");
					},
					verificationFailed: (error) => {
						_message = error;
						LoginFailed?.Invoke(_message);
					},
					codeSent: (id, token) => {
						_message = "The verification code was sent to " + phone;
						UnityEngine.Debug.Log(_message);
						PlayerPrefs.SetString("verificationId", id);
						CodeSent?.Invoke();
					},
					codeAutoRetrievalTimeOut: (id) => {
						CodeAutoRetrievalTimeOut?.Invoke();
					});
				
			}
			public void LoginWithCode(string verificationCode)
			{
				var verificationId = PlayerPrefs.GetString("verificationId");
				var provider = PhoneAuthProvider.GetInstance(_auth);
				PhoneAuthCredential credential =
										provider.GetCredential(verificationId, verificationCode);
				_auth.SignInAndRetrieveDataWithCredentialAsync(credential).ContinueWith(task =>
				{
					if (task.IsFaulted)
					{
						UnityEngine.Debug.LogError("SignInAndRetrieveDataWithCredentialAsync encountered an error: " + task.Exception);
						LoginFailed?.Invoke(task.Exception.Message);
					}
					else
					{
						LoginCompleted?.Invoke();
					}
				});
			}
			public void Register(string name, string email, string password, string confirmPassword)
			{
				_userContainer.StartCoroutine(RegisterAsync(name, email, password, confirmPassword));
			}
			private IEnumerator RegisterAsync(string name, string email, string password, string confirmPassword)
			{
				if (string.IsNullOrEmpty(email))
				{
					_message = "Email field is empty";
					UnityEngine.Debug.LogError("Email field is empty");
					RegistrationFailed?.Invoke(_message);
				}
				else if (password != confirmPassword)
				{
					_message = "Password does not match";
					UnityEngine.Debug.Log("Password does not match");
					RegistrationFailed?.Invoke(_message);
				}
				else
				{
					var task = _auth.CreateUserWithEmailAndPasswordAsync(email, password);

					yield return new WaitUntil(() => task.IsCompleted);

					if (task.Exception != null)
					{
						UnityEngine.Debug.LogWarning(task.Exception);
						var firebaseException = task.Exception.GetBaseException() as FirebaseException;
						var authError = (AuthError) firebaseException.ErrorCode;
						_message = "Registration failed! Because ";
						switch (authError)
						{
							case AuthError.InvalidEmail:
								_message += "email is invalid";
								break;
							case AuthError.WrongPassword:
								_message += "wrong password";
								break;
							case AuthError.MissingEmail:
								_message += "email is missing";
								break;
							default:
								_message += "registration failed";
								break;
						}
						UnityEngine.Debug.Log(_message);
						RegistrationFailed?.Invoke(_message);
					}
					else
					{
						_user = task.Result.User;
						var profile = new UserProfile { DisplayName = name };
						var updateProfileTask = _user.UpdateUserProfileAsync(profile);
						yield return new WaitUntil(() => updateProfileTask.IsCompleted);
						if (updateProfileTask.Exception != null)
						{
							_user.DeleteAsync();
						    UnityEngine.Debug.Log(updateProfileTask.Exception);
							var firebaseException = updateProfileTask.Exception.GetBaseException() as FirebaseException;
							var authError = (AuthError)firebaseException.ErrorCode;
							_message = "Profile update failed! Because ";
							switch (authError)
							{
								case AuthError.InvalidEmail:
									_message += "email is invalid";
									break;
								case AuthError.WrongPassword:
									_message += "wrong password";
									break;
								case AuthError.MissingEmail:
									_message += "email is missing";
									break;
								default:
									_message += "registration failed";
									break;
							}
							UnityEngine.Debug.Log(_message);
							RegistrationFailed?.Invoke(_message);
						}
						else
						{
							RegistrationCompleted?.Invoke();
							UnityEngine.Debug.Log("Registration succesful. Welcome " + _user.DisplayName);
						}
					}
				}
			}
			public void LoginGoogle()
			{
#if UNITY_ANDROID
				GoogleSignIn.Configuration = _googleSignInConfiguration;
				GoogleSignIn.Configuration.UseGameSignIn = false;
				GoogleSignIn.Configuration.RequestIdToken = true;
				GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnGoogleAuthFinished);
#endif
			}
			public void LoginApple()
			{
				var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);
				_appleAuthManager.LoginWithAppleId(
			loginArgs,
				credential =>
				{
					var appleIdCredential = credential as IAppleIDCredential;

					var identityToken = Encoding.UTF8.GetString(
					appleIdCredential.IdentityToken,
					0,
					appleIdCredential.IdentityToken.Length);

					var authorizationCode = Encoding.UTF8.GetString(
					appleIdCredential.AuthorizationCode,
					0,
					appleIdCredential.AuthorizationCode.Length);
					OnAppleIdAuthFinished(identityToken, authorizationCode);
			},
			error =>
			{
				var authorizationErrorCode = error.GetAuthorizationErrorCode();
				LoginFailed?.Invoke("Sign in with Apple failed " + authorizationErrorCode.ToString() + " " + error.ToString());
			});
			}
			private void OnAppleIdAuthFinished(string appleIdToken, string rawNonce)
			{
				var credential = Firebase.Auth.OAuthProvider.GetCredential("apple.com", appleIdToken, rawNonce, null);
				_auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task =>
				{
					if (task.IsFaulted || task.IsCanceled)
					{
						LoginFailed?.Invoke("Login failed!");
					}
					else
					{
						_user = _auth.CurrentUser;
						LoginCompleted?.Invoke();
					}
				});

			}
#if UNITY_ANDROID
			private void OnGoogleAuthFinished(Task<GoogleSignInUser> task)
			{
				if (task.IsFaulted)
				{
					LoginFailed?.Invoke("Login failed!");
					Debug.LogError(task.Exception.Message);
					foreach (var ex in task.Exception.InnerExceptions) Debug.LogError(ex.Message);
				}
				else if (task.IsCanceled)
				{
					LoginFailed?.Invoke("Canceled.");
				}
				else
				{
					var credential = Firebase.Auth.GoogleAuthProvider.GetCredential(task.Result.IdToken, null);

					_auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task =>
					{
						if (task.IsFaulted || task.IsCanceled)
						{
							LoginFailed?.Invoke("Login failed!");
							Debug.LogError(task.Exception.Message);
							foreach (var ex in task.Exception.InnerExceptions) Debug.LogError(ex.Message);
						}
						else
						{
							_user = _auth.CurrentUser;
							LoginCompleted?.Invoke();
						}
					});
				}
			}
#endif

			public void Logout()
			{
				if (_auth != null && _user != null)
				{
					_auth.SignOut();
					LogoutCompleted?.Invoke();
				}
			}
			private IEnumerator SendEmailForVerificationAsync()
			{
				if (_user != null)
				{
					var task = _user.SendEmailVerificationAsync();
					yield return new WaitUntil(() => task.IsCompleted);

					if (task.Exception != null)
					{
						var firebaseException = task.Exception.GetBaseException() as FirebaseException;
						var authError = (AuthError)firebaseException.ErrorCode;
						_message = "Unknown error: Please try again later";
						switch (authError)
						{
							case AuthError.Cancelled:
								_message += "Email varification was Cancelled";
								break;
							case AuthError.TooManyRequests:
								_message += "Too many request";
								break;
							case AuthError.InvalidRecipientEmail:
								_message += "Email is invalid";
								break;
						}
						UnityEngine.Debug.Log(_message);
					}
					else
					{

					}
				}
			}
		}
	}
}
