﻿using ByHeartSchool.Services;
using Firebase.Firestore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Repositories
	{
		public class UserRepository : IBaseRepository<User>
		{
			private FirebaseFirestore _dbRef;
			private readonly IDataSaver _dataSaver;
			private readonly UserManager _userManager;
			private bool _isUpdating;
			public UserRepository(IDataSaver dataSaver, UserManager userManager)
			{
				_dataSaver = dataSaver;
				_userManager = userManager;
				_userManager.Initialized += Init;
			}
			public async UniTask<List<User>> GetAll()
			{
				var users = new List<User>();
				var quarry = await _dbRef.Collection("users").GetSnapshotAsync();
				foreach (var user in quarry)
				{
					users.Add(user.ConvertTo<User>());
				}
				return users;
			}
			public void Init()
			{
				_dbRef = FirebaseFirestore.DefaultInstance;
			}
			public async UniTask<User> Get(string id)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<User>(id);
					if (save.id == id)
					{
						var saveUser = new User()
						{
							id = save.id,
							_version = save._version,
							nik = save.nik,
							phone = save.phone,
							prefs = GetEditPrefs(save),
							reg = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(save.reg)),
						};
						return saveUser;
					}
					return new User();
				}
				var quarry = await _dbRef.Collection("users").Document(id).GetSnapshotAsync().AsUniTask();
				var user = new User();
				if (quarry.Exists)
				{
					user = quarry.ConvertTo<User>();
				}
				var oldSave = await _dataSaver.LoadDataAsync<User>(user.id);
				if (!oldSave.Equals(user) && !string.IsNullOrEmpty(oldSave.id))
				{
					user.prefs = GetEditPrefs(oldSave);
					await Update(user);
					return user;
				}
				_dataSaver.SaveDataAsync(user, id);
				return user;
			}
			private Dictionary<string, object> GetEditPrefs(User user)
			{
				var _prefs = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(user.prefs));
				Dictionary<string, object> _editPrefs = new Dictionary<string, object>();
				foreach (var key in _prefs.Keys)
				{
					switch (key)
					{
						case "user-languages":
							_editPrefs.Add(key, JsonConvert.DeserializeObject<List<object>>(JsonConvert.SerializeObject(_prefs[key])));
							break;
						case "user-balance":
							_editPrefs.Add(key, JsonConvert.DeserializeObject<int>(JsonConvert.SerializeObject(_prefs[key])));
							break;
						case "transactions":
							_editPrefs.Add(key, JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(JsonConvert.SerializeObject(_prefs[key])));
							break;
						case "override_events":
							_editPrefs.Add(key, JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(JsonConvert.SerializeObject(_prefs[key])));
							break;
						case "override_bubles":
							_editPrefs.Add(key, JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(JsonConvert.SerializeObject(_prefs[key])));
							break;
					}
				}
				return _editPrefs;
			}
			public async UniTask Update(User newUser)
			{
				while (_isUpdating)
				{
					await UniTask.Yield();
				}

				_isUpdating = true;
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					var save = await _dataSaver.LoadDataAsync<User>(newUser.id);
					if (save.id == newUser.id)
					{
						await _dataSaver.SaveDataAsync(newUser, newUser.id);
					}
					return;
				}
				await _dbRef.Collection("users").Document(newUser.id).SetAsync(newUser).AsUniTask();
				await _dataSaver.SaveDataAsync(newUser, newUser.id);
				_isUpdating = false;
			}
		}
	}
}
