﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class UserManagerInstaller: Installer<UserManagerInstaller>
		{
			public override void InstallBindings()
			{
				Container.Bind<UserManager>().AsSingle();
			}
		}
	}
}
