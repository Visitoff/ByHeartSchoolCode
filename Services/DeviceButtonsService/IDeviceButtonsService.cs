﻿using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IDeviceButtonsService
		{
			void BackButtonOnClick(UnityAction action);
		}
	}
}
