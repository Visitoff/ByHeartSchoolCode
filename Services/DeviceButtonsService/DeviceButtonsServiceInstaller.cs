﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class DeviceButtonsServiceInstaller : Installer<DeviceButtonsServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<DeviceButtonsService>().AsSingle();
			}
		}
	}
}
