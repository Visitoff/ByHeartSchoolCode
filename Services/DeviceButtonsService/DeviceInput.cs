﻿using UnityEngine;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public class DeviceInput: MonoBehaviour
		{
			public UnityAction backButtonAction;
			private void Update()
			{
				if (Input.GetKeyUp(KeyCode.Escape))
				{
					backButtonAction?.Invoke();
				}
			}
		}
	}
}
