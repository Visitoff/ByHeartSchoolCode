﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public class DeviceButtonsService : IDeviceButtonsService, IDisposable
		{
			private readonly Transform _container;
			private readonly List<IDisposable> _disposables = new();
			private readonly DeviceInput _deviceInput;
			public DeviceButtonsService()
            {
				_container = new GameObject("DeviceInputContainer").transform;
				_deviceInput = _container.gameObject.AddComponent<DeviceInput>();
				UnityEngine.Object.DontDestroyOnLoad(_container);
			}
            public void BackButtonOnClick(UnityAction action)
			{
				_deviceInput.backButtonAction = action;
			}

			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
