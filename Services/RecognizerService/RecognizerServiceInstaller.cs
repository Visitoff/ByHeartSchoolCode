﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class RecognizerServiceInstaller : Installer<RecognizerServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<RecognizerService>().AsSingle();
			}
		}
	}

}
