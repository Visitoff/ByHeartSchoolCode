﻿using ByHeartSchool.Menu;
using ByHeartSchool.Response;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IRecognizerService
		{
			bool IsRecognize { get;  set; }
			Func<bool> IsRecognizePossible { get; set; }
			UnityAction onStartProcessing { get;  set; }
			void Init(ListAdapter adapter, List<string> phrases, UnityAction onComplete, List<string> listenerPhrases, List<string> viewPhrases);
			IBaseResponse StartRecognizing(int index, bool hardMode);
			IBaseResponse StopRecognizing();
			IBaseResponse Update();
			IBaseResponse OnHide();
			void Dispose();


		}

	}

}
