﻿using Zenject;
using UnityEngine;
using Recognizer;
using ByHeartSchool.Menu;
using SimpleUi.Signals;
using ByHeartSchool.Response;
using System;
using DG.Tweening;
using System.Collections.Generic;
using UniRx;
using UnityEngine.Events;
using Unity.VisualScripting;

namespace ByHeartSchool
{
	namespace Services
	{
		public class RecognizerService : Zenject.IInitializable, IRecognizerService, IDisposable
		{
			private ListenerManager _listenerManager;
			private RecognizerManager _recognizerManager;
			private readonly SignalBus _signalBus;
			private readonly ILanguageService _languageService;
			private int _recogIndex = 0;
			private string _saveText;
			public bool IsRecognize { get; set; }
			public Func<bool> IsRecognizePossible { get; set; }
			private ListAdapter _adapter;
			private List<string> _phrases;
			private readonly List<IDisposable> _disposables = new();
			private bool _finished;
			private List<string> _viewPhrases = new();
			public UnityAction onStartProcessing { get; set; }
			public List<char> punctuation = new List<char> {'.', '?', '!', ',', ';', '。', '、', '！', '？', '…','?', '!', '，', '一' };
			public RecognizerService(SignalBus signalBus, ILanguageService languageService)
			{
				_signalBus = signalBus;
				_languageService = languageService;
			}
			public void Initialize()
			{
				var listener = GameObject.Find("Listener");
				_listenerManager = listener.GetComponent<ListenerManager>();
				_recognizerManager = listener.GetComponent<RecognizerManager>();
			}
			public void Init(ListAdapter adapter, List<string> phrases, UnityAction onComplete, List<string> listenerPhrases, List<string> viewPhrases)
			{
				_adapter = adapter;
				_phrases = phrases;
				_viewPhrases = viewPhrases;
				//_recognizerManager.OnFastRecognize = FastResult;
				_recognizerManager.OnSlowRecognize = (i) => SlowResult(i, onComplete);
				if (_listenerManager == null || _listenerManager.IsDestroyed()) Initialize();


				_listenerManager.Init((SystemLanguage)_languageService.GetCurrentLanguageIndex(), listenerPhrases.ToArray());
				_listenerManager.OnStartProcessing = () =>
				{
					onStartProcessing();
					OnStartProcessing();
				};
				if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
				{
					Application.RequestUserAuthorization(UserAuthorization.Microphone);
				}
			}
			private void OnStartProcessing()
			{
				var view = _adapter.GetItemView(_recogIndex);
				if (view != null)
				{
					if (view.name != null)
					{
						var text = view.name.text;
						view.name.color = Color.black;
						string alpha = view.fade == 0 ? "00" : "FF";
						view.name.text = $"<color=#808080><alpha=#{alpha}>" + _viewPhrases[_recogIndex] + $"</color><alpha=#{alpha}>";
					}
					if (view.text != null)
					{
						var textIndex = _adapter.GetTextIndex(view, _recogIndex);
						var text = view.text.GetTextValues(textIndex).Item2;
						view.text.text.color = Color.black;
						string alpha = view.fade == 0 ? "00" : "FF";
						view.text.SetTextValue(textIndex, $"<color=#808080><alpha=#{alpha}>" + _viewPhrases[_recogIndex] + $"</color><alpha=#{alpha}>");
						view.text.text.text = $"<alpha=#{alpha}>" + view.text.text.text;
					}
				}
			}
			public IBaseResponse StartRecognizing(int index, bool hardMode)
			{
				try
				{
					GC.Collect();
					Resources.UnloadUnusedAssets();
					_recogIndex = index;
					_listenerManager.CreatePhrase(index, hardMode);
					return new BaseResponse()
					{
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse()
					{
						Description = $"[StartRecognizing] : {ex.Message}",
						StatusCode = Response.StatusCode.OK
					};
				}
			}

			public IBaseResponse StopRecognizing()
			{
				try
				{
					_listenerManager.Stop();
					var view = _adapter.GetItemView(_recogIndex);
					if (view != null)
					{
						if (view.name != null)
						{
							view.name.text = _phrases[_recogIndex];
							view.name.DOFade(view.fade, 0);
						}
						if (view.text != null)
						{
							var textIndex = _adapter.GetTextIndex(view, _recogIndex);
							view.text.SetTextValue(textIndex, _phrases[_recogIndex]);
						}
					}
					return new BaseResponse()
					{
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse()
					{
						Description = $"[StopRecognizing] : {ex.Message}",
						StatusCode = Response.StatusCode.OK
					};
				}
			}
/*			private void FastResult(int index)
			{
				if (IsRecognizePossible.Invoke()) return;
				Dispose();
				Debug.Log($"fast result index: {index}");
				var view = _adapter.GetItemView(_recogIndex);
				string alpha = view.fade == 0 ? "00" : "FF";
				if (view.name != null) _saveText = view.name.text;
				if (view.text != null)
				{
					var textIndex = _adapter.GetTextIndex(view, _recogIndex);
					_saveText = view.text.GetTextValues(textIndex).Item2;
				}
				var mainText = _viewPhrases[_recogIndex];
				string newText = "<alpha=#FF>";
				int j = 0;
				for (int i = 0; i < mainText.Length; i++)
				{
					char c = mainText[i];
					if (c == '؜' || (punctuation.Contains(c) && i + 1 != mainText.Length && char.IsLetter(mainText[i + 1])))
					{
						if (j == index) newText += $"<color=#808080><alpha=#{alpha}>";
						j++;
					}
					newText += c;
				}
				if (view.name != null) _adapter.GetItemView(_recogIndex).name.text = newText + $"</color><alpha=#{alpha}>";
				if (view.text != null)
				{
					var textIndex = _adapter.GetTextIndex(view, _recogIndex);
					view.text.SetTextValue(textIndex, newText + $"</color><alpha=#{alpha}>");
					view.text.text.text = $"<alpha=#{alpha}>" + view.text.text.text;
				}
				Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(l =>
				{
					if (view.name != null) _adapter.GetItemView(_recogIndex).name.text = _saveText;
					if (view.text != null)
					{
						var textIndex = _adapter.GetTextIndex(view, _recogIndex);
						view.text.SetTextValue(textIndex, _saveText);
						view.text.text.text = $"<alpha=#{alpha}>" + view.text.text.text;
					}
				}).AddTo(_disposables);
			}*/
			private void SlowResult(int index, UnityAction onComplete)
			{
				if (IsRecognizePossible.Invoke() || _finished) return;
				Dispose();
				Debug.Log($"slow result index: {index}");
				var text = _viewPhrases[_recogIndex];
				var view = _adapter.GetItemView(_recogIndex);
				string alpha = view.fade == 0 ? "00" : "FF";
				string newText = "<alpha=#FF>";
				int j = 0;
				for (int i = 0; i < text.Length; i++)
				{
					char c = text[i];
					if (j == index && (i == text.Length - 1 || ((c == '؜') && _listenerManager.GetPhrasesForRecogniseCount() - 1 == index)))
					{
						newText += $"<color=#808080><alpha=#{alpha}>";
						newText += c;
						if (i != text.Length - 1)
						{
							for (int k = i + 1; k < text.Length; k++)
							{
								newText += text[k];
							}
						}
						_finished = true;
						Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe(l =>
						{
							if (view.name != null) view.name.text = _phrases[_recogIndex];
							if (view.text != null)
							{
								var textIndex = _adapter.GetTextIndex(view, _recogIndex);
								view.text.SetTextValue(textIndex, _phrases[_recogIndex]);
							}
							onComplete?.Invoke();
							_finished = false;
						}).AddTo(_disposables);
						break;
					}
					if (c == '؜' || (punctuation.Contains(c) && i + 1 != text.Length && char.IsLetter(text[i + 1])))
					{
						if (j == index) newText += $"<color=#808080><alpha=#{alpha}>";
						j++;
					}
					newText += c;
				}
				if (view.name != null) _adapter.GetItemView(_recogIndex).name.text = newText + $"</color><alpha=#{alpha}>";
				if (view.text != null)
				{
					var textIndex = _adapter.GetTextIndex(view, _recogIndex);
					view.text.SetTextValue(textIndex, newText + $"</color><alpha=#{alpha}>");
					view.text.text.text = $"<alpha=#{alpha}>" + view.text.text.text;
				}
			}
			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}

			public IBaseResponse Update()
			{
				try
				{
					_listenerManager.UpdateLanguages((SystemLanguage)_languageService.GetCurrentLanguageIndex());
					return new BaseResponse()
					{
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}

				catch (Exception ex)
				{
					return new BaseResponse()
					{
						Description = $"[Update] : {ex.Message}",
						StatusCode = Response.StatusCode.OK
					};
				}
			}
			public IBaseResponse OnHide()
			{
				try
				{
					_listenerManager.Stop();
					return new BaseResponse()
					{
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}

				catch (Exception ex)
				{
					return new BaseResponse()
					{
						Description = $"[OnHide] : {ex.Message}",
						StatusCode = Response.StatusCode.OK
					};
				}
			}

		}

	}
}
