﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class EditEventsServiceInstaller : Installer<EditEventsServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<EditEventsService>().AsSingle();
			}
		}
	}
}
