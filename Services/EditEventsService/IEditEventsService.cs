﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IEditEventsService
		{
			UniTask<IBaseResponse<string>> Override(string eventId, string newText);
			UniTask<List<Dictionary<string, object>>> GetOverrideEvents();
			UniTask<IBaseResponse<bool>> ResetPlaylist(string playlistId);
			UniTask<IBaseResponse<string>> OverrideBuble(string eventId, string type);
			UniTask<List<Dictionary<string, object>>> GetOverrideBubles();
		}
	}
}

