﻿using ByHeartSchool.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using ByHeartSchool.Response;
using Unity.VisualScripting;
using System.Linq;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public class EditEventsService: IEditEventsService, IInitializable
		{
			private readonly UserManager _userManager;
			private readonly IBaseRepository<User> _userRepository;
			private User _user;
			private List<Dictionary<string, object>> _events = new();
			private List<Dictionary<string, object>> _ABbubles = new();
			private bool _init;
			public EditEventsService(UserManager userManager, IBaseRepository<User> userRepository)
			{
				_userManager = userManager;
				_userRepository = userRepository;
			}

			public async void Initialize()
			{
				if (!_userManager.Success) _userManager.Initialized += async () => await Init();
				else await Init();
			}
			private async UniTask Init()
			{
				if (_userManager.Success)
				{
					_user = await _userRepository.Get(_userManager.User.UserId);
					var prefs = (Dictionary<string, object>)_user.prefs;
					object events = new List<Dictionary<string, object>>();
					object bubles = new List<Dictionary<string, object>>();
					if (prefs.TryGetValue("override_events", out events))
					{
						_events = (List<Dictionary<string, object>>)events;
					}
					else
					{
						prefs.Add("override_events", new List<Dictionary<string, object>>());
					}
					if (prefs.TryGetValue("override_bubles", out bubles))
					{
						_ABbubles = (List<Dictionary<string, object>>)bubles;
					}
					else
					{
						prefs.Add("override_bubles", new List<Dictionary<string, object>>());
					}
					_user.prefs = prefs;
					await _userRepository.Update(_user);
					_init = true;
				}
			}
			public async UniTask<List<Dictionary<string, object>>> GetOverrideEvents()
			{
				if (!_init) await Init();
				return _events;
			}
			public async UniTask<List<Dictionary<string, object>>> GetOverrideBubles()
			{
				if (!_init) await Init();
				return _ABbubles;
			}
			public async UniTask<IBaseResponse<string>> Override(string eventId, string newText)
			{
				try
				{
					if (!_init) await Init();
					var prefs = (Dictionary<string, object>)_user.prefs;
					var events = prefs["override_events"].ConvertTo<List<Dictionary<string, object>>>();
					events.Add(new Dictionary<string, object> { { eventId, newText } });
					prefs["override_events"] = events;
					_events = events;
					_user.prefs = prefs;
					await _userRepository.Update(_user);
					return new BaseResponse<string>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = newText
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<string>()
					{
						Description = $"[Override] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public async UniTask<IBaseResponse<string>> OverrideBuble(string eventId, string type)
			{
				try
				{
					if (!_init) await Init();
					var prefs = (Dictionary<string, object>)_user.prefs;
					var events = prefs["override_bubles"].ConvertTo<List<Dictionary<string, object>>>();
					var buble = events.FindLast(x => x.ContainsKey(eventId));
					if (buble != null)
					{
						buble[eventId] = type;
					}
					else
					{
						events.Add(new Dictionary<string, object> { { eventId, type } });
					}
					prefs["override_bubles"] = events;
					_ABbubles = events;
					_user.prefs = prefs;
					await _userRepository.Update(_user);
					return new BaseResponse<string>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = type
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<string>()
					{
						Description = $"[OverrideBuble] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}

			public async UniTask<IBaseResponse<bool>> ResetPlaylist(string playlistId)
			{
				try
				{
					if (!_init) await Init();
					var prefs = (Dictionary<string, object>)_user.prefs;
					var events = prefs["override_events"].ConvertTo<List<Dictionary<string, object>>>();
					foreach (var ev in events)
					{
						if (ev.Keys.ToList()[0].Split('/')[0] == playlistId)
						{
							events.Remove(ev);
						}
					}
					_events = events;
					prefs["override_events"] = events;
					_user.prefs = prefs;
					await _userRepository.Update(_user);
					return new BaseResponse<bool>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = true
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<bool>()
					{
						Description = $"[ResetPlaylist] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
		}
	}
}

