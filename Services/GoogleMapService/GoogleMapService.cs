﻿using ByHeartSchool.Response;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public class GoogleMapService : IGoogleMapService 
		{
			private const string apiKey = "AIzaSyDYtpwZAFny6OXTZ3wZOnaQmcHKsXq_n8I";
			private const int _mapWidth = 2000;
			private const int _mapHeight = 2000;
			private const int _scale = 1;
			private const string _mapType = "roadmap";
/*			public async Task<IBaseResponse<GoogleMapsView>> Get(RectTransform rectTransform,float lat, float lng,float zoom = 15, float tilt = 0,float bearing = 0)
			{
				*//*try
				{*//*
					if (Application.internetReachability == NetworkReachability.NotReachable)
					{
						return new BaseResponse<GoogleMapsView>()
						{
							StatusCode = StatusCode.Failed,
							Description = "Application.internetReachability == NetworkReachability.NotReachable",
						};
					}
					var options = CreateMapViewOptions(lat, lng, zoom, tilt, bearing);
					GoogleMapsView view = null;
					GoogleMapsView.CreateAndShow(options,rectTransform.rect, (obj) => view = obj);
					while (view == null)
					{
						await Task.Yield();
					}
				Debug.LogError("Done");
					return new BaseResponse<GoogleMapsView>()
					{
						Data = view,
						StatusCode = StatusCode.OK,
						Description = "DONE",
					};
	*//*			}
				catch (Exception ex)
				{
					return new BaseResponse<GoogleMapsView>()
					{
						StatusCode = StatusCode.Failed,
						Description = $"[Open] : {ex.Message}",
					};
				}*//*
			}*/

			public IBaseResponse Open(string url)
			{
				try
				{
					if (Application.internetReachability == NetworkReachability.NotReachable)
					{
						return new BaseResponse()
						{
							StatusCode = StatusCode.Failed,
							Description = "Application.internetReachability == NetworkReachability.NotReachable",
						};
					}
					Application.OpenURL(url);
					return new BaseResponse()
					{
						StatusCode = StatusCode.OK,
						Description = "Done",
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse()
					{
						StatusCode = StatusCode.Failed,
						Description = $"[Open] : {ex.Message}",
					};
				}
			}
/*			private GoogleMapsOptions CreateMapViewOptions(float lat, float lng, float zoom = 15, float tilt = 0, float bearing = 0)
			{
				var options = new GoogleMapsOptions();

				options.MapType(GoogleMapType.Hybrid);

				// Camera position
				options.Camera(new CameraPosition(
						new LatLng(lat, lng),
						zoom,
						tilt,
						bearing));

				// Specifies a LatLngBounds to constrain the camera target
				// so that when users scroll and pan the map, the camera target does not move outside these bounds.
				var southWest = new LatLng(lat -1, lng + 1);
				var northEast = new LatLng(lat + 1, lng -1);
				options.LatLngBoundsForCameraTarget(new LatLngBounds(southWest, northEast));

				// Other settings
				options.AmbientEnabled(true);
				options.CompassEnabled(true);
				options.LiteMode(true);
				options.MapToolbarEnabled(true);
				options.RotateGesturesEnabled(true);
				options.ScrollGesturesEnabled(true);
				options.TiltGesturesEnabled(true);
				options.ZoomGesturesEnabled(true);
				options.ZoomControlsEnabled(true);

				options.MinZoomPreference(zoom - 2);
				//options.MaxZoomPreference(zoom + 2);

				return options;
			}*/
		}
	}
}