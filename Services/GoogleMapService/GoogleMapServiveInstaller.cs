﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class GoogleMapServiveInstaller : Installer<GoogleMapServiveInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<GoogleMapService>().AsSingle();
			}
		}
	}
}