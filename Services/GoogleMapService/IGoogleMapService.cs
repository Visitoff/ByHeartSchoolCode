﻿using ByHeartSchool.Response;
using System.Threading.Tasks;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IGoogleMapService
		{
			//Task<IBaseResponse<GoogleMapsView>> Get(RectTransform rectTransform, float lat, float lng, float zoom = 15, float tilt = 0, float bearing = 0);
			IBaseResponse Open(string key);
		}
	}
}