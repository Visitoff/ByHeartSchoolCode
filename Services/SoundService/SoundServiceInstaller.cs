﻿using Zenject;
using UniRx;

namespace ByHeartSchool
{
	namespace Services
	{
		public class SoundServiceInstaller : Installer<SoundServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<SoundService>().AsSingle();
			}
		}
	}
}
