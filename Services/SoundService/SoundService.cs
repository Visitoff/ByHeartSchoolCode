﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Unity.VisualScripting;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public class SoundService : ISoundService, Zenject.IInitializable, IDisposable
		{
			private readonly Transform _container;
			private List<GameObject> _audioPool;

			private readonly Dictionary<string, AudioClip> _audioClips = new();
			private readonly List<IDisposable> _disposables = new();
			private Dictionary<string, AudioSource> _currentPlayList = new();
			public Sprite MusicIcon {  get;  set; }
			public SoundService()
			{
				_container = new GameObject("AudioSourceContainer").transform;
				UnityEngine.Object.DontDestroyOnLoad(_container);
			}

			private void CreatePool(int count = 4)
			{
				_audioPool = new List<GameObject>();
				for (int i = 0; i < count; i++)
				{
					CreateObject();
				}
			}

			private GameObject CreateObject(bool activeByDefault = false)
			{
				var createdObject = new GameObject("AudioSource#" + _audioPool.Count);
				createdObject.transform.SetParent(_container);
				createdObject.AddComponent<AudioSource>();
				createdObject.SetActive(activeByDefault);
				_audioPool.Add(createdObject);
				return createdObject;
			}

			private bool HasFreeElements(out GameObject element)
			{
				foreach (var gameObject in _audioPool)
				{
					if (!gameObject.activeInHierarchy)
					{
						element = gameObject;
						return true;
					}
				}

				element = null;
				return false;
			}

			private GameObject GetFreeElement() => HasFreeElements(out var element) ? element : CreateObject(true);

			private void LoadSounds()
			{
				var clips = Resources.LoadAll<AudioClip>("Audio");
				foreach (var audioClip in clips)
				{
					_audioClips.Add(audioClip.name, audioClip);
				}
			}
			private void Fade(float fraction)
			{
				foreach (var pool in _audioPool)
				{
					var source = pool.GetComponent<AudioSource>();
					if (source != null)
					{
						source.volume *= fraction;
					}
				}
			}
			public AudioSource PlaySound(string name, Action onComplete = null, bool isLoop = false, float volume = 1f)
			{
				Fade(0.8f);
				var player = GetFreeElement().GetComponent<AudioSource>();
				player.gameObject.SetActive(true);
				player.clip = _audioClips[name];
				player.volume = volume;
				if (isLoop)
				{
					player.loop = true;
					_currentPlayList.Add(name, player);
				}
				else
				{
					Observable.Timer(TimeSpan.FromSeconds(player.clip.length)).Subscribe(l =>
					{
						Fade(1.25f);
						StopPlay(player, onComplete);
					}).AddTo(_disposables);
				}
				player.Play();
				return player;
			}
			public AudioSource PlaySound(AudioClip clip, Action onComplete = null, bool isLoop = false, float volume = 1f, float? time = null)
			{
				Fade(7f / 10f);
				var player = GetFreeElement().GetComponent<AudioSource>();
				player.gameObject.SetActive(true);
				player.clip = clip;
				player.volume = volume;
				if (time.HasValue)
				{
					if (clip.length > time)
					{
						player.time = time.Value;
					}
					else if (isLoop)
					{
						player.time = time.Value % clip.length;
					}
					else
					{
						return null;
					}
				}
				if (isLoop)
				{
					player.loop = true;
					_currentPlayList.Add(clip.name, player);
				}
				else
				{
					if (_currentPlayList.ContainsKey(clip.name))
					{
						_currentPlayList.Remove(clip.name);
					}
					_currentPlayList.Add(clip.name, player);
					Observable.Timer(TimeSpan.FromSeconds(player.clip.length)).Subscribe(l =>
					{
						Fade(10f / 7f);
						StopPlay(player, onComplete);
					}).AddTo(_disposables);
				}
				player.Play();
				return player;
			}

			private void StopPlay(AudioSource player, Action onComplete)
			{
				player.Pause();
				player.clip = null;
				player.loop = false;
				player.gameObject.SetActive(false);
				onComplete?.Invoke();
			}

			public void StopSound(string name)
			{
				if (!_currentPlayList.ContainsKey(name)) return;
				StopPlay(_currentPlayList[name], null);
				_currentPlayList.Remove(name);
			}

			public void StopAllSounds()
			{
				foreach (var keyValuePair in _currentPlayList)
				{
					if (keyValuePair.Value != null && !keyValuePair.Value.IsDestroyed()) StopPlay(keyValuePair.Value, null);
				}
				Clear();
				_currentPlayList = new Dictionary<string, AudioSource>();
			}

			public void Initialize()
			{
				LoadSounds();
				CreatePool();
			}

			public void Dispose()
			{
				Clear();
			}

			public void Clear()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
