using System;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface ISoundService
		{
			public Sprite MusicIcon { get; set; }
			public AudioSource PlaySound(string name, Action onComplete = null, bool isLoop = false, float volume = 1f);
			public AudioSource PlaySound(AudioClip clip, Action onComplete = null, bool isLoop = false, float volume = 1f, float? time = null);
			public void StopSound(string name);
			public void StopAllSounds();
			public void Dispose();
		}
	}

}
