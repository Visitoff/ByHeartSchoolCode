﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlaylistServiceInstaller : Installer<PlaylistServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<PlaylistService>().AsSingle();
			}
		}
	}
}
