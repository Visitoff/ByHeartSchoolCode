﻿using System.Collections.Generic;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IPlaylistService
		{
			public UnityAction<bool> OnStartUpdate { get; set; }
			public UnityAction OnEndUpdate { get; set; }
			public UniTask<bool> IsDownload(string id, string courseId = "", string recursion = "");
			UniTask<IBaseResponse<bool>> Download(string courseId, string playlistId, string recursion = "");
			UniTask<IBaseResponse<bool>> Delete(string courseId, string playlistId);
			UniTask<IBaseResponse<List<InPlayerEvent>>> Open(string courseId, string playlistId, bool recursion = false);
			IBaseResponse<bool> DeleteAll();
			List<InPlayerEvent> GetEvents();
			UniTask Update(bool withoutActions = false, bool settingsSignal = false);
			string ParsePhraseString(string text, bool isForView = false, bool recognizedFlagsAdd = false);
			bool HasAudio();
		}
	}
}


