﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace ByHeartSchool
{
	namespace Services
	{
		public struct InPlayerEvent
		{
			public string id;
			public string type;
			public string version;
			public string voice;
			public string placement;
			public string media;
			public Dictionary<string, object> phrase;
			public Dictionary<string, AudioClip> clip;
			public List<string> options;
			public bool ss;
			public string playlistId;
			public Sprite img;
			public int order;
			public string videoUrl;
			public Dictionary<string, object> description;
		}
	}
}


