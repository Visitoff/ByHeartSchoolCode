﻿using ByHeartSchool.Repositories;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using ByHeartSchool.Response;
using Unity.VisualScripting;
using UnityEngine.Events;
using Cysharp.Threading.Tasks;
using System.IO;
using UnityEditor;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlaylistService : IPlaylistService
		{
			private readonly IExRepository<AudioClip> _audioClipRepository;
			private readonly IChildChildDownloadDeleteRepository<Repositories.Event> _eventRepository;
			private readonly ILanguageService _languageService;
			private readonly IBaseRepository<Course> _courseRepository;
			private readonly IGetUpdateRepository<Dictionary<string, DateTimeOffset>> _dateTimeRepository;
			private readonly IGetDownloadRepository<Texture2D> _imageRepository;
			private readonly IGetDownloadDeleteRepository<AudioClip> _musicRepository;
			private readonly IGetDownloadDeleteRepository<string> _videoRepository;
			private readonly IChildRepository<Playlist> _playlistRepository;
			private readonly IMediaSceneService _mediaSceneService;
			private List<InPlayerEvent> _events = new List<InPlayerEvent>();
			private string lang;
			private List<string> _userLanguages;
			private List<Course> _courses = new();
			private Dictionary<string, DateTimeOffset> _dates = new();
			public UnityAction<bool> OnStartUpdate { get; set; }
			public UnityAction OnEndUpdate { get; set; }
			private Dictionary<string, List<string>> _downloads = new();
			private List<string> _downloadChecks = new();
			private List<string> _openings = new();
			private int _downloadingTasks;
			private bool _hasAudio;

			public PlaylistService(
				IExRepository<AudioClip> audioClipRepository,
				IChildChildDownloadDeleteRepository<Repositories.Event> eventRepository,
				ILanguageService languageService,
				IBaseRepository<Course> courseRepository,
				IGetUpdateRepository<Dictionary<string, DateTimeOffset>> dataTimeRepository,
				IGetDownloadDeleteRepository<AudioClip> musicRepository,
				IGetDownloadRepository<Texture2D> imageRepository,
				IGetDownloadDeleteRepository<string> videoRepository,
				IChildRepository<Playlist> playlistRepository,
				IMediaSceneService mediaSceneService)
			{
				_audioClipRepository = audioClipRepository;
				_eventRepository = eventRepository;
				_languageService = languageService;
				_courseRepository = courseRepository;
				_dateTimeRepository = dataTimeRepository;
				_musicRepository = musicRepository;
				_imageRepository = imageRepository;
				_videoRepository = videoRepository;
				_playlistRepository = playlistRepository;
				_mediaSceneService = mediaSceneService;
			}
			public List<InPlayerEvent> GetEvents()
			{
				var events = new List<InPlayerEvent>(_events);
				_events = new();
				Resources.UnloadUnusedAssets();
				return events;
			}

			public async UniTask Update(bool withoutActions = false, bool settingsSignal = false)
			{
				try
				{
					if (!withoutActions) OnStartUpdate?.Invoke(settingsSignal);
					_downloads = new();
					_downloadChecks = new();
					_dates = await _dateTimeRepository.Get("dateTimesCourses");
					lang = _languageService.GetCurrentLanguage();
					_userLanguages = await _languageService.GetUserLanguages();
					if (!_userLanguages.Contains(lang)) _userLanguages.Add(lang);
					_courses = await _courseRepository.GetAll();
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
				}
				if (!withoutActions) OnEndUpdate?.Invoke();
			}
			private char[] _spaceChars = { '-', '—' };
			public string ParsePhraseString(string text, bool isForView = false, bool recognizedFlagsAdd = false)
			{
				if (string.IsNullOrEmpty(text)) return text;
				string newText = "";
				bool deleteMode = false;
				char[] chars = { '(', ')', ']', '[' };
				char wordChar = '^';
				char startChar = '[';
				char endChar = ']';
				char transcriptionChar = ':';
				bool transcriptionMode = false;
				if (isForView)
				{
					startChar = '(';
					endChar = ')';
				}
				int j = 0;
				var deleleWordsCount = 0;
				string word = "";
				bool wordMode = false;
				List<string> words = new List<string>();
				for (int i = 0; i < text.Length; i++)
				{
					char c = text[i];
					char prevC = i > 0 ? text[i - 1] : c;
					if (transcriptionChar == c && isForView && (chars.Contains(prevC) || transcriptionMode))
					{
						//if (!transcriptionMode) newText += "\"";
						transcriptionMode = true;
						deleteMode = !deleteMode;
					}
					if (c == wordChar)
					{
						wordMode = !wordMode;
						if (!wordMode)
						{
							words.Add(word);
							word = "";
						}
					}
					if (wordMode && c != wordChar)
					{
						word += c;
					}
					if (c == startChar && (prevC == ']' || !isForView))
					{
						deleteMode = true;
						deleleWordsCount = 0;
					}
					if (c == endChar && deleteMode)
					{
						deleteMode = false;
						if (recognizedFlagsAdd) newText += new string('؜', deleleWordsCount);
						if (transcriptionMode)
						{
							//newText += "\"";
							transcriptionMode = false;
						}
					}
					if (!deleteMode && !chars.Contains(c) && c != wordChar && !(c == transcriptionChar && transcriptionMode))
					{
						newText += c;
						if (recognizedFlagsAdd && char.IsWhiteSpace(c) && (char.IsLetterOrDigit(prevC) || ((char.IsPunctuation(prevC) || prevC == wordChar) && !_spaceChars.Contains(prevC)))) newText += '؜';
					}
					if (deleteMode && recognizedFlagsAdd && (char.IsWhiteSpace(c) || char.IsPunctuation(c)) && !chars.Contains(c) && c != wordChar)
					{
						deleleWordsCount++;
					}
				}
				newText = newText.TrimStart(' ');
				newText = newText.Replace("  ", " ");
				newText = newText.Replace("   ", " ");
				newText = newText.Replace(" .", ".");
				string wordsString = "";
				foreach (string _word in words)
				{
					wordsString += _word + (isForView ? '\n' : ' ');
					if (recognizedFlagsAdd)
					{
						wordsString += '؜';
					}

					if (isForView) wordsString = "<b>" + wordsString + "</b>";

				}
				if (recognizedFlagsAdd)
				{
					wordsString = wordsString.Replace(" ", " ؜");
				}
				return wordsString + newText.Replace("\"\"", "\"");
			}
			public async UniTask<bool> IsDownload(string id, string courseId = "", string recursion = "")
			{
				if (!string.IsNullOrEmpty(recursion))
				{
					if (!_downloadChecks.Contains(id))
					{
						_downloadChecks.Add(id);
					}
					else
					{
						return true;
					}
				}
				bool isDownload = false;
				if (_eventRepository.IsDownload(id))
				{
					bool dateTimesEquals = false;
					if (!string.IsNullOrEmpty(courseId))
					{
						if (_dates.ContainsKey(courseId))
						{
							var course = _courses.Find(x => x.id == courseId);
							if (!string.IsNullOrEmpty(course.id))
							{
								var date = course.last_update;
								dateTimesEquals = date == _dates[courseId];
								_dates[courseId] = date;
							}
							else
							{
								dateTimesEquals = true;
							}
						}
						else
						{
							var course = _courses.Find(x => x.id == courseId);
							dateTimesEquals = true;
							if (!string.IsNullOrEmpty(course.id)) _dates.Add(courseId, course.last_update);
						}
						_dateTimeRepository.Update(_dates, "dateTimesCourses");
						if (!dateTimesEquals)
						{
							_eventRepository.LocalDelete(id);
							return false;
						}
					}
					isDownload = true;
					var events = await _eventRepository.GetAllChild(courseId, id);
					await UniTask.WhenAll(
					events.Select(async ev =>
					{
						if (isDownload && ev.mp3 != null)
						{
							foreach (var lang in _userLanguages)
							{
								if (ev.mp3.ContainsKey(lang) && !_audioClipRepository.IsDownload(lang, ev.mp3[lang].ToString()))
								{
									isDownload = false;
								}
							}
						}
						if (isDownload && ev.type == "MUSIC" && !string.IsNullOrEmpty(ev.media))
						{
							if (!_musicRepository.IsDownload(ev.media)) isDownload = false;
						}
						if (isDownload && !string.IsNullOrEmpty(ev.media) && ev.type == "IMG")
						{
							if (!_imageRepository.IsDownload(ev.media)) isDownload = false;
						}
						if (isDownload && ev.type == "VIDEO" && !string.IsNullOrEmpty(ev.media) && !(ev.options != null && ev.options.Contains("YOUTUBE")))
						{
							if (!_videoRepository.IsDownload(ev.media)) isDownload = false;
						}
						if (isDownload && (ev.type == "R" || ev.type == "L") && !string.IsNullOrEmpty(ev.media))
						{
							if (!await IsDownload(ev.media.Substring(ev.media.IndexOf('/') + 1), ev.media.Substring(0, ev.media.IndexOf('/')), id)) isDownload = false;
						}
					}));
				}
				return isDownload;
			}

			public async UniTask<IBaseResponse<bool>> Delete(string courseId, string playlistId)
			{
				try
				{
					var events = await _eventRepository.GetAllChild(courseId, playlistId);
					var statusCode = Response.StatusCode.OK;
					if (events == null || events.Count == 0)
					{
						return new BaseResponse<bool>()
						{
							Data = false,
							Description = "Not found",
							StatusCode = Response.StatusCode.OK
						};
					}
					var sortedEventsMp3 = events.Where(x => x.mp3 != null);
					var sortedEventsMusic = events.Where(x => x.type == "MUSIC" && !string.IsNullOrEmpty(x.media));
					var sortedEventVideo = events.Where(x => x.type == "VIDEO" && !(x.options != null && x.options.Contains("YOUTUBE")));
					var sortedEventsReferences = events.Where(x => (x.type == "R" || x.type == "L") && !string.IsNullOrEmpty(x.media));
					if (sortedEventsMp3.Any())
					{
						await UniTask.WhenAll(sortedEventsMp3.Select(async n =>
						{
							foreach (var x in n.mp3.Keys)
							{
								await _audioClipRepository.LocalDelete(Path.Combine(x, n.mp3[x].ToString()));
							}
						}));
					}
					if (sortedEventsMusic.Any())
					{
						await UniTask.WhenAll(sortedEventsMusic.Select(async x =>
						{
							await _musicRepository.LocalDelete(x.media);
						}));
					}
					if (sortedEventVideo.Any())
					{
						await UniTask.WhenAll(sortedEventVideo.Select(async x =>
						{
							await _videoRepository.LocalDelete(x.media);
						}));
					}
					if (sortedEventsReferences.Any())
					{
						await UniTask.WhenAll(sortedEventsReferences.Select(async x =>
						{
							await Delete(x.media.Substring(0, x.media.IndexOf('/')), x.media.Substring(x.media.IndexOf('/') + 1));
						}));
					}
					await _eventRepository.LocalDelete(playlistId);
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = statusCode
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex);
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Delete] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			private async UniTask CollectGarbage()
			{
				GC.Collect();
				await Resources.UnloadUnusedAssets();
			}
			public async UniTask<IBaseResponse<bool>> Download(string courseId, string playlistId, string recursion = "")
			{
				try
				{
					if (!string.IsNullOrEmpty(recursion))
					{
						if (recursion == playlistId)
						{
							return new BaseResponse<bool>()
							{
								Description = "Recursion",
								StatusCode = StatusCode.OK
							};
						}
						if (!_downloads.ContainsKey(recursion)) _downloads.Add(recursion, new());
						if (_downloads[recursion].Contains(playlistId))
						{
							return new BaseResponse<bool>()
							{
								Description = "Recursion",
								StatusCode = StatusCode.OK
							};
						}
						else
						{
							_downloads[recursion].Add(playlistId);
						}
						await CollectGarbage();
					}
					else
					{
						if (_downloadingTasks > 2)
						{
							return new BaseResponse<bool>()
							{
								Data = false,
								Description = "_downloadingTasks > 3",
								StatusCode = Response.StatusCode.Failed
							};
						}
						_downloadingTasks++;
					}
					var events = await _eventRepository.GetAllChild(courseId, playlistId);
					var statusCode = Response.StatusCode.OK;
					if (events == null || events.Count == 0)
					{
						if (string.IsNullOrEmpty(recursion)) _downloadingTasks--;
						Debug.LogError("events == null");
						return new BaseResponse<bool>()
						{
							Data = false,
							Description = "Not found",
							StatusCode = Response.StatusCode.NotFound
						};
					}
					var sortedEventsMp3 = events.Where(x => x.mp3 != null);
					var sortedEventsMusic = events.Where(x => x.type == "MUSIC" && !string.IsNullOrEmpty(x.media));
					var sortedEventImg = events.Where(x => (!string.IsNullOrEmpty(x.media) && x.type == "IMG"));
					var sortedEventVideo = events.Where(x => x.type == "VIDEO" && !(x.options != null && x.options.Contains("YOUTUBE")));
					var sortedEventsReferences = events.Where(x => (x.type == "R" || x.type == "L") && !string.IsNullOrEmpty(x.media));
					var sortedEventsScenes = events.Where(x => x.type == "UNITY" && !string.IsNullOrEmpty(x.media));
					if (sortedEventsMp3.Any())
					{
						foreach (var x in _userLanguages)
						{
							await UniTask.WhenAll(sortedEventsMp3.Select(async n =>
							{
								if (n.mp3.ContainsKey(x))
								{
									var clip = await _audioClipRepository.GetChild(x, n.mp3[x].ToString());
									if (clip == null)
									{
										Debug.LogError("audio == null");
										statusCode = Response.StatusCode.Failed;
									}
									return clip;
								}
								return null;
							}));
						}
					}
					await CollectGarbage();
					if (sortedEventsMusic.Any())
					{
						await UniTask.WhenAll(sortedEventsMusic.Select(async x =>
						{
							var clip = await _musicRepository.Get(x.media);
							if (clip == null)
							{
								Debug.LogError("music == null");
								statusCode = Response.StatusCode.Failed;
							}
							return clip;
						}));
					}
					await CollectGarbage();
					if (sortedEventImg.Any())
					{
						await UniTask.WhenAll(sortedEventImg.Select(async x =>
						{
							var clip = await _imageRepository.Get(x.media);
							if (clip == null)
							{
								Debug.LogError("image == null");
								statusCode = Response.StatusCode.Failed;
							}
							return clip;
						}));
					}
					await CollectGarbage();
					if (sortedEventVideo.Any())
					{
						await UniTask.WhenAll(sortedEventVideo.Select(async x =>
						{
							var url = await _videoRepository.Get(x.media);
							if (string.IsNullOrEmpty(url))
							{
								Debug.LogError("video == null");
								statusCode = Response.StatusCode.Failed;
							}
							return url;
						}));
					}
					await CollectGarbage();
					if (sortedEventsReferences.Any())
					{
						foreach (var x in sortedEventsReferences)
						{
							await Download(x.media.Substring(0, x.media.IndexOf('/')), x.media.Substring(x.media.IndexOf('/') + 1), playlistId);
						}
					}
					await CollectGarbage();
					if (sortedEventsScenes.Any())
					{
						await _mediaSceneService.Download("_download");
					}
					if (_downloads.ContainsKey(recursion)) _downloads.Remove(recursion);
					if (string.IsNullOrEmpty(recursion)) _downloadingTasks--;
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = statusCode
					};
				}
				catch (Exception ex)
				{
					if (_downloads.ContainsKey(recursion)) _downloads.Remove(recursion);
					if (string.IsNullOrEmpty(recursion)) _downloadingTasks--;
					Debug.LogError(ex);
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Download] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public async UniTask<IBaseResponse<List<InPlayerEvent>>> Open(string courseId, string playlistId, bool recursion = false)
			{
				try
				{
					if (Application.internetReachability == NetworkReachability.NotReachable && !(await IsDownload(playlistId)))
					{
						return new BaseResponse<List<InPlayerEvent>>()
						{
							Description = "Not found",
							StatusCode = Response.StatusCode.Failed
						};
					}
					if (recursion)
					{
						if (_openings.Contains(playlistId))
						{
							return new BaseResponse<List<InPlayerEvent>>()
							{
								Description = "recursion",
								StatusCode = Response.StatusCode.Failed
							};
						}
						_openings.Add(playlistId);
					}
					else
					{
						_openings = new();
						_hasAudio = false;
					}
					var playlist = (await _playlistRepository.GetAllChild(courseId)).FirstOrDefault(pl => pl.id == playlistId);
					if (playlist.learn_languages == null || !playlist.learn_languages.Contains(_languageService.GetCurrentLanguage())) 
					{
						return new BaseResponse<List<InPlayerEvent>>()
						{
							Description = "!playlist.learn_languages.Contains(_languageService.GetCurrentLanguage())",
							StatusCode = Response.StatusCode.NotFound
						};
					}
					var events = await _eventRepository.GetAllChild(courseId, playlistId);
					if (events == null || !events.Any())
					{
						return new BaseResponse<List<InPlayerEvent>>()
						{
							Description = "Not found",
							StatusCode = Response.StatusCode.NotFound
						};
					}
					var inPlayerEvents = new List<InPlayerEvent>();
					var references = new Dictionary<int, List<InPlayerEvent>>();
					await UniTask.WhenAll(events.Select(async ev =>
					{
						Dictionary<string, AudioClip> clip = new();
						Sprite img = null;
						string videoUrl = null;
						Dictionary<string, object> description = null;
						Dictionary<string, object> phrase = ev.phrase;
						if (ev.mp3 != null)
						{
							await UniTask.WhenAll(
							_userLanguages.Select(async x =>
							{
								if (ev.mp3.ContainsKey(x))
								{
									var audio = await _audioClipRepository.GetChild(x, ev.mp3[x].ToString());
									if (!_hasAudio && audio != null && x == _languageService.GetCurrentLanguage())
									{
										_hasAudio = true;
									}
									clip.Add(x, audio);
								}
							}));
						}
						if (ev.type == "MUSIC" && !string.IsNullOrEmpty(ev.media))
						{
							clip.Add("music", await _musicRepository.Get(ev.media));
						}
						if (!string.IsNullOrEmpty(ev.media) && (ev.type == "IMG" || ev.type == "L"))
						{
							var texture = await _imageRepository.Get(ev.media);
							if (texture == null) { texture = new Texture2D(1, 1); }
							img = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
						}
						if (ev.type == "VIDEO" && !string.IsNullOrEmpty(ev.media))
						{
							videoUrl = await _videoRepository.Get(ev.media);
						}
						if (ev.type == "R" && !string.IsNullOrEmpty(ev.media))
						{
							var response = await Open(ev.media.Substring(0, ev.media.IndexOf('/')), ev.media.Substring(ev.media.IndexOf('/') + 1), true);
							if (response.StatusCode == StatusCode.OK) references.Add(ev.order, response.Data);
						}
						if (ev.type == "L" && !string.IsNullOrEmpty(ev.media))
						{
							string courseid = ev.media.Substring(0, ev.media.IndexOf('/'));
							string playlistid = ev.media.Substring(ev.media.IndexOf('/') + 1);
							var playlist = (await _playlistRepository.GetAllChild(courseid)).FirstOrDefault(pl => pl.id == playlistid);
							var playlistName = playlist.name;
							phrase = playlistName.ConvertTo<Dictionary<string, object>>();
							description = (await _eventRepository.GetAllChild(courseid, playlistid)).OrderBy(ev => ev.order)
							.FirstOrDefault(ev => (ev.type == "T" || ev.type == "SS" || ev.type == "SA" || ev.type == "SB" || ev.type == "S") && ev.phrase != null).phrase;
							if (playlist.learn_languages == null || !playlist.learn_languages.Contains(_languageService.GetCurrentLanguage())) ev.media = string.Empty;
						}
						if (ev.type == "UNITY" && !string.IsNullOrEmpty(ev.media))
						{
							await _mediaSceneService.Download("_download");
						}
						inPlayerEvents.Add(new InPlayerEvent()
						{
							id = ev.id,
							phrase = phrase,
							type = ev.type,
							version = ev.version,
							voice = ev.voice,
							clip = clip,
							options = ev.options,
							placement = ev.placement,
							playlistId = playlistId,
							media = ev.media,
							img = img,
							order = ev.order,
							videoUrl = videoUrl,
							description = description,
						});
					}));
					_events = new List<InPlayerEvent>();
					inPlayerEvents = inPlayerEvents.OrderBy(x => x.order).ToList();
					var newEvents = new List<InPlayerEvent>();
					foreach (var newEv in inPlayerEvents)
					{
						if (references.ContainsKey(newEv.order))
						{
							newEvents.AddRange(references[newEv.order]);
							continue;
						}
						newEvents.Add(newEv);
					}
					_events = newEvents;
					if (_events.Count > 0)
					{
						return new BaseResponse<List<InPlayerEvent>>()
						{
							Data = newEvents,
							Description = "Done",
							StatusCode = Response.StatusCode.OK
						};
					}
					return new BaseResponse<List<InPlayerEvent>>()
					{
						Data = newEvents,
						Description = "Count = 0",
						StatusCode = Response.StatusCode.NotFound
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse<List<InPlayerEvent>>()
					{
						Description = $"[Open] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public bool HasAudio() => _hasAudio;
			public IBaseResponse<bool> DeleteAll()
			{
				try
				{
					_audioClipRepository.LocalDeleteAll();
					_eventRepository.LocalDeleteAll();
					_videoRepository.LocalDeleteAll();
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done.",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[DeleteAll] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}

			}
		}
	}
}


