﻿using Zenject;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayCountService : IInitializable, IPlayCount
		{
			private int _playedCount = 1;

			public void Initialize()
			{
				if (PlayerPrefs.HasKey("playCount"))
				{
					_playedCount = PlayerPrefs.GetInt("playCount");
					_playedCount++;
					PlayerPrefs.SetInt("playCount", _playedCount);
				}
				else
				{
					PlayerPrefs.SetInt("playCount", 1);
				};
			}

			public int GetPlayCount() => _playedCount;
			public bool IsFirstPlay() => _playedCount == 1;
		}

	}

}
