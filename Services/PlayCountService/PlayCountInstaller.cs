﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayCountInstaller : Installer<PlayCountInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<PlayCountService>().AsSingle();
			}
		}
	}

}
