﻿using ByHeartSchool.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IPlayCount
		{
			public int GetPlayCount();
			public bool IsFirstPlay();
		}
	}
}
