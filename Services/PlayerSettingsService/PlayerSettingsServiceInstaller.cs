﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayerSettingsServiceInstaller : Installer<PlayerSettingsServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<PlayerSettingsService>().AsSingle();
			}
		}
	}
}
