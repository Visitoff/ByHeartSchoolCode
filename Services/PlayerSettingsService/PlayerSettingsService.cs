﻿using ByHeartSchool.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zenject;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayerSettingsService : IPlayerSettingsService, IInitializable
		{
			private readonly IGetAllRepository<Set> _setRepository;
			private readonly UserManager _userManager;
			private List<Set> _sets = new List<Set>();
			private bool _init;
			public PlayerSettingsService(IGetAllRepository<Set> setRepository, UserManager userManager)
			{
				_userManager = userManager;
				_setRepository = setRepository;
			}
			public async void Initialize()
			{
				if (!_userManager.Success) _userManager.Initialized += async () =>  await Init(); 
				else await Init();
			}
			public async Task Init()
			{
				if (_userManager.Success)
				{
					_sets = await _setRepository.GetAll();
					_init = true;
				}
			}
			public UniTask<IBaseResponse<List<Setting>>> GetSetSettings(int index)
			{
				return GetSettings(new List<string>() { "SET" + index.ToString() });
			}
			public async UniTask<IBaseResponse<List<Setting>>> GetSettings(List<string> options)
			{
				try
				{
					if (!_init) await Init();
					var settings = new List<Setting>();
					var setOptions = new List<string>();
					var newOptions = new List<string>();
					foreach (var option in options)
					{
						var o = option.ToLower();
						if (o.Contains("set"))
						{
							setOptions.AddRange(_sets.Find(set => set.id == o).options);
						}
					}
					newOptions.AddRange(setOptions);
					newOptions.AddRange(options);
					foreach (var option in newOptions)
					{
						var setting = new Setting();
						var o = option.ToUpper();
						if (!o.Contains("=")) { continue; }
						setting.type = o.Contains('.') ? o.Split('.')[0] : "ALL";
						o = o.Contains('.') ? o.Substring(o.IndexOf(".") + 1) : o;
						string optionName = o.Split("=")[0];
						string optionEquals = o.Split("=")[1];
						switch (optionName)
						{
							case "CHARACTER":
								setting.character = optionEquals;
								break;
							case "CAMSET":
								setting.camSet = optionEquals;
								break;
							case "CAMERA":
								setting.camera = optionEquals;
								break;
							case "LIGHTSET":
								setting.lightSet = optionEquals;
								break;
							case "LIGHT":
								setting.light = optionEquals;
								break;
							case "TEXT":
								setting.text = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "AUDIOREPEAT":
								setting.audioRepeat = new(int.Parse(optionEquals));
								break;
							case "AUDIOVOICE":
								setting.audioVoice = optionEquals;
								break;
							case "TRANSLATION":
								setting.translation = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "TRANSLATIONLANG":
								setting.translationLang = optionEquals;
								break;
							case "TRANSLATIONAUDIOREPEAT":
								setting.translationAudioRepeat = new(int.Parse(optionEquals));
								break;
							case "DESCRIPTION":
								setting.description = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "DESCRIPTIONAUDIO":
								setting.descriptionAudio = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "EDIT":
								setting.edit = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "TIMECODE":
								setting.timeCode = new(int.Parse(optionEquals));
								break;
							case "TRACK":
								setting.track = optionEquals == "PLAY" ? new(true) : new(false);
								break;
							case "MICREPEAT":
								setting.micRepeat = new(int.Parse(optionEquals));
								break;
							case "AUDIOORDER":
								setting.audioOrder = optionEquals == "REVERSE" ? new(true) : new(false);
								break;
							case "SETTINGENABLE":
								setting.settingEnable = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "REPEATENABLE":
								setting.repeatEnable = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "DELAYENABLE":
								setting.delayEnable = optionEquals == "ON" ? new(true) : new(false);
								break;
							case "RECOGNITION":
								setting.recognitionHardMode = optionEquals == "STRICT" ? new(true) : new(false);
								break;
							case "DELAY":
								setting.delay = new(float.Parse(optionEquals));
								break;
							case "VOLUME":
								setting.volume = new(float.Parse(optionEquals) / 100f);
								break;
						}
						settings.Add(setting);
					}
					return new BaseResponse<List<Setting>>()
					{
						Description = "Done",
						StatusCode = Response.StatusCode.OK,
						Data = settings
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse<List<Setting>>()
					{
						Description = $"[Override] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}

			public async UniTask<IBaseResponse<List<Set>>> GetSets()
			{
				if (!_init) await UniTask.Yield();
				return new BaseResponse<List<Set>>()
				{
					StatusCode = Response.StatusCode.OK,
					Data = _sets
				};
			}
		}
	}
}
