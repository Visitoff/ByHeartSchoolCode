﻿namespace ByHeartSchool
{
	namespace Services
	{
		public class Setting
		{
			public string type = "ALL"; 
			public string character; 
			public string camSet; 
			public string camera; 
			public string lightSet; 
			public string light; 
			public ValueWrapper<bool> text; 
			public ValueWrapper<int> audioRepeat; 
			public string audioVoice; // later
			public ValueWrapper<bool> translation; 
			public string translationLang; 
			public ValueWrapper<int> translationAudioRepeat; 
			public ValueWrapper<bool> description; // later
			public ValueWrapper<bool> descriptionAudio; // later
			public ValueWrapper<bool> edit;
			public ValueWrapper<int> timeCode;
			public ValueWrapper<int> micRepeat;
			public ValueWrapper<bool> track;
			public ValueWrapper<bool> audioOrder;
			public ValueWrapper<bool> settingEnable;
			public ValueWrapper<bool> repeatEnable;
			public ValueWrapper<bool> delayEnable;
			public ValueWrapper<bool> recognitionHardMode;
			public ValueWrapper<float> delay;
			public ValueWrapper<float> volume;
		}
	}
}
