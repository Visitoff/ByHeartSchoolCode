﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ByHeartSchool.Repositories;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IPlayerSettingsService
		{
			UniTask<IBaseResponse<List<Set>>> GetSets();
			UniTask<IBaseResponse<List<Setting>>> GetSettings(List<string> options);
			UniTask<IBaseResponse<List<Setting>>> GetSetSettings(int index);
		}
	}
}
