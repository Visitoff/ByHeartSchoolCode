﻿using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using ByHeartSchool.Response;
using System;
using Unity.VisualScripting;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace ByHeartSchool
{
	namespace Services
	{
		public class MediaSceneService : IMediaSceneService
		{
			private SceneInstance _scene;
			private IMediaScene _container;
			private AssetReference _assetReference;
			private bool _disposed = true;
			private readonly List<IDisposable> _disposables = new();
			private string _sceneName;
			private bool _initializing;
			private object _currentOptions;
			private const string _resourcesPath = "Info/MediaScenes";
			public async UniTask<IBaseResponse<bool>> Disable()
			{
				try
				{
					_sceneName = string.Empty;
					_currentOptions = null;
					if (_scene.Scene != null)
					{
						await _assetReference.UnLoadScene();
						await Resources.UnloadUnusedAssets();
						GC.Collect();
						_disposed = true;
					}
					else
					{
						_disposed = false;
					}
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Disable] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public async UniTask<IBaseResponse<bool>> Download(string name)
			{
				try
				{
					var info = Resources.Load<AssetReferencesInfo>(_resourcesPath);
					if (info != null)
					{
						AssetReference assetReference = null;
						if (info.AssetReferences.Any())
						{
							assetReference = info.AssetReferences[info.AssetReferences.Keys.FirstOrDefault(x => x == name)];
							await assetReference.LoadSceneAsync(LoadSceneMode.Additive);
							await assetReference.UnLoadScene();
						}
					}
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Download] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public async UniTask<IBaseResponse<bool>> Init(RenderTexture texture, string name, List<string> options)
			{
				try
				{
					while (_initializing)
					{
						await Task.Yield();
					}
					_initializing = true;
					if (_sceneName == name)
					{
						if (!_currentOptions?.Equals(options) ?? options != null)
						{
							_currentOptions = options;
							_container.UpdateScene(options, texture);
						}
						_initializing = false;
						return new BaseResponse<bool>()
						{
							Data = false,
							Description = "Done",
							StatusCode = Response.StatusCode.OK
						};
					}
					else
					{
						Disable();
						_disposed = true;
					}
					var info = Resources.Load<AssetReferencesInfo>(_resourcesPath);
					if (info != null)
					{
						if (info.AssetReferences.TryGetValue(name.ToUpper(), out _assetReference))
						{
							_sceneName = name;
							_scene = await _assetReference.LoadSceneAsync(LoadSceneMode.Additive, true);
							_container = _scene.Scene.GetRootGameObjects().First(x => x.name == _scene.Scene.name).GetComponent<IMediaScene>();
							if (!_disposed)
							{
								await Disable();
							}
							else
							{
								_currentOptions = options;
								_container.Init(options, texture);
							}
						}
					}
					_initializing = false;
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
					_initializing = false;
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Init] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}

			public IBaseResponse<bool> Play(int order, string phrase, AudioClip clip, string type, bool changeCam)
			{
				try
				{
					if (_scene.Scene == null)
					{
						return new BaseResponse<bool>()
						{
							Data = false,
							Description = "Scene is NULL",
							StatusCode = Response.StatusCode.Failed
						};
					}
					_container.Play(new MediaSceneEvent()
					{
						order = order,
						phrase = phrase,
						clip = clip,
						type = type,
						changeCam = changeCam
					});
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					Debug.LogException(ex);
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Play] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}

			public IBaseResponse<bool> Stop()
			{
				try
				{
					if (_scene.Scene == null)
					{
						return new BaseResponse<bool>()
						{
							Data = false,
							Description = "Scene is NULL",
							StatusCode = Response.StatusCode.Failed
						};
					}
					_container.Stop();
					return new BaseResponse<bool>()
					{
						Data = true,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<bool>()
					{
						Data = false,
						Description = $"[Stop] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
		}
	}

}
