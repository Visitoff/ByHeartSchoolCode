﻿using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		/// <summary>
		/// Тебе нужно реализовать этот интерфейс на самом GameObject сцены. Я его получаю через _scene.GetComponent(IMediaScene)
		/// </summary>
		public interface IMediaScene
		{
			/// <summary>
			/// Ссылка на текстуру, которую указывать в Camera => Output Texture. 
			/// Если камеру нужно сменить, то просто укажи эту ссылку в другой камере. (Возможно нужно почистить ссылку в предыдущей)
			/// </summary>
			/// <param name="texture"></param>
			void Init(List<string> options,RenderTexture texture);

			void UpdateScene(List<string> options, RenderTexture texture);
			/// <summary>
			/// Эвент, который нужно проиграть
			/// </summary>
			/// <param name="ev"></param>
			void Play(MediaSceneEvent ev);
			/// <summary>
			/// Возвращаем сцену в состояние начала настоящего эвента
			/// </summary>
			void Stop();

		}
//		public class SceneController : MonoBehaviour, IMediaScene
//		{
//          Texture texture;
//			public void Play(MediaSceneEvent ev)
//			{
//				
//			}
//			public void SetCameraTexture(Texture texture)
//			{
//				this.texture = texture;
//				И потом прокидываешь это в камеры
//			}
//
//			public void Stop()
//			{
//				
//			}
//		}
	}

}
