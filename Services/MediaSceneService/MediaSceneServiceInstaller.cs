﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class MediaSceneServiceInstaller : Installer<MediaSceneServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<MediaSceneService>().AsSingle();
			}
		}
	}

}
