﻿using UnityEngine;
using System.Threading.Tasks;
using ByHeartSchool.Response;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IMediaSceneService
		{
			UniTask<IBaseResponse<bool>> Init(RenderTexture texture, string name, List<string> options);
			IBaseResponse<bool> Play(int order, string phrase, AudioClip clip, string type, bool changeCam);
			IBaseResponse<bool> Stop();
			UniTask<IBaseResponse<bool>> Disable();
			UniTask<IBaseResponse<bool>> Download(string name);
		}
	}
}
