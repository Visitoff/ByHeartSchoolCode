﻿using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Services
	{
		public struct MediaSceneEvent
		{
			public int order;
			public string phrase;
			public string type;
			public AudioClip clip;
			public bool changeCam;
		}
	}

}
