﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IBankService
		{
			UniTask<int> GetBalance();
			UniTask<IBaseResponse<Dictionary<string, object>>> BuyTransaction(string section, string productName, int price);
			UniTask<IBaseResponse<Dictionary<string, object>>> GetTransaction(string section, string productName, int count);
		}
	}
}

