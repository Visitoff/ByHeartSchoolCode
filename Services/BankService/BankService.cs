﻿using ByHeartSchool.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using ByHeartSchool.Response;
using Unity.VisualScripting;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Services
	{
		public class BankService : IBankService
		{
			private readonly UserManager _userManager;
			private readonly IBaseRepository<User> _userRepository;
			private User _user;
			private int _balance;
			private List<Dictionary<string, object>> _transactions = new List<Dictionary<string, object>>();

			public BankService(UserManager userManager, IBaseRepository<User> userRepository)
			{
				_userManager = userManager;
				_userRepository = userRepository;
			}
			public async UniTask<int> GetBalance()
			{
				await Init();
				return _balance;
			}
			private async UniTask Init()
			{
				if (_userManager.Success)
				{
					_user = await _userRepository.Get(_userManager.User.UserId);
					var prefs = (Dictionary<string, object>)_user.prefs;
					object balance;
					if (prefs.TryGetValue("user-balance", out balance))
					{
						_balance = balance.ConvertTo<int>();
					}
					else
					{
						prefs.Add("user-balance", 0);
					}
					object transactions = new List<Dictionary<string, object>>();
					if (prefs.TryGetValue("transactions", out transactions))
					{
						_transactions = (List<Dictionary<string, object>>)transactions;
					}
					else
					{
						prefs.Add("transactions", new List<Dictionary<string, object>>());
					}
					_user.prefs = prefs;
					await _userRepository.Update(_user);
				}
			}
			public async UniTask<IBaseResponse<Dictionary<string, object>>> BuyTransaction(string section, string productName, int price)
			{
				return await Transaction(section, productName, price, true);
			}
			public async UniTask<IBaseResponse<Dictionary<string, object>>> GetTransaction(string section, string productName, int count)
			{
				return await Transaction(section, productName, count, false);
			}
			public async UniTask<IBaseResponse<Dictionary<string, object>>> Transaction(string section, string productName, int count, bool isBuyTransaction)
			{
				try
				{
					await Init();
					if (isBuyTransaction)
					{
						count *= -1;
					}
					var prefs = (Dictionary<string, object>)_user.prefs;
					var balance = prefs["user-balance"].ConvertTo<int>();
					if (balance + count < 0)
					{
						return new BaseResponse<Dictionary<string, object>>()
						{
							Description = "Insufficient funds.",
							StatusCode = Response.StatusCode.Failed
						};
					}
					balance += count;
					_balance = balance;
					var transaction = new Dictionary<string, object>
					{
						{"date", DateTime.Now},
						{"section", section},
						{"name", productName},
						{"count", count},
						{"balance", balance},
					};
					_transactions.Add(transaction);
					prefs["user-balance"] = balance;
					prefs["transactions"] = _transactions;
					_user.prefs = prefs;
					await _userRepository.Update(_user);
					return new BaseResponse<Dictionary<string, object>>()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK,
						Data = transaction
					};
				}
				catch (Exception ex)
				{
					return new BaseResponse<Dictionary<string, object>>()
					{
						Description = $"[Transaction] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
		}
	}
}

