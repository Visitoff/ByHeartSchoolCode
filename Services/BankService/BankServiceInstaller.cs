﻿using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class BankServiceInstaller : Installer<BankServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<BankService>().AsSingle();
			}
		}
	}
}
