﻿using UnityEngine.UIElements;
using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{




		public class PlayerServiceInstaller : Installer<PlayerServiceInstaller>
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<PlayerService>().AsSingle();
			}
		}
	}
}