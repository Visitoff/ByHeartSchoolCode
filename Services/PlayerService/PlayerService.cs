﻿using ByHeartSchool.Menu;
using ByHeartSchool.PlayerReader;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace ByHeartSchool
{
	namespace Services
	{
		public class PlayerService : IPlayerService
		{
			private readonly SignalBus _signalBus;
			private readonly IPlayerSettingsService _settingsService;

			private List<ByHeartSchool.Menu.Item> _items;
			private List<InPlayerEvent> _events;
			private List<ByHeartSchool.Menu.ItemView> _views;
			private float _currentPause = 0;
			private readonly List<string> _ssTypes = new List<string>() { "SS", "SA", "S", "SB" };
			private int _currentRepeats = 0;
			private bool _cycleRepeatModeEnabled;
			private Dictionary<int, List<Setting>> _settings = new();
			private List<Setting> _currentSettings = new();
			private Dictionary<int, int> _phraseIndexes = new Dictionary<int, int>();
			public UnityAction<List<bool?>> ChangeEnableStateOfPlayerButtons { get; set; }
			public UnityAction<List<bool?>, List<bool?>, List<int?>, List<string>> ChangeStateOfPlayerButtons { get; set; }
			private List<string> _languages = new();
			private bool _isAudioReversed;
			public PlayerService(
				SignalBus signalBus,
				IPlayerSettingsService settingsService)
			{
				_signalBus = signalBus;
				_settingsService = settingsService;
			}
			public int GetTruePhraseIndex(int index)
			{
				if (_phraseIndexes.ContainsKey(index)) return _phraseIndexes[index];
				return index;
			}
			public async void Clear()
			{
				_events = new();
				_views = new();
				_items = new();
				_settings = new()
				{
					{ -1, (await _settingsService.GetSetSettings(0)).Data }
				};
				_cycleRepeatModeEnabled = false;
				_currentPause = 0;
				_currentRepeats = 0;
				_isAudioReversed = false;
			}
			public void Init(List<ByHeartSchool.Menu.Item> items, List<InPlayerEvent> events, List<ByHeartSchool.Menu.ItemView> views, List<string> languages)
			{
				_items = items;
				_events = events;
				_views = views;
				_phraseIndexes = new();
				_languages = languages;
			}
			public IBaseResponse<float> SetDelay()
			{
				try
				{
					_currentPause++;
					if (_currentPause > 4)
					{
						_currentPause = 0;
					}
					for (int i = 0; i < _items.Count; i++)
					{
						var item = _items[i];
						var ev = _events[i];
						if (!_ssTypes.Contains(ev.type) && !item.pauseOverrided)
						{
							item.pause = _currentPause;
						}
					}
					if (_currentPause == 0)
					{
						SetLastPlaylistSettings();
					}
					return new BaseResponse<float>()
					{
						Data = _currentPause,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse<float>()
					{
						Description = $"[SetDelay] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public void SetActiveRepeat(int index, int repeats, ERepeatIndicatorType type, bool anim)
			{
				int repeat;
				if (type != ERepeatIndicatorType.SS)
				{
					repeat = _items[index].repeat;
				}
				else
				{
					repeat = _items[index].translation_repeat;
				}
				_views[index].repeatIndicator?.SetActive(true, repeats > 0 ? repeats - 1 : repeat - 1, type, anim);
				_views[index].repeatIndicator?.SetLine(type == ERepeatIndicatorType.MIC ? _items[index].micRepeat - 1 : _items[index].repeat - 1, type);
				_views[index].indicatorImage?.DOFade(0, 0.25f);
			}
			public void SetDelayRepeat(int index, float time)
			{
				_views[index].repeatIndicator?.SetActive(false);
				_views[index].repeatIndicator?.SetDelay(time);
			}
			public void SetDeactiveRepeat(int index)
			{
				_views[index].repeatIndicator?.SetActive(false);
				ERepeatIndicatorType type = ERepeatIndicatorType.S;
				int repeats = _items[index].repeat;
				if (repeats == 0 && _items[index].micRepeat > 0)
				{
					repeats = _items[index].micRepeat;
					type = ERepeatIndicatorType.MIC;
				}
				_views[index].repeatIndicator?.SetLine(repeats - 1, type);
				_views[index].indicatorImage?.DOFade(1, 0.25f);
			}
			public IBaseResponse<int> SetRepeat(int repeats)
			{
				try
				{
					_currentRepeats = repeats;
					if (_currentRepeats > 4)
					{
						_cycleRepeatModeEnabled = true;
					}
					if (_currentRepeats > 5)
					{
						_cycleRepeatModeEnabled = false;
						_currentRepeats = 0;
					}

					for (int i = 0; i < _items.Count; i++)
					{
						var item = _items[i];
						var ev = _events[i];
						var view = _views[i];
						if (!_ssTypes.Contains(ev.type))
						{
							item.repeat = item.repeat != 0 ? _currentRepeats : 0;
							item.translation_repeat = item.translation_repeat != 0 ? _currentRepeats == 0 ? 1 : _currentRepeats : 0;
							item.micRepeat = item.micRepeat != 0 ? _currentRepeats : 0;
							var type = ERepeatIndicatorType.S;
							var lines = item.repeat == 0 ? item.repeat - 1 : _currentRepeats - 1;
							if (item.repeat == 0 && item.micRepeat > 0)
							{
								type = ERepeatIndicatorType.MIC;
								lines = item.micRepeat == 0 ? item.micRepeat - 1 : _currentRepeats - 1;
							}
							view.repeatIndicator?.SetLine(lines, type);
						}
					}
					return new BaseResponse<int>()
					{
						Data = _currentRepeats,
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse<int>()
					{
						Description = $"[SetRepeat] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public IBaseResponse<int> SetRepeat()
			{
				if (_cycleRepeatModeEnabled)
				{
					_currentRepeats = -1;
				}
				_cycleRepeatModeEnabled = false;
				return SetRepeat(++_currentRepeats);
			}
			public IBaseResponse<int> RepeatCycle()
			{
				if (!_cycleRepeatModeEnabled)
				{
					return new BaseResponse<int>()
					{
						Description = "!_cycleRepeatModeEnabled",
						StatusCode = Response.StatusCode.Failed
					};
				}
				return SetRepeat(_currentRepeats > 1 ? --_currentRepeats : 1);
			}
			public async UniTask<IBaseResponse<List<Setting>>> GetSetting(int index, List<string> options)
			{
				try
				{
					var response = await _settingsService.GetSettings(options);
					if (response.StatusCode == StatusCode.OK)
					{
						if (_settings.ContainsKey(index))
						{
							_settings[index] = response.Data;
						}
						else
						{
							_settings.Add(index, response.Data);
						}
						return new BaseResponse<List<Setting>>()
						{
							Data = response.Data,
							Description = "Done.",
							StatusCode = Response.StatusCode.OK
						};
					}
					else
					{
						return new BaseResponse<List<Setting>>()
						{
							Data = response.Data,
							Description = "Not found",
							StatusCode = Response.StatusCode.NotFound
						};
					}
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse<List<Setting>>()
					{
						Description = $"[GetSetting] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public IBaseResponse SetDefaultPlaylistSettings()
			{
				try
				{
					if (_settings.Any())
					{
						for (int k = 0; k < _settings.Keys.Count; k++)
						{
							int key = _settings.Keys.ToList()[k];
							int nextKey = _items.Count;
							if (k + 1 != _settings.Keys.Count)
							{
								nextKey = _settings.Keys.ToList()[k + 1];
							}
							SetSettings(_settings[key], key, nextKey);
						}

					}
					_currentSettings = new();
					return new BaseResponse()
					{
						Description = "Done",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse()
					{
						Description = $"[SetDefaultPlaylistSettings] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}
			}
			public IBaseResponse SetSettings(List<Setting> settings, int startInd, int endInd)
			{
				try
				{
					bool audioRepeatSetted = false;
					bool delayOverrided = false;
					bool audioOrderOverrided = false;
					if (startInd < 0) startInd = 0;
					foreach (var setting in settings)
					{
						if (setting.edit != null)
						{
							for (int i = startInd; i < endInd; i++)
							{
								var ev = _events[i];
								var view = _views[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									view.edit = setting.edit.Value;
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "Edit=" + (setting.edit.Value ? "On" : "Off") + "<color=red> start=" + startInd + "end=" + endInd + "</color>");
						}
						if (setting.translationLang != null)
						{
							for (int i = startInd; i < endInd; i++)
							{
								var ev = _events[i];
								var item = _items[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.translationLang = setting.translationLang;
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "TranslationLang=" + (setting.translationLang) + "<color=red> start=" + startInd + "end=" + endInd + "</color>");

						}
						if (setting.audioRepeat != null)
						{
							var views = _views;
							audioRepeatSetted = true;
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								var view = views[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.repeat = setting.audioRepeat.Value;
									if (item.repeat != 0) view.repeatIndicator?.SetLine(item.repeat - 1, ERepeatIndicatorType.S);
									else view.repeatIndicator?.SetLine(item.micRepeat - 1, ERepeatIndicatorType.MIC);
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "AudioRepeat=" + (setting.audioRepeat.Value) + "<color=red> start=" + startInd + "end=" + endInd + "</color>");

						}
						else if (!audioRepeatSetted)
						{
							var views = _views;
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								var view = views[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.repeat = 0;
									if (item.micRepeat == 0) view.repeatIndicator?.SetLine(-1, ERepeatIndicatorType.S);
									else view.repeatIndicator?.SetLine(item.micRepeat - 1, ERepeatIndicatorType.MIC);
								}
							}
						}
						if (setting.text != null)
						{
							var views = _views;
							for (int i = startInd; i < endInd; i++)
							{
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									views[i].fade = setting.text.Value ? 1 : 0;
									if (views[i].text != null) views[i].text.Alpha = setting.text.Value ? "FF" : "00";
									if (views[i].name != null) views[i].name.DOFade(views[i].fade, 0);

								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "Text=" + (setting.text.Value ? "On" : "Off") + "<color=red> start=" + startInd + "end=" + endInd + "</color>");

						}
						if (setting.translationAudioRepeat != null)
						{
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.translation_repeat = setting.translationAudioRepeat.Value;
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "TranslationAudioRepeat=" + (setting.translationAudioRepeat.Value) + "<color=red> start=" + startInd + "end=" + endInd + "</color>");

						}
						if (setting.translation != null)
						{
							var views = _views;
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.translation = setting.translation.Value;
									if (views[i].description != null)
									{
										item.translation = false;
										views[i].translation = setting.translation.Value;
										views[i].description.gameObject.SetActive(setting.translation.Value);
									}
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "Translation=" + (setting.translation.Value ? "On" : "Off") + "<color=red> start=" + startInd + "end=" + endInd + "</color>");

						}
						if (setting.micRepeat != null)
						{
							var views = _views;
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								var view = views[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.micRepeat = setting.micRepeat.Value;
									if (item.repeat == 0)
										view.repeatIndicator?.SetLine(item.micRepeat - 1, ERepeatIndicatorType.MIC);
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "MicRepeat=" + setting.micRepeat.Value + "<color=red> start=" + startInd + "end=" + endInd + "</color>");
						}
						if (setting.audioOrder != null)
						{
							audioOrderOverrided = true;
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.audioOrderReverse = setting.audioOrder.Value;
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "AudioOrder=" + (setting.audioOrder.Value ? "Reverse" : "Direct") + "<color=red> start=" + startInd + "end=" + endInd + "</color>");
						}
						if (setting.recognitionHardMode != null)
						{
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.recognitionHardMode = setting.recognitionHardMode.Value;
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "Recognition=" + (setting.recognitionHardMode.Value ? "Strict" : "Lax") + "<color=red> start=" + startInd + "end=" + endInd + "</color>");
						}
						if (setting.delay != null)
						{
							delayOverrided = true;
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (item.pauseOverrided) continue;
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.pause = setting.delay.Value;
									if (item.pause == 0 && item.savePause > 0)
									{
										item.pause = item.savePause;
									}
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "Delay=" + setting.delay.Value + "<color=red> start=" + startInd + "end=" + endInd + "</color>");
						}
						else
						{
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									if (item.pause == 0 && item.savePause > 0)
									{
										item.pause = item.savePause;

									}
								}
							}
						}
						if (setting.volume != null)
						{
							for (int i = startInd; i < endInd; i++)
							{
								var item = _items[i];
								var ev = _events[i];
								if (setting.type == "ALL" || setting.type == ev.type)
								{
									item.volume = setting.volume.Value;
								}
							}
							Debug.Log("<color=yellow>SETTING: </color>" + setting.type + "." + "Volume=" + setting.volume.Value + "<color=red> start=" + startInd + "end=" + endInd + "</color>");
						}
						if (setting.delayEnable != null)
						{
							ChangeEnableStateOfPlayerButtons(new() { null, setting.delayEnable.Value, null });
						}
						if (setting.repeatEnable != null)
						{
							ChangeEnableStateOfPlayerButtons(new() { null, null, setting.repeatEnable.Value });
						}
						if (setting.settingEnable != null)
						{
							_signalBus.Fire(new MenuSignal(ESignalType.SetListAdapterEnable, setting.settingEnable.Value));
						}
					}
					if (delayOverrided)
					{
						ChangeStateOfPlayerButtons(null, null, new() { null, 0 }, new() { null, "" });
					}
					if (audioOrderOverrided)
					{
						_isAudioReversed = false;
						ChangeStateOfPlayerButtons(null, null, new() { null, null, null, null, 0 }, null);
					}
					_currentSettings = settings;
					_currentRepeats = 0;
					return new BaseResponse()
					{
						Description = "Done.",
						StatusCode = Response.StatusCode.OK
					};
				}
				catch (Exception ex)
				{
					Debug.LogError(ex.Message);
					return new BaseResponse()
					{
						Description = $"[SetSettings] : {ex.Message}",
						StatusCode = Response.StatusCode.Failed
					};
				}

			}

			public UniTask<IBaseResponse<List<Setting>>> GetSetSettings(int index) => _settingsService.GetSetSettings(index);

			public void ToPortrait()
			{
				UnityEngine.Screen.autorotateToPortraitUpsideDown = false;
				UnityEngine.Screen.autorotateToPortrait = true;
				UnityEngine.Screen.autorotateToLandscapeLeft = false;
				UnityEngine.Screen.autorotateToLandscapeRight = false;
				UnityEngine.Screen.orientation = ScreenOrientation.Portrait;
			}
			public void ToLandscape()
			{
				UnityEngine.Screen.autorotateToPortraitUpsideDown = false;
				UnityEngine.Screen.autorotateToPortrait = true;
				UnityEngine.Screen.autorotateToLandscapeLeft = true;
				UnityEngine.Screen.autorotateToLandscapeRight = true;
				UnityEngine.Screen.orientation = ScreenOrientation.AutoRotation;
			}

			public void SetLastPlaylistSettings()
			{
				if (_currentSettings.Any()) SetSettings(_currentSettings, 0, _items.Count);
				else SetDefaultPlaylistSettings();
			}

			public void SetLastPlaylistRepeats()
			{
				if (_currentRepeats != 0) SetRepeat(_currentRepeats);
			}

			public float GetAudioTime(int startInd, int endInd)
			{
				float time = 0;
				for (int i = startInd; i < endInd; i++)
				{
					var item = _items[i];
					var ev = _events[i];
					var current = _languages[0];
					var translate = _languages[1];
					if (ev.clip != null && ev.clip.ContainsKey(current) && item.repeat > 0 && ev.clip[current] != null)
					{
						time += ev.clip[current].length * item.repeat;
						time += item.pause * item.repeat;
					}
					if (ev.clip != null && ev.clip.ContainsKey(translate) && item.translation_repeat > 0 && ev.clip[translate] != null)
					{
						time += ev.clip[translate].length * item.translation_repeat;
						time += item.pause * item.translation_repeat;
					}
				}
				return time;
			}

			public IBaseResponse<bool> SetAudioReverse()
			{
				if (_items == null) 
				{
					return new BaseResponse<bool>()
					{
						StatusCode = Response.StatusCode.NotFound
					};
				}
				_isAudioReversed = !_isAudioReversed;
				foreach (var item in _items)
				{
					item.audioOrderReverse = _isAudioReversed;
				}
				return new BaseResponse<bool>()
				{
					Data = _isAudioReversed,
					Description = "Done.",
					StatusCode = Response.StatusCode.OK
				};
			}
		}
	}
}