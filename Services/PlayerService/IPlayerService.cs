﻿using ByHeartSchool.PlayerReader;
using ByHeartSchool.Response;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Services
	{
		public interface IPlayerService
		{
			void Init(List<ByHeartSchool.Menu.Item> items, List<InPlayerEvent> events, List<ByHeartSchool.Menu.ItemView> views, List<string> languages);
			IBaseResponse<float> SetDelay();
			IBaseResponse<int> SetRepeat();
			IBaseResponse<int> SetRepeat(int repeats);
			IBaseResponse SetDefaultPlaylistSettings();
			UniTask<IBaseResponse<List<Setting>>> GetSetting(int index, List<string> options);
			UniTask<IBaseResponse<List<Setting>>> GetSetSettings(int index);
			IBaseResponse SetSettings(List<Setting> settings, int startInd, int endInd);
			void ToPortrait();
			void ToLandscape();
			UnityAction<List<bool?>> ChangeEnableStateOfPlayerButtons { get; set; }
			UnityAction<List<bool?>, List<bool?>, List<int?>, List<string>> ChangeStateOfPlayerButtons { get; set; }
			void Clear();
			IBaseResponse<int> RepeatCycle();
			IBaseResponse<bool> SetAudioReverse();
			void SetLastPlaylistSettings();
			void SetLastPlaylistRepeats();
			void SetActiveRepeat(int index, int repeats, ERepeatIndicatorType type, bool anim);
			void SetDeactiveRepeat(int index);
			void SetDelayRepeat(int index, float time);
			float GetAudioTime(int startInd, int endInd);
		}
	}
}