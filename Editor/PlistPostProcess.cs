#if UNITY_IOS || UNITY_TVOS
#define UNITY_XCODE_EXTENSIONS_AVAILABLE
#endif
using UnityEditor.Callbacks;
#if UNITY_XCODE_EXTENSIONS_AVAILABLE
using UnityEditor.iOS.Xcode;
#endif
using UnityEditor;
using System.IO;

public static class PlistPostProcess
{
	private const int CallOrder = 2;

#if UNITY_IOS

	[PostProcessBuild(CallOrder)]
	public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
	{
		// Read plist
		var plistPath = Path.Combine(path, "Info.plist");
		var plist = new PlistDocument();
		plist.ReadFromFile(plistPath);

		// Update value
		PlistElementDict rootDict = plist.root;
		rootDict.SetString("ITSAppUsesNonExemptEncryption", "NO");
		rootDict.SetString("BGTaskSchedulerPermittedIdentifiers", "NO");
		var array = rootDict.CreateArray("BGTaskSchedulerPermittedIdentifiers");
		array.AddString("$(PRODUCT_BUNDLE_IDENTIFIER)");
		// Write plist
		File.WriteAllText(plistPath, plist.WriteToString());
	}
#endif
}
