[System.Serializable]
public class AnimationState
{
    public int index;
    public string name = "default";

    public int[] idleIndexes;
    public int[] speakIndexes;
}
