using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationTestDouble : MonoBehaviour
{

    [SerializeField] private Button button1;
    [SerializeField] private Button button2;

    [SerializeField] private AudioSource audioSource1;
    [SerializeField] private AudioSource audioSource2;

    // Start is called before the first frame update
    void Start()
    {
        button1.onClick.AddListener(Play1);
        button2.onClick.AddListener(Play2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Play1() {
        audioSource1.Play();
    }

    private void Play2() {
        audioSource2.Play();
    }
}
