using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationManager : MonoBehaviour
{

    [SerializeField] private Button buttonSpeak;
    [SerializeField] private Button buttonYes;
    [SerializeField] private Button buttonNo;
    [SerializeField] private Button buttonBye;

    [SerializeField] private AnimationStatePlayer animationStatePlayer;
    [SerializeField] private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        buttonSpeak.onClick.AddListener(Speak);
        buttonYes.onClick.AddListener(Yes);
        buttonNo.onClick.AddListener(No);
        buttonBye.onClick.AddListener(Bye);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Speak() {
        audioSource.Play();
    }

    private void Yes() {
        animationStatePlayer.PlayEmoji(1);
    }

    private void No() {
        animationStatePlayer.PlayEmoji(3);
    }

    private void Bye() {
        animationStatePlayer.PlayEmoji(2);
    }

}
