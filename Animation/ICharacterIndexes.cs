using System.Collections.Generic;

namespace Code.Animation
{
    public interface ICharacterIndexes
    {
        public List<string> characterIndexes { get; set; }
    }
}