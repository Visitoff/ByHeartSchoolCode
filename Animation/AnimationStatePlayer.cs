using System.Collections.Generic;
using Code.Animation;
using UnityEngine;
using UnityEngine.UI;

public class AnimationStatePlayer : MonoBehaviour, ICharacterIndexes
{

    [Header("Linked Components")]
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;

    [Header("Linked UI")]
    [SerializeField] private Text textAnimationIndex;
    [SerializeField] private Text textModeName;

    [Header("Configuration")]
    [SerializeField] private AnimationState[] IdleStates;
    [SerializeField] private AnimationState[] SpeakStates;

    [Header("Debug Info")]
    [SerializeField] private bool bShowDebugInfo;
    [SerializeField] private bool bRepeatAnimations;

    // private stuff
    private AnimationState currentState;

    private bool bSpeakModeEnabled;
    private bool bIdleModeEnabled;
    private bool bEmojiPlaying;
    private bool bSpeakModeWasEnabled;
    private bool bBlinked;

    public int currentstateINDEX;
    public List<string> characterIndexes { get; set; } = new();

    private int emojiIndex;

    // Start is called before the first frame update
    void Start()
    {
        currentState = SelectRandomState(IdleStates);

        bIdleModeEnabled = true;
        bSpeakModeEnabled = false;
        bEmojiPlaying = false;
        bSpeakModeWasEnabled = false;
        bBlinked = false;

        Invoke("Blink", Random.Range(3,5));

        emojiIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
       // currentState.index = currentstateINDEX;
        Animate();
        UpdateBlinking();
    /*    if (bShowDebugInfo)
            ShowDebugInfo();*/
    }

    private AnimationState SelectRandomState(AnimationState[] animationStates) {
        return animationStates[Random.Range(0,animationStates.Length)];
    }

    private AnimationState GetStateByIndex(int index) {
        foreach (AnimationState animationState in IdleStates) {
            if (animationState.index == index) {
                return animationState;
            }
        }

        foreach (AnimationState animationState in SpeakStates) {
            if (animationState.index == index) {
                return animationState;
            }
        }

        return null;
    }

    private int GetRandomInt(int[] array) {
        return array[Random.Range(0,array.Length)];
    }

    public void ChangeCurrentAnimation() {
        bEmojiPlaying = false;

        int prevIndex = currentState.index;

        if (audioSource.isPlaying && !bSpeakModeWasEnabled) {
            int stateIndex = GetRandomInt(currentState.speakIndexes);
            bSpeakModeWasEnabled = true;

            if (!bRepeatAnimations) {
                while (stateIndex == prevIndex) {
                    stateIndex = GetRandomInt(currentState.speakIndexes);
                }
            }
            currentState = GetStateByIndex(stateIndex);
        } else {
            int stateIndex = GetRandomInt(currentState.idleIndexes);

            if (!bRepeatAnimations) {
                while (stateIndex == prevIndex) {
                    stateIndex = GetRandomInt(currentState.idleIndexes);
                }
            }
            currentState = GetStateByIndex(stateIndex);
        }
    }

    public void StopSpeakAnimation() {

    }

    private void Animate() {
        animator.SetInteger("currentIndex", currentState.index);
        animator.SetInteger("emojiIndex", emojiIndex);

        if (audioSource.isPlaying && !bSpeakModeEnabled && bIdleModeEnabled && !bSpeakModeWasEnabled) {
            bSpeakModeEnabled = true;
            bIdleModeEnabled = false;

            ChangeCurrentAnimation();
        }
        if (!audioSource.isPlaying && !bIdleModeEnabled && bSpeakModeEnabled) {
            bSpeakModeEnabled = false;
            bIdleModeEnabled = true;

            ChangeCurrentAnimation();
        }

        if (!audioSource.isPlaying) {
            bSpeakModeWasEnabled = false;
        }
    }

    private void ShowDebugInfo() {
        textAnimationIndex.text = "Anim: " + currentState.index.ToString();
        if (bEmojiPlaying) {
            textModeName.text = "EMOJI";
        } else {
            if (audioSource.isPlaying)
                textModeName.text = "SPEAKING";
            else
                textModeName.text = "IDLE";
        }
    }

    public void PlayEmoji(int emojiIndex) {
        if (bEmojiPlaying)
            return;

        this.emojiIndex = emojiIndex;
        Debug.Log("Emoji: " + emojiIndex.ToString());
    }

    public void StopEmoji() {
        bEmojiPlaying = false;
        emojiIndex = 0;
        Debug.Log("Emoji stopped");
    }

    public void StopBlinking() {
        bBlinked = false;
        Invoke("Blink", Random.Range(3,5));
    }

    private void Blink() {
        bBlinked = true;
    }

    private void UpdateBlinking() {
        float blinkValue = skinnedMeshRenderer.GetBlendShapeWeight(17);
      //  Debug.Log(blinkValue);
        if (bBlinked) {
            blinkValue += Time.deltaTime * 270;
        } else {
            blinkValue -= Time.deltaTime * 270;
        }

        blinkValue = Mathf.Clamp(blinkValue, 0, 100);
        if (blinkValue >= 99) {
            StopBlinking();
        }

        skinnedMeshRenderer.SetBlendShapeWeight(17, blinkValue);
        skinnedMeshRenderer.SetBlendShapeWeight(18, blinkValue);
    }
}
