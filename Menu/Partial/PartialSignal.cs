﻿using System.Drawing;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class PartialSignal
		{
			public ESignalType signalType;
			public string message;

			public PartialSignal(ESignalType signalType, string message)
			{
				this.signalType = signalType;
				this.message = message;
			}
			public PartialSignal()
			{
				signalType = ESignalType.Close;
			}
		}
	}

}
