﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class PartialView : UiView
		{
			[SerializeField] public Text message;
			[SerializeField] public RectTransform header;
			[SerializeField] public RectTransform dHeaderPos;
			[SerializeField] public RectTransform sHeaderPos;
		}
	}

}
