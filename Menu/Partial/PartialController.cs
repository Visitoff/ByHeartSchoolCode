﻿using DG.Tweening;
using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class PartialController : UiController<PartialView>
		{
			private readonly PartialView _view;
			private readonly SignalBus _signalBus;
			private Tween _tween;
			public PartialController(
				PartialView view,
				SignalBus signalBus)
			{
				_view = view;
				_signalBus = signalBus;
			}
			public void Signal(PartialSignal signal)
			{
				_tween.Kill();
				switch (signal.signalType)
				{
					case ESignalType.GreenSignal:
						_view.header.GetComponentInChildren<Image>().color = Color.green;
						Open(signal.message);
						_tween = DOVirtual.DelayedCall(3f, () => Close());
						break;
					case ESignalType.RedSignal:
						_view.header.GetComponentInChildren<Image>().color = Color.red;
						Open(signal.message);
						_tween = DOVirtual.DelayedCall(3f, () => Close());
						break;
					case ESignalType.Close:
						Close();
						break;
				}
			}
			private void Open(string message)
			{
				_view.message.text = message;
				_view.header.DOAnchorPos(_view.dHeaderPos.anchoredPosition, 0.5f);
				_view.gameObject.SetActive(true);
			}
			private void Close()
			{
				_view.header.DOAnchorPos(_view.sHeaderPos.anchoredPosition, 0.5f).OnComplete(() => _view.gameObject.SetActive(false));
			}
		}
	}

}
