﻿using SimpleUi;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class PartialInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<PartialController, PartialView>(GetComponent<PartialView>(), transform.parent);
				Container.DeclareSignal<PartialSignal>();
				Container.BindSignal<PartialSignal>().ToMethod<PartialController>(x => x.Signal).FromResolve();
			}
		}
	}

}
