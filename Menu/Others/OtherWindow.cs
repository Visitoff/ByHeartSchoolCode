﻿using SimpleUi;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class OtherWindow : WindowBase
		{
			public override string Name => "Use";
			protected override void AddControllers()
			{
				AddController<OtherController>();
			}
		}
	}
}
