﻿using SimpleUi;
using Zenject;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class OtherInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<OtherController, OtherView>(GetComponent<OtherView>(), transform.parent);
				Container.BindInterfacesAndSelfTo<OtherWindow>().AsSingle();
				Container.DeclareSignal<OtherUpdateSignal>().OptionalSubscriber();
				Container.BindSignal<OtherUpdateSignal>().ToMethod<OtherController>(x => x.Update).FromResolve();
			}

		}
	}
}
