﻿using SimpleUi;
using Zenject;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class GamesInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<GamesController, GamesView>(GetComponent<GamesView>(), transform.parent);
				Container.BindInterfacesAndSelfTo<GamesWindow>().AsSingle();
				Container.DeclareSignal<GamesUpdateSignal>().OptionalSubscriber();
				Container.BindSignal<GamesUpdateSignal>().ToMethod<GamesController>(x => x.Update).FromResolve();
			}
		}

	}
}
