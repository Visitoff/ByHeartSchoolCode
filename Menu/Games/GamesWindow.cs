﻿using SimpleUi;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class GamesWindow : WindowBase
		{
			public override string Name => "Play";
			protected override void AddControllers()
			{
				AddController<GamesController>();
			}
		}
	}
}
