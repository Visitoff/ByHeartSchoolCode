﻿using SimpleUi;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class LearningWindow : WindowBase
		{
			public override string Name => "Learn";
			protected override void AddControllers()
			{
				AddController<LearningController>();
			}
		}
	}
}
