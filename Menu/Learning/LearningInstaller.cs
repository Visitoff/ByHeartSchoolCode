﻿using SimpleUi;
using Zenject;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class LearningInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<LearningController, LearningView>(GetComponent<LearningView>(), transform.parent);
				Container.BindInterfacesAndSelfTo<LearningWindow>().AsSingle();
				Container.DeclareSignal<LearningUpdateSignal>().OptionalSubscriber();
				Container.BindSignal<LearningUpdateSignal>().ToMethod<LearningController>(x => x.Update).FromResolve();
			}
		}
		public abstract class UpdateSignal
		{
			public UpdateSignal() { }
		}
		public class LearningUpdateSignal: UpdateSignal { }
		public class OtherUpdateSignal : UpdateSignal { }
		public class GamesUpdateSignal : UpdateSignal { }
		public class FeedUpdateSignal : UpdateSignal { }
		public class MyUpdateSignal : UpdateSignal { }
	}
}
