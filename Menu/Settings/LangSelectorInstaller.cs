﻿using SimpleUi;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class LangSelectorInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<LangSelectorController, LangSelectorView>(GetComponent<LangSelectorView>(), transform.parent);
				Container.DeclareSignal<LangSelectorSignal>();
				Container.BindSignal<LangSelectorSignal>().ToMethod<LangSelectorController>(x => x.Signal).FromResolve();
			}
		}
	}
}
