﻿using ByHeartSchool.Services;
using SimpleUi.Abstracts;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class AppLangSelectorController : UiController<LangSelectorView>, IInitializable
		{
			private readonly AppLangSelectorView _view;
			private readonly SignalBus _signalBus;
			private readonly ILanguageService _languageService;
			private readonly IScreenService _screenService;
			private UnityAction<string> Language;
			private UnityAction save;
			private Dictionary<ScreenAction, UnityAction<string>> actions;
			public AppLangSelectorController(AppLangSelectorView view, SignalBus signalBus, ILanguageService languageService, IScreenService screenService)
			{
				_view = view;
				_signalBus = signalBus;
				_languageService = languageService;
				_screenService = screenService;
			}
			public void Signal(AppLangSelectorSignal signal)
			{
				switch (signal.eSignalType)
				{
					case ESignalType.Open:
						Open(signal.saveAction);
						break;
					case ESignalType.Close:
						Close();
						break;
				}
			}
			private async void Open(UnityAction saveAction)
			{
				save = saveAction;
				_view.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
				_view.gameObject.SetActive(true);
				var response = await _screenService.GetScreen(Configuration.LANGUAGEScreenId, actions, true);
				if (response.StatusCode == Response.StatusCode.OK)
				{
					_view.confirmBtn.onClick.RemoveAllListeners();
					_view.confirmBtn.onClick.AddListener(() => {
						_languageService.SetLanguage(_languageService.GetCurrentLanguage());
						saveAction.Invoke();
						Close();
					});
					_view.adapter.Show(response.Data);
				}
			}
			private void SetLanguage(string lang)
			{
				var current = _languageService.GetCurrentLanguage();
				if (current != lang)
				{
					_languageService.SetLanguage(lang);
					save?.Invoke();
				}
				else
				{
					_signalBus.Fire(new MenuSignal(ESignalType.ShowHeader));
					_signalBus.Fire(new MenuSignal(ESignalType.ShowLanguageBtn));
				}
				Close();
			}
			private void Close()
			{
				_view.gameObject.SetActive(false);
			}

			public void Initialize()
			{
				Language += (s) => { SetLanguage(s); };
				actions = new Dictionary<ScreenAction, UnityAction<string>>()
					{
						{ScreenAction.Language, Language},
					};
			}
		}
	}
}
