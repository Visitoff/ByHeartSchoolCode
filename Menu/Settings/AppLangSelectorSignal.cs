﻿using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class AppLangSelectorSignal
		{
			public ESignalType eSignalType;
			public UnityAction saveAction;
			public AppLangSelectorSignal(ESignalType eSignalType, UnityAction saveAction)
			{
				this.eSignalType = eSignalType;
				this.saveAction = saveAction;
			}
		}
	}
}
