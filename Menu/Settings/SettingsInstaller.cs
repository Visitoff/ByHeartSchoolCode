﻿using ByHeartSchool.PlayerReader;
using SimpleUi;
using Zenject;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class SettingsInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<SettingsController, SettingsView>(GetComponent<SettingsView>(), transform.parent);
				Container.DeclareSignal<SettingsSignal>();
				Container.BindSignal<SettingsSignal>().ToMethod<SettingsController>(x => x.Signal).FromResolve();
			}
		}
		public class SettingsSignal
		{
			public ESignalType signalType;
			public string settingId;
			public SettingsSignal(ESignalType signalType, string settingId) 
			{ 
				this.signalType = signalType;
				this.settingId = settingId;
			}
		}
	}
}
