﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class EditProfileView : UiView
		{
			[SerializeField] public InputField nickname;
			[SerializeField] public Button saveBtn;
		}
	}
}
