﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class LangSelectorView : UiView
		{
			[SerializeField] public ListAdapter adapter;
			[SerializeField] public Button confirmBtn;
		}
	}
}
