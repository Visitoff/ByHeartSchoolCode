﻿using SimpleUi;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class AppLangSelectorInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<AppLangSelectorController, AppLangSelectorView>(GetComponent<AppLangSelectorView>(), transform.parent);
				Container.DeclareSignal<AppLangSelectorSignal>();
				Container.BindSignal<AppLangSelectorSignal>().ToMethod<AppLangSelectorController>(x => x.Signal).FromResolve();
			}
		}
	}
}
