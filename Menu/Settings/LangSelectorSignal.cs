﻿using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class LangSelectorSignal
		{
			public ESignalType eSignalType;
			public UnityAction confirm;
            public LangSelectorSignal(ESignalType eSignalType, UnityAction confirm = null)
            {
                this.eSignalType = eSignalType;
				this.confirm = confirm;
            }
        }
	}
}
