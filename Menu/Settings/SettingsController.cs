﻿using ByHeartSchool.PlayerReader;
using ByHeartSchool.Repositories;
using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class SettingsController : UiController<SettingsView>, IDisposable
		{
			private readonly SettingsView _view;
			private readonly SignalBus _signalBus;
			private readonly UserManager _userManager;
			private readonly IBaseRepository<User> _userRepository;
			private readonly IDeviceButtonsService _deviceButtonsService;
			private readonly List<IDisposable> _disposables = new();
			private User _user;
			private static int choised = -1;
			private static ILanguageService _languageService;
			private readonly IPlayCount _playCount;
			private readonly IBankService _bankService;
			private readonly IPlaylistService _playlistService;
			private readonly IFontService _fontService;
			private int _balance = -1;
			private GameObject _parent;
			public SettingsController(
				SettingsView view, 
				SignalBus signalBus,
				UserManager userManager,
				IDeviceButtonsService deviceButtonsService,
				IBaseRepository<User> userRepository,
				ILanguageService languageService,
				IPlayCount playCount,
				IBankService bankService,
				IPlaylistService playlistService,
				IFontService fontService)
			{
				_signalBus = signalBus;
				_view = view;
				_userManager = userManager;
				_deviceButtonsService = deviceButtonsService;
				_userRepository = userRepository;
				_languageService = languageService;
				_playCount = playCount;
				_bankService = bankService;
				_playlistService = playlistService;
				_fontService = fontService;
			}
			public async void Show()
			{
				var parent = GameObject.Find("SettingsContainer");
				if (parent != null)
				{
					_view.transform.SetParent(parent.transform, false);
					_view.gameObject.SetActive(true);
				}
				else
				{
					var login = GameObject.Find("LoginContainer");
					if (login != null)
					{
						_view.transform.SetParent(login.transform, false);
						_view.gameObject.SetActive(true);
						_parent = login;
					}
				}
				if (_userManager.Success)
				{
					await Profile();
				}
				else
				{
					Auth();
				}

			}
			public void Signal(SettingsSignal signal)
			{
				switch (signal.signalType)
				{
					case ESignalType.Open:
						Show();
						break;
				}
			}
			private async UniTask Profile(bool loginCompleted = false)
			{
				_signalBus.Fire(new LoadingSignal(LoadingSignalType.SetImage, "profile"));	
				_signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project);
				_view.signIn.SetActive(false);
				_view.profile.SetActive(true);
				if (string.IsNullOrEmpty(_user.id))
				{
					_balance = -1;
					_user = await _userRepository.Get(_userManager.User.UserId);
					if (string.IsNullOrEmpty(_user.id))
					{
						await UniTask.Delay(1500);
						_signalBus.BackWindow(EWindowLayer.Project);
						await Profile();
						return;
					}
				}
				try
				{
					var _prefs = (Dictionary<string, object>)_user.prefs;
					if (_prefs["user-languages"] != null)
					{
						if (loginCompleted)
						{
							_signalBus.Fire(new MenuSignal(ESignalType.OpenFeed));
							choised = -1;
						}
					}
				}
				catch 
				{
					OpenLangSelector(false, true, true);
					_signalBus.BackWindow(EWindowLayer.Project);
					_signalBus.BackWindow(EWindowLayer.Project);
					_signalBus.BackWindow(EWindowLayer.Project);
					return;
				}
				_signalBus.Fire(new MenuSignal(ESignalType.CheckLang));
				switch (choised)
				{
					case 0:
						OpenLangSelector(true, false);
						break;
					case 1:
						EditProfile();
						break;
				}
				_view.editBtn.onClick.RemoveAllListeners();
				_view.editBtn.onClick.AddListener(EditProfile);
				_view.logoutBtn.onClick.RemoveAllListeners();
				_view.logoutBtn.onClick.AddListener(() =>
				{
					_signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project);
					_userManager.Logout();
					_signalBus.OpenWindow("My");
					_signalBus.Fire(new MenuSignal(ESignalType.SwipeHandlerNonInteractable));
				});
				if (string.IsNullOrEmpty(_user.nik))
					_view.displayName.text = "anonymous";
				else 
					_view.displayName.text = _user.nik;
				_view.displayNameFirstChar.text = _view.displayName.text[0].ToString().ToUpper();
				if (_balance < 0) _balance = await _bankService.GetBalance();
				_view.balance.text = "Your balance:" + _balance.ToString();
				_view.removeAllBtn.onClick.RemoveAllListeners();
				_view.removeAllBtn.onClick.AddListener(() => _signalBus.Fire(new MessageBoxSignal(EMessageBoxType.Question, "Sure to continue?", () =>
				{
					_signalBus.Fire(new LearningUpdateSignal());
					_signalBus.Fire(new GamesUpdateSignal());
					_signalBus.Fire(new OtherUpdateSignal());
					_signalBus.Fire(new FeedUpdateSignal());
					_signalBus.Fire(new MyUpdateSignal());
					_view.removeAllBtn.interactable = false;
					DOVirtual.DelayedCall(2f, () => _view.removeAllBtn.interactable = true); 
					_playlistService.DeleteAll();
					_signalBus.Fire(new PartialSignal(ESignalType.GreenSignal, "Downloads removed succesfully!"));
				})));
				var langs = new List<object>();
				var prefs = (Dictionary<string, object>)_user.prefs;
				if (prefs.ContainsKey("user-languages"))
				{
					langs = (List<object>)prefs["user-languages"];
				}
				else
				{
					prefs.Add("user-languages", new List<object>());
					langs = (List<object>)prefs["user-languages"];
					_user.prefs = prefs;
				}
				var langItems = new List<Item>();
				foreach (var item in langs)
				{
					string _item = item.ToString();
					var newString = _item[0].ToString() + _item[1].ToString();
					var type = 0;
					if (_item.Length > 2)
					{
						type = 2;
					}
					langItems.Add(new Item()
					{
						name = new Tuple<string, string>(_languageService.GetAll()[newString].Item1, ""),
						contentTypeIndex = type
					});
				}
				langItems.Add(new Item()
				{
					contentTypeIndex = 1,
					onClick = () => OpenLangSelector(true, true)
				});
				_view.langAdapter.Show(langItems);
				_deviceButtonsService.BackButtonOnClick(() =>Application.Quit());
				_view.fontSlider.onValueChanged.AddListener((f) =>
				{
					_view.font_Example.fontSize = _fontService.GetDefaultFontSize() * f;
					_fontService.SetFontSize(f);
					LayoutRebuilder.ForceRebuildLayoutImmediate(_view.A);
				});
				_view.fontSlider.value = _fontService.GetFontSizeMultiplyer();
				_signalBus.BackWindow(EWindowLayer.Project);
				_signalBus.BackWindow(EWindowLayer.Project);
				_signalBus.BackWindow(EWindowLayer.Project);
			}
			private void EditProfile()
			{
				UnityAction saveAction = async () =>
				{
					_user.id = "";
					choised = -1;
					await Profile();
					_signalBus.Fire(new EditProfileSignal(ESignalType.Close, null));
					_signalBus.Fire(new LearningUpdateSignal());
					_signalBus.Fire(new GamesUpdateSignal());
					_signalBus.Fire(new OtherUpdateSignal());
					_signalBus.Fire(new FeedUpdateSignal());
					_signalBus.Fire(new MyUpdateSignal());
				};
				_signalBus.Fire(new EditProfileSignal(ESignalType.Open, saveAction));
				_signalBus.Fire(new MenuSignal(ESignalType.HideFooter));
				choised = 1;
				UnityAction action = async () =>
				{
					choised = -1;
					_signalBus.Fire(new EditProfileSignal(ESignalType.Close, null));
				};
				_signalBus.Fire(new MenuSignal(ESignalType.ShowBackBtn, action, action));
				_deviceButtonsService.BackButtonOnClick(action);
			}
			private void OpenLangSelector(bool downSize, bool update, bool loginCompleted = false)
			{
				choised = 0;
				UnityAction action = async () =>
				{
					_signalBus.Fire(new MenuSignal(ESignalType.Lock));
					await _playlistService.Update(false, true);
					_signalBus.Fire(new PlayerSignal(ESignalType.Update));
					_signalBus.Fire(new MenuSignal(ESignalType.Unlock));
					_user.id = "";
					choised = -1;
					await Profile();
					_signalBus.Fire(new LangSelectorSignal(ESignalType.Close));
					_signalBus.Fire(new LearningUpdateSignal());
					_signalBus.Fire(new GamesUpdateSignal());
					_signalBus.Fire(new OtherUpdateSignal());
					_signalBus.Fire(new FeedUpdateSignal());
					_signalBus.Fire(new MyUpdateSignal());
					if (loginCompleted) _signalBus.Fire(new MenuSignal(ESignalType.OpenFeed));
				};
				if (downSize)
				{
					_signalBus.Fire(new LangSelectorSignal(update ? ESignalType.OpenUpdately : ESignalType.Open, action));
					_signalBus.Fire(new MenuSignal(ESignalType.ShowBackBtn, action, action));
					_signalBus.Fire(new MenuSignal(ESignalType.SwipeHandlerNonInteractable));
					_signalBus.Fire(new MenuSignal(ESignalType.HideFooter));
					_deviceButtonsService.BackButtonOnClick(action);
				}
				else
				{
					_signalBus.Fire(new LangSelectorSignal(update ? ESignalType.OpenUpdately : ESignalType.Open, action));
				}
			}
			private void Auth()
			{
#if UNITY_EDITOR
				_view.testAccountBtn.gameObject.SetActive(true);
#elif UNITY_IOS
				_view.appleBtn.gameObject.SetActive(true);
#elif UNITY_ANDROID
				_view.googleBtn.gameObject.SetActive(true);
#endif

				_userManager.LoginCompleted +=  async () =>
				{
					_signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project);
					if (_parent) _parent.SetActive(false);
					_user.id = "";
					await Profile(true);
					_signalBus.Fire(new PartialSignal(ESignalType.GreenSignal, "You are successfully logged in!"));
					Interactable(true);
				};
				_userManager.LoginFailed += (s) => {
					_signalBus.Fire(new PartialSignal(ESignalType.RedSignal, s));
					Interactable(true);
				};
				_view.signIn.SetActive(true);
				_view.profile.SetActive(false);
				_view.testAccountBtn.onClick.RemoveAllListeners();
				_view.testAccountBtn.onClick.AddListener(() =>
				{
					_userManager.Login();
					_signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project);
				});
				_view.googleBtn.onClick.RemoveAllListeners();
				_view.googleBtn.onClick.AddListener(() =>
				{
					_userManager.LoginGoogle();
				});
				_view.appleBtn.onClick.RemoveAllListeners();
				_view.appleBtn.onClick.AddListener(() =>
				{
					_userManager.LoginApple();
				});
				_deviceButtonsService.BackButtonOnClick(() => Application.Quit());
			}
			private void Interactable(bool interactable)
			{
				_view.googleBtn.interactable = interactable;
				_view.testAccountBtn.interactable = interactable;
			}
			public override void OnHide()
			{
				_signalBus.Fire(new EditProfileSignal(ESignalType.Close, null));
				_signalBus.Fire(new LangSelectorSignal(ESignalType.Close));
				_signalBus.BackWindow(EWindowLayer.Project);
			}
			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
