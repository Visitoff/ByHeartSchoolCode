﻿using SimpleUi;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class EditProfileInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<EditProfileController, EditProfileView>(GetComponent<EditProfileView>(), transform.parent);
				Container.DeclareSignal<EditProfileSignal>();
				Container.BindSignal<EditProfileSignal>().ToMethod<EditProfileController>(x => x.Signal).FromResolve();
			}
		}
	}
}
