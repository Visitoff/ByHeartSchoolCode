﻿using SimpleUi.Abstracts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class SettingsView : UiView
		{
			[SerializeField] public ListAdapter adapter;
			[SerializeField] public ListAdapter langAdapter;

			[Header("Sign-In")]
			[SerializeField] public GameObject signIn;
			[SerializeField] public Button testAccountBtn;
			[SerializeField] public Button googleBtn;
			[SerializeField] public Button appleBtn;
			[Header("Profile")]
			[SerializeField] public GameObject profile;
			[SerializeField] public Button logoutBtn;
			[SerializeField] public Button editBtn;
			[SerializeField] public Text displayName;
			[SerializeField] public Text displayNameFirstChar;
			[SerializeField] public Text balance;
			[SerializeField] public Button removeAllBtn;
			[SerializeField] public TMP_Text font_Example;
			[SerializeField] public Slider fontSlider;
			[SerializeField] public RectTransform A;
			[SerializeField] public Toggle enableRecognizeDetails;

		}
	}
}
