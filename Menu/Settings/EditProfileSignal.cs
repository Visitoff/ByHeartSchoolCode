﻿using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class EditProfileSignal
		{
			public ESignalType signalType;
			public UnityAction saveAction;
            public EditProfileSignal(ESignalType signalType, UnityAction saveAction)
            {
                this.signalType = signalType;
				this.saveAction = saveAction;
            }
        }
	}
}
