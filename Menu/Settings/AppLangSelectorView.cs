﻿using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class AppLangSelectorView : UiView
		{
			[SerializeField] public ListAdapter adapter;
			[SerializeField] public Button confirmBtn;
		}
	}
}
