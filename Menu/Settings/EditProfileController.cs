﻿using ByHeartSchool.Repositories;
using ByHeartSchool.Services;
using SimpleUi.Abstracts;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class EditProfileController : UiController<EditProfileView>
		{
			private readonly IBaseRepository<User> _userRepository;
			private readonly UserManager _userManager;
			private readonly EditProfileView _view;
			private readonly SignalBus _signalBus;
			private User _user;
			public EditProfileController(IBaseRepository<User> userRepository, UserManager userManager, EditProfileView view, SignalBus signalBus)
			{
				_userManager = userManager;
				_userRepository = userRepository;
				_view = view;
				_signalBus = signalBus;

			}
			public async void Signal(EditProfileSignal signal)
			{
				switch (signal.signalType)
				{
					case ESignalType.Open:
						await Open(signal.saveAction);
						break;
					case ESignalType.Close:
						Close();
						break;
				}
			}
			private async Task Open(UnityAction saveAction)
			{
				_view.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
				_view.gameObject.SetActive(true);
				_view.nickname.text = "";
				_view.saveBtn.interactable = false;
				_view.nickname.interactable = true;
				_view.nickname.onValueChanged.RemoveAllListeners();
				_view.saveBtn.onClick.RemoveAllListeners();
				_view.saveBtn.onClick.AddListener(async () => await SaveData(saveAction));
				_user = await _userRepository.Get(_userManager.User.UserId);
				_view.nickname.onValueChanged.AddListener((s) => _view.saveBtn.interactable = true);
			}
			private async Task SaveData(UnityAction saveAction)
			{
				if (_view.nickname.text.Length < 17 && _view.nickname.text.Length > 3)
				{
					_user.nik = _view.nickname.text;
					_view.saveBtn.interactable = false;
					_view.nickname.interactable = false;
					await _userRepository.Update(_user);
					saveAction.Invoke();
					_signalBus.Fire(new PartialSignal(ESignalType.GreenSignal, "Saved."));
				}
				else
				{
					_signalBus.Fire(new PartialSignal(ESignalType.RedSignal, "Nickname length from 3 to 16 characters!"));
				}
			}
			private void Close()
			{
				_view.gameObject.SetActive(false);
			}
		}
	}
}
