﻿using ByHeartSchool.Repositories;
using ByHeartSchool.Services;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class LangSelectorController: UiController<LangSelectorView>
		{
			private readonly IBaseRepository<User> _userRepository;
			private readonly UserManager _userManager;
			private readonly LangSelectorView _view;
			private readonly SignalBus _signalBus;
			private readonly ILanguageService _languageService;
			private User _user;
			private List<object> _userLangs;
			private UnityAction confirm;

            public LangSelectorController(IBaseRepository<User> userRepository, UserManager userManager, LangSelectorView view, SignalBus signalBus, ILanguageService languageService)
			{
				_userManager = userManager;
				_userRepository = userRepository;
				_view = view;
				_signalBus = signalBus;
				_languageService = languageService;
			}
			public async void Signal(LangSelectorSignal signal)
			{
				switch (signal.eSignalType)
				{
					case ESignalType.OpenUpdately:
						confirm = signal.confirm;
						await Open();
						break;
					case ESignalType.Close:
						Close();
						break;
					case ESignalType.Open:
						_view.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
						_view.gameObject.SetActive(true);
						break;
				}
			}
			private async Task Open()
			{
				_view.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
				_view.gameObject.SetActive(true);
				_user = await _userRepository.Get(_userManager.User.UserId);
				_userLangs = new List<object>();
				var prefs = (Dictionary<string, object>)_user.prefs;
				try
				{
					_userLangs = (List<object>)prefs["user-languages"];
				}
				catch (Exception ex)
				{
					prefs.Add("user-languages", new List<object>());
					_userLangs = (List<object>)prefs["user-languages"];
					_user.prefs = prefs;
				}
				var allLangs = _languageService.GetAll();
				var langItems = new List<Item>();
				foreach (var item in allLangs.Keys)
				{
					string _item = item;
					bool isOn2 = _userLangs.Contains(_item + ":native");
					bool isOn = _userLangs.Contains(_item) || isOn2;
					langItems.Add(new Item()
					{
						code = item,
						name = new Tuple<string, string>("<b>" + allLangs[item].Item1.Split(';')[0] + "</b>" + " <color=#808080>(" + allLangs[item].Item2 + ")</color>", null),
						contentTypeIndex = 0,
						isOn = isOn,
						isOn2 = isOn2,
						toggleOnClick = (b) => { EditLangList(b, _item); _view.confirmBtn.interactable = true; },
						childToggleOnClick = (b) => { IsNativeLang(b, _item); _view.confirmBtn.interactable = true; }
					});
				}
				_view.adapter.Show(langItems);
				_view.confirmBtn.interactable = false;
				_view.confirmBtn.onClick.RemoveAllListeners();
				_view.confirmBtn.onClick.AddListener(()=> { Close(); confirm?.Invoke(); });
			}
			private void EditLangList(bool i, string code)
			{
				if (i)
				{
					_userLangs.Add(code);
				}
				else
				{
					_userLangs.Remove(code);
					_userLangs.Remove(code + ":native");
				}
				SortAndUpdate();
			}
			private void IsNativeLang(bool i, string code)
			{
				if (i)
				{
					_userLangs.Remove(code);
					_userLangs.Add(code + ":native");
				}
				else
				{
					if (_userLangs.Contains(code + ":native"))
					{
						_userLangs.Remove(code + ":native");
						_userLangs.Add(code);
					}

				}
				SortAndUpdate();
			}
			private void SortAndUpdate()
			{
				var langs = from lang in _userLangs
							orderby lang.ToString().Substring(0, 1)
							orderby lang.ToString().Length descending
							select lang;
				var prefs = (Dictionary<string, object>)_user.prefs; 
				prefs["user-languages"] = langs.ToList();
				_userRepository.Update(_user);
			}
			private void Close()
			{
				_view.gameObject.SetActive(false);
			}
		}
	}
}
