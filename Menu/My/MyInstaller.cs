﻿using SimpleUi;
using Zenject;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class MyInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<MyController, MyView>(GetComponent<MyView>(), transform.parent);
				Container.BindInterfacesAndSelfTo<MyWindow>().AsSingle();
				Container.DeclareSignal<MyUpdateSignal>().OptionalSubscriber();
				Container.BindSignal<MyUpdateSignal>().ToMethod<MyController>(x => x.Update).FromResolve();
			}
		}
	}
}
