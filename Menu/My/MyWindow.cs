﻿using SimpleUi;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class MyWindow : WindowBase
		{
			public override string Name => "My";
			protected override void AddControllers()
			{
				AddController<MyController>();
			}
		}
	}
}
