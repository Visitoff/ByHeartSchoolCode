﻿using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MessageBoxSignal
		{
			public EMessageBoxType type;
			public string message;
			public UnityAction callback;
            public MessageBoxSignal(EMessageBoxType type, string message, UnityAction callback)
            {
                this.type = type;
				this.message = message;
				this.callback = callback;
            }
        }
	}
}


