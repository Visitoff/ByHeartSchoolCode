﻿using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using SimpleUi.Abstracts;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MessageBoxController: UiController<MessageBoxView>
		{
			private readonly MessageBoxView _view;
            public MessageBoxController(MessageBoxView view)
            {
                _view = view;
            }
			public void Signal(MessageBoxSignal signal) 
			{
				UnityAction action = () => {
					_view.text.text = signal.message;
					_view.noBtn.onClick.AddListener(Close);
					_view.backgroundBtn.onClick.AddListener(Close);
					_view.yesBtn.onClick.AddListener(() => { signal.callback?.Invoke(); Close(); });
					};
				Open(action);
			}
			private void Open(UnityAction action)
			{
				_view.gameObject.SetActive(true);
				_view.backgroundBtn.GetComponent<Image>().DOFade(0.5f, 0.5f);
				_view.window.DOFade(1, 0.5f);
				_view.yesBtnBack.DOFade(1, 0.5f);
				_view.noBtnShadow.DOFade(0.25f, 0.5f);
				_view.yesBtnShadow.DOFade(0.25f, 0.5f);
				_view.noBtnBack.DOFade(1, 0.5f);
				_view.text.DOFade(1, 0.5f);
				_view.yesBtn.GetComponentInChildren<TextMeshProUGUI>().DOFade(1, 0.5f);
				_view.noBtn.GetComponentInChildren<TextMeshProUGUI>().DOFade(1, 0.5f).OnComplete(() => action.Invoke());
			}
			private void Close()
			{
				_view.yesBtn.onClick.RemoveAllListeners();
				_view.noBtn.onClick.RemoveAllListeners();
				_view.backgroundBtn.onClick.RemoveAllListeners();
				_view.backgroundBtn.GetComponent<Image>().DOFade(0, 0.5f);
				_view.window.DOFade(0, 0.5f);
				_view.yesBtnBack.DOFade(0, 0.5f);
				_view.noBtnBack.DOFade(0, 0.5f);
				_view.noBtnShadow.DOFade(0, 0.5f);
				_view.yesBtnShadow.DOFade(0, 0.5f);
				_view.text.DOFade(0, 0.5f);
				_view.yesBtn.GetComponentInChildren<TextMeshProUGUI>().DOFade(0, 0.5f);
				_view.noBtn.GetComponentInChildren<TextMeshProUGUI>().DOFade(0, 0.5f).OnComplete(() => _view.gameObject.SetActive(false));

			}
        }
	}
}


