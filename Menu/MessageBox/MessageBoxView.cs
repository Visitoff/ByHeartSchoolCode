﻿using UnityEngine;
using Zenject;
using TMPro;
using UnityEngine.UI;
using SimpleUi.Abstracts;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MessageBoxView: UiView
		{
			public TextMeshProUGUI text;
			public Button yesBtn;
			public Image yesBtnBack;
			public Image noBtnBack;
			public Image noBtnShadow;
			public Image yesBtnShadow;
			public Button noBtn;
			public Button okBtn;
			public Button backgroundBtn;
			public Image window;
		}
	}
}


