﻿using ByHeartSchool.Menu;
using SimpleUi;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MessageBoxInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<MessageBoxController, MessageBoxView>(GetComponent<MessageBoxView>(), transform.parent);
				Container.DeclareSignal<MessageBoxSignal>();
				Container.BindSignal<MessageBoxSignal>().ToMethod<MessageBoxController>(x => x.Signal).FromResolve();
			}
		}
	}
}


