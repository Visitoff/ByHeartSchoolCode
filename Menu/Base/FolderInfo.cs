﻿using UnityEngine;

namespace ByHeartSchool
{
	namespace Menu
	{
		[CreateAssetMenu(fileName = "FolderInfo", menuName = "Info/New FolderInfo")]
		public class FolderInfo : ScriptableObject
		{
			[SerializeField] private int _id;
			[SerializeField] private string _name;
			[SerializeField] private string _description;
			[SerializeField] private Sprite _mainImage;
			public int Id => _id;
			public string Name => _name;
			public string Description => _description;
			public Sprite MainImage => _mainImage;
		}
	}
}
