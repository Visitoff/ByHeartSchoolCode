﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class SlideToggle: MonoBehaviour
		{
			[SerializeField] private Image circle;
			[SerializeField] private Image rectangle;
			public void IsOn(bool value)
			{
				if (value)
				{
					GetComponent<RectTransform>().DOAnchorPosX(30, 0.5f);
					circle.DOColor(Color.white, 0.5f);
					rectangle.DOFade(1, 0.5f);
				}
				else
				{
					GetComponent<RectTransform>().DOAnchorPosX(-30, 0.5f);
					circle.DOColor(new Color(0.1254902f, 0.1254902f, 0.1254902f, 1), 0.5f);
					rectangle.DOFade(0, 0.5f);
				}
			}

		}
	}
}
