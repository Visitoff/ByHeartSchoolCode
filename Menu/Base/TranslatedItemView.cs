﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class TranslatedItemView : ItemView
        {
			private float _nameA;
			private float _descA;
			private float _specA;
			private const float _fadeTime = 0.1f;
			[SerializeField] private bool _changeAlignment = true;
			public void Start()
			{
				nameUnTranslated = name.text;
				descriptionUnTranslated = description.text;
				specificationUnTranslated = specification.text;
				OnLongPress += () =>
				{
					_nameA = name.color.a;
					_descA = description.color.a;
					_specA = specification.color.a;
					name.DOFade(0, _fadeTime).OnComplete(() =>
					{
						name.text = nameTranslated;
						description.text = descriptionTranslated;
						specification.text = specificationTranslated;
						name.DOFade(_nameA, _fadeTime);
						description.DOFade(_descA, _fadeTime);
						specification.DOFade(_specA, _fadeTime);
						if (_changeAlignment)
						{
							name.alignment = translateRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
							description.alignment = translateRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
							specification.alignment = translateRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
						}
						name.isRightToLeftText = translateRightToLeft;
						description.isRightToLeftText = translateRightToLeft;
						specification.isRightToLeftText = translateRightToLeft;
					});
					description.DOFade(0, _fadeTime);
					specification.DOFade(0, _fadeTime);
					GetComponentInParent<ScrollRect>().enabled = false;
				};
				OnLongPressExit += () =>
				{
					name.DOFade(0, _fadeTime).OnComplete(() =>
					{
						name.text = nameUnTranslated;
						description.text = descriptionUnTranslated;
						specification.text = specificationUnTranslated;
						name.DOFade(_nameA, _fadeTime);
						description.DOFade(_descA, _fadeTime);
						specification.DOFade(_specA, _fadeTime);
						if (_changeAlignment)
						{
							name.alignment = targetRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
							description.alignment = targetRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
							specification.alignment = targetRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
						}
						name.isRightToLeftText = targetRightToLeft;
						description.isRightToLeftText = targetRightToLeft;
						specification.isRightToLeftText = targetRightToLeft;
					});
					description.DOFade(0,	_fadeTime);
					specification.DOFade(0, _fadeTime);
					GetComponentInParent<ScrollRect>().enabled = true;
				};
			}
		}
	}
}
