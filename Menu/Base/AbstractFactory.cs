﻿using UnityEngine;

namespace ByHeartSchool
{
	namespace Menu
	{
		public abstract class AbstractFactory
		{
			public abstract GameObject Create(Transform spawnPoint, Transform parent, int index = 0);
		}
	}
}
