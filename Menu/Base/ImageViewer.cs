﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class ImageViewer : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
		{
			private int _choisedItem = 0;
			[SerializeField] private ScrollRect _scrollRect;
			private int _itemsCount;
			private bool _isPress;
			private Vector2 _position;
			[SerializeField] private TMP_Text _counter;
			private void Start()
			{
				_itemsCount = _scrollRect.content.childCount;
				if (_counter != null) _counter.text = $"{_choisedItem + 1}/{_itemsCount}";
			}
			public void Click()
			{
				_scrollRect.content.GetChild(_choisedItem)?.GetComponent<Button>().onClick?.Invoke();
			}

			public void OnPointerDown(PointerEventData eventData)
			{
				_isPress = true;
				_position = eventData.position;
			}

			public void OnPointerUp(PointerEventData eventData)
			{
				if (Vector2.Distance(_position, eventData.position) < 30f && _isPress)
				{
					Click();
					return;
				}
				ChangeImage();
			}
			private void ChangeImage()
			{
				_itemsCount = _scrollRect.content.childCount;
				if (_scrollRect.velocity.x < -0.1f)
				{
					if (_choisedItem + 1 != _itemsCount) _choisedItem++;
				}
				else if (_scrollRect.velocity.x > 0.1f)
				{
					if (_choisedItem != 0) _choisedItem--;
				}
				_scrollRect.DOHorizontalNormalizedPos(1f / (_itemsCount - 1) * _choisedItem, 0.25f);
				if (_counter != null) _counter.text = $"{_choisedItem + 1}/{_itemsCount}";
			}


		}
	}
}
