﻿using ByHeartSchool.PlayerReader;
using ByHeartSchool.Response;
using ByHeartSchool.Services;
using Cysharp.Threading.Tasks;
using SimpleUi.Abstracts;
using SimpleUi.Signals;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		[Serializable]
		public enum EScreenType
		{
			FEED,
			MY,
			LEARN,
			USE,
			PLAY
		}
		public class LearningController : AScreenController<LearningView>
		{
			public LearningController(LearningView view,
				SignalBus signalBus,
				ILanguageService languageService,
				IScreenService screenService,
				UserManager userManager,
				IPlaylistService playlistService,
				ISceneNavigator sceneNavigator) : base(view, signalBus, languageService, screenService, userManager, playlistService, sceneNavigator)
			{

			}
		}
		public class OtherController : AScreenController<OtherView>
		{
			public OtherController(OtherView view,
				SignalBus signalBus,
				ILanguageService languageService,
				IScreenService screenService,
				UserManager userManager,
				IPlaylistService playlistService,
				ISceneNavigator sceneNavigator) : base(view, signalBus, languageService, screenService, userManager, playlistService, sceneNavigator)
			{

			}
		}
		public class GamesController : AScreenController<GamesView>
		{
			public GamesController(GamesView view,
			  SignalBus signalBus,
		   	ILanguageService languageService,
			  IScreenService screenService,
			 UserManager userManager,
			 IPlaylistService playlistService,
			 ISceneNavigator sceneNavigator) : base(view, signalBus, languageService, screenService, userManager, playlistService, sceneNavigator)
			{

			}
		}
		public class FeedController : AScreenController<FeedView>
		{
			public FeedController(FeedView view,
				SignalBus signalBus,
				ILanguageService languageService,
				IScreenService screenService,
				UserManager userManager,
				IPlaylistService playlistService,
				ISceneNavigator sceneNavigator) : base(view, signalBus, languageService, screenService, userManager, playlistService, sceneNavigator)
			{

			}
		}
		public class MyController : AScreenController<MyView>
		{
			public MyController(MyView view,
				SignalBus signalBus,
				ILanguageService languageService,
				IScreenService screenService,
				UserManager userManager,
				IPlaylistService playlistService,
				ISceneNavigator sceneNavigator) : base(view, signalBus, languageService, screenService, userManager, playlistService, sceneNavigator)
			{

			}
		}
		public abstract class AScreenController<T> : UiController<T>, Zenject.IInitializable, IDisposable where T : AView
		{
			private readonly T _view;
			private readonly SignalBus _signalBus;
			private readonly ILanguageService _languageService;
			private readonly IScreenService _screenService;
			private readonly IPlaylistService _playlistService;
			private readonly ISceneNavigator _sceneNavigator;
			private readonly UserManager _userManager;
			private readonly List<IDisposable> _disposables = new();
			private UnityAction<string> Game;
			private UnityAction<string> Screen;
			private UnityAction<string> Course;
			private UnityAction<string> Playlist;
			private UnityAction<string> Download;
			private UnityAction<string> DownloadAndOpen;
			private UnityAction<string> Library;
			private UnityAction<string> Delete;
			private UnityAction<string> Settings;
			private UnityAction OnComplete;
			private Dictionary<ScreenAction, UnityAction<string>> actions;
			private static List<(ScreenAction, string, Func<bool, UniTask>)> _commands = new();
			private static Func<UniTask> _lastCmd;
			private static Func<UniTask> _lastCmdCache;
			private static GameObject _present;
			private static GameObject _present2;
			private static List<float> _contentPositions = new();
			private bool _isOpen;
			private bool _isLoad;
			private static int _backCounter;
			private bool _lock;
			private bool _updating;
			private string _lastOpenedCourse = "_NaN_";
			private string _lastOpenedScreen = "_NaN_";

			public AScreenController(
				T view,
				SignalBus signalBus,
				ILanguageService languageService,
				IScreenService screenService,
				UserManager userManager,
				IPlaylistService playlistService,
				ISceneNavigator sceneNavigator)
			{
				_signalBus = signalBus;
				_view = view;
				_languageService = languageService;
				_screenService = screenService;
				_userManager = userManager;
				_playlistService = playlistService;
				_sceneNavigator = sceneNavigator;
			}
			public async void Initialize()
			{
				_userManager.LoginCompleted += async () => await Init();
				_userManager.Initialized += async () => await Init();
				_view.courseAdapter.Update += () => Update();
				_view.adapter.Update += () => Update();
				if (_userManager.Success) await Init();
			}
			private void CollectGarbage()
			{
				GC.Collect();
				Resources.UnloadUnusedAssets();
			}
			private async UniTask Init()
			{
				if (_userManager.Success)
				{
					this.Screen = async (s) => { OnComplete = null; await OpenScreen(s); };
					this.Course = async (s) => { OnComplete = null; await OpenCourse(s); };
					this.Library = async (s) => { OnComplete = null; await OpenLibrary(); };
					this.Playlist = async (s) => { OnComplete = null; await OpenPlaylist(s); };
					this.Download = async (s) => { OnComplete = null; await DownloadPlaylist(s); };
					this.Delete = async (s) => { OnComplete = null; await DeletePlaylist(s); };
					this.DownloadAndOpen = async (s) =>
					{
						OnComplete = () => Playlist.Invoke(s);
						await DownloadPlaylist(s, true);
					};
					this.Game = async (s) => { OnComplete = null; await OpenGame(s); };
					this.Settings = async (s) => { OnComplete = null; await OpenSettings(s); };
					actions = new Dictionary<ScreenAction, UnityAction<string>>()
					{
						{ScreenAction.Screen, Screen},
						{ScreenAction.Courses,  Course},
						{ScreenAction.Playlist, Playlist},
						{ScreenAction.Download, Download},
						{ScreenAction.Delete, Delete},
						{ScreenAction.Games, Game},
						{ScreenAction.Library, Library},
						{ScreenAction.DownloadAndOpen, DownloadAndOpen},
						{ScreenAction.Settings, Settings}
					};
					_view.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
					_signalBus.Fire(new MenuSignal(() => _lock = true, () => _lock = false));
					if (_commands.Count == 0)
					{
						await OpenMainScreen();
					}
					else
					{
						await OpenLastScreen();
					}
				}
			}
			private async UniTask OpenLastScreen()
			{
				var newCommands = new List<(ScreenAction, string, Func<bool, UniTask>)>();
				for (int i = 0; i < _commands.Count; i++)
				{
					var cmd = _commands[i];
					switch (cmd.Item1)
					{
						case ScreenAction.Screen:
							newCommands.Add((ScreenAction.Screen, cmd.Item2, async (b) => { OnComplete = null; await OpenScreen(cmd.Item2, true, b); }));
							break;
						case ScreenAction.Courses:
							newCommands.Add((ScreenAction.Courses, cmd.Item2, async (b) => { OnComplete = null; await OpenCourse(cmd.Item2, true, true); }));
							break;
						case ScreenAction.Library:
							newCommands.Add((ScreenAction.Library, cmd.Item2, async (b) => { OnComplete = null; await OpenLibrary(true, b); }));
							break;
						case ScreenAction.Playlist:
							newCommands.Add((ScreenAction.Playlist, cmd.Item2, async (b) => { OnComplete = null; await OpenPlaylist(cmd.Item2, true, b); }));
							break;
						case ScreenAction.Settings:
							newCommands.Add((ScreenAction.Settings, cmd.Item2, async (b) => { OnComplete = null; await OpenSettings(cmd.Item2, true, b); }));
							break;
					}
				}
				var action = newCommands[_commands.Count - 1];
				_commands = newCommands;
				await action.Item3.Invoke(false);

			}
			private async UniTask OpenSettings(string settingsId, bool isBackButton = false, bool isSlideBackCmd = false)
			{
				if ((_lock && !isSlideBackCmd && !_updating) || string.IsNullOrEmpty(settingsId)) return;
				CollectGarbage();
				_signalBus.Fire(new MenuSignal(ESignalType.Lock));
				_view.courseAdapter.gameObject.SetActive(false);
				_view.adapter.gameObject.SetActive(false);
				_view.playerContainer.gameObject.SetActive(false);
				_view.settingsContainer.gameObject.SetActive(true);
				_present = _view.settingsContainer.gameObject;
				_present2 = _commands.Last().Item1 == ScreenAction.Settings ? _view.settingsContainer.gameObject : _view.adapter.gameObject;
				if (!isBackButton)
				{
					_contentPositions.Add(0);
					_commands.Add((ScreenAction.Settings, settingsId, async (b) => { OnComplete = null; await OpenSettings(settingsId, true); }));
					_lastCmd = async () => await OpenSettings(settingsId, false, true);
					_backCounter = 0;
				}
				else
				{
					if (_backCounter > 0)
					{
						_lastCmd = _lastCmdCache;
					}

					_lastCmdCache = async () => await OpenSettings(settingsId, false, true);

					_backCounter++;
				}
				CheckCommandsCountAndMenuSignal(isBackButton);
				_signalBus.Fire(new SettingsSignal(ESignalType.Open, settingsId));
				_signalBus.Fire(new MenuSignal(ESignalType.Unlock));

			}
			private async UniTask OpenGame(string gameId, bool isBackButton = false)
			{
				if (_lock) return;
				if (!_isLoad)
				{
					CollectGarbage();
					_sceneNavigator.SwitchAddressableScene(gameId);
				}
			}
			private async UniTask DownloadPlaylist(string playlistRef, bool open = false)
			{
				if (_lock) return;
				CollectGarbage();
				string[] strings = playlistRef.Split('/');
				string playlistId = strings[1].Contains('|') ? strings[1].Substring(0, strings[1].IndexOf('|')) : strings[1];
				var playlistIndex = strings[1].Contains('|') ? int.Parse(strings[1].Substring(strings[1].IndexOf('|') + 1)) : 0;
				string courseId = strings[0];
				var itemView = _view.courseAdapter.GetItemView(playlistIndex);
				itemView.ShowLoadingIndicator();
				var response = await _playlistService.Download(courseId, playlistId);
				if (response.StatusCode != Response.StatusCode.OK)
				{
					response = await _playlistService.Download(courseId, playlistId);
				}
				if (response.StatusCode == Response.StatusCode.OK)
				{
					itemView.button.onClick.RemoveAllListeners();
					itemView.button.onClick.AddListener(async () => await OpenPlaylist(playlistRef));
					itemView.HideLoadingIndicator(true);
					itemView.button2.gameObject.SetActive(false);
					itemView.button3.gameObject.SetActive(true);
					if (open && _isOpen)
					{
						OnComplete?.Invoke();
					}
					return;
				}
				itemView.HideLoadingIndicator(false);
			}
			private async UniTask DeletePlaylist(string playlistRef)
			{
				if (_lock) return;
				CollectGarbage();
				string[] strings = playlistRef.Split('/');
				string playlistId = strings[1].Contains('|') ? strings[1].Substring(0, strings[1].IndexOf('|')) : strings[1];
				var playlistIndex = strings[1].Contains('|') ? int.Parse(strings[1].Substring(strings[1].IndexOf('|') + 1)) : 0;
				string courseId = strings[0];
				var itemView = _view.courseAdapter.GetItemView(playlistIndex);
				_lock = true;
				var response = await _playlistService.Delete(courseId, playlistId);
				_lock = false;
				if (response.StatusCode == Response.StatusCode.OK)
				{
					itemView.button.onClick.RemoveAllListeners();
					itemView.button.onClick.AddListener(() => DownloadAndOpen?.Invoke(playlistRef));
					itemView.button2.gameObject.SetActive(true);
					itemView.indicator.gameObject.SetActive(true);
					itemView.indicator.ResetLoading();
					itemView.button3.gameObject.SetActive(false);
					return;
				}
			}
			private async UniTask OpenPlaylist(string playlistRef, bool isBackButton = false, bool isSlideBackCmd = false)
			{
				if ((_lock && !isSlideBackCmd && !_updating) || string.IsNullOrEmpty(playlistRef)) return;
				CollectGarbage();
				ShowLoading(_isOpen);
				_signalBus.Fire(new MenuSignal(ESignalType.Lock));
				string[] strings = playlistRef.Split('/');
				string playlistId = strings[1].Contains('|') ? strings[1].Substring(0, strings[1].IndexOf('|')) : strings[1];
				string courseId = strings[0];
				IBaseResponse<List<InPlayerEvent>> response = await _playlistService.Open(courseId, playlistId);
				if (response.StatusCode == Response.StatusCode.OK)
				{
					CollectGarbage();
					_view.courseAdapter.gameObject.SetActive(false);
					_view.adapter.gameObject.SetActive(false);
					_view.playerContainer.gameObject.SetActive(true);
					_view.settingsContainer.gameObject.SetActive(false);
					_present = _view.playerContainer.gameObject;
					_present2 = _commands.Last().Item1 == ScreenAction.Playlist ? _view.playerContainer.gameObject : _view.courseAdapter.gameObject;
					if (!isBackButton)
					{
						float contentPos = _commands.Last().Item1 == ScreenAction.Playlist || isSlideBackCmd ? _view.playerContainer.GetComponentInChildren<ListAdapter>().GetCurrentYPos() : _view.courseAdapter.GetCurrentYPos();
						_contentPositions.Add(contentPos);
						_commands.Add((ScreenAction.Playlist, playlistRef, async (b) => { OnComplete = null; await OpenPlaylist(playlistRef, true); }));
						_lastCmd = async () => await OpenPlaylist(playlistRef, false, true);
						_backCounter = 0;
					}
					else
					{
						if (_backCounter > 0)
						{
							_lastCmd = _lastCmdCache;
						}

						_lastCmdCache = async () => await OpenPlaylist(playlistRef, false, true);

						_backCounter++;
					}
					CheckCommandsCountAndMenuSignal(isBackButton);
					_signalBus.Fire(new PlayerSignal(ESignalType.OpenUpdately, Playlist, (isBackButton || isSlideBackCmd) ? _contentPositions.Last() : null));
					_signalBus.Fire(new MenuSignal(ESignalType.Unlock));
					HideLoading(_isOpen);
					return;
				}
				_signalBus.Fire(new MenuSignal(ESignalType.Unlock));
				HideLoading(_isOpen);
			}
			private void ShowLoading(bool i = true)
			{
				if (i)
				{
					_signalBus.Fire(new LoadingSignal(LoadingSignalType.SetImage, _languageService.GetCurrentLanguage()));
					_signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project);
				}
				_isLoad = true;
			}
			private void HideLoading(bool i = true)
			{
				if (i) _signalBus.BackWindow(EWindowLayer.Project);
				_isLoad = false;
			}
			private async UniTask OpenMainScreen()
			{
				_commands = new();

				try
				{
					await OpenScreen(GetScreenId());
				}
				catch (Exception e)
				{
					Debug.LogError(e);
					await UniTask.Delay(1000);
					await OpenMainScreen();
				}
			}
			private string GetScreenId() => _view.screenType switch
			{
				EScreenType.USE => Configuration.USEScreenId,
				EScreenType.FEED => Configuration.FEEDScreenId,
				EScreenType.PLAY => Configuration.PLAYScreenId,
				EScreenType.MY => Configuration.MYScreenId,
				EScreenType.LEARN => Configuration.LEARNScreenId,
				_ => Configuration.LEARNScreenId,
			};
			private async UniTask OpenScreen(string screenId, bool isBackButton = false, bool isUpdating = false, bool isSlideBackCmd = false)
			{
				if (_lock && !isSlideBackCmd && !_updating) return;
				CollectGarbage();
				ShowLoading(_isOpen && !isUpdating);
				IBaseResponse<List<Item>> response = null;
				if (_lastOpenedScreen != screenId) response = await _screenService.GetScreen(screenId, actions);
				if (_lastOpenedScreen == screenId || response.StatusCode == Response.StatusCode.OK)
				{
					_view.courseAdapter.gameObject.SetActive(false);
					_view.adapter.gameObject.SetActive(true);
					_view.playerContainer.gameObject.SetActive(false);
					_view.settingsContainer.gameObject.SetActive(false);
					_present = _view.adapter.gameObject;
					_present2 = _view.adapter.gameObject;
					if (!isBackButton)
					{
						_lastCmd = async () => await OpenScreen(screenId, false, isUpdating, true);
						float contentPos = _view.adapter.GetCurrentYPos();
						_contentPositions.Add(contentPos);
						_commands.Add((ScreenAction.Screen, screenId, async (b) => { OnComplete = null; await OpenScreen(screenId, true, b); }));
						_backCounter = 0;
					}
					else
					{
						if (_backCounter > 0)
						{
							_lastCmd = _lastCmdCache;
						}
						_lastCmdCache = async () => await OpenScreen(screenId, false, isUpdating, true);
						_backCounter++;
					}
					CheckCommandsCountAndMenuSignal(isBackButton);
					if (_lastOpenedScreen != screenId)
					{
						_lastOpenedScreen = screenId;
						_view.adapter.Show(response.Data, _contentPositions.Count > 1 && isBackButton && !isUpdating ? _contentPositions[_contentPositions.Count - 1] : null);
					}
					else if (!isBackButton)
					{
						_view.adapter.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
					}
					HideLoading(_isOpen && !isUpdating);
					return;
				}
				HideLoading(_isOpen && !isUpdating);
			}
			private void CheckCommandsCountAndMenuSignal(bool isBackButton = false, bool onOpen = false)
			{
				if (_isOpen)
				{
					if (onOpen)
					{
						if (_commands.Count >= 2) _signalBus.Fire(new MenuSignal(ESignalType.ShowBackBtn, () => BackButton(), async () => { if (!_isLoad) await OpenMainScreen(); }, _lastCmd, _present));
						else _signalBus.Fire(new MenuSignal(ESignalType.ShowLanguageBtn, null, null, _lastCmd, _present));
						return;
					}
					if (_commands.Count >= 2) _signalBus.Fire(new MenuSignal(ESignalType.ShowBackBtn, () => BackButton(), async () => { if (!_isLoad) await OpenMainScreen(); }, _lastCmd, _present, isBackButton, _present2));
					else _signalBus.Fire(new MenuSignal(ESignalType.ShowLanguageBtn, null, null, _lastCmd, _present));
				}
			}
			private async UniTask OpenCourse(string courseId, bool isBackButton = false, bool isSlideBackCmd = false, bool isUpdating = false)
			{
				if (_lock && !isSlideBackCmd && !_updating) return;
				CollectGarbage();
				if (!_isLoad)
				{
					ShowLoading(false);
					IBaseResponse<List<Item>> response = null;
					if (courseId != _lastOpenedCourse) response = await _screenService.GetCourse(courseId, actions);
					if (courseId == _lastOpenedCourse || response.StatusCode == Response.StatusCode.OK)
					{
						_view.courseAdapter.gameObject.SetActive(true);
						_view.adapter.gameObject.SetActive(false);
						_view.playerContainer.gameObject.SetActive(false);
						_view.settingsContainer.gameObject.SetActive(false);
						_present = _view.courseAdapter.gameObject;
						_present2 = _view.adapter.gameObject;
						if (!isBackButton)
						{
							float contentPos = _view.adapter.GetCurrentYPos();
							_contentPositions.Add(contentPos);
							_commands.Add((ScreenAction.Courses, courseId, async (b) => { OnComplete = null; await OpenCourse(courseId, true, false, b); }));
							_lastCmd = async () => await OpenCourse(courseId, false, true, isUpdating);
							_backCounter = 0;
						}
						else
						{
							if (_backCounter > 0)
							{
								_lastCmd = _lastCmdCache;
							}

							_lastCmdCache = async () => await OpenCourse(courseId, false, true, isUpdating);

							_backCounter++;
						}
						CheckCommandsCountAndMenuSignal(isBackButton);
						if (courseId != _lastOpenedCourse)
						{
							_view.courseAdapter.Show(response.Data, _contentPositions.Count > 1 && isBackButton && !isUpdating ? _contentPositions[_contentPositions.Count - 1] : null);
							_lastOpenedCourse = courseId;
						}
						else if (!isBackButton)
						{
							_view.courseAdapter.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
						}
						HideLoading(_isOpen);
						return;
					}
					if (_view.courseAdapter.gameObject.activeSelf && response.StatusCode == Response.StatusCode.NotFound)
					{
						BackButton(true);
					}
					HideLoading(false);
				}
			}
			private async UniTask OpenLibrary(bool isBackButton = false, bool isSlideBackCmd = false, bool isUpdating = false)
			{
				if (_lock && !isSlideBackCmd && !_updating) return;
				CollectGarbage();
				if (!_isLoad)
				{
					ShowLoading(false);
					var response = await _screenService.GetLibrary(actions);
					if (response.StatusCode == Response.StatusCode.OK)
					{
						_view.courseAdapter.gameObject.SetActive(true);
						_view.adapter.gameObject.SetActive(false);
						_view.settingsContainer.gameObject.SetActive(false);
						_present = _view.courseAdapter.gameObject;
						if (!isBackButton)
						{
							float contentPos = _view.adapter.GetCurrentYPos();
							_contentPositions.Add(contentPos);
							_commands.Add((ScreenAction.Library, "", async (b) => { OnComplete = null; await OpenLibrary(true, false, b); }));
							_lastCmd = async () => await OpenLibrary(false, true, isUpdating);
							_backCounter = 0;
						}
						else
						{
							if (_backCounter > 0)
							{
								_lastCmd = _lastCmdCache;
							}

							_lastCmdCache = async () => await OpenLibrary(false, true, isUpdating);

							_backCounter++;
						}
						CheckCommandsCountAndMenuSignal(isBackButton);
						_view.courseAdapter.Show(response.Data, _contentPositions.Count > 1 && isBackButton && !isUpdating ? _contentPositions[_contentPositions.Count - 1] : null);
						HideLoading(_isOpen);
						return;
					}
					if (_view.courseAdapter.gameObject.activeSelf && response.StatusCode == Response.StatusCode.NotFound)
					{
						BackButton(true);
					}
					HideLoading(false);
				}
			}
			private async void BackButton(bool _ignoreLoading = false)
			{
				if (_commands.Count >= 2 && (!_isLoad || _ignoreLoading))
				{
					_commands.RemoveAt(_commands.Count - 1);
					var action = _commands[_commands.Count - 1];
					await action.Item3.Invoke(false);
					_contentPositions.RemoveAt(_contentPositions.Count - 1);
				}
				else
				{
					Application.Quit();
				}
			}
			public async void Update(UpdateSignal signal = null)
			{
				while (!_commands.Any())
				{
					await UniTask.Yield();
				}
				_lastOpenedScreen = "_NaN_";
				_lastOpenedCourse = "_NaN_";
				_updating = true;
				_signalBus.Fire(new MenuSignal(ESignalType.Lock));
				await _screenService.Update();
				var action = _commands[_commands.Count - 1];
				await action.Item3.Invoke(signal == null);
				_updating = false;
				_view.adapter.Updated(signal != null);
				_view.courseAdapter.Updated(signal != null);
				_signalBus.Fire(new MenuSignal(ESignalType.Unlock));
			}
			public async override void OnShow()
			{
				_isOpen = true;
				if (_isLoad) ShowLoading();
				CheckCommandsCountAndMenuSignal(true);
				if (_commands.Any())
				{
					var last = _commands.Last();
					if (last.Item3 != null && last.Item1 == ScreenAction.Settings)
					{
						await OpenLastScreen();
					}
				}
			}
			public override void OnHide()
			{
				_signalBus.Fire(new MenuSignal(ESignalType.ShowLanguageBtn));
				_isOpen = false;
				HideLoading();
			}

			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
