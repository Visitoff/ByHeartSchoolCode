﻿using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class ItemFactory : AbstractFactory
		{
			private readonly List<GameObject> _gameObjects;
            public ItemFactory(List<GameObject> gameObjects)
            {
                _gameObjects = gameObjects;
            }
            public override GameObject Create(Transform spawnPoint, Transform parent, int index = 0)
			{
				if (index >= _gameObjects.Count) index = _gameObjects.Count - 1;
				if (_gameObjects[index] != null)
				{
					var go = _gameObjects[index];
					return Object.Instantiate(go, spawnPoint.position, spawnPoint.rotation, parent);
				}
				return null;
			}
		}
	}
}
