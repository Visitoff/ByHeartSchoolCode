﻿using UnityEngine;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class PlusHeigth: MonoBehaviour
		{
			[SerializeField] public RectTransform parent;
			[SerializeField] public RectTransform target;
			private float height;
			public void Start()
			{
				height = target.rect.height;
			}
			public void OnEnable()
			{
				ToRightPos();
			}
			private void ToRightPos()
			{
				target.sizeDelta = new Vector2(target.rect.width, height + parent.rect.height);
			}
		}
	}
}
