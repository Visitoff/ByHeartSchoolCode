﻿namespace ByHeartSchool
{
	namespace Response
	{
		public interface IBaseResponse<T>
		{
			public string Description { get; set; }
			public StatusCode StatusCode { get; set; }

			public T Data { get; set; }
		}
		public interface IBaseResponse
		{
			public string Description { get; set; }
			public StatusCode StatusCode { get; set; }
		}
	}
}

