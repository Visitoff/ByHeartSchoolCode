﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using System.Threading.Tasks;
using Unity.Services.Analytics.Internal;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class SwipeHandler: MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler
		{
			[SerializeField] private Canvas _canvas;
			public UnityAction beginSwipe; 
			public Func<bool, UniTask> endSwipe;
			public UnityAction finallySwipe;
			private GameObject mirror;
			private GameObject mirrorSwipeLeft;
			public GameObject present;
			private Vector2 startDragPoint;
			public RectTransform endSwipePoint;
			public Transform swipeObjects;
			private float beginDragTimer;
			[SerializeField] private Image slideImage;
			[SerializeField] private bool interactable;
			[SerializeField] private GameObject _lock;
			public bool Interactable { get { return interactable; }  set { interactable = value;  slideImage.raycastTarget = value; } }
			public bool FastSwipe { get; set; }
			private bool _fastSwipe;
			private float _presentContentAncPosX;
			private float _vNormalizePresentPos;
			private bool onPointerDown;
			Tween tween;
			Tween tween2;
			private bool _swiped = true;
			private bool _swipeLeftInProgress;

			public void OnBeginDrag(PointerEventData eventData)
			{
				_fastSwipe = FastSwipe;
				if (_swipeLeftInProgress) return;
				if (present == null)
				{
					if (eventData == null && _swiped)
					{
						beginSwipe?.Invoke();
						finallySwipe?.Invoke();
					}
					return;
				}
				if (!interactable)
				{
					if (present != null && present.GetComponentInChildren<ScrollRect>() != null &&  eventData != null)
					{
						present.GetComponentInChildren<ScrollRect>().OnBeginDrag(eventData);
					}
					return;
				}
				if (eventData == null || Math.Abs(eventData.delta.x * 15) > Math.Abs(eventData.delta.y))
				{
					if ((eventData == null || eventData.delta.x > 0.05f) && (mirror == null || mirror.IsDestroyed()))
					{
						_swiped = false;
						tween.Kill();
						tween2?.Kill();
						interactable = false;
						beginDragTimer = 0;
						_presentContentAncPosX = present.GetComponent<RectTransform>().anchoredPosition.x;
						mirror = Instantiate(present, new Vector2(eventData != null ? eventData.position.x : 0, 0), Quaternion.identity);
						Vector2 pos = present.GetComponent<RectTransform>().anchoredPosition;
						mirror.transform.SetParent(swipeObjects, false);
						mirror.GetComponent<RectTransform>().anchoredPosition = pos;
						if (present.GetComponentInChildren<ScrollRect>() != null) 
						{
							_vNormalizePresentPos = present.GetComponentInChildren<ScrollRect>().verticalNormalizedPosition;
							mirror.GetComponentInChildren<ScrollRect>().verticalNormalizedPosition = _vNormalizePresentPos;
							mirror.GetComponentInChildren<ScrollRect>().enabled = false; 
						}
						var rects = mirror.GetComponentsInChildren<ScrollRect>();
						foreach (var rect in rects)
						{
							rect.enabled = false;
						}
						_lock.SetActive(true);
						beginSwipe?.Invoke();
						startDragPoint = eventData != null ? eventData.position : new Vector2(0, 0);
						onPointerDown = true;
						return;
					}
				}
				if (present != null  && present.GetComponentInChildren<ScrollRect>() != null && eventData != null)
				{
					present.GetComponentInChildren<ScrollRect>().OnBeginDrag(eventData);
				}
			}
			public void Update()
			{
				if (onPointerDown)
				{
					beginDragTimer += Time.deltaTime;
					if (Input.touchCount == 0 || _fastSwipe)
					{
						onPointerDown = false;
						if (beginDragTimer < 0.55f || _fastSwipe)
						{
							EndSwipe(true);
						}
						else
						{
							if (Math.Abs(Input.mousePosition.x - startDragPoint.x) > (Screen.width / 4))
							{
								EndSwipe(true);
							}
							else
							{
								EndSwipe(false);
							}
						}
					}
					if (Input.touchCount > 0 && !_fastSwipe)
					{
						var pos = new Vector2(Input.mousePosition.x / _canvas.scaleFactor, mirror.GetComponent<RectTransform>().anchoredPosition.y);
						var pos2 = new Vector2((Input.mousePosition.x - (Screen.width)) * 0.5f / _canvas.scaleFactor, present.GetComponent<RectTransform>().anchoredPosition.y);
						mirror.GetComponent<RectTransform>().anchoredPosition = pos;
						present.GetComponent<RectTransform>().anchoredPosition = pos2;
					}
				}
			}
			public void OnDrag(PointerEventData eventData)
			{
				if (present != null && present.GetComponentInChildren<ScrollRect>() != null && eventData != null)
				{
					present.GetComponentInChildren<ScrollRect>().OnDrag(eventData);
				}
			}
			private async UniTask EndSwipe(bool done)
			{
				if (done)
				{
					tween2 = present.GetComponent<RectTransform>().DOAnchorPosX(_presentContentAncPosX, 0.10f);
					tween = mirror.GetComponent<RectTransform>().DOAnchorPosX(endSwipePoint.anchoredPosition.x, 0.3f).OnComplete(() => 
					{ 
						Destroy(mirror); 
						endSwipe?.Invoke(done); 
						finallySwipe?.Invoke();
						_lock.SetActive(false);
						_swiped = true;
					});
				}
				else
				{
					tween2 = present.GetComponent<RectTransform>().DOAnchorPosX(-Screen.width/2/_canvas.scaleFactor, 0.4f);
					tween = mirror.GetComponent<RectTransform>().DOAnchorPosX(0, 0.4f).OnComplete(async () => 
					{
						present.GetComponent<RectTransform>().DOAnchorPosX(_presentContentAncPosX, 0f); 
						await endSwipe.Invoke(done);
						await UniTask.Delay(100);
						_swiped = true;
						finallySwipe?.Invoke();
						_lock.SetActive(false);
						await UniTask.Yield();
						if (present.GetComponentInChildren<ScrollRect>() != null) tween = present.GetComponentInChildren<ScrollRect>().DOVerticalNormalizedPos(_vNormalizePresentPos, 0f).OnComplete(() => Destroy(mirror));
						else Destroy(mirror);
					});
				}
			}
			public void SwipeLeft(GameObject present = null)
			{
				if (present == null || onPointerDown || !_swiped) return;
				_lock.SetActive(true);
				_swipeLeftInProgress = true;
				tween.Kill();
				tween2.Kill();
				_presentContentAncPosX = present.GetComponent<RectTransform>().anchoredPosition.x;
				mirrorSwipeLeft = Instantiate(present, new Vector2(0, 0), Quaternion.identity);
				mirrorSwipeLeft.SetActive(true);
				Vector2 pos = present.GetComponent<RectTransform>().anchoredPosition;
				mirrorSwipeLeft.transform.SetParent(present.transform.parent, false);
				mirrorSwipeLeft.transform.SetAsFirstSibling();
				mirrorSwipeLeft.GetComponent<RectTransform>().anchoredPosition = pos;
				if (present.GetComponentInChildren<ScrollRect>() != null)
				{
					_vNormalizePresentPos = present.GetComponentInChildren<ScrollRect>().verticalNormalizedPosition;
					mirrorSwipeLeft.GetComponentInChildren<ScrollRect>().verticalNormalizedPosition = _vNormalizePresentPos;
					mirrorSwipeLeft.GetComponentInChildren<ScrollRect>().enabled = false;
				}
				this.present.GetComponent<RectTransform>().DOAnchorPosX(_presentContentAncPosX + Screen.width * 2, 0);
				mirrorSwipeLeft.GetComponent<RectTransform>().DOAnchorPosX(mirrorSwipeLeft.GetComponent<RectTransform>().anchoredPosition.x - 500, 0.55f);
				this.present.GetComponent<RectTransform>().DOAnchorPosX(_presentContentAncPosX, 0.4f).OnComplete(() => { Destroy(mirrorSwipeLeft); _swipeLeftInProgress = false; _lock.SetActive(false); });
				
			}

			public void OnEndDrag(PointerEventData eventData)
			{
				if (present != null && present.GetComponentInChildren<ScrollRect>() != null && eventData != null)
				{
					present.GetComponentInChildren<ScrollRect>().OnEndDrag(eventData);
				}
			}
		}
	}
}
