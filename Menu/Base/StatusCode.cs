﻿namespace ByHeartSchool
{
	namespace Response
	{
		public enum StatusCode
		{
			OK,
			NotFound,
			Failed,
		}
	}
}
