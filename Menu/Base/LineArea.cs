﻿using DG.Tweening;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
using System.Collections.Generic;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class LineArea: MonoBehaviour, IPointerDownHandler,IPointerUpHandler, IDragHandler, IPointerExitHandler
		{
			[SerializeField] private Image _line;
			[SerializeField] private TMP_Text _tutor;
			private bool _isActive;
			private bool _isEnabled;
			private bool _isPress;
			private float _time;
			private bool onLongPress;
			private bool _dragged;
			public UnityAction<PointerEventData> onDrag;
			public UnityAction<PointerEventData> onPointerDownLongPress;
			public List<Tween> _tweens = new();
			public bool isEnabled { get => _isEnabled; set { _isEnabled = value; Enabled(value); } }

			private void Enabled(bool enabled)
			{
				foreach (var t in _tweens)
				{
					t.Kill();
				}
				_tweens = new();
				if (enabled)
				{
					var fadeTween1 = _line.DOFade(1, 0.25f);
					var fadeTween2 = _tutor.DOFade(1, 0.25f);
					var anchorTween = _tutor.rectTransform.DOAnchorPosY(-210, 0.25f).OnComplete(() => { var anchorTween2 = _tutor.rectTransform.DOAnchorPosY(-230, 0.25f); _tweens.Add(anchorTween2); } );
					var callTween = DOVirtual.DelayedCall(5f, () => { var fadeTween = _tutor.DOFade(0, 0.25f); _tweens.Add(fadeTween); });
					_tweens.Add(fadeTween1);
					_tweens.Add(fadeTween2);
					_tweens.Add(anchorTween);
					_tweens.Add(callTween);
				}
				else
				{
					Deactivate(false);
					var fadeTween1 = _line.DOFade(0, 0.25f);
					var fadeTween2 = _tutor.DOFade(0, 0.25f);
					_tweens.Add(fadeTween1);
					_tweens.Add(fadeTween2);
				}
			}
			public void HideTutor()
			{
				var fadeTween = _tutor.DOFade(0, 0.25f);
				_tweens.Add(fadeTween);
			}

			public void Activate()
			{
				if (!isEnabled || _isActive) return;
                Vibration.VibratePop();
				_line.rectTransform.DOAnchorPosY(-170, 0.25f);
				_line.DOColor(new Color(0.3320754f, 0.3320754f, 0.3320754f), 0.25f).OnComplete(() => _line.rectTransform.DOAnchorPosY(-200, 0.25f));
				var fadeTween = _tutor.DOFade(0, 0.25f);
				_tweens.Add(fadeTween);
				_dragged = false;
				_isActive = true;

			}
			public void Deactivate()
			{
				Deactivate(true);
			}
			private void Deactivate(bool isPublic = false)
			{
				if (isPublic && !isEnabled) return;
				_line.DOColor(Color.white, 0.25f);
				_isActive = false;
			}
			public void OnPointerDown(PointerEventData eventData)
			{
				if (!isEnabled) return;
				_isPress = true;
				if (_isActive) { onPointerDownLongPress?.Invoke(eventData); onLongPress = true; }
			}
			private void Update()
			{
				if (_isPress)
				{
					_time += Time.deltaTime;
					if (_time > 0.1f)
					{
						if (!onLongPress)
						{
							onLongPress = true;
							Activate();
						}
					}
				}
			}

			public void OnPointerUp(PointerEventData eventData)
			{
				if (!isEnabled) return;
				Deactivate(false);
				_isPress = false;
				onLongPress = false;
				_time = 0f;
			}

			public void OnDrag(PointerEventData eventData)
			{
				if (!isEnabled) return;
				if (_isActive)
				{
					if (!_dragged)
					{
						onPointerDownLongPress?.Invoke(eventData);
						_dragged = true;
					}
					onDrag?.Invoke(eventData);
				}
			}

			public void OnPointerExit(PointerEventData eventData)
			{
				if (!isEnabled) return;
				if (!_isActive) OnPointerUp(eventData);
			}
		}
	}

}
