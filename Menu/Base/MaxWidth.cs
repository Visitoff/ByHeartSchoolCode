﻿using SimpleUi.Abstracts;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MaxWidth: MonoBehaviour
		{
			[SerializeField] public float maxWidth;
			[SerializeField] private LayoutElement layoutElement;
			[SerializeField] public RectTransform rectTransform;
			[SerializeField] private RectTransform _parent;
			[SerializeField] public VerticalLayoutGroup content;
			[SerializeField] private TMP_Text text;
			[SerializeField] private float _parentPlus = 0;
			[SerializeField] private RectTransform children;
			[SerializeField] protected float _childrenPlus = 0;
			public void ResetLayoutElement()
			{
				if (layoutElement) layoutElement.preferredWidth = -1;

				Canvas.ForceUpdateCanvases();
			}
			public void Start()
			{
				ToRightPos();
			}
			public void UpdateMaxWidth()
			{
				if (content != null && text != null && text.textInfo != null && content.padding.top != -100 * text.textInfo.lineCount)
				{
					content.padding.top = -100 * text.textInfo.lineCount;
					Canvas.ForceUpdateCanvases();
					if (layoutElement)layoutElement.enabled = true;
					LayoutRebuilder.ForceRebuildLayoutImmediate(content.GetComponent<RectTransform>());
				}
			}
			private void OnRectTransformDimensionsChange()
			{
				ToRightPos();
			}
			private void ToRightPos()
			{
				if (_parent != null)
				{
					if (rectTransform.rect.width != _parent.rect.width)
					{
						if (!layoutElement)
						{
							rectTransform.sizeDelta = new Vector2(rectTransform.rect.height, _parent.rect.width + _parentPlus);
						}
						else
						{
							layoutElement.preferredWidth = _parent.rect.width + _parentPlus;
						}
					}
					return;
				}
				if (rectTransform.rect.width > maxWidth)
				{
					layoutElement.preferredWidth = maxWidth;
				}
				if (children) children.sizeDelta = new Vector2(rectTransform.rect.width + _childrenPlus, 0);
			}
		}
	}
}
