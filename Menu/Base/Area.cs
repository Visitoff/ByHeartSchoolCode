﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class Area : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IPointerExitHandler
		{
			public void OnDrag(PointerEventData eventData)
			{
				eventData.Use();
			}

			public void OnPointerDown(PointerEventData eventData)
			{
				eventData.Use();
			}

			public void OnPointerExit(PointerEventData eventData)
			{
				eventData.Use();
			}

			public void OnPointerUp(PointerEventData eventData)
			{
				eventData.Use();
			}
		}
	}

}
