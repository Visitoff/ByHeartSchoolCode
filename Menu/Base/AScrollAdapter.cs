﻿using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Menu
	{
		public abstract class AScrollAdapter : MonoBehaviour
		{
			[SerializeField] public List<GameObject> itemPrefabs;
			[SerializeField] public RectTransform content;
			protected AbstractFactory factory;
			protected AbstractFactory blockFactory;
		}
	}
}
