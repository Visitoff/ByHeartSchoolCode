﻿using ByHeartSchool.PlayerReader;
using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class ItemView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IPointerExitHandler, IBeginDragHandler, IEndDragHandler, IInitializePotentialDragHandler
		{
			[SerializeField] public TextMeshProUGUI name;
			[SerializeField] public Image mainImage;
			[SerializeField] public Image indicatorImage;
			[SerializeField] public RawImage rawImage;
			[SerializeField] public TextMeshProUGUI description;
			[SerializeField] public LayoutElement layoutElement;
			[SerializeField] public TextMeshProUGUI specification;
			[SerializeField] public Button button;
			[SerializeField] public Button button2;
			[SerializeField] public Button button3;
			[SerializeField] public List<Button> buttons;
			[SerializeField] public Toggle toggle;
			[SerializeField] public Toggle childToggle;
			[SerializeField] public int preferredHeight;
			public bool overrideHeight = true;
			public float resizeImg;
			[SerializeField] private Image loadingIndicator;
			[SerializeField] private Image loadingIndicatorBack;
			[SerializeField] private Image specImage;
			[SerializeField] public LoadingIndicator indicator;
			[SerializeField] public ClickableText text;
			[SerializeField] public TranslatedBuble translatedBuble;
			[SerializeField] public TextMeshProUGUI number;
			[SerializeField] public TMP_InputField inputField;
			[SerializeField] public MaxWidth maxWidth;
			[SerializeField] public RectTransform heightRect;
			[SerializeField] public RepeatIndicator repeatIndicator;
			[SerializeField] public RectTransform background;
			[SerializeField] public AB ab;
			public string nameTranslated;
			public string descriptionTranslated;
			public string specificationTranslated;
			private bool _isPress;
			private float _time;
			public event UnityAction OnLongPress;
			public event UnityAction OnLongPressExit;
			private bool onLongPress;
			protected string nameUnTranslated;
			protected string descriptionUnTranslated;
			protected string specificationUnTranslated;
			private Vector2 _position;
			private Camera _camera;
			private Tween _tween;
			private Vector3 scale;
			private bool _lock;
			[SerializeField] private float _longPressTime = 1f;
			[SerializeField] private Image shadow;
			[SerializeField] private List<Image> shadows;
			[SerializeField] public VerticalLayoutGroup verticalLayoutGroup;
			public float fade = 1;
			private List<Tween> _tweens;
			public bool translation;
			[SerializeField] public VideoPlayer videoPlayer;
			public double videoLength;
			public bool translateRightToLeft;
			public bool edit;
			private bool editEnable;
			public GameObject bubleMenu;
			public bool targetRightToLeft;
			[SerializeField] public RectTransform rectTransform;
			public UnityAction onEdit;
			public BtnView btnView;

			public float LongPressTime { get => _longPressTime; set {  _longPressTime = value; } }
			public void ShowLoadingIndicator()
			{
				indicator.StartLoading();
			}
			public void HideLoadingIndicator(bool succesed)
			{
				_tween.Kill();
				if (succesed)
				{
					indicator.Hide();
				}
				else
				{
					indicator.StopLoading();
					indicator.ResetLoading();
				}
			}

			public void OnPointerDown(PointerEventData eventData)
			{
				_position = eventData.position;
				_camera = eventData.pressEventCamera;
				if (GetComponentInParent<ScrollRect>() == null ||  (Math.Abs(GetComponentInParent<ScrollRect>().velocity.y) < 30 && (eventData.delta.x == 0 && eventData.delta.y == 0)))
				{
					if (text != null) text.OnPointerDown(eventData);
					_isPress = true;
					if (shadow != null)
					{
						if (_tweens != null)
						{
							foreach (var tween in _tweens)
							{
								tween.Kill();
							}
						}
						_tweens = new();

						var tw = shadow.DOFade(0.6f, 0.25f);
						var tw1 = transform.DOLocalMoveZ(50, 0.25f);
						_tweens.Add(tw);
						_tweens.Add(tw1);
						foreach (var sh in shadows)
						{
							var t = sh.DOFade(0.6f, 0.25f);
							_tweens.Add(t);
						}
					}
				}

			}
			public void OnPointerUp(PointerEventData eventData)
			{
				if (onLongPress)
				{
					OnLongPressExit?.Invoke();
				}
				else if ( Vector2.Distance(_position, eventData.position) < 30f && _isPress && (eventData.delta.x == 0 && eventData.delta.y == 0))
				{
					if (button != null) button.onClick?.Invoke();
					if (text != null) text.OnPointerClick(eventData);
				}
				_isPress = false;

				if (shadow != null)
				{
					if (_tweens != null)
					{
						foreach (var tween in _tweens)
						{
							tween.Kill();
						}
					}
					_tweens = new();
					var tw = shadow.DOFade(1f, 0.25f);
					var tw1 = transform.DOLocalMoveZ(0, 0.25f);
					_tweens.Add(tw);
					_tweens.Add(tw1);
					foreach (var sh in shadows)
					{
						var t = sh.DOFade(1, 0.25f);
						_tweens.Add(t);
					}
				}
				onLongPress = false;
				_time = 0f;
			}
			private void Update()
			{
				if (_isPress)
				{
					_time += Time.deltaTime;
					if (_time > (edit ? 1.25f : _longPressTime))
					{
						if (!onLongPress)
						{
							onLongPress = true;
							OnLongPress?.Invoke();
							if (text != null) text.OnLongPress(_position, _camera);
							if (translatedBuble != null && !edit) translatedBuble.OnLongPress();
							if (edit)
							{
								editEnable = !editEnable;
								Edit(editEnable);
							}
						}
					}
				}
			}
			public void Edit(bool enable)
			{
                if (enable)
                {
                    onEdit?.Invoke();
                }
                foreach (var btn in buttons)
				{
					btn.gameObject.SetActive(enable);
				}
				if (bubleMenu) bubleMenu.gameObject.SetActive(enable);
				editEnable = enable;
			}
			public void OnDrag(PointerEventData eventData)
			{
				var scrollrect = GetComponentInParent<ScrollRect>();
				if (scrollrect != null) scrollrect.OnDrag(eventData);
				if (_isPress && !onLongPress && Vector2.Distance(_position, eventData.position) >= 30f) OnPointerUp(eventData);
			}

			public void OnPointerExit(PointerEventData eventData)
			{
				OnPointerUp(eventData);
			}

			public void OnBeginDrag(PointerEventData eventData)
			{
				var scrollrect = GetComponentInParent<ScrollRect>();
				if (scrollrect != null) scrollrect.OnBeginDrag(eventData);
			}

			public void OnEndDrag(PointerEventData eventData)
			{
				var scrollrect = GetComponentInParent<ScrollRect>();
				if (scrollrect != null) scrollrect.OnEndDrag(eventData);
			}

			public void OnInitializePotentialDrag(PointerEventData eventData)
			{
				var scrollrect = GetComponentInParent<ScrollRect>();
				if (scrollrect != null) scrollrect.OnInitializePotentialDrag(eventData);
			}
		}
	}
}
