﻿using UnityEngine;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class OrientationEventHandler: MonoBehaviour 
		{
			public UnityAction Horizontal;
			public UnityAction Vertical;
			private bool isHorizontal;
			public bool IsOn { get;  set; }
			private void LateUpdate()
			{
				if (!IsOn) return;
				if ((UnityEngine.Screen.orientation == ScreenOrientation.LandscapeLeft || UnityEngine.Screen.orientation == ScreenOrientation.LandscapeRight) && !isHorizontal)
				{
					isHorizontal = true;
					Horizontal?.Invoke();
				}
				else if ((UnityEngine.Screen.orientation == ScreenOrientation.Portrait || UnityEngine.Screen.orientation == ScreenOrientation.PortraitUpsideDown) && isHorizontal)
				{
					isHorizontal = false;
					Vertical?.Invoke();
				}
			}
			public void ResetOrientation()
			{
				isHorizontal = false;
			}
		}
	}
}
