﻿using ByHeartSchool.Services;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class Item
		{
			public int id;
			public string code;
			public Tuple<string, string> name;
			public Sprite mainImage;
			public Tuple<string, string> description;
			public Tuple<string, string> specification;
			public UnityAction onClick;
			public UnityAction<string,bool, int, int> onTranslated;
			public UnityAction<string, bool> onUnTranslated;
			public UnityAction<string, RectTransform> onFastTranslated;
			public UnityAction<string> onFastUnTranslated;
			public UnityAction onClick2;
			public UnityAction onClick3;
			public int contentTypeIndex;
			public float ratio = -1;
			public bool isOn;
			public bool isOn2;
			public bool empty;
			public UnityAction<bool> toggleOnClick;
			public UnityAction<bool> childToggleOnClick;
			[HideInInspector] public int index;
			public int toSpecialBlock = -1;
			public float fontMultiplyer = 1;
			public ValueWrapper<bool> ss;
			public float pause;
			public bool p;
			public int repeat;
			public string translationLang;
			public int translation_repeat;
			public bool translation;
			public bool toRight;
			public UnityAction<bool> OnTriggered;
			public bool isNull;
			public int micRepeat;
			public bool audioOrderReverse;
			public int cycleIndex;
			public int cycleRepeat;
			public string videoUrl;
			public bool? isTargetLanguageRightToLeft;
			public bool? isTranslationLanguageRightToLeft;
			public bool descriptionTranslated;
			public UnityAction<RectTransform> onAwake;
			public UnityAction onA;
			public UnityAction onB;
			public bool? isOnA;
			public List<string> options;
			public bool background;
			public bool recognitionHardMode;
			public List<UnityAction> onClicks;
			public UnityAction onEdit;
			public float savePause;
			public float volume = 1;
			public bool changeParentBlockHeight;
			public bool raycastTargetMainImage;
			public bool pauseOverrided;
		}
	}
}
