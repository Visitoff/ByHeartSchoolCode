﻿using ByHeartSchool.PlayerReader;
using DG.Tweening;
using SimpleUi.Abstracts;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
        public static class StaticVariablesClicableText
        {
			public static bool onLongPress;
			public static List<ClickableTextController> textControllers = new List<ClickableTextController>();
			public static bool ss;
        }
        public class ClickableTextController
		{
			private string _alpha = "FF";
			private TextMeshProUGUI _text;
			private Dictionary<string,UnityAction> onClickList = new();
			private List<(float, string)> _textValues = new();
			private List<string> _texts = new();
			private List<string> _translates = new();
			private int _index = 0;
			private int _fastIndex = 0;
			private UnityAction<string, bool, int, int> _onTranslate;
			private UnityAction<string,bool> _onEndTranslate;
			private UnityAction<string, RectTransform> _onFastTranslate;
			private UnityAction<string> _onFastEndTranslate;
			private Tween _backTween;
			public bool OnLongPress { get { return StaticVariablesClicableText.onLongPress; } set { StaticVariablesClicableText.onLongPress = value; } }
			private int _controllerIndex;
			public bool ss;
			private bool SS_Swaped {
				get => StaticVariablesClicableText.ss;
				set => StaticVariablesClicableText.ss = value;
			}
			public string GetAlpha() => _alpha;
			public void SetAlpha(string alpha)
			{
				_alpha = alpha;
				Update();
			}
			public ClickableTextController(TextMeshProUGUI text, bool ss)
            {
                _text = text;
				StaticVariablesClicableText.textControllers.Add(this);
				_controllerIndex = StaticVariablesClicableText.textControllers.Count - 1;
				this.ss = ss;
				SS_Swaped = false;
			}
			public void AddText(string text, string translate,UnityAction onClick)
			{
				string newText = "<link=\"" + GenerateLink(onClick) + "\">" + text + "</link>";
				_text.text += newText;
				_text.ForceMeshUpdate();
				_textValues.Add((_text.bounds.size.y, newText));
				_texts.Add(text);
				_translates.Add(translate);
			}
			private string GenerateLink(UnityAction onClick)
			{
				onClickList.Add(onClickList.Count.ToString(), onClick);
				return (onClickList.Count - 1).ToString();
			}
			public (float, string) GetTextValues(int index)
			{
				return _textValues[index];
			}
			public void OnClick(string link)
			{
				_backTween?.Kill();
				onClickList[link]?.Invoke();
			}
			public void SSTranslate()
			{
				if (!ss) return;
				for (int i = 0; i <  _texts.Count; i++)
				{
					SetTextValue(i, !string.IsNullOrEmpty(_translates[i]) ? _translates[i] + " " : "");
				}
			}
			public void SSUnTranslate()
			{
				if (!ss) return;
				for (int i = 0; i < _texts.Count; i++)
				{
					SetTextValue(i, _texts[i]);
				}
			}
			public void Translate(string selectedLink)
			{
				OnLongPress = true;
				_backTween?.Kill();
				if (ss)
				{
					foreach (var controller in StaticVariablesClicableText.textControllers)
					{
						if (StaticVariablesClicableText.textControllers.IndexOf(controller) != _controllerIndex)
						{
							if (controller.ss)
							{
								if (!SS_Swaped)
								{
									controller.SSTranslate();
								}
								else
								{
									controller.SSUnTranslate();
								}	
								
							}
							else
							{
								controller.OtherElementTranslate();
							}
						}
					}
					_index = int.Parse(selectedLink);
					_onTranslate?.Invoke(_translates[_index], ss, _index, _texts.Count);
					if (!SS_Swaped)
					{
						SSTranslate();
					}
					else
					{
						SSUnTranslate();
					}
					SS_Swaped = !SS_Swaped;
				}
				else
				{
					foreach (var controller in StaticVariablesClicableText.textControllers)
					{
						if (StaticVariablesClicableText.textControllers.IndexOf(controller) != _controllerIndex && !controller.ss) controller.OtherElementTranslate();
					}
					SetTextValue(_index, _texts[_index]);
					_index = int.Parse(selectedLink);
					_onTranslate?.Invoke(_translates[_index], ss, _index, _texts.Count);
					SetTextValue(_index, "<color=black>" + _texts[_index] + $"</color><alpha=#{_alpha}>");
				}
			}
			public void OnFastTranslate(string selectedLink)
			{
				if (ss) return;
				_fastIndex = int.Parse(selectedLink);
				_onFastTranslate?.Invoke(_translates[_fastIndex], _text.rectTransform);
			}
			public void OnFastEndTranslate()
			{
				if (ss) return;
				_onFastEndTranslate?.Invoke(_texts[_fastIndex]);
			}
			public void OtherElementTranslate()
			{
				SetTextValue(_index, ss ? _textValues[_index].Item2 :_texts[_index]);
			}
			public void Back(bool isEndTranslate)
			{
				OnLongPress = false;
				if (isEndTranslate)
				{
					DOVirtual.DelayedCall(0.201f, () => 
					{
						foreach (var controller in StaticVariablesClicableText.textControllers)
						{
							controller.OtherElementTranslate();
						}
					});
					_onEndTranslate?.Invoke(_texts[_index], ss);
				}
				if (ss) return;
				_backTween?.Kill();
				_backTween = DOVirtual.DelayedCall(0.2f, () => SetTextValue(_index, _texts[_index]));
			}
			public void UTagSet(string link)
			{
				_backTween?.Kill();
				string newText = $"<alpha=#{_alpha}>";
				for (int i = 0; i < _textValues.Count; i++)
				{
					var item = _textValues[i];
					if (i == int.Parse(link))
					{
						newText += $"<alpha=#FF><u><alpha=#{_alpha}>" + item.Item2 + "</u>";
						continue;
					}
					newText += item.Item2;
				}
				_text.text = newText;
			}
			public void SetTextValue(int index, string newString)
			{
				_backTween?.Kill();
				if (newString.Contains("</link>")) 
					_textValues[index] = (_textValues[index].Item1, newString);
				else
					_textValues[index] = (_textValues[index].Item1, "<link=\"" +index + "\">" + newString + "</link>");
				Update();

			}
			public void Update()
			{
				_backTween?.Kill();
				string newText = $"<alpha=#{_alpha}>";
				foreach (var item in _textValues)
				{
					newText += item.Item2;
				}
				_text.text = newText;
			}
			public void SetTranslateAction(UnityAction<string, bool, int, int> onTranslate)
			{
				_onTranslate = onTranslate;
			}
			public void SetEndTranslateAction(UnityAction<string, bool> onEndTranslate)
			{
				_onEndTranslate = onEndTranslate;
			}
			public void SetFastTranslateAction(UnityAction<string, RectTransform> onTranslate)
			{
				_onFastTranslate = onTranslate;
			}
			public void SetFastEndTranslateAction(UnityAction<string> onEndTranslate)
			{
				_onFastEndTranslate = onEndTranslate;
			}
		}
		public class ClickableText: MonoBehaviour, IPointerUpHandler , IPointerMoveHandler
		{
			[SerializeField] public TextMeshProUGUI text;
			[SerializeField] public bool ss;
			private ClickableTextController _controller;
			public bool TranslateModeOn { get => _controller.OnLongPress; private set => _controller.OnLongPress = value; }
			public string Alpha
			{
				get { return _controller.GetAlpha(); }
				set
				{
					_controller.SetAlpha(value);
				}
			}

			public void SetActions(UnityAction<string, bool, int, int> onTranslate, UnityAction<string, bool> onEndTranslate, UnityAction<string, RectTransform> onFastTranslate, UnityAction<string> onFastEndTranslate)
			{
				_controller.SetEndTranslateAction(onEndTranslate);
				_controller.SetTranslateAction(onTranslate);
				_controller.SetFastTranslateAction(onFastTranslate);
				_controller.SetFastEndTranslateAction(onFastEndTranslate);

			}
			public void AddText(string text, string translate,UnityAction onClick)
			{
				if (_controller == null) _controller = new ClickableTextController(this.text, ss);
				_controller.AddText(text, translate,onClick);
			}
			public void OnPointerClick(PointerEventData eventData)
			{
				int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, eventData.position, eventData.pressEventCamera);
				if (linkIndex == -1)
					return;
				TMP_LinkInfo linkInfo = text.textInfo.linkInfo[linkIndex];
				string selectedLink = linkInfo.GetLinkID();
				if (selectedLink != "")
				{
					_controller.OnClick(selectedLink);
				}
			}
			public void OnLongPress(Vector2 pos, Camera cam)
			{
				int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, pos, cam);
				if (linkIndex == -1)
					return;
				TMP_LinkInfo linkInfo = text.textInfo.linkInfo[linkIndex];
				string selectedLink = linkInfo.GetLinkID();
				if (selectedLink != "")
				{
					_controller.Translate(selectedLink);
				}
				var scrollRect = GetComponentInParent<ScrollRect>();
				if (scrollRect != null) scrollRect.enabled = false;
				if (_controller.ss)
				{
					OnLongPressExit(true);
				}
			}
			public void OnLongPressExit(bool pointerUp)
			{
				_controller.Back(pointerUp);
				var scrollRect = GetComponentInParent<ScrollRect>();
				if (scrollRect != null) scrollRect.enabled = true;
			}
			public void Invoke(int i)
			{
				_controller.OnClick(i.ToString());
			}
			public (float, string) GetTextValues(int index)
			{
				return _controller.GetTextValues(index);
			}
			public void UnClick()
			{
				_controller.Update();
			}
			public void SetTextValue(int index, string newString)
			{
				_controller.SetTextValue(index, newString);
			}
			public void UTagSet(string link)
			{
				_controller.UTagSet(link);
			}

			public void OnPointerUp(PointerEventData eventData)
			{
				if (_controller != null && _controller.OnLongPress && !_controller.ss) OnLongPressExit(true);
			}

			public void OnPointerMove(PointerEventData eventData)
			{
				if (_controller != null && _controller.OnLongPress && !_controller.ss) OnLongPress(eventData.position, eventData.pressEventCamera);
			}

			public void OnPointerDown(PointerEventData eventData)
			{
				int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, eventData.position, eventData.pressEventCamera);
				if (linkIndex == -1)
					return;
				TMP_LinkInfo linkInfo = text.textInfo.linkInfo[linkIndex];
				string selectedLink = linkInfo.GetLinkID();
				if (selectedLink != "")
				{
					_controller?.OnFastTranslate(selectedLink);
					_controller?.OnFastEndTranslate();
				}
			}
		}
	}
}
