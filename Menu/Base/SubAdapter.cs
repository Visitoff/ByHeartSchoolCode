﻿using ByHeartSchool.Menu;
using DG.Tweening;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class SubAdapter : MonoBehaviour
		{
			[SerializeField] private List<GameObject> items = new List<GameObject>();
			private List<GameObject> objects = new List<GameObject>();
			private AbstractFactory factory;
			[SerializeField] private VerticalLayoutGroup _verticalLayoutGroup;
			public void Show(Menu.Item item, int index)
			{
				foreach (GameObject obj in objects)
				{
					Destroy(obj);
				}
				_verticalLayoutGroup.padding.top = -100;
				factory = new ItemFactory(items);
				var itemGo = factory.Create(transform, this.transform, index);
				var itemView = itemGo.GetComponent<Menu.ItemView>();
				if (itemView.number != null) itemView.number.text = 1.ToString() + ".";
				if (itemView.name != null)
				{
					itemView.name.text = item.name != null ? item.name.Item1 : "";
					itemView.name.ForceMeshUpdate(true, true);
				}
				if (itemView.description != null)
				{
					itemView.description.text = item.description != null ? item.description.Item1 : "";
					itemView.description.ForceMeshUpdate(true, true);
				}
				if (itemView.specification != null) itemView.specification.text = item.specification != null ? item.specification.Item1 : "";
				if (itemView.layoutElement != null) itemView.layoutElement.preferredHeight = item.ratio > 0 ? item.ratio * itemView.preferredHeight : itemView.preferredHeight;
				if (itemView.maxWidth != null) itemView.maxWidth.content = _verticalLayoutGroup;
				itemView.nameTranslated = item.name != null ? item.name.Item2 : "";
				itemView.descriptionTranslated = item.description != null ? item.description.Item2 : "";
				itemView.specificationTranslated = item.specification != null ? item.specification.Item2 : "";
				if (itemView.button != null)
				{
					itemView.button.onClick.RemoveAllListeners();
					itemView.button.onClick.AddListener(() =>
					{
						item.onClick?.Invoke();

					});
				}
				if (itemView.toggle != null)
				{
					itemView.toggle.isOn = item.isOn;
					if (item.isOn)
					{
						itemView.childToggle.interactable = true;
					}
					itemView.toggle.onValueChanged.RemoveAllListeners();
					itemView.toggle.onValueChanged.AddListener((b) =>
					{
						item.toggleOnClick?.Invoke(b);
						if (itemView.childToggle != null)
							if (!b)
							{
								itemView.childToggle.isOn = false;
								itemView.childToggle.interactable = false;
							}
							else
							{
								itemView.childToggle.interactable = true;
							}
					});
				}
				if (itemView.childToggle != null)
				{
					itemView.childToggle.isOn = item.isOn2;
					itemView.childToggle.onValueChanged.RemoveAllListeners();
					itemView.childToggle.onValueChanged.AddListener((b) =>
					{
						item.childToggleOnClick?.Invoke(b);
					});
				}
				itemView.gameObject.SetActive(true);
				if (itemView.mainImage != null)
				{
					itemView.mainImage.sprite = item.mainImage;
					var rect = itemView.mainImage.GetComponent<RectTransform>();
					if (itemView.mainImage.sprite != null)
					{
						float deltaImage = itemView.mainImage.sprite.bounds.size.y / itemView.mainImage.sprite.bounds.size.x;
						var deltaRect = rect.rect.height / rect.rect.width;
						var newHeight = rect.rect.width * deltaImage;
						if (item.ratio > 0 ? newHeight < item.ratio * itemView.preferredHeight : false)
						{
							newHeight = item.ratio * itemView.preferredHeight;
						}
						rect.sizeDelta = new Vector2(rect.rect.width, newHeight);
					}
				}
				objects.Add(itemGo);
				Canvas.ForceUpdateCanvases();
			}
		}
	}
}

