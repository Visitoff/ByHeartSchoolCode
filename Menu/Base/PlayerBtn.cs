﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class PlayerBtn : MonoBehaviour, IPointerClickHandler, IDisposable
		{
			public UnityAction onClick;
			public UnityAction onDoubleClick;
			private readonly List<IDisposable> _disposables = new();
			public bool _doubleClicked;

			public void OnPointerClick(PointerEventData eventData)
			{
				Dispose();
				if (_doubleClicked)
				{
					_doubleClicked = false;
					onDoubleClick?.Invoke();
					return;
				}
				_doubleClicked = true;
				Observable.Timer(TimeSpan.FromSeconds(0.2f)).Subscribe(l =>
				{
					onClick?.Invoke();
					_doubleClicked = false;
				}).AddTo(_disposables);
			}
			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
