﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class TranslatedBuble: MonoBehaviour, IPointerMoveHandler, IPointerUpHandler, IPointerExitHandler
		{
			private UnityAction _onTranslate;
			private UnityAction _onEndTranslate;
			[SerializeField] GameObject _trasnslate;
			[SerializeField] TMP_Text _text;
			[SerializeField] ItemView itemView;
			public void SetActions(UnityAction onTranslate, UnityAction onEndTranslate)
			{
				_onEndTranslate = onEndTranslate;
				_onTranslate = onTranslate;
			}
			public void OnLongPress()
			{
				StaticVariablesClicableText.onLongPress = true;
				_trasnslate.SetActive(true);
				_text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 1);
				_onTranslate?.Invoke();
				foreach (var controller in StaticVariablesClicableText.textControllers)
				{
					if (!controller.ss) controller.OtherElementTranslate();
				}
				var scrollRect = itemView.GetComponentInParent<ScrollRect>();
				if (scrollRect != null) scrollRect.enabled = false;
			}
			private void OnLongPressExit()
			{
				if (!StaticVariablesClicableText.onLongPress) return;
				StaticVariablesClicableText.onLongPress = false;
				DOVirtual.DelayedCall(0.201f, () =>
				{
					foreach (var controller in StaticVariablesClicableText.textControllers)
					{
						controller.OtherElementTranslate();
					}
				});
				_onEndTranslate?.Invoke();
				if (!itemView.translation) _trasnslate.SetActive(false);
				_text.color = new Color(_text.color.r, _text.color.g, _text.color.b, itemView.fade);
				var scrollRect = itemView.GetComponentInParent<ScrollRect>();
				if (scrollRect != null) scrollRect.enabled = true;
			}
			public void OnPointerMove(PointerEventData eventData)
			{
				if (StaticVariablesClicableText.onLongPress) OnLongPress();
			}
			public void OnPointerExit(PointerEventData eventData)
			{
				if (StaticVariablesClicableText.onLongPress && !itemView.translation)
				{
					_trasnslate.SetActive(false);
				}
				_text.color = new Color(_text.color.r, _text.color.g, _text.color.b, itemView.fade);
			}
			public void OnPointerUp(PointerEventData eventData)
			{
				if (StaticVariablesClicableText.onLongPress) OnLongPressExit();
			}
		}
	}
}
