﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class ListAdapter : AScrollAdapter, IDisposable
		{
			List<GameObject> itemList = new List<GameObject>();
			List<GameObject> blockList = new List<GameObject>();
			Dictionary<int, (UnityAction<bool>, Transform)> _triggerActions = new();
			[SerializeField] private Transform _triggerTransformStatic;
			[SerializeField] private Transform _triggerTransformDynamic;
			private int _currentTriggger = -1;
			public event UnityAction Update;
			[SerializeField] private float startPosUpdater;
			[SerializeField] public List<GameObject> specialBlocks;
			private bool _updateEnabled;
			private bool _updateTriggered;
			[SerializeField] private LoadingIndicator updater;
			[SerializeField] private Transform updaterStartPos;
			[SerializeField] public ScrollRect scrollRect;
			private readonly List<IDisposable> _disposables = new();
			private float _startUpdateContentPos;
			private float _startContentPosX;
			[SerializeField] public float startContentPositionX = -983.2748f;
			bool _touch;
			private GameObject _item;
			[SerializeField] public VerticalLayoutGroup verticalLayoutGroup;
			[SerializeField] private GameObject _content;
			private bool _clear = true;
			private void Awake()
			{
				_startContentPosX = startContentPositionX;
				FixContentAnchorPos();
			}
			public float GetCurrentYPos()
			{
				return scrollRect.verticalNormalizedPosition;
			}
			private void Start()
			{
				if (updater != null)
				{
					_updateEnabled = true;
					startPosUpdater = updaterStartPos.GetComponent<RectTransform>().position.y;
					updater.root.DOFade(0.5f, 0.5f);
				}
			}
			public ItemView GetItemView(int index)
			{
				return itemList[index].GetComponent<ItemView>();
			}
			public List<ItemView> GetItemViews()
			{
				return itemList.Select(x => x.GetComponent<ItemView>()).ToList();
			}
			public List<LayoutElement> GetBlockList()
			{
				return blockList.Select(x => x.GetComponent<LayoutElement>()).ToList();
			}
			private void UpdateList()
			{
				Update?.Invoke();
				updater.StartLoading();
				_updateTriggered = false;
				scrollRect.enabled = false;
				_startUpdateContentPos = scrollRect.content.transform.position.y;
			}
			public void Updated(bool isSignalRequest)
			{
				updater.StopLoading();
				updater.root.DOFade(0.5f, 0.5f);
				scrollRect.enabled = true;
				scrollRect.elasticity = 0.1f;
				if (!isSignalRequest)
				{
					FixContentAnchorPos();
					scrollRect.content.DOMoveY(_startUpdateContentPos, 0f);
				}

			}
			private void FixContentAnchorPos(float? contentPosY = null, UnityAction action = null, float? indent = null)
			{
				if (startContentPositionX != 0) scrollRect.content.GetComponent<RectTransform>().DOAnchorPosX(_startContentPosX, -1).OnComplete(() =>
				{
					float defaultPosY = 1;

					if (action != null)
					{
						if (verticalLayoutGroup != null)
						{
							LayoutRebuilder.ForceRebuildLayoutImmediate(content);
							Canvas.ForceUpdateCanvases();
							defaultPosY = 1 - verticalLayoutGroup.padding.top / scrollRect.content.rect.height + indent ?? indent.Value;
						}
						scrollRect.DOVerticalNormalizedPos(contentPosY != null ? contentPosY.Value : defaultPosY, -1).OnComplete(() => action?.Invoke());
					}
				});
				else
				{
					action?.Invoke();
				}
			}
			public void SnapTo(int index, UnityAction action = null, float height = -1)
			{
				scrollRect.velocity = Vector3.zero;
				if (index >= itemList.Count)
				{
					scrollRect.DOVerticalNormalizedPos(0, 0.25f).OnComplete(() => action?.Invoke());
					return;
				}
				var itemView = itemList[index].GetComponent<ItemView>();
				if (itemView.layoutElement != null && itemView.layoutElement.ignoreLayout) return;
				var target = itemView.GetComponent<RectTransform>();
				var vertLayoutGroup = itemView.verticalLayoutGroup;
				float spacing = 0;
				float minHeight = 0;
				if (itemView.heightRect != null)
				{
					minHeight = itemView.heightRect.gameObject.activeSelf ? itemView.heightRect.rect.height : 0;
					if (vertLayoutGroup != null) spacing = itemView.heightRect.gameObject.activeSelf ? 0 : vertLayoutGroup.spacing;
				}
				if (itemView.text != null)
				{
					var values = itemView.text.GetTextValues(GetTextIndex(itemView, index));
					scrollRect.content.DOAnchorPosY(
								scrollRect.transform.InverseTransformPoint(scrollRect.content.position).y
								- scrollRect.transform.InverseTransformPoint(target.position).y
								+ scrollRect.GetComponent<RectTransform>().rect.height * height *
								0.475f + values.Item1 + 20, 0.25f).OnComplete(() => action?.Invoke());
				}
				else
				{
					scrollRect.content.DOAnchorPosY(
								scrollRect.transform.InverseTransformPoint(scrollRect.content.position).y
								- scrollRect.transform.InverseTransformPoint(target.position).y
								+ scrollRect.GetComponent<RectTransform>().rect.height * height *
								0.5f + target.rect.height - minHeight + spacing + 150, 0.25f).OnComplete(() => action?.Invoke());
				}
				FixContentAnchorPos();
			}
			public int GetTextIndex(ItemView view, int index)
			{
				int textIndex = -1;
				for (int i = index; i >= 0; i--)
				{
					if (GetItemView(i) == view) textIndex++;
					else break;
				}
				return textIndex;
			}
			public void Clear()
			{
				if (_clear) return;
				Destroy(scrollRect.content.gameObject);
				var newContent = Instantiate(_content, scrollRect.viewport).GetComponent<RectTransform>();
				scrollRect.content = newContent;
				content = newContent;
				updater = newContent.GetComponentInChildren<LoadingIndicator>();
				if (verticalLayoutGroup != null) verticalLayoutGroup = newContent.GetComponent<VerticalLayoutGroup>();
				itemList = new List<GameObject>();
				blockList = new List<GameObject>();
				_triggerActions = new();
			}
			public void Show(List<Item> newItems, float? contentPosY = null, float? indent = null)
			{
				factory = new ItemFactory(itemPrefabs);
				blockFactory = new ItemFactory(specialBlocks);
				StaticVariablesClicableText.textControllers = new();
				scrollRect.elasticity = 0;
				Clear();
				_clear = false;
				var typeBlock = -1;
				var typeText = -1;
				Transform parent = content.transform;
				SpecialBlockView blockView = null;
				for (int i = 0; i < newItems.Count; i++)
				{
					bool ignoreLayout = false;
					if (newItems[i].toSpecialBlock >= 0)
					{
						if (typeBlock != newItems[i].toSpecialBlock)
						{
							var block = blockFactory.Create(content.transform, content.transform, newItems[i].toSpecialBlock);
							blockList.Add(block);
							blockView = block.GetComponent<SpecialBlockView>();
							parent = block.GetComponent<ScrollRect>()?.content ?? block.transform;
							typeBlock = newItems[i].toSpecialBlock;
						}
					}
					else
					{
						blockView = null;
						typeBlock = -1;
						parent = content.transform;
					}
					bool isNull = newItems[i].isNull;
					bool createdNow = false;
					if (newItems[i].p) typeText = -1;
					if (typeText != newItems[i].contentTypeIndex && (!isNull || !itemList.Any()))
					{
						_item = factory.Create(content.transform, parent, newItems[i].contentTypeIndex);
						createdNow = true;
					}

					if (_item == null)
					{
						typeText = -1;
						typeBlock = -1;
						continue;
					}
					_item.transform.localPosition = new Vector3(_item.transform.localPosition.x, _item.transform.localPosition.y, 0);
					itemList.Add(_item);


					var clickableText = _item.GetComponent<ClickableText>();
					if (clickableText != null)
					{
						if (!newItems[i].isNull) typeText = newItems[i].contentTypeIndex;
						if (createdNow) clickableText.text.fontSize *= newItems[i].fontMultiplyer;
						clickableText.AddText(newItems[i].name.Item1, newItems[i].description.Item1, newItems[i].onClick);
						clickableText.SetActions(newItems[i].onTranslated, newItems[i].onUnTranslated, newItems[i].onFastTranslated, newItems[i].onFastUnTranslated);
						clickableText.text.isRightToLeftText = newItems[i].isTargetLanguageRightToLeft ?? false;
						if (newItems[i].isTargetLanguageRightToLeft != null) clickableText.text.alignment = newItems[i].isTargetLanguageRightToLeft.Value ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
					}
					else
					{
						typeText = -1;
					}
					var itemView = _item.GetComponent<ItemView>();
					itemView.translateRightToLeft = newItems[i].isTranslationLanguageRightToLeft ?? false;
					itemView.targetRightToLeft = newItems[i].isTargetLanguageRightToLeft ?? false;
					if (!isNull && itemView.number != null) itemView.number.text = (i + 1).ToString() + ".";
					if (!isNull && itemView.translatedBuble != null)
					{
						int j = i;
						itemView.translatedBuble.SetActions(() => newItems[j].onTranslated?.Invoke(newItems[j].description.Item1, false, 0, 1), () => newItems[j].onUnTranslated?.Invoke("", false));
					}
					if (!isNull && itemView.name != null)
					{
						itemView.name.text = newItems[i].name != null ? newItems[i].name.Item1 : "";
						itemView.name.fontSize *= newItems[i].fontMultiplyer;
						itemView.name.isRightToLeftText = newItems[i].isTargetLanguageRightToLeft ?? false;
						if (newItems[i].isTargetLanguageRightToLeft != null) itemView.name.alignment = newItems[i].isTargetLanguageRightToLeft.Value ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
					}
					if (!isNull && itemView.description != null)
					{
						itemView.description.text = newItems[i].description != null ? newItems[i].description.Item1 : "";
						itemView.description.fontSize *= newItems[i].fontMultiplyer;
						itemView.description.isRightToLeftText = newItems[i].isTargetLanguageRightToLeft ?? false;
						if (newItems[i].descriptionTranslated && newItems[i].isTranslationLanguageRightToLeft != null)
						{
							itemView.description.isRightToLeftText = newItems[i].isTranslationLanguageRightToLeft ?? false;
							itemView.description.alignment = newItems[i].isTranslationLanguageRightToLeft.Value ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
						}
						else if (newItems[i].isTargetLanguageRightToLeft != null) itemView.description.alignment = newItems[i].isTargetLanguageRightToLeft.Value ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
					}
					if (!isNull && itemView.specification != null)
					{
						itemView.specification.text = newItems[i].specification != null ? newItems[i].specification.Item1 : "";
						itemView.specification.fontSize *= newItems[i].fontMultiplyer;
						itemView.specification.isRightToLeftText = newItems[i].isTargetLanguageRightToLeft ?? false;
						if (newItems[i].isTargetLanguageRightToLeft != null) itemView.specification.alignment = newItems[i].isTargetLanguageRightToLeft.Value ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
					}
					if (itemView.layoutElement != null)
					{
						if (!isNull)
						{
							var ratio = newItems[i].ratio > 0 ? newItems[i].ratio * itemView.preferredHeight : itemView.preferredHeight;
							if (itemView.overrideHeight)itemView.layoutElement.preferredHeight = ratio;
							if (blockView != null && blockView.layoutElement && blockView.layoutElement.minHeight < ratio && newItems[i].changeParentBlockHeight) blockView.layoutElement.minHeight = ratio;
						}
						if (itemView.layoutElement.ignoreLayout) ignoreLayout = true;
					}
					itemView.onEdit = newItems[i].onEdit;
					itemView.nameTranslated = newItems[i].name != null ? newItems[i].name.Item2 : "";
					itemView.descriptionTranslated = newItems[i].description != null ? newItems[i].description.Item2 : "";
					itemView.specificationTranslated = newItems[i].specification != null ? newItems[i].specification.Item2 : "";
					if (!isNull && itemView.button != null)
					{
						itemView.button.onClick.RemoveAllListeners();
						int j = i;
						itemView.button.onClick.AddListener(() =>
						{
							newItems[j].onClick?.Invoke();

						});
						if (itemView.mainImage && newItems[j].onClick != null)
						{
							itemView.mainImage.raycastTarget = newItems[j].raycastTargetMainImage;
						}
					}
					if (!isNull && itemView.button2 != null)
					{
						itemView.button2.onClick.RemoveAllListeners();
						int j = i;
						itemView.button2.onClick.AddListener(() =>
						{
							newItems[j].onClick2?.Invoke();

						});
					}
					if (!isNull && itemView.button3 != null && newItems[i].onClick3 != null)
					{
						itemView.button3.onClick.RemoveAllListeners();
						int j = i;
						itemView.button3.onClick.AddListener(() =>
						{
							newItems[j].onClick3?.Invoke();

						});
					}
					if (!isNull && itemView.buttons != null && newItems[i].onClicks != null)
					{
						for (int j = 0; i < (itemView.buttons.Count < newItems[i].onClicks.Count ? itemView.buttons.Count : newItems[i].onClicks.Count); j++)
						{
							var btn = itemView.buttons[i];
							var onClickAction = newItems[i].onClicks[j];
							btn.onClick.AddListener(onClickAction);
						}
					}
					if (!isNull && itemView.toggle != null)
					{
						itemView.toggle.isOn = newItems[i].isOn;
						if (newItems[i].isOn)
						{
							itemView.childToggle.interactable = true;
						}
						itemView.toggle.onValueChanged.RemoveAllListeners();
						int j = i;
						itemView.toggle.onValueChanged.AddListener((b) =>
						{
							newItems[j].toggleOnClick?.Invoke(b);
							if (itemView.childToggle != null)
								if (!b)
								{
									itemView.childToggle.isOn = false;
									itemView.childToggle.interactable = false;
								}
								else
								{
									itemView.childToggle.interactable = true;
								}
						});
					}
					if (!isNull && itemView.childToggle != null)
					{
						itemView.childToggle.isOn = newItems[i].isOn2;
						itemView.childToggle.onValueChanged.RemoveAllListeners();
						int j = i;
						itemView.childToggle.onValueChanged.AddListener((b) =>
						{
							newItems[j].childToggleOnClick?.Invoke(b);
						});
					}
					itemView.gameObject.SetActive(true);
					if (!isNull && itemView.mainImage != null)
					{
						itemView.mainImage.sprite = newItems[i].mainImage;
						var rect = itemView.mainImage.GetComponent<RectTransform>();
						if (itemView.mainImage.sprite != null)
						{
							float deltaImage = itemView.mainImage.sprite.bounds.size.y / itemView.mainImage.sprite.bounds.size.x;
							var deltaRect = rect.rect.height / rect.rect.width;
							var newHeight = rect.rect.width * deltaImage;
							if (newItems[i].ratio > 0 ? newHeight < newItems[i].ratio * itemView.preferredHeight : false)
							{
								newHeight = newItems[i].ratio * itemView.preferredHeight;
							}
							rect.sizeDelta = new Vector2(rect.rect.width, newHeight);
							if (blockView != null && blockView.layoutElement && itemView.resizeImg > 0) rect.localScale = new(itemView.resizeImg, itemView.resizeImg);
						}
					}
					if (!isNull && itemView.videoPlayer != null)
					{
						var renderTexture = new RenderTexture(960, 540, 32, RenderTextureFormat.ARGBFloat);
						renderTexture.Create();
						itemView.videoPlayer.url = newItems[i].videoUrl;
						itemView.videoPlayer.targetTexture = renderTexture;
						itemView.rawImage.texture = renderTexture;
						itemView.videoPlayer.Prepare();
						itemView.videoPlayer.Play();
						itemView.videoPlayer.Pause();
						itemView.videoPlayer.time = 0;
						float newHeight = itemView.rawImage.rectTransform.rect.width / 16 * 9;
						itemView.rawImage.rectTransform.sizeDelta = new Vector2(itemView.rawImage.rectTransform.rect.width, newHeight);
						itemView.layoutElement.preferredHeight = newHeight;
					}
					if (itemView.rectTransform != null)
					{
						newItems[i].onAwake?.Invoke(itemView.rectTransform);
					}
					if (!isNull && itemView.ab != null)
					{
						itemView.ab.onA = newItems[i].onA;
						itemView.ab.onB = newItems[i].onB;
						itemView.ab.isOnA = newItems[i].isOnA;
					}
					if (!isNull && itemView.background != null)
					{
						itemView.background.gameObject.SetActive(newItems[i].background);
						itemView.repeatIndicator.gameObject.SetActive(newItems[i].background);
						itemView.name.alignment = TMPro.TextAlignmentOptions.BottomLeft;
					}
					if (newItems[i].OnTriggered != null)
					{
						_triggerActions.Add(i, (newItems[i].OnTriggered, ignoreLayout ? null : itemView.transform));

					}
					if (_triggerActions.Any())
					{
						var last = _triggerActions.Last();
						if (last.Value.Item2 == null && !ignoreLayout)
						{
							_triggerActions[last.Key] = (last.Value.Item1, itemView.transform);
						}
					}
				}
				scrollRect.onValueChanged.RemoveAllListeners();
				UnityAction action = () =>
				{
					scrollRect.onValueChanged.AddListener((b) =>
				{
					SynchronizeTriggerActions(scrollRect.velocity.y < 0);
					scrollRect.elasticity = 0.05f;
					if (_touch && Input.touchCount == 0)
					{
						_touch = false;
						switch (Math.Abs(scrollRect.velocity.y))
						{
							case < 1500:
								scrollRect.velocity *= 0f;
								break;
							case > 3500:
								scrollRect.velocity *= 2.75f;
								break;
							case > 2250:
								scrollRect.velocity *= 1.65f;
								break;
						}
					}
					else if (!_touch && Input.touchCount != 0)
					{
						_touch = true;
					}
					if (_updateEnabled)
					{
						if (startPosUpdater - updater.transform.position.y > 18 && Input.touchCount != 0)
						{
							updater.root.DOFade(1, 0.5f);
							_updateTriggered = true;
						}
						if (_updateTriggered)
						{
							if (startPosUpdater - updater.transform.position.y < 17 && Input.touchCount == 0)
							{
								if (Application.internetReachability != NetworkReachability.NotReachable) UpdateList();
							}
							else if (startPosUpdater - updater.transform.position.y < 17 && Input.touchCount != 0)
							{
								updater.root.DOFade(0.5f, 0.5f);
								_updateTriggered = false;
							}
						}
					}
				});
				};
				FixContentAnchorPos(contentPosY, action, indent);
			}
			private void SynchronizeTriggerActions(bool isUpScroll)
			{
				if (!_triggerActions.Any(x => x.Value.Item2 != null && x.Value.Item2.position.y >= _triggerTransformStatic.position.y)) return;
				var pair = _triggerActions.LastOrDefault(x =>  x.Value.Item2.position.y >= _triggerTransformStatic.position.y);
				if (_currentTriggger == pair.Key) return;
				_currentTriggger = pair.Key;
				pair.Value.Item1.Invoke(isUpScroll);
			}
			public void InvokeOnClick(int i)
			{
				itemList[i].GetComponent<Button>().onClick.Invoke();
			}
			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
