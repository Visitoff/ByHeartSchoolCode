﻿using SimpleUi.Abstracts;
using UnityEngine;

namespace ByHeartSchool
{
	namespace Menu
	{
		public abstract class AView : UiView
		{
			[SerializeField] public ListAdapter adapter;
			[SerializeField] public ListAdapter courseAdapter;
			[SerializeField] public RectTransform playerContainer;
			[SerializeField] public RectTransform settingsContainer;
			[SerializeField] public EScreenType screenType;
		}
	}
}
