﻿using ByHeartSchool.Menu;
using ByHeartSchool.Services;
using DG.Tweening;
using SimpleUi.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using Zenject;
using System.Threading.Tasks;
using UnityEngine.Video;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace PlayerReader
	{

		public class PlayerController : UiController<PlayerView>, Zenject.IInitializable, IDisposable
		{
			private readonly PlayerView _view;
			private readonly IPlaylistService _playlistService;
			private readonly IScreenService _screenService;
			private readonly ISoundService _soundService;
			private readonly IMediaSceneService _mediaSceneService;
			private readonly ILanguageService _languageService;
			private readonly IFontService _fontService;
			private readonly IPlayerEditService _editService;
			private readonly IRecognizerService _recognizerService;
			private readonly IPlayerService _playerService;
			private readonly IPlayerSettingsService _playerSettingsService;
			private readonly SignalBus _signalBus;
			private List<InPlayerEvent> _inPlayerEvents = new();
			private int currentPhraseIndex = 0;
			private int prevPhraseIndex = 0;
			private List<ByHeartSchool.Menu.Item> _newList = new();
			private bool _footerShowed;
			private bool _isPlaying;
			private readonly List<IDisposable> _disposables = new();
			private Tween _tween;
			private Tween _tweenShowPlayBtn;
			private Tween _tweenOnUnTranslated;
			private ValueWrapper<bool> _verticalNormPosChanging = new(false);
			private List<UnityAction> _actions = new();
			private float _posFooter = 625.575f;
			private List<bool> _selectButtons = new();
			private List<bool> _selectButtonsSets = new();
			private List<bool> _enabledButtons = new();
			private List<string> _textButtons = new();
			private List<string> _textButtonsSets = new();
			private List<int> _spriteIndexes = new();
			private List<string> _phrases = new();
			private List<string> _phrasesTranslate = new();
			private List<string> _userLangs = new();
			private ValueWrapper<bool> _editModeEnabled = new(false);
			private int _isMediaIsActive = -1;
			private bool _isOpen;
			private Dictionary<int, string> _sceneNames = new();
			private Dictionary<int, (string, bool, bool)> _videos = new();
			private Dictionary<int, Sprite> _images = new();
			private Dictionary<int, (AudioClip, bool, bool)> _music = new();
			private List<int> _hideKeys = new();
			private int _playingKey = -1;
			private int _currentSetSetting = -1;
			private bool _ss_swaped;
			private Dictionary<string, int> _types;
			private Dictionary<string, string> _tOptions;
			private Dictionary<string, string> _sOptions;
			private Dictionary<string, string> _t1Options;
			private Dictionary<string, string> _t2Options;
			private bool _playLock;
			private UnityAction<bool> _onComplete;
			private VideoPlayer _mbVideoPlayer;
			private bool _bgVideoPlaying;
			private UnityAction<string> OpenPlaylist;
			private int _currentMicRepeats;
			private Canvas _canvas;
			private int _setsCount;
			private Dictionary<int, int> _setMap = new() { { -1, -1 } };
			private bool _musicIsPaused;
			public PlayerController(
				PlayerView view,
				IPlaylistService playlistService,
				ISoundService soundService,
				IScreenService screenService,
				SignalBus signalBus,
				ILanguageService languageService,
				IMediaSceneService mediaSceneService,
				IFontService fontService,
				IPlayerEditService editService,
				IRecognizerService recognizerService,
				IPlayerService playerService,
				IPlayerSettingsService playerSettingsService)
			{
				_view = view;
				_playlistService = playlistService;
				_soundService = soundService;
				_screenService = screenService;
				_signalBus = signalBus;
				_languageService = languageService;
				_mediaSceneService = mediaSceneService;
				_fontService = fontService;
				_editService = editService;
				_recognizerService = recognizerService;
				_playerService = playerService;
				_playerSettingsService = playerSettingsService;
			}
			public async void Initialize()
			{
				_soundService.MusicIcon = _view.nativePlayerIcon;
				_view.orientationEventHandler.Horizontal = ToHorizontalOrientation;
				_view.orientationEventHandler.Vertical = ToVerticalOrientation;
				_view.backBtn.onClick.AddListener(BackBtn);
				_view.Hide += OnHide;
				_view.playerBtnRightH.onDoubleClick = RightBtn;
				_view.playerBtnLeftH.onDoubleClick = LeftBtn;
				_view.playerBtnRightH.onClick = ShowPlayBtn;
				_view.playerBtnLeftH.onClick = ShowPlayBtn;
				_view.playBtn.onClick.AddListener(PlayBtn);
				_actions = new List<UnityAction>
				{
					PlayBtn,
					DelayBtn,
					RepeatBtn,
					MusicBtn,
					AudioReverseBtn
				};
				_view.gameObject.SetActive(true);
				_editService.onSelest = _playerService.ToPortrait;
				_editService.onDeselest = _playerService.ToLandscape;
				_editService.EditModeEnabled = _editModeEnabled;
				_editService.VerticalPosChanging = _verticalNormPosChanging;
				_recognizerService.IsRecognizePossible = IsRecognizePossible;
				_recognizerService.onStartProcessing = () => { _playerService.SetActiveRepeat(currentPhraseIndex, _currentMicRepeats, ERepeatIndicatorType.MIC, true); };
				_types = Resources.Load<TypesInfo>("Info/TypesInfo").typesMap;
				var optionsInfo = Resources.Load<OptionsInfo>("Info/OptionsInfo");
				_sOptions = optionsInfo.s_optionsMap;
				_tOptions = optionsInfo.t_optionsMap;
				_t1Options = optionsInfo.t1_optionsMap;
				_t2Options = optionsInfo.t2_optionsMap;
				_playerService.ChangeEnableStateOfPlayerButtons = ChangeEnableStateOfPlayerButtons;
				_playerService.ChangeStateOfPlayerButtons = ChangeStateOfPlayerButtons;
			}
			private async UniTask InitSets()
			{
				var sets = (await _playerSettingsService.GetSets()).Data.Where(x => x.shown).OrderBy(x => x.order);
				var items = new List<ByHeartSchool.Menu.Item>();

				_setsCount = sets.Count();
				foreach (var set in sets)
				{
					_selectButtonsSets.Add(true);
					_textButtonsSets.Add(set.name);
					_setMap.Add(set.index, items.Count);
					items.Add(new ByHeartSchool.Menu.Item()
					{
						name = Tuple.Create(set.name, set.name),
						onClick = () => { SettingBtn(set.index); }
					});
				}
				_signalBus.Fire(new MenuSignal(ESignalType.SetListAdapter, items));
			}
			private void ChangeEnableStateOfPlayerButtons(List<bool?> enabledButtons)
			{
				for (int i = 0; i < enabledButtons.Count; i++)
				{
					if (enabledButtons[i].HasValue) _enabledButtons[i] = enabledButtons[i].Value;
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
			}
			private void ChangeStateOfPlayerButtons(List<bool?> selectButtons, List<bool?> enabledButtons, List<int?> spriteIndexes, List<string> textButtons)
			{
				if (selectButtons != null)
				{
					for (int i = 0; i < selectButtons.Count; i++)
					{
						if (selectButtons[i].HasValue) _selectButtons[i] = selectButtons[i].Value;
					}
				}
				if (enabledButtons != null)
				{
					for (int i = 0; i < enabledButtons.Count; i++)
					{
						if (enabledButtons[i].HasValue) _enabledButtons[i] = enabledButtons[i].Value;
					}
				}
				if (spriteIndexes != null)
				{
					for (int i = 0; i < spriteIndexes.Count; i++)
					{
						if (spriteIndexes[i].HasValue) _spriteIndexes[i] = spriteIndexes[i].Value;
					}
				}
				if (textButtons != null)
				{
					for (int i = 0; i < textButtons.Count; i++)
					{
						if (textButtons[i] != null) _textButtons[i] = textButtons[i];
					}
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
			}
			private void RepeatBtn()
			{
				if (_isPlaying) PlayBtn();
				var response = _playerService.SetRepeat();
				if (response.StatusCode == Response.StatusCode.OK && response.Data != 0)
				{
					if (_currentSetSetting >= 0) _textButtonsSets[_setMap[_currentSetSetting]] = _currentSetSetting + "*";
				}
				else
				{
					if (_currentSetSetting >= 0) _textButtonsSets[_setMap[_currentSetSetting]] = _currentSetSetting.ToString();
					_playerService.SetLastPlaylistSettings();
				}
				_spriteIndexes[2] = response.Data;
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerSettingButtons, _selectButtonsSets, _textButtonsSets));
			}
			private void DelayBtn()
			{
				var response = _playerService.SetDelay();
				if (response.StatusCode == Response.StatusCode.OK && response.Data != 0)
				{
					_textButtons[1] = response.Data != 0 ? response.Data.ToString() : "";
					_spriteIndexes[1] = response.Data != 0 ? 1 : 0;
					if (_currentSetSetting >= 0) _textButtonsSets[_setMap[_currentSetSetting]] = _currentSetSetting + (response.Data != 0 ? "*" : "");
				}
				else
				{
					_textButtons[1] = "";
					_spriteIndexes[1] = 0;
					if (_currentSetSetting >= 0) _textButtonsSets[_setMap[_currentSetSetting]] = _currentSetSetting.ToString();
					_playerService.SetLastPlaylistSettings();
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerSettingButtons, _selectButtonsSets, _textButtonsSets));
			}
			private void AudioReverseBtn()
			{
				var response = _playerService.SetAudioReverse();
				if (response.StatusCode == Response.StatusCode.OK && response.Data)
				{
					_spriteIndexes[4] = response.Data ? 1 : 0;
					if (_currentSetSetting >= 0) _textButtonsSets[_setMap[_currentSetSetting]] = _currentSetSetting + (response.Data ? "*" : "");
				}
				else
				{
					_spriteIndexes[4] = 0;
					if (_currentSetSetting >= 0) _textButtonsSets[_setMap[_currentSetSetting]] = _currentSetSetting.ToString();
					_playerService.SetLastPlaylistSettings();
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerSettingButtons, _selectButtonsSets, _textButtonsSets));
			}
			private void MusicBtn()
			{
				_musicIsPaused = !_musicIsPaused;
				if (_musicIsPaused)
				{
					_spriteIndexes[3] = 1;
				}
				else
				{
					_spriteIndexes[3] = 0;
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
				Music(true);
			}
			private void Music(bool isButton = false)
			{
				if (!_isPlaying) return;
				if (_music.Any() && !_musicIsPaused)
				{
					int key = _music.Keys.LastOrDefault(x => x <= currentPhraseIndex && _music[x].Item2);
					if (_music.ContainsKey(key) && (_playingKey != key || isButton))
					{
						var music = _music[key];
						if (_playingKey > 0) _soundService.StopSound(_music[_playingKey].Item1.name);
						_playingKey = key;

						if (music.Item1 != null) _soundService.PlaySound(music.Item1, null, music.Item3, _newList[_playingKey].volume, _playerService.GetAudioTime(_playingKey, currentPhraseIndex));
					}
				}
				else
				{
					foreach (var key in _music.Keys)
					{
						var music = _music[key];
						if (music.Item2 && music.Item1)
						{
							_soundService.StopSound(_music[key].Item1.name);
						}
					}
				}
			}
			private bool IsRecognizePossible()
			{
				return (!_recognizerService.IsRecognize && !(_newList[currentPhraseIndex].micRepeat > 0)) || !_isPlaying;
			}
			private async void SettingBtn(int set)
			{
				if (_isPlaying) PlayBtn();
				_currentSetSetting = _currentSetSetting == set ? -1 : set;
				var response = await _playerService.GetSetSettings(_currentSetSetting);
				var response2 = await _playerService.GetSetSettings(0);
				_spriteIndexes[2] = 0;
				if (response.StatusCode == Response.StatusCode.OK)
				{
					var setList = _setMap.Keys.ToList();
					for (int i = 0; i < _selectButtonsSets.Count; i++)
					{
						_selectButtonsSets[i] = true;
						_textButtonsSets[i] = (setList[i + 1]).ToString();
					}
					_selectButtonsSets[_setMap[set]] = false;
					_playerService.SetSettings(response.Data, 0, _newList.Count);
				}
				else
				{
					_textButtonsSets[_setMap[set]] = set.ToString();
					_selectButtonsSets[_setMap[set]] = true;
					_currentSetSetting = -1;
					if (response2.StatusCode == Response.StatusCode.OK)
					{
						_playerService.SetSettings(response2.Data, 0, _newList.Count);
					}
					_playerService.SetDefaultPlaylistSettings();
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerSettingButtons, _selectButtonsSets, _textButtonsSets));
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
			}
			private void Edit(bool enable, int index)
			{
				_editService.Edit(enable, _view.adapterVertical, _inPlayerEvents, _phrases, _phrasesTranslate, index);
			}
			private void ShowPlayBtn()
			{
				if (_view.playBtn.gameObject.activeSelf)
				{
					_view.playBtn.gameObject.SetActive(false);
					_tweenShowPlayBtn.Kill();
				}
				else
				{
					_view.playBtn.gameObject.SetActive(true);
					_tweenShowPlayBtn = DOVirtual.DelayedCall(3f, () =>
					{
						ShowPlayBtn();
					});
				}
			}
			private void SetHorizontalMediaViewerSize()
			{
				var rectTransform = _view.horizontalViewer.GetComponent<RectTransform>();
				var screen = new Vector2(UnityEngine.Screen.width / _canvas.scaleFactor, UnityEngine.Screen.height / _canvas.scaleFactor);
				rectTransform.sizeDelta = screen;
				if (rectTransform.rect.width / rectTransform.rect.height != 16 / 9)
				{
					if (rectTransform.rect.width / 16 > rectTransform.rect.height / 9)
					{
						var newHeight = rectTransform.rect.width / 16 * 9;
						rectTransform.sizeDelta = new Vector2(rectTransform.rect.width, newHeight);
					}
					else
					{
						var newWidth = rectTransform.rect.height / 9 * 16;
						rectTransform.sizeDelta = new Vector2(newWidth, rectTransform.rect.height);
					}
				}
			}
			private void StartRecognizing()
			{
				_recognizerService.StartRecognizing(currentPhraseIndex, _newList[currentPhraseIndex].recognitionHardMode);
			}
			private void StopRecognizing()
			{
				Dispose();
				_recognizerService.StopRecognizing();
			}
			public void Signal(PlayerSignal signal)
			{
				switch (signal.signalType)
				{
					case ESignalType.Open:
						Show();
						if (_newList.Any()) _view.adapterHorizontal.Show(_newList[currentPhraseIndex], _newList[currentPhraseIndex].contentTypeIndex < 4 ? _newList[currentPhraseIndex].contentTypeIndex : 3);
						break;
					case ESignalType.OpenUpdately:
						OpenPlaylist = signal.OpenPlaylist;
						Show();
						Update(signal.contentPos);
						break;
					case ESignalType.Update:
						UpdateData();
						break;
					case ESignalType.SetRefs:
						_mbVideoPlayer = signal.videoPlayer;
						break;
				}
			}
			private async void UpdateData()
			{
				_recognizerService.Update();
				_userLangs = await _languageService.GetUserLanguages();
				_editService.SelectOvertoneLanguages(_userLangs);
			}
			private async void Update(float? contentPos)
			{
				if (_setsCount == 0) await InitSets();
				if (!_canvas)
				{
					_canvas = _view.GetComponentInParent<Canvas>();
				}
				_view.orientationEventHandler.IsOn = true;
				_playerService.Clear();
				_playLock = !_playlistService.HasAudio();
				_selectButtons = new List<bool> { true, true, true, true, true };
				_enabledButtons = new List<bool> { !_playLock, true, true, false, true };
				_spriteIndexes = new List<int> { 0, 0, 0, 0, 0 };
				_textButtons = new List<string> { "", "", "", "" , ""};
				for (int i = 0; i < _selectButtonsSets.Count; i++)
				{
					_selectButtonsSets[i] = true;
					_textButtonsSets[i] = _textButtonsSets[i].Replace("*", "");
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerSettingButtons, _selectButtonsSets, _textButtonsSets));
				_ss_swaped = false;
				_musicIsPaused = false;
				_currentSetSetting = -1;
				_sceneNames = new();
				_videos = new();
				_music = new();
				_images = new();
				_hideKeys = new();
				_editService.ClearOverrideClips();
				currentPhraseIndex = 0;
				prevPhraseIndex = 0;
				_inPlayerEvents = new();
				_inPlayerEvents.AddRange(_playlistService.GetEvents());
				_newList = new List<ByHeartSchool.Menu.Item>();
				var phrases = new List<string>();
				var phrasesForRecognizerView = new List<string>();
				_phrases = new List<string>();
				_phrasesTranslate = new List<string>();
				var _toSpecialBlock = -1;
				var override_events = await _editService.GetOverrideEvents();
				char bulletChar = 'A';
				int bulletInt = 1;
				char bulletPoint = '•';
				float p_pause = 0;
				_bgVideoPlaying = false;
				List<int> cycleRepeats = new();
				_isMediaIsActive = -1;
				bool isTranslationRightToLeft = _userLangs.Any() ? _languageService.IsRightToLeftLanguage(_userLangs[0]) : false;
				bool isTargetRightToLeft = _languageService.IsRightToLeftLanguage(_languageService.GetCurrentLanguage());
				var clips = new List<AudioClip>();
				bool p_pauseOverrided = false;
				for (int i = 0; i < _inPlayerEvents.Count; i++)
				{
					int j = i;
					UnityAction<bool> OnTriggered = null;
					UnityAction onClick = () =>
					{
						Pause();
						currentPhraseIndex = j;
						PlayBtn();
					};
					bool pauseOverrided = false;
					bool changeParentHeight = false;
					bool raycastTargetMainImage = true;
					UnityAction<RectTransform> onAwake = null;
					bool isNull = false;
					var e = _inPlayerEvents[i];
					string mainText =e.phrase != null && e.phrase.ContainsKey(_languageService.GetCurrentLanguage()) ? _screenService.GetTextTuple(e.phrase, false).Item1 : string.Empty;
					string translatedText = e.phrase != null &&  _userLangs.Any() && e.phrase.ContainsKey(_userLangs[0]) ? _screenService.GetTextTuple(e.phrase, false).Item2 : string.Empty;
					var description = _screenService.GetTextTuple(e.description, false);
					foreach (var e2 in override_events)
					{
						if (e2.ContainsKey(e.playlistId + "/" + e.id + _languageService.GetCurrentLanguage()))
						{
							mainText = e2[e.playlistId + "/" + e.id + _languageService.GetCurrentLanguage()].ToString() + '*';
							_editService.AddOverrideClip(i, null, false);
						}
						if (_userLangs.Any() && e2.ContainsKey(e.playlistId + "/" + e.id + _userLangs.First()))
						{
							translatedText = e2[e.playlistId + "/" + e.id + _userLangs.First()].ToString() + '*';
							_editService.AddOverrideClip(i, null, true);
						}
					}
					EBulletType bullet = EBulletType.None;
					var phraseForRecognizerView = _playlistService.ParsePhraseString(mainText, true, true);
					phrases.Add(_playlistService.ParsePhraseString(mainText));
					mainText = _playlistService.ParsePhraseString(mainText, true);
					translatedText = _playlistService.ParsePhraseString(translatedText, true);
					int type = 0;
					if (e.type != null && _types.ContainsKey(e.type))
						type = _types[e.type];
					var options = new List<string>();
					if (e.options != null)
					{
						foreach (var option in e.options)
							options.Add(option.Replace(" ", string.Empty));
					}
					var toSpecialBlock = -1;
					var pause = 0f;
					int cycleRepeat = 0;
					int cycleIndex = -1;
					ValueWrapper<bool> ss = null;
					bool toRight = false;
					string url = string.Empty;
					if (type != 4)
					{
						pause += p_pause;
						p_pause = 0f;
						pauseOverrided = p_pauseOverrided;
						p_pauseOverrided = false;
					}
					bool background = false;
					bool isBuble = false;
					switch (type)
					{
						case 1:
							isBuble = true;
							goto case 2;
						case 2:
							isBuble = true;
							toRight = true;
							break;
						case 0:
						case 4:
							toSpecialBlock = _toSpecialBlock;
							p_pause += 1f;
							foreach (var option in options)
							{
								if (option.Contains("sec"))
								{
									p_pauseOverrided = true;
									int.TryParse(option.Substring(0, option.IndexOf('s')), out int parsed);
									p_pause = parsed;
								}
								else
								{
									int.TryParse(option, out int parsed);
									p_pause = parsed;
									mainText = new('\n', parsed);
									mainText += '؜';
									type = 47;
								}
								if (e.placement =="MB")
								{
									type = 25;
									isNull = true;
									_hideKeys.Add(i);
									OnTriggered = (b) => MediaTrigger(j, b);
								}
							}
							break;
						case 7:
							toRight = true;
							goto case 6;
						case 6:
							toSpecialBlock = 0;
							pause += 1f;
							break;
						case 3:
							GetTextTypeOptions(options, _tOptions, ref type, ref bullet, e.placement);
							break;
						case 5:
							var unTranslatedText = mainText;
							mainText = translatedText;
							translatedText = unTranslatedText;
							ss = new(true);
							toSpecialBlock = 0;
							GetTextTypeOptions(options, _sOptions, ref type, ref bullet, e.placement);
							break;
						case 15:
							GetTextTypeOptions(options, _t1Options, ref type, ref bullet, e.placement);
							break;
						case 16:
							GetTextTypeOptions(options, _t2Options, ref type, ref bullet, e.placement);
							break;
						case 17:
							ss = new(false);
							toSpecialBlock = 0;
							GetTextTypeOptions(options, _sOptions, ref type, ref bullet, e.placement);
							break;
						case 24:
							toSpecialBlock = _toSpecialBlock;
							isNull = true;
							if (_isMediaIsActive < 0 && e.placement == "MB")
							{
								_isMediaIsActive = i;
							}
							_sceneNames.Add(i, e.media);
							OnTriggered = (b) => MediaTrigger(j, b);
							break;
						case 26:
							isNull = true;
							toSpecialBlock = _toSpecialBlock;
							if (e.clip.ContainsKey("music"))
							{
								bool bg = options.Contains("BG");
								_music.Add(i, (e.clip["music"], bg, options.Contains("LOOP")));
								if (bg) _enabledButtons[3] = true;
							}
							break;
						case 28:
							toSpecialBlock = _toSpecialBlock;
							await _playerService.GetSetting(i, options);
							isNull = true;
							break;
						case 30:
							toSpecialBlock = _toSpecialBlock;
							type = GetMediaType(options, e.placement, e.type);
							if (string.IsNullOrEmpty(mainText))
							{
								onClick = null;
								background = false;
							}
							else
							{
								background = true;
							}
							if (type == 4 && e.placement == "MB")
							{
								_images.Add(i, e.img);
								if (_isMediaIsActive < 0) _isMediaIsActive = i;
								OnTriggered = (b) => MediaTrigger(j, b);
							}
							else
							{
								if ((i != _inPlayerEvents.Count - 1 && _inPlayerEvents[i + 1].type == "IMG" && _inPlayerEvents[i + 1].placement != "MB") || (i != 0 && _inPlayerEvents[i - 1].type == "IMG" && _inPlayerEvents[i - 1].placement != "MB"))
								{
									raycastTargetMainImage = false;
									changeParentHeight = true;
									toSpecialBlock = 2;
								}
							}

							break;
						case 32:
							toSpecialBlock = _toSpecialBlock;
							cycleRepeats.Add(i);
							if (options.Any())
							{
								bool numbered = false;
								for (int k = 0; k < options.Count; k++)
								{
									if (k == 0)
									{
										if (int.TryParse(options[0], out cycleRepeat))
										{
											numbered = true;
											continue;
										}
									}
									await _playerService.GetSetting(i, new List<string> { options[k] });
									if (!numbered) cycleRepeat++;
									break;
								}
								options.RemoveAt(0);
							}
							break;
						case 33:
							toSpecialBlock = _toSpecialBlock;
							if (cycleRepeats.Any())
							{
								cycleIndex = cycleRepeats.Last();
								cycleRepeats.RemoveAt(cycleRepeats.Count - 1);
							}
							break;
						case 34:
							toSpecialBlock = _toSpecialBlock;
							type = GetMediaType(options, e.placement, e.type);
							if (type == 4 && e.placement == "MB")
							{
								bool bg = e.options != null && options.Contains("BG");
								bool loop = e.options != null && options.Contains("LOOP");
								_videos.Add(i, (e.videoUrl, bg, loop));
								if (bg)
								{
									if (_isMediaIsActive < 0) _isMediaIsActive = i;
									OnTriggered = (b) => MediaTrigger(j, b);
								}
							}
							url = e.videoUrl;
							break;
						case 36:
							toSpecialBlock = _toSpecialBlock;
							type = GetMediaType(options, e.placement, e.type);
							url = e.media;
							//onClick = () => _googleMapService.Open(url); 
							//onAwake = async (rectTransform) => await _googleMapService.Get(rectTransform, 50, 50);
							break;
						case 38:
							toSpecialBlock = 1;
							onClick = () => { Hide(); OpenPlaylist?.Invoke(e.media); };
							break;
						case 39:
							onClick = () =>
							{
								ABOnclick(j);
							};
							break;
						case 46:
							toSpecialBlock = 0;
							p_pause += 1f;
							break;
					}
					if (toSpecialBlock != 1 && toSpecialBlock != 2) _toSpecialBlock = toSpecialBlock;
					bool p = false;
					if (bullet != EBulletType.None)
					{
						var bulletString = "";
						switch (bullet)
						{
							case EBulletType.Default:
								bulletString += bulletPoint;
								break;
							case EBulletType.Num:
								bulletString += bulletInt;
								bulletString += ".";
								break;
							case EBulletType.Char:
								bulletString += bulletChar;
								bulletString += ".";
								break;
						}
						mainText = bulletString + "<indent=100>" + mainText;
						phraseForRecognizerView = bulletString + "<indent=100>" + phraseForRecognizerView;
						if (bulletInt > 1)
						{
							mainText = "</indent>" + "\r\n" + mainText;
							phraseForRecognizerView = "</indent>" + "\r\n" + phraseForRecognizerView;
						}
						bulletChar++;
						bulletInt++;
					}

					if (!string.IsNullOrEmpty(mainText)) mainText += " ";
					if (!string.IsNullOrEmpty(phraseForRecognizerView)) phraseForRecognizerView += " ";

					if (type != 22 && type != 23)
					{
						bulletChar = 'A';
						bulletInt = 1;
					}
					_phrasesTranslate.Add(translatedText);
					_phrases.Add(mainText);
					phrasesForRecognizerView.Add(phraseForRecognizerView);
					_newList.Add(new Menu.Item()
					{
						id = currentPhraseIndex,
						name = Tuple.Create(mainText, mainText),
						description = Tuple.Create(translatedText, translatedText),
						specification = description ?? Tuple.Create(translatedText, translatedText),
						contentTypeIndex = type,
						onClick = onClick,
						toSpecialBlock = toSpecialBlock,
						fontMultiplyer = _fontService.GetFontSizeMultiplyer(),
						ss = ss,
						pause = pause,
						savePause = pause,
						p = p,
						onUnTranslated = (s, ss) =>
						{
							_tweenOnUnTranslated.Kill();
							_tweenOnUnTranslated = DOVirtual.DelayedCall(2f, () =>
							{
								_signalBus.Fire(new MenuSignal(ESignalType.DeactivateLine));
							});
							if (ss) return;
							var views = _view.adapterVertical.GetItemViews();
							DOVirtual.DelayedCall(0.201f, CheckTextAndAddUTag);
							foreach (var view in views)
							{
								if (view.text != null)
								{
									view.text.text.DOColor(new Color(0f, 0f, 0f, 1), 0.2f);
								}
								if (!view.translation && view.description != null && view.translatedBuble != null)
								{
									view.description.gameObject.SetActive(false);
								}
							}
						},
						onTranslated = (s, ss, textIndex, linksCount) =>
						{
							_tweenOnUnTranslated.Kill();
							if (_isPlaying) PlayBtn();
							if (ss)
							{
								_signalBus.Fire(new MenuSignal(ESignalType.HideText, s, true));
								_ss_swaped = !_ss_swaped;
								return;
							}
							CheckTextAndRemoveUTag();
							currentPhraseIndex = j + textIndex - (linksCount - 1);
							prevPhraseIndex = currentPhraseIndex;
							if (!isBuble) _signalBus.Fire(new MenuSignal(ESignalType.ShowText, s, true, toRight, isTranslationRightToLeft));
							else _signalBus.Fire(new MenuSignal(ESignalType.HideText, s, true));
							var views = _view.adapterVertical.GetItemViews();
							foreach (var view in views)
							{
								if (view.text != null && !view.text.ss)
								{
									view.text.text.DOColor(new Color(0.8f, 0.8f, 0.8f, 1), 0.2f);
								}
								view.Edit(false);
							}
						},
						onFastUnTranslated = (s) =>
						{
							if (_isPlaying && _newList[currentPhraseIndex].translation)
							{
								return;
							}
							_tweenOnUnTranslated.Kill();
							_tweenOnUnTranslated = DOVirtual.DelayedCall(0.3f, () =>
							{
								_signalBus.Fire(new MenuSignal(ESignalType.HideText));
							});
						},
						onFastTranslated = (s, rt) =>
						{
							_tweenOnUnTranslated.Kill();
							_signalBus.Fire(new MenuSignal(ESignalType.ShowText, s, false, toRight, isTranslationRightToLeft));
						},
						mainImage = e.img,
						ratio = e.img == null ? 0 : e.img.bounds.size.y / e.img.bounds.size.x,
						toRight = toRight,
						isNull = isNull,
						OnTriggered = OnTriggered,
						cycleIndex = cycleIndex,
						cycleRepeat = cycleRepeat,
						videoUrl = url,
						isTranslationLanguageRightToLeft = isTranslationRightToLeft,
						isTargetLanguageRightToLeft = isTargetRightToLeft,
						descriptionTranslated = true,
						onAwake = onAwake,
						onA = () => OnAB(j, "A"),
						onB = () => OnAB(j, "B"),
						isOnA = e.type == "AB" ? await _editService.IsA_ABBubleState(e.id) : null,
						options = options,
						background = background,
						onEdit = () =>
						{
							if (_isPlaying) PlayBtn();
							HideEdit();
							Edit(true, j);
						},
						changeParentBlockHeight = changeParentHeight,
						raycastTargetMainImage = raycastTargetMainImage,
						pauseOverrided = pauseOverrided
					});


				}
				if (_newList.Any()) _view.adapterHorizontal.Show(_newList[currentPhraseIndex], _newList[currentPhraseIndex].contentTypeIndex < 4 ? _newList[currentPhraseIndex].contentTypeIndex : 3);

				var maybefirst = _inPlayerEvents.First(x => x.type != "P" && x.type != "SP" && x.type != "SETTING").type;
				var first = _inPlayerEvents.First().type;
				float indent = 0.01f;
				if (maybefirst == "IMG" && first == "SP") indent = -0.005f;
				else if (maybefirst == "IMG") indent = -0.015f;
				_view.adapterVertical.Show(_newList, contentPos, indent);

				_view.adapterVertical.scrollRect.onValueChanged.RemoveAllListeners();
				_view.adapterVertical.scrollRect.onValueChanged.AddListener((v) =>
				{
					if (!_verticalNormPosChanging.Value && Input.touchCount > 0 && _isPlaying)
						PlayBtn();
					if (_editModeEnabled.Value && !_verticalNormPosChanging.Value && Input.touchCount > 0) _editService.StopEditing(_inPlayerEvents, phrases, _phrasesTranslate, _view.adapterVertical);
				});
				_recognizerService.Init(_view.adapterVertical, _phrases, () => _onComplete.Invoke(true), phrases, phrasesForRecognizerView);
				_playerService.Init(_newList, _inPlayerEvents, _view.adapterVertical.GetItemViews(), new() { _languageService.GetCurrentLanguage(), _userLangs.Any() ? _userLangs[0] : _languageService.GetCurrentLanguage() });
				_playerService.SetDefaultPlaylistSettings();
				CalculatePaddingTop();
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
				if (_isMediaIsActive >= 0 && _isMediaIsActive < 10) MediaTrigger(_isMediaIsActive, false);
				await _editService.Synchronize(_inPlayerEvents, _phrases, false);
				await _editService.Synchronize(_inPlayerEvents, _phrasesTranslate, true);
			}
			private void HideEdit()
			{
				var views = _view.adapterVertical.GetItemViews();
				foreach (var view in views)
				{
					view.Edit(false);
				}
			}
			private void ABOnclick(int index)
			{
				if (_inPlayerEvents[index].type != "AB")
				{
					currentPhraseIndex = index;
					PlayBtn();
				}
				else
				{
					if (_isPlaying) PlayBtn();
					currentPhraseIndex = index;
					ChangeVerticalNormPos();
				}
			}
			private void OnAB(int index, string type)
			{
				var ev = _inPlayerEvents[index];
				ev.type = type;
				_inPlayerEvents[index] = ev;
				_playerService.SetLastPlaylistSettings();
				_playerService.SetLastPlaylistRepeats();
				_editService.SetABBuble(ev.id, type);
				if (_isPlaying || currentPhraseIndex == index)
				{
					ABOnclick(index);
				}
			}
			private async void MediaTrigger(int index, bool isUp)
			{
				if (_isPlaying) return;
				int hideKey = _hideKeys.LastOrDefault(x => x <= index);
				int sceneKey = _sceneNames.Keys.LastOrDefault(x => x <= index);
				int imageKey = _images.Keys.LastOrDefault(x => x <= index);
				int videoKey = _videos.Keys.LastOrDefault(x => x <= index && _videos[x].Item2);
				_bgVideoPlaying = false;
				if (hideKey > videoKey && hideKey > imageKey && hideKey > sceneKey)
				{
					_signalBus.Fire(new MenuSignal(ESignalType.HideMedia));
				}
				else if (videoKey > imageKey && videoKey > sceneKey && _videos.ContainsKey(videoKey))
				{
					_bgVideoPlaying = true;
					ShowMediaBlock(isUp, null, true, _videos[videoKey].Item1, _videos[videoKey].Item3);
					_mediaSceneService.Disable();

				}
				else if (imageKey > sceneKey && _images.ContainsKey(imageKey))
				{
					ShowMediaBlock(isUp, _images[imageKey], false);
					_mediaSceneService.Disable();
				}
				else if (_sceneNames.ContainsKey(sceneKey))
				{
					var response = await _mediaSceneService.Init(_view.cameraViewer, _sceneNames[sceneKey], _inPlayerEvents[index].options);
					ShowMediaBlock(isUp, null, response.Data);
				}
			}
			private int GetMediaType(List<string> options, string placement, string type)
			{
				switch (placement)
				{
					case "BG":
						return _types[type + "-BG"];
					case "MB":
						return _types[type + "-MB"];
				}
				if (options != null && options.Contains("WIDE"))
				{
					return _types[type + "-WIDE"];
				}
				return _types[type];
			}
			private void GetTextTypeOptions(List<string> options, Dictionary<string, string> special_options, ref int type, ref EBulletType bulletType, string placement)
			{
				if (placement == "MB")
				{
					type = 25;
					return;
				}
				if (options == null) return;
				foreach (var option in options)
				{
					if (_sOptions.ContainsKey(option))
					{
						type = _types[special_options[option]];
						if (options.Contains("BULLET"))
						{
							switch (option)
							{
								case "UL":
									bulletType = EBulletType.Default;
									break;
								case "NL":
									bulletType = EBulletType.Num;
									break;
								case "AL":
									bulletType = EBulletType.Char;
									break;
							}
						}
						break;
					}
				}
			}
			private void RightBtn()
			{
				if (currentPhraseIndex == _newList.Count - 1) return;
				currentPhraseIndex++;
				if (string.IsNullOrEmpty(_newList[currentPhraseIndex].name.Item1))
				{
					RightBtn();
					return;
				}
				if (_newList.Any()) _view.adapterHorizontal.Show(_newList[currentPhraseIndex], _newList[currentPhraseIndex].contentTypeIndex < 4 ? _newList[currentPhraseIndex].contentTypeIndex : 3);
				PlayBtn(); PlayBtn();
			}
			private void LeftBtn()
			{
				if (currentPhraseIndex == 0) return;
				currentPhraseIndex--;
				if (string.IsNullOrEmpty(_newList[currentPhraseIndex].name.Item1))
				{
					LeftBtn();
					return;
				}
				if (_newList.Any()) _view.adapterHorizontal.Show(_newList[currentPhraseIndex], _newList[currentPhraseIndex].contentTypeIndex < 4 ? _newList[currentPhraseIndex].contentTypeIndex : 3);
				PlayBtn(); PlayBtn();
			}
			private void Show()
			{
				UnityEngine.Screen.sleepTimeout = SleepTimeout.NeverSleep;
				_view.adapterVertical.GetComponent<RectTransform>().offsetMin = new Vector2(0, _posFooter);
				_isOpen = true;
				var parent = GameObject.Find("PlayerContainer");
				if (parent != null)
				{
					_view.transform.SetParent(parent.transform, false);
					_view.gameObject.SetActive(true);
				}
				_signalBus.Fire(new MenuSignal(ESignalType.BuildToPlayer, _actions, _view.adapterVertical.scrollRect));
				_playerService.ToLandscape();
				Canvas.ForceUpdateCanvases();
			}
			private void PlayBtn()
			{
				if (_isPlaying)
				{
					_view.playBtn.GetComponent<BtnView>().selested.DOFade(1, 0.1f);
					_view.playBtn.GetComponent<BtnView>().unselested.DOFade(0, 0.5f);
					_selectButtons[0] = true;
					Pause();
				}
				else
				{
					_view.playBtn.GetComponent<BtnView>().selested.DOFade(0, 0.1f);
					_view.playBtn.GetComponent<BtnView>().unselested.DOFade(1, 0.5f);
					_selectButtons[0] = false;
					Play(true);
				}
				_signalBus.Fire(new MenuSignal(ESignalType.ChangeStateOfPlayerButtons, _selectButtons, _enabledButtons, _spriteIndexes, _textButtons));
			}
			private void ChangeVerticalNormPos()
			{
				_verticalNormPosChanging.Value = true;
				_view.adapterVertical.SnapTo(currentPhraseIndex, () => _verticalNormPosChanging.Value = false);
			}
			private async void Play(bool ignorePause, bool ss = false, int repeats = 0, int ss_repeats = -1, int mic_repeats = -1)
			{
				HideEdit();
				_bgVideoPlaying = false;
				var prevIt = _newList[prevPhraseIndex];
				CheckTextAndRemoveUTag(prevPhraseIndex);
				_playerService.SetDeactiveRepeat(prevPhraseIndex);
				if (_playLock || !_isOpen)
				{
					currentPhraseIndex = _editService.GetSaveKey();
					return;
				}
				_playLock = true;
				if (currentPhraseIndex == _inPlayerEvents.Count)
				{
					_playerService.RepeatCycle();
					currentPhraseIndex = 0;
					ChangeVerticalNormPos();
					_soundService.PlaySound("cycle", () =>
					{
						ChangeVerticalNormPos();
						Play(false);
					}, false, 0.2f);
					_playLock = false;
					return;
				}
				prevPhraseIndex = currentPhraseIndex;
				var view = _view.adapterVertical.GetItemView(currentPhraseIndex);
				if (view.text != null && view.repeatIndicator != null)
				{
					view.repeatIndicator.MoveY(-(view.text.GetTextValues(_view.adapterVertical.GetTextIndex(view, currentPhraseIndex)).Item1 - 50));
				}
				CheckTextAndRemoveUTag();
				_isPlaying = true;
				var ev = _inPlayerEvents[currentPhraseIndex];
				var item = _newList[currentPhraseIndex];
				if (ev.type == "AB")
				{
					_playLock = false;
					ABOnclick(currentPhraseIndex);
					return;
				}
				if (_recognizerService.IsRecognize || prevIt.micRepeat > 0 || item.micRepeat > 0) StopRecognizing();
				if (mic_repeats < 0) mic_repeats = item.micRepeat;
				if (item.cycleIndex >= 0 && _newList[item.cycleIndex].cycleRepeat > 0)
				{
					_newList[item.cycleIndex].cycleRepeat--;
					if (_newList[item.cycleIndex].options != null && _newList[item.cycleIndex].options.Any())
					{
						_newList[item.cycleIndex].options.RemoveAt(0);
						if (_newList[item.cycleIndex].options.Any())
						{
							await _playerService.GetSetting(item.cycleIndex, new List<string> { _newList[item.cycleIndex].options.First() });
							_playerService.SetLastPlaylistSettings();
						}
					}
					_soundService.PlaySound("cycle", () =>
					{
						ChangeVerticalNormPos();
						Play(false);
					}, false, 0.2f);
					currentPhraseIndex = item.cycleIndex;
					ChangeVerticalNormPos();
					_playLock = false;
					return;
				}
				if (_newList.Any()) _view.adapterHorizontal.Show(item, item.contentTypeIndex < 4 ? item.contentTypeIndex : 3);
				if ((_editService.OverridedClips.ContainsKey(currentPhraseIndex)) || (ev.clip != null && ev.clip.ContainsKey(_languageService.GetCurrentLanguage()) && ev.clip[_languageService.GetCurrentLanguage()] != null))
				{
					_onComplete = (b) =>
					{
						if (item.audioOrderReverse && item.translation_repeat > 0) ss = !ss;
						CheckTextAndRemoveUTag();
						if (b) _soundService.PlaySound("recognized");
						if (item.translation_repeat > 0 && !ss && ss_repeats != 0)
						{
							if (ss_repeats == -1) Play(false, true, repeats, item.translation_repeat > 1 ? item.translation_repeat : 0);
							else Play(false, true, repeats, ss_repeats - 1, mic_repeats);
						}
						else if ((_recognizerService.IsRecognize || item.micRepeat > 0) && !b && mic_repeats > 0)
						{
							_currentMicRepeats = mic_repeats;
							mic_repeats--;
							StartRecognizing();
						}
						else if (item.repeat > 1 && repeats != 1)
						{
							if (repeats <= 0) Play(false, false, item.repeat - 1, ss_repeats, mic_repeats);
							else Play(false, false, repeats - 1, ss_repeats, mic_repeats);
						}
						else if (item.translation_repeat > 0 && ss && ss_repeats > 1)
						{
							Play(false, true, repeats, ss_repeats - 1, mic_repeats);
						}
						else if (b && mic_repeats > 0)
						{
							_currentMicRepeats = mic_repeats;
							mic_repeats--;
							StartRecognizing();
						}
						else
						{
							currentPhraseIndex++;
							Play(false);
						}
					};
					int indexLang = 0;
					string overrideLang = "";
					if (!string.IsNullOrEmpty(item.translationLang))
					{
						string lang = item.translationLang.ToLower();
						if (lang.Contains("user"))
						{
							indexLang = int.Parse(lang[4].ToString()) - 1;
						}
						else
						{
							overrideLang = lang;
						}
					}
					if (item.audioOrderReverse && item.translation_repeat > 0)
					{
						ss = !ss;
						if (ss_repeats == -1 && repeats == 0)
						{
							repeats = item.translation_repeat;
							ss_repeats = item.repeat + 1;
						}
					}
					var userLang = _userLangs.Any() ? _userLangs[indexLang] : _languageService.GetCurrentLanguage();
					var language = ((item.ss != null && (item.ss.Value ^ _ss_swaped)) ^ ss) ? userLang : _languageService.GetCurrentLanguage();

					if (!string.IsNullOrEmpty(overrideLang) && ((item.ss != null && item.ss.Value) || ss)) language = overrideLang;

					AudioClip clip = AudioClip.Create("empty", 1, 8, 1000, false);
					if (item.repeat > 0)
					{
						clip = await _editService.GetOverrideClipAsync(currentPhraseIndex, ss);
						if (clip == null && ev.clip != null && ev.clip.ContainsKey(_languageService.GetCurrentLanguage()) && ev.clip[_languageService.GetCurrentLanguage()] != null)
							clip = ev.clip[language];
					}
					if (!ignorePause && item.pause > 0) _playerService.SetDelayRepeat(currentPhraseIndex, item.pause);
					_playerService.SetActiveRepeat(currentPhraseIndex, ss ^ item.audioOrderReverse ? ss_repeats : repeats, ss ? ERepeatIndicatorType.SS : ERepeatIndicatorType.S, false);
					Observable.Timer(TimeSpan.FromSeconds(ignorePause ? 0 : item.pause)).Subscribe(async l =>
					{
						_playerService.SetActiveRepeat(currentPhraseIndex, ss ^ item.audioOrderReverse ? ss_repeats : repeats, ss ? ERepeatIndicatorType.SS : ERepeatIndicatorType.S, true);
						Music();
						if (_isMediaIsActive <= currentPhraseIndex && (_sceneNames.Any() || _images.Any() || _videos.Any(x => x.Value.Item2)))
						{
							int sceneKey = _sceneNames.Keys.LastOrDefault(x => x <= currentPhraseIndex);
							int imageKey = _images.Keys.LastOrDefault(x => x <= currentPhraseIndex);
							int vidKey = _videos.LastOrDefault(x => x.Key <= currentPhraseIndex && x.Value.Item2).Key;
							int hideKey = _hideKeys.LastOrDefault(x => x <= currentPhraseIndex);
							if (hideKey > vidKey && hideKey > imageKey && hideKey > sceneKey)
							{
								_signalBus.Fire(new MenuSignal(ESignalType.HideMedia));
							}
							else if (vidKey > sceneKey && vidKey > imageKey && _videos.ContainsKey(vidKey))
							{
								_bgVideoPlaying = true;
								ShowMediaBlock(false, null, _mbVideoPlayer == null ? true : _mbVideoPlayer.url != _videos[vidKey].Item1, _videos[vidKey].Item1, _videos[vidKey].Item3);
								_mediaSceneService.Disable();
								if (item.contentTypeIndex != 25)
								{
									if (item.translation)
									{
										_tweenOnUnTranslated.Kill();
										_signalBus.Fire(new MenuSignal(ESignalType.ShowText, _phrasesTranslate[currentPhraseIndex], false, item.toRight, item.isTargetLanguageRightToLeft ?? false));
									}
									else
									{
										_signalBus.Fire(new MenuSignal(ESignalType.HideText));
									}
								}
								else
								{
									_signalBus.Fire(new MenuSignal(ESignalType.ShowText, item.name.Item1, false, item.toRight, item.isTargetLanguageRightToLeft ?? false));
								}
							}
							else if (imageKey > sceneKey && _images.ContainsKey(imageKey))
							{
								ShowMediaBlock(false, _images[imageKey], false);
								_mediaSceneService.Disable();
								if (item.contentTypeIndex != 25)
								{
									if (item.translation)
									{
										_tweenOnUnTranslated.Kill();
										_signalBus.Fire(new MenuSignal(ESignalType.ShowText, _phrasesTranslate[currentPhraseIndex], false, item.toRight, item.isTargetLanguageRightToLeft ?? false));
									}
									else
									{
										_signalBus.Fire(new MenuSignal(ESignalType.HideText));
									}
								}
								else
								{
									_signalBus.Fire(new MenuSignal(ESignalType.ShowText, item.name.Item1, false, item.toRight, item.isTargetLanguageRightToLeft ?? false));
								}
							}
							else if (_sceneNames.ContainsKey(sceneKey))
							{
								var response = await _mediaSceneService.Init(_view.cameraViewer, _sceneNames[sceneKey], _inPlayerEvents[sceneKey].options);
								ShowMediaBlock(false, null, response.Data);
								if (item.contentTypeIndex != 25)
								{
									_mediaSceneService.Play(
									currentPhraseIndex, ev.phrase[_languageService.GetCurrentLanguage()].ToString(),
									clip,
									_inPlayerEvents[currentPhraseIndex].type, !ss);
									if (item.translation)
									{
										_tweenOnUnTranslated.Kill();
										_signalBus.Fire(new MenuSignal(ESignalType.ShowText, _phrasesTranslate[currentPhraseIndex], false, item.toRight, item.isTranslationLanguageRightToLeft ?? false));
									}
									else
									{
										_signalBus.Fire(new MenuSignal(ESignalType.HideText));
									}
								}
								else
								{
									_signalBus.Fire(new MenuSignal(ESignalType.ShowText, item.name.Item1, false, item.toRight, item.isTargetLanguageRightToLeft ?? false));
								}
							}
						}
						else
						{
							if (item.contentTypeIndex == 25) _signalBus.Fire(new MenuSignal(ESignalType.ShowText, item.name.Item1, false, item.toRight, item.isTargetLanguageRightToLeft ?? false));
							else
							{
								if (item.translation)
								{
									_tweenOnUnTranslated.Kill();
									_signalBus.Fire(new MenuSignal(ESignalType.ShowText, _phrasesTranslate[currentPhraseIndex], false, item.toRight, item.isTranslationLanguageRightToLeft ?? false));
								}
								else
								{
									_signalBus.Fire(new MenuSignal(ESignalType.HideText));
								}
							}
						}
						_soundService.PlaySound(clip, () => _onComplete(false), false, item.volume);
						ChangeVerticalNormPos();
					}).AddTo(_disposables);
					ChangeVerticalNormPos();
				}
				else if (_music.ContainsKey(currentPhraseIndex) && !_music[currentPhraseIndex].Item2 && _music[currentPhraseIndex].Item1)
				{
					var music = _music[currentPhraseIndex];
					_soundService.PlaySound(music.Item1, () =>
					{
						currentPhraseIndex++;
						Play(false);
					}, false, item.volume);
				}
				else if (!string.IsNullOrEmpty(item.videoUrl))
				{
					_playerService.SetActiveRepeat(currentPhraseIndex, ss ^ item.audioOrderReverse ? ss_repeats : repeats, ss ? ERepeatIndicatorType.SS : ERepeatIndicatorType.S, true);
					_signalBus.Fire(new MenuSignal(ESignalType.HideText));
					if (view.videoPlayer != null)
					{
						if (view.videoLength == 0)
						{
							view.videoLength = view.videoPlayer.length;
						}
						view.videoPlayer.Stop();
						view.videoPlayer.Play();
						ChangeVerticalNormPos();
					}
					else if (_videos.ContainsKey(currentPhraseIndex))
					{
						if (!_videos[currentPhraseIndex].Item2)
						{
							ShowMediaBlock(false, null, true, _videos[currentPhraseIndex].Item1);
							if (view.videoLength == 0)
							{
								while (!_mbVideoPlayer.isPrepared)
								{
									await Task.Yield();
								}
								view.videoLength = _mbVideoPlayer.length;
							}
						}
					}
					Observable.Timer(TimeSpan.FromSeconds(view.videoLength)).Subscribe(l =>
					{
						CheckTextAndRemoveUTag();
						currentPhraseIndex++;
						Play(false);
					}).AddTo(_disposables);
				}
				else
				{
					Observable.Timer(TimeSpan.FromSeconds(0.2)).Subscribe(l =>
					{
						CheckTextAndRemoveUTag();
						currentPhraseIndex++;
						Play(false);
					}).AddTo(_disposables);
				}
				CheckTextAndAddUTag();
				_playLock = false;
			}
			private void ShowMediaBlock(bool isUp, Sprite sprite, bool newMedia, string videoUrl = "", bool loop = false)
			{

				if (!string.IsNullOrEmpty(videoUrl))
					_signalBus.Fire(new MenuSignal(ESignalType.ShowVideo, sprite, isUp, newMedia, videoUrl, loop));
				else if (sprite != null)
					_signalBus.Fire(new MenuSignal(ESignalType.ShowImage, sprite, isUp, newMedia, videoUrl, loop));
				else
					_signalBus.Fire(new MenuSignal(ESignalType.ShowSceneViewer, sprite, isUp, newMedia, videoUrl, loop));

			}
			private void Pause()
			{
				_playingKey = -1;
				_isPlaying = false;
				Dispose();
				_mediaSceneService.Stop();
				_soundService.StopAllSounds();
				_playerService.SetDeactiveRepeat(prevPhraseIndex);
				if (_view.adapterVertical.GetItemView(currentPhraseIndex).videoPlayer) _view.adapterVertical.GetItemView(currentPhraseIndex).videoPlayer?.Stop();
				if (_recognizerService.IsRecognize || _newList[currentPhraseIndex].micRepeat > 0) StopRecognizing();
				if (!_bgVideoPlaying) _signalBus.Fire(new MenuSignal(ESignalType.StopVideo));
			}
			private void CheckTextAndRemoveUTag(int phraseIndex = -1)
			{
				var itemView = _view.adapterVertical.GetItemView(phraseIndex >= 0 ? phraseIndex : currentPhraseIndex);
				if (itemView != null && itemView.text != null)
				{
					if (itemView.text.TranslateModeOn) return;
					itemView.text.OnLongPressExit(false);
					itemView.text.UnClick();
				}
			}
			private void CheckTextAndAddUTag()
			{
				var itemView = _view.adapterVertical.GetItemView(currentPhraseIndex);
				if (itemView != null && itemView.text != null)
				{
					int textIndex = _view.adapterVertical.GetTextIndex(itemView, currentPhraseIndex);
					itemView.text.UTagSet(textIndex.ToString());
				}
			}
			private void FooterBtn()
			{
				if (_footerShowed)
				{
					_signalBus.Fire(new MenuSignal(ESignalType.Unlock));
					_tween.Kill();
				}
				else
				{
					_signalBus.Fire(new MenuSignal(ESignalType.Lock));
					_tween = DOVirtual.DelayedCall(3f, FooterBtn);
				}
				_footerShowed = !_footerShowed;
			}
			private void ToHorizontalOrientation()
			{
				_view.horizontal.SetActive(true);
				_view.vertical.SetActive(false);
				_signalBus.Fire(new MenuSignal(ESignalType.HideHeader));
				SetHorizontalMediaViewerSize();
				if (_newList.Any()) _view.adapterHorizontal.Show(_newList[currentPhraseIndex], _newList[currentPhraseIndex].contentTypeIndex < 4 ? _newList[currentPhraseIndex].contentTypeIndex : 3);
				Canvas.ForceUpdateCanvases();
			}
			private void ToVerticalOrientation()
			{
				_view.orientationEventHandler.ResetOrientation();
				_view.horizontal.SetActive(false);
				_view.vertical.SetActive(true);
				_signalBus.Fire(new MenuSignal(ESignalType.ShowHeader));
				CalculatePaddingTop();
			}
			private void CalculatePaddingTop()
			{
				var rect = _view.adapterVertical.scrollRect.viewport.rect;
				_view.adapterVertical.verticalLayoutGroup.padding.top = (int)(rect.height);
			}
			public override void OnHide()
			{
				_signalBus.Fire(new MenuSignal(ESignalType.BuildToMenu));
				Hide();
			}
			private void Hide()
			{
				_view.orientationEventHandler.IsOn = false;
				UnityEngine.Screen.sleepTimeout = SleepTimeout.SystemSetting;
				_isOpen = false;
				_playerService.ToPortrait();
				_playLock = false;
				if (_isPlaying) PlayBtn();
				if (_footerShowed) FooterBtn();
				_recognizerService.OnHide();
				_mediaSceneService.Disable();
				if (_recognizerService.IsRecognize || (_newList.Count > currentPhraseIndex && _newList[currentPhraseIndex].micRepeat > 0)) StopRecognizing();
				_playerService.Clear();
				_view.adapterVertical.Clear();
				Dispose();
				_inPlayerEvents = new();
				_soundService.Dispose();
				Resources.UnloadUnusedAssets();
			}
			private void BackBtn()
			{
				ToVerticalOrientation();
				_signalBus.Fire(new MenuSignal(ESignalType.Back));
			}

			public void Dispose()
			{
				_recognizerService.Dispose();
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
			private enum EBulletType
			{
				None = 0,
				Default = 1,
				Num = 2,
				Char = 3,
			}
		}
	}
}
