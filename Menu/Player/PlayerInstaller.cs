﻿using ByHeartSchool.Menu;
using SimpleUi;
using Zenject;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class PlayerInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<PlayerController, PlayerView>(GetComponent<PlayerView>(), transform.parent);
				Container.DeclareSignal<PlayerSignal>();
				Container.BindSignal<PlayerSignal>().ToMethod<PlayerController>(x => x.Signal).FromResolve();
			}
		}
	}
}
