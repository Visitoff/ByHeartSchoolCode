﻿using ByHeartSchool.Menu;
using Recognizer;
using SimpleUi.Abstracts;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class PlayerView : UiView
		{
			[SerializeField] public OrientationEventHandler orientationEventHandler;

			[Header("Vertical")]
			[SerializeField] public ListAdapter adapterVertical;
			[SerializeField] public GameObject vertical;
			[Header("Horizontal")]
			[SerializeField] public GameObject horizontal;
			[SerializeField] public SubAdapter adapterHorizontal;
			[SerializeField] public PlayerBtn playerBtnRightH;
			[SerializeField] public PlayerBtn playerBtnLeftH;
			[SerializeField] public Button playBtn;
			[SerializeField] public Button backBtn;
			[SerializeField] public RawImage horizontalViewer;
			public UnityAction Hide;
			[SerializeField] public RenderTexture cameraViewer;
			[SerializeField] public Sprite nativePlayerIcon;

			protected override void OnDisable()
			{
				Hide?.Invoke();
			}
		}
	}
}
