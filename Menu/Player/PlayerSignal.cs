﻿using ByHeartSchool.Menu;
using UnityEngine.Events;
using UnityEngine.Video;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class PlayerSignal
		{
			public ESignalType signalType;
			public VideoPlayer videoPlayer;
			public UnityAction<string> OpenPlaylist;
			public float? contentPos;
			public PlayerSignal(ESignalType signalType)
            {
                this.signalType = signalType;
            }
			public PlayerSignal(ESignalType signalType, VideoPlayer videoPlayer)
			{
				this.signalType= signalType;
				this.videoPlayer = videoPlayer;
			}
			public PlayerSignal(ESignalType signalType, UnityAction<string> openPlaylist, float? contentPos = null)
			{
				this.signalType = signalType;
				this.OpenPlaylist = openPlaylist;
				this.contentPos = contentPos;
			}
		}
	}
}
