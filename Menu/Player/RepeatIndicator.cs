﻿using DG.Tweening;
using LottiePlugin.UI;
using System.Collections.Generic;
using UnityEngine;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public enum ERepeatIndicatorType
		{
			S,
			SS, 
			MIC
		}
		public class RepeatIndicator: MonoBehaviour
		{
			[SerializeField] private List<GameObject> _repeatLines = new List<GameObject>();
			[SerializeField] private List<GameObject> _repeatLinesMic = new List<GameObject>();
			[SerializeField] private List<RectTransform> _actives = new List<RectTransform>();
			[SerializeField] private List<RectTransform> _activesSS = new List<RectTransform>();
			[SerializeField] private List<RectTransform> _activesMic = new List<RectTransform>();
			[SerializeField] private List<AnimatedImage> _animatedImages = new List<AnimatedImage>();
			[SerializeField] private RectTransform _animatedDelay;
			public void SetLine(int index, ERepeatIndicatorType type)
			{
				for (int i = 0;  i < _repeatLines.Count; i++)
				{
					_repeatLines[i].SetActive(i == index && (type == ERepeatIndicatorType.S || type == ERepeatIndicatorType.SS));
					_repeatLinesMic[i].SetActive(i == index && type == ERepeatIndicatorType.MIC);
				}
			}
			public void SetDelay(float delay)
			{
				SetActive(false);
				_animatedDelay.gameObject.SetActive(true);
				var img = _animatedDelay.GetComponentInChildren<AnimatedImage>();
				img.Stop();
				img.AnimationSpeed = 1 / delay;
				img.Play();
			}
			public void SetActive(bool active, int index = -1, ERepeatIndicatorType type = ERepeatIndicatorType.S, bool anim = true)
			{
				if (anim)_animatedDelay.gameObject.SetActive(false);
				for (int i = 0; i < _actives.Count; i++)
				{
					_actives[i].gameObject.SetActive(active && index == i && type == ERepeatIndicatorType.S);
					_activesSS[i].gameObject.SetActive(active && index == i && type == ERepeatIndicatorType.SS);
					_activesMic[i].gameObject.SetActive(active && index == i && type == ERepeatIndicatorType.MIC);
				}
				Animate(anim);
			}
			private void Animate(bool active)
			{
				if (active)
				{
					foreach (var image in _animatedImages)
					{
						if (image.gameObject.active)
						{
							image.RawImage.enabled = true;
							image.Play();
						}
					}
				}
				else
				{
					foreach (var image in _animatedImages)
					{
						if (image.gameObject.active)
						{
							image.RawImage.enabled = false;
							image.Stop();
						}
					}
				}
			}
			public void MoveY(float y)
			{
				_animatedDelay.anchoredPosition = new Vector2(_animatedDelay.anchoredPosition.x, y);
				for (int i = 0; i < _actives.Count; i++)
				{
					_actives[i].anchoredPosition = new Vector2(_actives[i].anchoredPosition.x, y);
					_activesSS[i].anchoredPosition = new Vector2(_activesSS[i].anchoredPosition.x, y);
					_activesMic[i].anchoredPosition = new Vector2(_activesMic[i].anchoredPosition.x, y);
				}
			}
		}
	}
} 
