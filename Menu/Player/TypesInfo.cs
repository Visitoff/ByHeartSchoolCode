﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
#if PLATFORM_ANDROID
#endif

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		[CreateAssetMenu(fileName = "TypesInfo", menuName = "Info/New TypesInfo")]
		public class TypesInfo : ScriptableObject
		{
			[SerializeField] private List<string> keys;
			[SerializeField] private List<int> types;
			public Dictionary<string, int> typesMap { get { return keys.Zip(types, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v); } }
		}
	}
} 
