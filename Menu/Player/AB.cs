﻿using ByHeartSchool.Menu;
using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class AB: MonoBehaviour, IPointerMoveHandler, IPointerUpHandler
		{
			[SerializeField] private ScrollRect scrollRect;
			[SerializeField] private HorizontalLayoutGroup horizontalLayoutGroup;
			public UnityAction onA;
			public UnityAction onB;
			[SerializeField] private RectTransform _repeatIndicator;
			[SerializeField] private List<RectTransform> _heightTransforms = new();
			[SerializeField] private LayoutElement _layoutElement;
			[SerializeField] private MaxWidth maxWidth;
			public bool? isOnA = null;
			[SerializeField] RectTransform _bubleLayout;
			private void Start()
			{
				Rebuild();
				if (isOnA.HasValue)
				{
					if (isOnA.Value)
					{
						ToA(true);
					}
					else
					{
						ToB(true);
					}
					scrollRect.enabled = true;
				}
			}
			private void Rebuild()
			{
				LayoutRebuilder.ForceRebuildLayoutImmediate(scrollRect.content);
				LayoutRebuilder.ForceRebuildLayoutImmediate(_bubleLayout);
				float preferredHeight = 0;
				foreach (var transform in _heightTransforms)
				{
					if (transform.gameObject.activeSelf) preferredHeight += transform.rect.height;
				}
				_layoutElement.preferredHeight = preferredHeight;
			}
			public void OnPointerMove(PointerEventData eventData)
			{
				if (scrollRect.content.anchoredPosition.x > 200)
				{
					ToB();
				}
				if (scrollRect.content.anchoredPosition.x < -200)
				{
					ToA();
				}
			}
			private void ToA(bool withoutVibration = false)
			{
				horizontalLayoutGroup.padding.left = -250;
				if (!withoutVibration) Vibration.VibratePop();
				onA?.Invoke();
				scrollRect.enabled = false;
				scrollRect.horizontalNormalizedPosition = 0;
				_repeatIndicator.anchorMax = new(0, 1);
				_repeatIndicator.anchorMin = new(0, 0);
				_repeatIndicator.anchoredPosition = new(130, _repeatIndicator.anchoredPosition.y);
				Rebuild();
			}
			private void ToB(bool withoutVibration = false)
			{
				horizontalLayoutGroup.padding.left = 300 + (maxWidth.maxWidth - maxWidth.rectTransform.rect.width).ConvertTo<Int32>();
				if (!withoutVibration) Vibration.VibratePop();
				onB?.Invoke();
				scrollRect.enabled = false;
				scrollRect.horizontalNormalizedPosition = 0;
				_repeatIndicator.anchorMax = new(1, 1);
				_repeatIndicator.anchorMin = new(1, 0);
				_repeatIndicator.anchoredPosition = new(0, _repeatIndicator.anchoredPosition.y);
				Rebuild();
			}

			public void OnPointerUp(PointerEventData eventData)
			{
				scrollRect.enabled = true;
			}
		}
	}
} 
