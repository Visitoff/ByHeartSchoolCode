﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		[CreateAssetMenu(fileName = "OptionsInfo", menuName = "Info/New OptionsInfo")]
		public class OptionsInfo : ScriptableObject
		{
			[SerializeField] private List<string> keys;
			[SerializeField] private List<string> t_options;
			[SerializeField] private List<string> t1_options;
			[SerializeField] private List<string> t2_options;
			[SerializeField] private List<string> s_options;
			public Dictionary<string, string> s_optionsMap { get { return keys.Zip(s_options, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v); } }
			public Dictionary<string, string> t_optionsMap { get { return keys.Zip(t_options, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v); } }
			public Dictionary<string, string> t1_optionsMap { get { return keys.Zip(t1_options, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v); } }
			public Dictionary<string, string> t2_optionsMap { get { return keys.Zip(t2_options, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v); } }
		}
	}
} 
