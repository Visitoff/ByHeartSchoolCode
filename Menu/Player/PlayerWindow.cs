﻿using SimpleUi;

namespace ByHeartSchool
{
	namespace PlayerReader
	{
		public class PlayerWindow : WindowBase
		{
			public override string Name => "Player";
			protected override void AddControllers()
			{
				AddController<PlayerController>();
			}
		}
	}
}
