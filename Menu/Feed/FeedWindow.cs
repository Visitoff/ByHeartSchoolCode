﻿using SimpleUi;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class FeedWindow : WindowBase
		{
			public override string Name => "Feed";
			protected override void AddControllers()
			{
				AddController<FeedController>();
			}
		}
	}
}
