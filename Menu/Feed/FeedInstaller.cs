﻿using SimpleUi;
using Zenject;
namespace ByHeartSchool
{
	namespace Menu
	{
		public class FeedInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindUiView<FeedController, FeedView>(GetComponent<FeedView>(), transform.parent);
				Container.BindInterfacesAndSelfTo<FeedWindow>().AsSingle();
				Container.DeclareSignal<FeedUpdateSignal>().OptionalSubscriber();
				Container.BindSignal<FeedUpdateSignal>().ToMethod<FeedController>(x => x.Update).FromResolve();
			}
		}
	}
}
