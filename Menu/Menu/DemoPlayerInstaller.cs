﻿using Zenject;

namespace ByHeartSchool
{
	namespace DemoPlayer
	{
		public class DemoPlayerInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<DemoPlayerView>().FromInstance(GetComponent<DemoPlayerView>()).AsSingle();
				Container.BindExecutionOrder<DemoPlayerController>(1);
				Container.BindInterfacesAndSelfTo<DemoPlayerController>().AsSingle();
				Container.DeclareSignal<DemoPlayerSignal>();
				Container.BindSignal<DemoPlayerSignal>().ToMethod<DemoPlayerController>(x => x.Signal).FromResolve();
			}
		}
	}

}
