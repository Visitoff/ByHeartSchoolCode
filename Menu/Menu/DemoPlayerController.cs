﻿using ByHeartSchool.Services;
using System.Collections.Generic;
using Zenject;
using UniRx;
using System.Linq;

namespace ByHeartSchool
{
	namespace DemoPlayer
	{
		public class DemoPlayerController: IInitializable
		{
			private readonly IPlaylistService _playlistService;
			private readonly ISceneNavigator _sceneNavigator;
			private List<InPlayerEvent> _phrases;
			private readonly DemoPlayerView _view;
			private int index;
			public DemoPlayerController(IPlaylistService playlistService, DemoPlayerView view, ISceneNavigator sceneNavigator)
			{
				_playlistService = playlistService;
				_view = view;
				_sceneNavigator = sceneNavigator;
			}

			public void Initialize()
			{
			}
			public void Signal(DemoPlayerSignal signal)
			{

			}
		}
	}

}
