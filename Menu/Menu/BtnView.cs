﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.Events;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class BtnView: MonoBehaviour
		{
			[SerializeField] public Image selested;
			[SerializeField] public TextMeshProUGUI text;
			[SerializeField] public Image unselested;
			[SerializeField] public Image btnImage;
			public bool isSelect;
			[SerializeField] public bool isSelectable = true;
			[SerializeField] public List<Sprite> sprites = new();
		}
	}

}
