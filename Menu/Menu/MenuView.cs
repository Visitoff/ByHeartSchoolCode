﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MenuView : MonoInstaller
		{
			[SerializeField] public Canvas canvas;

			[SerializeField] public GameObject container;
			[SerializeField] public GameObject containerChildMenuBtns;
			[SerializeField] public GameObject containerChildPlayerBtns;
			[SerializeField] public List<Button> buttons = new List<Button>();
			[SerializeField] public List<Button> playerButtons = new List<Button>();
			[SerializeField] public List<BtnView> playerButtonViews = new List<BtnView>();
			[SerializeField] public Button backButton;

			[SerializeField] public Button langButton;

			[SerializeField] public RectTransform header;
			[SerializeField] public SwipeHandler swipeHandler;

			[SerializeField] public RawImage rawViewer;
			[SerializeField] public Image imageViewer;
			[SerializeField] public Button mediaButton;
			[SerializeField] public Button mediaButtonOpen;
			[SerializeField] public TMP_Text translateTextMedia;
			[SerializeField] public TMP_Text translateTextMediaOverH;
			[SerializeField] public RectTransform translateTextMediaParentRect;
			[SerializeField] public RectTransform translateTextMediaOverHParentRect;
			[SerializeField] public GameObject overHBackground;

			[SerializeField] public TMP_Text translateText;
			[SerializeField] public Image imageBackground;
			[SerializeField] public Button textButton;
			[SerializeField] public LineArea lineArea;
			[SerializeField] public VideoPlayer videoPlayer;
			[SerializeField] public RenderTexture renderTexture;


			[SerializeField] public ListAdapter setHorizontalAdapter;

			[SerializeField] public GameObject lockImage;
		}
	}
}
