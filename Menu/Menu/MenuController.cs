﻿using ByHeartSchool.Services;
using DG.Tweening;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using Zenject;
using SimpleUi.Signals;
using UnityEngine;
using UniRx;
using UnityEngine.Events;
using ByHeartSchool.Repositories;
using System.Linq;
using ByHeartSchool.PlayerReader;
using Cysharp.Threading.Tasks;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MenuController : IInitializable, IDisposable
		{
			private readonly MenuView _view;
			private readonly SignalBus _signalBus;
			private readonly ISceneNavigator _sceneNavigator;
			private readonly UserManager _userManager;
			private static string _selestedButton = "Feed";
			private readonly List<IDisposable> _disposables = new();
			private event UnityAction HomeEvent;
			private readonly ILanguageService _languageService;
			private readonly IBaseRepository<User> _userRepository;
			private readonly IDeviceButtonsService _buttonsService;
			private readonly IPlaylistService _playlistService;
			private readonly IMediaSceneService _mediaSceneService;
			private readonly IFontService _fontService;
			private readonly IWallpapersService _wallpapersService;
			private User _user;
			private Tween tween;
			private Tween tweenBack;
			private bool langsIsChoised;
			private int _lockCount;
			private bool _swipeHandleLocked;
			private UnityAction _Lock;
			private UnityAction _Unlock;
			private float _footerStartPointY;
			private bool _mediaShow;
			private bool _textShow;
			private bool _imageShow;
			private bool _mediaNotNull;
			private Vector2 _offset;
			private RectTransform _rect;
			private bool _isLineDragged;
			private float y;
			private ScrollRect _scrollRect;
			private float _upPosFooter = 950;
			private bool _footerPosOverrided;
			private const float imageMB_YLocalPos = 132.6649f;
			private const float sceneMB_YLocalPos = 132.6f;
			public MenuController(
				MenuView view,
				SignalBus signalBus,
				ISceneNavigator sceneNavigator,
				UserManager userManager,
				ILanguageService languageService,
				IBaseRepository<User> userRepository,
				IDeviceButtonsService buttonsService,
				IPlaylistService playlistService,
				IMediaSceneService mediaSceneService,
				IFontService fontService,
				IWallpapersService wallpapersService
				)
			{
				_signalBus = signalBus;
				_view = view;
				_sceneNavigator = sceneNavigator;
				_userManager = userManager;
				_languageService = languageService;
				_userRepository = userRepository;
				_buttonsService = buttonsService;
				_playlistService = playlistService;
				_mediaSceneService = mediaSceneService;
				_fontService = fontService;
				_wallpapersService = wallpapersService;
			}
			public void Signal(MenuSignal signal)
			{
				try
				{
					switch (signal.signalType)
					{
						case ESignalType.ShowBackBtn:
							_view.backButton.onClick.RemoveAllListeners();
							_view.backButton.onClick.AddListener(() => _view.swipeHandler.OnBeginDrag(null));
							_buttonsService.BackButtonOnClick(() => _view.swipeHandler.OnBeginDrag(null));
							_view.langButton.enabled = false;
							tweenBack.Kill();
							tweenBack = _view.backButton.GetComponentInChildren<Image>().DOFade(1, 0.5f);
							tween.Kill();
							tween = _view.langButton.GetComponentInChildren<Image>().DOFade(0, 0.5f).OnComplete(() => { _view.backButton.enabled = true; _view.langButton.gameObject.SetActive(false); });
							_view.backButton.gameObject.SetActive(true);
							HomeEvent = signal.homeCallback;
							_view.swipeHandler.beginSwipe = () => { signal.callback.Invoke(); Lock(); };
							_view.swipeHandler.present = signal.present;
							_view.swipeHandler.endSwipe = async (b) =>
							{
								if (!b)
								{
									await signal.next.Invoke();
								}
							};
							_view.swipeHandler.finallySwipe = () => Unlock();
							_view.swipeHandler.Interactable = true;
							if (!signal.isBackScreen) _view.swipeHandler.SwipeLeft(signal.underPresent);
							_swipeHandleLocked = false;
							break;
						case ESignalType.ShowLanguageBtn:
							_buttonsService.BackButtonOnClick(() => Application.Quit());
							_view.backButton.onClick.RemoveAllListeners();
							_view.backButton.onClick.AddListener(() => signal.callback?.Invoke());
							_view.backButton.enabled = false;
							_view.header.gameObject.SetActive(_userManager.Success && langsIsChoised);
							tween.Kill();
							tween = _view.langButton.GetComponentInChildren<Image>().DOFade(1, 0.5f);
							tweenBack.Kill();
							tweenBack = _view.backButton.GetComponentInChildren<Image>().DOFade(0, 0.5f).OnComplete(() => { _view.langButton.enabled = true; _view.backButton.gameObject.SetActive(false); });
							_view.langButton.gameObject.SetActive(_userManager.Success && langsIsChoised);
							HomeEvent = null;
							_view.swipeHandler.beginSwipe = () => { signal.callback?.Invoke(); Lock(); };
							_view.swipeHandler.present = signal.present;
							_view.swipeHandler.endSwipe = async (b) =>
							{
								if (!b)
								{
									await signal.next.Invoke();
								}
							};
							_view.swipeHandler.finallySwipe = () => Unlock();
							_view.swipeHandler.Interactable = false;
							_swipeHandleLocked = true;
							break;
						case ESignalType.CheckLang:
							CheckLang();
							break;
						case ESignalType.CheckAuth:
							CheckAuth();
							break;
						case ESignalType.Lock:
							Lock();
							break;
						case ESignalType.Unlock:
							Unlock();
							break;
						case ESignalType.Message:
							_Lock += signal.Lock;
							_Unlock += signal.Unlock;
							break;
						case ESignalType.BuildToPlayer:
							_mediaNotNull = false;
							_view.containerChildMenuBtns.SetActive(false);
							_view.containerChildPlayerBtns.SetActive(true);
							InitializeLine(signal.scrollRect, _view.lineArea);
							_view.container.GetComponent<RectTransform>().DOAnchorPosY(_footerStartPointY, 0.5f);
							for (int i = 0; signal.playerActions.Count < _view.playerButtons.Count ? i < _view.playerButtons.Count : i < signal.playerActions.Count; i++)
							{
								_view.playerButtons[i].onClick.RemoveAllListeners();
								int j = i;
								var btn = _view.playerButtons[j];
								btn.onClick.AddListener(signal.playerActions[j]);
							}
							var swipeRectPlayer = _view.swipeHandler.GetComponent<RectTransform>();
							swipeRectPlayer.sizeDelta = new Vector2(0, swipeRectPlayer.rect.height);
							swipeRectPlayer.anchoredPosition = new Vector2(-200, swipeRectPlayer.anchoredPosition.y);
							_view.mediaButtonOpen.enabled = true;
							break;
						case ESignalType.ShowSceneViewer:
							ShowSceneViewer(signal.isUp, signal.newMedia);
							break;
						case ESignalType.ShowImage:
							ShowImage(signal.sprite, signal.isUp);
							break;
						case ESignalType.ShowVideo:
							ShowVideo(signal.videoUrl, signal.isUp, signal.newMedia, signal.loop);
							break;
						case ESignalType.HideMedia:
							HideMedia();
							break;
						case ESignalType.ButtonContainerDown:
							_view.container.GetComponent<RectTransform>().DOAnchorPosY(_footerStartPointY, 0.5f);
							break;
						case ESignalType.BuildToMenu:
							HideMedia();
							_view.overHBackground.SetActive(false);
							_footerPosOverrided = false;
							_view.lineArea.isEnabled = false;
							_view.container.GetComponent<RectTransform>().DOAnchorPosY(_footerStartPointY, 0.5f);
							_view.containerChildMenuBtns.SetActive(true);
							_view.containerChildPlayerBtns.SetActive(false);
							_view.mediaButtonOpen.enabled = false;
							var swipeRect = _view.swipeHandler.GetComponent<RectTransform>();
							swipeRect.sizeDelta = new Vector2(265, swipeRect.rect.height);
							swipeRect.anchoredPosition = new Vector2(96, swipeRect.anchoredPosition.y);
							HideMedia();
							HideText();
							break;
						case ESignalType.ShowHeader:
							_view.header.gameObject.SetActive(true);
							_view.swipeHandler.Interactable = true;
							_view.container.SetActive(_userManager.Success && langsIsChoised);
							break;
						case ESignalType.HideHeader:
							_view.header.gameObject.SetActive(false);
							_view.swipeHandler.Interactable = false;
							_view.container.SetActive(false);
							break;
						case ESignalType.Back:
							_view.swipeHandler.Interactable = true;
							_view.swipeHandler.OnBeginDrag(null);
							break;
						case ESignalType.ChangeStateOfPlayerButtons:
							_isLineDragged = false;
							for (int i = 0; i < _view.playerButtons.Count; i++)
							{
								var view = _view.playerButtonViews[i];
								view.isSelect = signal.boolList[i];
								bool enabled = signal.boolList2[i];
								view.gameObject.SetActive(enabled);
								int spriteIndex = signal.intList[i];
								if (view.sprites.Any())
								{
									view.btnImage.sprite = view.sprites[spriteIndex];
								}
								view.text.text = signal.stringList[i];
								if (view.isSelect)
								{
									view.selested.DOFade(0, 0.1f);
									view.unselested.DOFade(1, 0.5f);
								}
								else
								{
									view.selested.DOFade(1, 0.1f);
									view.unselested.DOFade(0, 0.5f);
								}
							}
							break;
						case ESignalType.SwipeHandlerInteractable:
							_view.swipeHandler.Interactable = true;
							_swipeHandleLocked = false;
							break;
						case ESignalType.SwipeHandlerNonInteractable:
							_view.swipeHandler.Interactable = false;
							_swipeHandleLocked = true;
							break;
						case ESignalType.ShowText:
							_view.lineArea.HideTutor();
							_isLineDragged = false;
							ShowText(0.25f, signal.text, signal.toRight, signal.isRightToLeft);
							if (signal.translate) _view.lineArea.Activate();
							break;
						case ESignalType.HideText:
							if (_isLineDragged) break;
							HideText();
							if (!signal.translate) _view.lineArea.Deactivate();
							else _view.lineArea.Activate();
							break;
						case ESignalType.DeactivateLine:
							if (_isLineDragged) break;
							_view.lineArea.Deactivate();
							break;
						case ESignalType.StopVideo:
							_view.videoPlayer.Stop();
							break;
						case ESignalType.SetListAdapter:
							_view.setHorizontalAdapter.Show(signal.items);
							break;
						case ESignalType.ChangeStateOfPlayerSettingButtons:
							var views = _view.setHorizontalAdapter.GetItemViews();
							for (int i = 0; i < views.Count; i++)
							{
								var view = views[i].btnView;
								view.isSelect = signal.boolList[i];
								view.text.text = signal.stringList[i];
								if (view.isSelect)
								{
									view.selested.DOFade(0, 0.1f);
									view.unselested.DOFade(1, 0.5f);
								}
								else
								{
									view.selested.DOFade(1, 0.1f);
									view.unselested.DOFade(0, 0.5f);
								}
							}
							break;
						case ESignalType.SetListAdapterEnable:
							_view.setHorizontalAdapter.gameObject.SetActive(signal.enable);
							break;
						case ESignalType.OpenFeed:
							OpenFeed();
							break;
						case ESignalType.HideFooter:
							_view.container.SetActive(false);
							break;
						case ESignalType.ShowFooter:
							_view.container.SetActive(true);
							break;
					}
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
			private void ShowSceneViewer(bool isUp, bool newMedia)
			{
				_view.videoPlayer.Stop();
				_view.videoPlayer.playOnAwake = false;
				ShowMedia(0.25f, isUp, false, newMedia);

			}
			private void ShowImage(Sprite sprite, bool isUp)
			{
				ShowMedia(0.25f, isUp, sprite, false);
			}
			private void ShowVideo(string videoUri, bool isUp, bool newMedia, bool loop)
			{
				_view.videoPlayer.isLooping = loop;
				ShowMedia(0.25f, isUp, false, newMedia, videoUri);
			}
			private void InitializeLine(ScrollRect scrollRect, LineArea line)
			{
				line.isEnabled = true;
				var rect = _view.container.GetComponent<RectTransform>();
				_rect = scrollRect.GetComponent<RectTransform>();
				_scrollRect = scrollRect;
				line.onDrag = (eventData) =>
				{
					_isLineDragged = true;
					_footerPosOverrided = true;
					var posY = (eventData.position / _view.canvas.scaleFactor - _offset).y;
					if (posY < _view.canvas.pixelRect.height * 0.6f / _view.canvas.scaleFactor && posY >= _footerStartPointY)
					{
						_rect.offsetMin = new Vector2(0, (posY + 700));
						rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, posY);
						_scrollRect.verticalNormalizedPosition -= eventData.delta.y / _scrollRect.content.rect.height;
					}
				};
				line.onPointerDownLongPress = (eventData) =>
				{
					_isLineDragged = true;
					_offset = eventData.position / _view.canvas.scaleFactor - (Vector2)rect.anchoredPosition;
				};
			}
			private void ShowMediaBtn()
			{
				if (!_mediaNotNull) return;
				ShowMedia(0.25f, null, _imageShow, false);
				if (_imageShow)
				{
					_view.rawViewer.DOFade(0, 0.25f);
					_view.imageViewer.DOFade(1, 0.25f);
				}
				else
				{
					_view.rawViewer.DOFade(1, 0.25f);
					_view.imageViewer.DOFade(0, 0.25f);
				}
			}
			private void ShowMedia(float time, bool? isUp, bool imageShow, bool newMedia, string videoUri = "")
			{
				ShowMedia(time, isUp, imageShow ? _view.imageViewer.sprite : null, newMedia, videoUri);
			}
			private void ShowMedia(float time, bool? isUp, Sprite image, bool _newMedia, string videoUri = "")
			{
				bool newMedia = true;
				if (_imageShow == (image != null) && (image == null || image == _view.imageViewer.sprite) && !_newMedia)
				{
					newMedia = false;
				}
				_mediaNotNull = true;
				_imageShow = image != null;
				GameObject mirror = null;
				if (isUp.HasValue && _mediaShow && newMedia)
				{
					mirror = GameObject.Instantiate(_view.imageViewer.gameObject, _view.imageViewer.rectTransform.position, Quaternion.identity, _view.mediaButton.transform);
					if (!_view.imageViewer.gameObject.activeSelf)
					{
						var texture = new Texture2D(_view.rawViewer.texture.width, _view.rawViewer.texture.height, TextureFormat.RGBA32, false);
						RenderTexture.active = (RenderTexture)_view.rawViewer.texture;
						texture.ReadPixels(new Rect(0, 0, _view.rawViewer.texture.width, _view.rawViewer.texture.height), 0, 0);
						texture.Apply();
						mirror.GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
					}
					mirror.SetActive(true);
					if (isUp.Value)
					{
						var rect = mirror.GetComponent<RectTransform>();
						rect.DOAnchorPosY(rect.anchoredPosition.y - rect.rect.height, time).OnComplete(() => GameObject.Destroy(mirror));
					}
					else
					{
						mirror.transform.SetSiblingIndex(1);
					}
				}
				if (_imageShow)
				{
					_view.imageViewer.DOFade(1, time);
					_view.imageViewer.gameObject.SetActive(true);
					_view.rawViewer.gameObject.SetActive(false);
					var rect = _view.imageViewer.rectTransform;
					float deltaImage = image.bounds.size.y / image.bounds.size.x;
					var newHeight = rect.rect.width * deltaImage;
					rect.sizeDelta = new Vector2(rect.rect.width, newHeight);
					if (!_footerPosOverrided)
					{
						float h = newHeight * 9.5f > _view.canvas.pixelRect.height * 0.6f / _view.canvas.scaleFactor ? _view.canvas.pixelRect.height * 0.6f / _view.canvas.scaleFactor : -725 + newHeight * 13.35139f;

						_view.container.GetComponent<RectTransform>().DOAnchorPosY(h, 0.5f);
						_scrollRect.GetComponent<RectTransform>().offsetMin = new Vector2(0, h + 700);
					}
					_view.imageViewer.sprite = image;
					if (isUp.HasValue && !isUp.Value && newMedia)
					{
						_view.imageViewer.rectTransform.anchoredPosition = new Vector2(-0.0014963f, imageMB_YLocalPos - newHeight);
						_view.imageViewer.rectTransform.DOAnchorPosY(imageMB_YLocalPos, time).OnComplete(() => GameObject.Destroy(mirror));
					}
				}
				else
				{
					_view.rawViewer.DOFade(1, time);
					_view.rawViewer.gameObject.SetActive(true);
					_view.imageViewer.gameObject.SetActive(false);
					if (!_footerPosOverrided)
					{
						_view.container.GetComponent<RectTransform>().DOAnchorPosY(354, 0.5f);
						_scrollRect.GetComponent<RectTransform>().offsetMin = new Vector2(0, 1054);
					}
					if (isUp.HasValue && !isUp.Value && newMedia)
					{
						_view.rawViewer.rectTransform.anchoredPosition = new Vector2(-0.2699924f, sceneMB_YLocalPos - _view.rawViewer.rectTransform.rect.height);
						_view.rawViewer.rectTransform.DOAnchorPosY(sceneMB_YLocalPos, time).OnComplete(() => GameObject.Destroy(mirror));
					}
				}
				_view.mediaButton.gameObject.SetActive(true);
				_view.mediaButton.onClick.RemoveAllListeners();
				DOVirtual.DelayedCall(time, () =>
				{
					_view.mediaButton.interactable = true;
					_view.mediaButton.onClick.AddListener(HideMedia);
				});
				if (!string.IsNullOrEmpty(videoUri) && newMedia)
				{
					_view.videoPlayer.url = videoUri;
					_view.videoPlayer.targetTexture = _view.renderTexture;
					_view.videoPlayer.Prepare();
					_view.videoPlayer.Stop();
					_view.videoPlayer.Play();
					_view.videoPlayer.playOnAwake = true;
					_signalBus.Fire(new PlayerSignal(ESignalType.SetRefs, _view.videoPlayer));
				}
				else if (newMedia)
				{
					_view.videoPlayer.url = "";
					_view.videoPlayer.Stop();
					ClearOutRenderTexture(_view.videoPlayer.targetTexture);
					_view.videoPlayer.targetTexture = null;
				}
				_mediaShow = true;
			}
			private void ClearOutRenderTexture(RenderTexture renderTexture)
			{
				RenderTexture rt = RenderTexture.active;
				RenderTexture.active = renderTexture;
				GL.Clear(true, true, Color.clear);
				RenderTexture.active = rt;
			}
			private void HideMedia()
			{
				_view.mediaButton.interactable = false;
				_view.imageViewer.DOFade(0, 0.25f);
				_view.rawViewer.DOFade(0, 0.25f);
				HideText(true);
				DOVirtual.DelayedCall(0.25f, () =>
				{
					_view.mediaButton.gameObject.SetActive(false);
				});
				_mediaShow = false;
			}
			private void ShowText(float time, string text, bool toRight, bool isRightToLeft)
			{
				_view.textButton.gameObject.SetActive(true);
				_view.textButton.onClick.RemoveAllListeners();
				_view.imageBackground.DOFade(1, 0.25f);
				_view.translateText.DOFade(1, 0.25f);

				_view.translateTextMedia.margin = toRight ? new Vector4(30, _view.translateTextMedia.margin.y, _view.translateTextMedia.margin.z, 5) :
					new Vector4(10, _view.translateTextMedia.margin.y, _view.translateTextMedia.margin.z, 5);

				_view.translateText.margin = toRight ? new Vector4(30, _view.translateText.margin.y, _view.translateText.margin.z, _view.translateText.margin.w) :
					new Vector4(10, _view.translateText.margin.y, _view.translateText.margin.z, _view.translateText.margin.w);

				_view.translateTextMediaOverH.margin = toRight ? new Vector4(30, _view.translateTextMediaOverH.margin.y, _view.translateTextMediaOverH.margin.z, _view.translateTextMediaOverH.margin.w) :
					new Vector4(10, _view.translateTextMediaOverH.margin.y, _view.translateTextMediaOverH.margin.z, _view.translateTextMediaOverH.margin.w);


				_view.translateText.isRightToLeftText = isRightToLeft;
				_view.translateTextMediaOverH.isRightToLeftText = isRightToLeft;
				_view.translateTextMedia.isRightToLeftText = isRightToLeft;

				_view.translateText.alignment = isRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
				_view.translateTextMediaOverH.alignment = isRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;
				_view.translateTextMedia.alignment = isRightToLeft ? TMPro.TextAlignmentOptions.Right : TMPro.TextAlignmentOptions.Left;

				_view.translateTextMedia.text = _view.translateTextMediaOverH.text == text ? "" : text;
				_view.translateText.text = text;
				_view.translateTextMediaOverH.text = _view.translateTextMediaOverH.text == text ? text : "";
				_view.translateText.fontSize = 6 * _fontService.GetFontSizeMultiplyer();
				_view.translateTextMedia.fontSize = 6 * _fontService.GetFontSizeMultiplyer();
				LayoutRebuilder.ForceRebuildLayoutImmediate(_view.translateTextMediaParentRect);
				if (_view.translateTextMedia.rectTransform.rect.height > 32)
				{
					_view.translateTextMedia.text = "";
					_view.translateTextMediaOverH.text = text;
					_view.translateTextMediaOverH.fontSize = 6 * _fontService.GetFontSizeMultiplyer();
				}

				_view.overHBackground.SetActive(!string.IsNullOrEmpty(_view.translateTextMediaOverH.text));

				_view.translateTextMediaParentRect.anchoredPosition =  new(_view.translateTextMediaParentRect.anchoredPosition.x, _imageShow ? _view.imageViewer.rectTransform.anchoredPosition.y - _view.imageViewer.rectTransform.rect.height  : _view.rawViewer.rectTransform.anchoredPosition.y - _view.rawViewer.rectTransform.rect.height);
				_view.translateTextMediaOverHParentRect.anchoredPosition = new(_view.translateTextMediaOverHParentRect.anchoredPosition.x, _imageShow ? _view.imageViewer.rectTransform.anchoredPosition.y - _view.imageViewer.rectTransform.rect.height + 24 : _view.rawViewer.rectTransform.anchoredPosition.y - _view.rawViewer.rectTransform.rect.height + 24);
				DOVirtual.DelayedCall(time, () =>
				{
					_view.textButton.interactable = true;
					_view.textButton.onClick.AddListener(() => HideText());
				});
				_textShow = true;
			}
			private void HideText(bool isHideMedia = false)
			{
				_view.textButton.interactable = false;
				_view.imageBackground.DOFade(0, 0.25f);
				_view.translateText.DOFade(0, 0.25f);
				DOVirtual.DelayedCall(0.25f, () =>
				{
					_view.translateText.text = "";
					_view.textButton.gameObject.SetActive(false);
					if (!isHideMedia)
					{
						_view.translateTextMediaOverH.text = "";
						_view.translateTextMedia.text = "";
					}
				});
				_textShow = false;
			}
			private void Lock()
			{
				_lockCount++;
				if (_lockCount > 0)
				{
					_view.lockImage.SetActive(true);
					_view.backButton.interactable = false;
					_view.langButton.interactable = false;
					_view.swipeHandler.Interactable = false;
					_Lock?.Invoke();
					foreach (Button button in _view.buttons)
					{
						button.interactable = false;
					}
					foreach (Button button in _view.playerButtons)
					{
						button.interactable = false;
					}
				}
			}
			private void Unlock()
			{
				if (_lockCount > 0)
				{
					_lockCount--;
					if (_lockCount == 0)
					{
						_view.lockImage.SetActive(false);
						_view.backButton.interactable = true;
						_view.swipeHandler.Interactable = !_swipeHandleLocked;
						_view.langButton.interactable = true;
						_Unlock?.Invoke();
						foreach (Button button in _view.buttons)
						{
							button.interactable = true;
						}
						foreach (Button button in _view.playerButtons)
						{
							button.interactable = true;
						}
					}
				}
			}
			public void Initialize()
			{
				Input.multiTouchEnabled = false;
				UnityEngine.Screen.autorotateToPortraitUpsideDown = false;
				UnityEngine.Screen.autorotateToPortrait = true;
				UnityEngine.Screen.autorotateToLandscapeLeft = false;
				UnityEngine.Screen.autorotateToLandscapeRight = false;

				UnityEngine.Screen.orientation = ScreenOrientation.Portrait;
#if UNITY_ANDROID
				ApplicationChrome.statusBarState = ApplicationChrome.States.Visible;
				ApplicationChrome.navigationBarState = ApplicationChrome.States.Visible;
#endif
				Vibration.Init();
				_sceneNavigator.OnSceneLoadingStart += () => { _signalBus.Fire(new LoadingSignal(LoadingSignalType.SetImage, "profile")); _signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project); if (this._view != null) Lock(); };
				_sceneNavigator.OnSceneLoadingEnd += () => { _signalBus.BackWindow(EWindowLayer.Project); if (this._view != null) Unlock(); };
				_playlistService.OnStartUpdate += (b) => { _signalBus.Fire(new LoadingSignal(LoadingSignalType.SetImage, b ? "profile":_languageService.GetCurrentLanguage())); _signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project); };
				_playlistService.OnEndUpdate += () => { _signalBus.BackWindow(EWindowLayer.Project); };
				_signalBus.Fire(new LoadingSignal(LoadingSignalType.SetImage, _languageService.GetCurrentLanguage()));
				_signalBus.OpenWindow<LoadingWindow>(EWindowLayer.Project);
				_view.langButton.onClick.AddListener(() => OpenAppLangSelector());
				_footerStartPointY = _view.container.GetComponent<RectTransform>().anchoredPosition.y;
				foreach (Button button in _view.buttons)
				{
					button.onClick.AddListener(() =>
					{
						foreach (Button button2 in _view.buttons)
						{
							if (button2 != button)
							{
								button2.GetComponent<BtnView>().text.DOFade(0, 0.5f);
								button2.GetComponent<BtnView>().selested.DOFade(0, 0.1f);
								button2.GetComponent<BtnView>().unselested.DOFade(1, 0.5f);
							}
							else
							{
								button2.GetComponent<BtnView>().text.text = button2.name;
								button2.GetComponent<BtnView>().text.DOFade(1, 0.5f);
								button2.GetComponent<BtnView>().selested.DOFade(1, 0.1f);
								button2.GetComponent<BtnView>().unselested.DOFade(0, 0.5f);
							}
						}
						if (_selestedButton == button.name)
						{
							HomeEvent?.Invoke();
						}
						else
						{
							_signalBus.OpenWindow(button.name);
						}
						_selestedButton = button.name;
					});
				}
				if (_userManager.Success)
				{
					CheckLang();
					CheckAuth();
				}
				_userManager.Initialized += CheckAuth;
				_userManager.Initialized += CheckLang;
				_userManager.LogoutCompleted += CheckAuth;
				_userManager.LogoutCompleted += CheckLang;
				_view.mediaButtonOpen.onClick.AddListener(ShowMediaBtn);

			}
			private void OpenAppLangSelector()
			{
				UnityAction saveAction = async () =>
				{
					_view.container.SetActive(true);
					_signalBus.Fire(new AppLangSelectorSignal(ESignalType.Close, null));
					_signalBus.Fire(new MenuSignal(ESignalType.ShowLanguageBtn));
					Lock();
					await _playlistService.Update();
					_signalBus.Fire(new LearningUpdateSignal());
					_signalBus.Fire(new GamesUpdateSignal());
					_signalBus.Fire(new OtherUpdateSignal());
					_signalBus.Fire(new FeedUpdateSignal());
					_signalBus.Fire(new MyUpdateSignal());
					_signalBus.Fire(new PlayerSignal(ESignalType.Update));
					Unlock();

				};
				UnityAction unSaveAction = async () =>
				{
					_view.container.SetActive(true);
					_signalBus.Fire(new AppLangSelectorSignal(ESignalType.Close, null));
					_signalBus.Fire(new MenuSignal(ESignalType.ShowLanguageBtn));
				};
				_signalBus.Fire(new AppLangSelectorSignal(ESignalType.Open, saveAction));
				_signalBus.Fire(new MenuSignal(ESignalType.ShowBackBtn, unSaveAction));
				_view.container.SetActive(false);

			}
			private async void CheckLang()
			{
				langsIsChoised = false;
				if (_userManager.Success)
				{
					_user = await _userRepository.Get(_userManager.User.UserId);
					var prefs = (Dictionary<string, object>)_user.prefs;
					if (prefs.ContainsKey("user-languages"))
					{
						_view.container.SetActive(true);
						_view.langButton.gameObject.SetActive(true);
						langsIsChoised = true;
					}
					else
					{
						OpenSettings();
					}
				}
			}
			private void OpenSettings()
			{
				_signalBus.Fire(new SettingsSignal(ESignalType.Open, "profile"));
			}
			private void OpenFeed()
			{
				var btn = _view.buttons.FirstOrDefault(x => x.name == "Feed");
				btn.onClick.Invoke();
			}
			private async UniTask AddWallpapers()
			{
				var wallpapersResponse = await _wallpapersService.GetWallpapers();
				if (wallpapersResponse.StatusCode == Response.StatusCode.OK)
				{
					_signalBus.Fire(new LoadingSignal(LoadingSignalType.AddImages, wallpapersResponse.Data));
				}
			}
			private async void CheckAuth()
			{
				if (_userManager.Success)
				{
					await AddWallpapers();
					await _playlistService.Update();
					_signalBus.Fire(new PlayerSignal(ESignalType.Update));
					_user = await _userRepository.Get(_userManager.User.UserId);
					_view.header.gameObject.SetActive(true);
					foreach (Button button in _view.buttons)
					{
						if (_selestedButton == button.name)
						{
							_selestedButton = "";
							button.onClick.Invoke();
							break;
						}
					}
				}
				else
				{
					OpenSettings();
					_view.container.SetActive(false);
					_view.header.gameObject.SetActive(false);
					_userManager.LoginCompleted += CheckAuth;
				}
				_signalBus.BackWindow(EWindowLayer.Project);
			}
			public void Dispose()
			{
				foreach (var disposable in _disposables)
				{
					disposable.Dispose();
				}
			}
		}
	}

}
