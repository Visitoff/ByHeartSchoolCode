﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MenuSignal
		{
			public ESignalType signalType;
			public UnityAction callback;
			public UnityAction homeCallback;
			public GameObject present;
			public Func<UniTask> next;
			public bool isBackScreen;
			public GameObject underPresent;
			public UnityAction Lock;
			public UnityAction Unlock;
			public List<UnityAction> playerActions;
			public List<bool> boolList;
			public Sprite sprite;
			public string text;
			public ScrollRect scrollRect;
			public bool translate;
			public bool toRight;
			public bool isUp;
			public bool newMedia;
			public List<bool> boolList2;
			public List<int> intList;
			public List<string> stringList;
			public string videoUrl;
			public bool isRightToLeft;
			public List<Item> items;
			public bool enable;
			public bool loop;
            public MenuSignal(ESignalType signalType, List<Item> items)
            {
				this.signalType = signalType;
                this.items = items;
            }
			public MenuSignal(ESignalType signalType, bool enable)
			{
				this.signalType = signalType;
				this.enable = enable;
			}
			public MenuSignal(ESignalType signalType, UnityAction callback = null, UnityAction homeCallback = null, Func<UniTask> next = null, GameObject present = null, bool isBackScreen = false, GameObject underPresent = null)
            {
                this.signalType = signalType;
				this.callback = callback;
				this.homeCallback = homeCallback;
				this.present = present;
				this.next = next;
				this.isBackScreen = isBackScreen;
				this.underPresent = underPresent;
			}
            public MenuSignal(UnityAction Lock, UnityAction Unlock)
            {
				signalType = ESignalType.Message;
				this.Lock = Lock;
				this.Unlock = Unlock;
            }
            public MenuSignal(ESignalType signalType, List<UnityAction> playerActions, ScrollRect scrollRect)
            {
                this.signalType= signalType;
				this.playerActions= playerActions;
				this.scrollRect= scrollRect;
            }
			public MenuSignal(ESignalType signalType, List<bool> boolList, List<bool> boolList2, List<int> intList, List<string> stringList)
			{
				this.signalType = signalType;
				this.boolList = boolList;
				this.boolList2= boolList2;
				this.intList = intList;
				this.stringList= stringList;
			}
			public MenuSignal(ESignalType signalType, List<bool> boolList, List<string> stringList)
			{
				this.signalType = signalType;
				this.boolList = boolList;
				this.stringList = stringList;
			}
			public MenuSignal(ESignalType signalType, Sprite sprite, bool isUp, bool newMedia, string videoUrl, bool loop)
			{
				this.signalType = signalType;
				this.sprite = sprite;
				this.isUp = isUp;
				this.newMedia = newMedia;
				this.videoUrl = videoUrl;
				this.loop = loop;
			}
			public MenuSignal(ESignalType signalType, string text, bool translate, bool toRight = false, bool isRightToLeft = false)
			{
				this.signalType = signalType;
				this.text = text;
				this.translate = translate;
				this.toRight =  toRight;
				this.isRightToLeft = isRightToLeft;
			}
		}
	}

}
