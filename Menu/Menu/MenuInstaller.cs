﻿using ByHeartSchool.Services;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using Zenject;

namespace ByHeartSchool
{
	namespace Menu
	{
		public class MenuInstaller : MonoInstaller
		{
			public override void InstallBindings()
			{
				Container.BindInterfacesAndSelfTo<MenuView>().FromInstance(GetComponent<MenuView>()).AsSingle();
				Container.BindExecutionOrder<MenuController>(1);
				Container.BindInterfacesAndSelfTo<MenuController>().AsSingle();
				Container.DeclareSignal<MenuSignal>();
				Container.BindSignal<MenuSignal>().ToMethod<MenuController>(x => x.Signal).FromResolve();
			}
		}
	}

}
