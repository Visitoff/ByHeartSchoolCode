﻿using UnityEngine.UI;
using Zenject;
using UnityEngine;

namespace ByHeartSchool
{
	namespace DemoPlayer
	{
		public class DemoPlayerView: MonoInstaller
		{
			[SerializeField] public Button backToMenuBtn;
			[SerializeField] public Text phrase;
			[SerializeField] public Button nextBtn;
			[SerializeField] public Button prevBtn;
		}
	}

}
