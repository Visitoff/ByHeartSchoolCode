using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [Serializable]
    public class CharactersSceneLocationInfo
    {
        [SerializeField] private List<Location> _locationsInfo;

        private Dictionary<string, Location> _locations;// LocationType, Location : "Left", Location
        public Dictionary<string, Location> locations => _locations ?? CreateLocationsDictionary();

        private Dictionary<string, Location> CreateLocationsDictionary()
        {
            _locations = new Dictionary<string, Location>();
            foreach (var locationInfo in _locationsInfo)
            {
                if(_locations.ContainsKey(locationInfo.uniqueSide))
                {
                    Debug.Log($"{locationInfo.uniqueSide} already exists\n{GetType()} callback");
                }
                else
                {
                    _locations.Add(locationInfo.uniqueSide, locationInfo);   
                }
            }

            return _locations;
        }
    }
}