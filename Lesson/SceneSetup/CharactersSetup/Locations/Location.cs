using System;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [Serializable]
    public class Location
    {
        public string uniqueSide;
        public Transform transform;
    }
}