using System;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [Serializable]
    public class CharacterSkin
    {
        public string uniqueName;
        public GameObject characterPrefab;
    }
}