using System.Collections.Generic;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [CreateAssetMenu(fileName = "CharacterSkins", menuName = "Info/New SkinsInfo", order = 0)]
    public class SkinsInfo : ScriptableObject
    {
        [SerializeField] private List<CharacterSkin> _characterSkins;

        private Dictionary<string, GameObject> _skins;
        public Dictionary<string, GameObject> skins => GetSkins();

        private Dictionary<string, GameObject> GetSkins()
        {
            if (_skins != null)
                return _skins;

            _skins = new Dictionary<string, GameObject>();

            foreach (var skin in _characterSkins)
            {
                if (_skins.ContainsKey(skin.uniqueName))
                {
                    Debug.Log($"Not unique name in the Skins\n{GetType()} callback");
                }
                _skins.Add(skin.uniqueName, skin.characterPrefab);
            }

            return _skins;
        }
    }
}