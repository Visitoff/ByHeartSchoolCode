using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [Serializable]
    public class SceneCharacters: ISceneCharacters
    {
        [SerializeField] private CharacterPool _pool;
        public List<CharacterView> charactersOnScene { get; set; }

        public void Init(List<string> charactersToCreate, List<string> charactersLocation, List<List<string>> charactersAnimationIndexes)
        {
            charactersOnScene = new List<CharacterView>();
            CreateCharacters(charactersToCreate, charactersLocation, charactersAnimationIndexes);
        }

        public void Update(List<string> charactersToCreate, List<string> charactersLocation, List<List<string>> charactersAnimationIndexes)
        {
            DisableAll();
            CreateCharacters(charactersToCreate, charactersLocation, charactersAnimationIndexes);
        }
        
        private void CreateCharacters(List<string> charactersToCreate, List<string> charactersLocation, List<List<string>> charactersAnimationIndexes)
        {
            if (charactersToCreate == null || charactersLocation == null || charactersAnimationIndexes == null)
                return;
            
            try
            {
                CharacterView characterView;
                for (int i = 0; i < charactersToCreate.Count; i++)
                {
                    characterView = _pool.TryGet(charactersToCreate[i]);
                    if (characterView == null)
                    {
                        _pool.TryCreateNew(charactersToCreate[i], charactersLocation[i], charactersAnimationIndexes[i]);
                        characterView = _pool.TryGet(charactersToCreate[i]);
                    }

                    if (characterView == null)
                        continue;

                    characterView.model.position = charactersLocation[i];
                    characterView.model.animationStatePlayer.characterIndexes = charactersAnimationIndexes[i];
                    characterView.Show();
                    charactersOnScene.Add(characterView);
                }
            }
            catch (Exception e)
            {
                Debug.Log($"UNABLE TO CREATE CHARACTERS\n{GetType()} callback");
            }
        }

        private void DisableAll()
        {
            _pool.Return(charactersOnScene);
            charactersOnScene = new List<CharacterView>();
        }
    }
}