using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [Serializable]
    public class CharacterPool
    {
        [SerializeField] private Transform _parent;
        [SerializeField] private InstantiatorView instantiatorView;
        [SerializeField] private SkinsInfo _skinsInfo;
        [SerializeField] private CharactersSceneLocationInfo _locationInfo;
        
        //characterType, Characters : "Woman3", List<CharacterView>;
        private Dictionary<string, List<CharacterView>> _pool = new ();

        public void TryCreateNew(string characterType, string locationType, List<string> characterAnimationIndex)
        {
            if (_skinsInfo.skins.ContainsKey(characterType) && _locationInfo.locations.ContainsKey(locationType))
            {
                var character = Create(characterType, locationType, characterAnimationIndex);
                character.Hide();
                if (_pool.ContainsKey(characterType))
                {
                    _pool[characterType].Add(character);
                }
                else
                {
                    _pool.Add(characterType, new List<CharacterView>{character});   
                }
            }
            else
            {
                Debug.Log($"Unable to create {characterType} with location = {locationType}. Not found in DataBase\n {GetType()} callback");
            }
        }
        
        public CharacterView TryGet(string characterType)
        {
            if (!_pool.ContainsKey(characterType))
                return null;

            var list = _pool[characterType];
            var character = list.First();
            character.Show();
            list.Remove(character);
            
            if (!list.Any())
            {
                _pool.Remove(characterType);
            }
            
            return character;
        }

        public void Return(List<CharacterView> characters)
        {
            if(characters == null)
                return;
            
            foreach (var character in characters)
            {
                Return(character);
            }
        }
        
        public void Return(CharacterView characterView)
        {
            var characterType = characterView.model.skinType;
            if (_pool.ContainsKey(characterType))
            {
                _pool[characterType].Add(characterView);
            }
            else
            {
                _pool.Add(characterType, new List<CharacterView> { characterView });
            }
            characterView.Hide();   
        }
        
        private CharacterView Create(string characterType, string locationType, List<string> characterAnimationIndex)
        {
            var location = _locationInfo.locations[locationType];
            var character = instantiatorView.Create(_skinsInfo.skins[characterType], _parent);
            character.transform.SetPositionAndRotation(location.transform.position, location.transform.rotation);
            return new CharacterView(character, characterType, locationType, characterAnimationIndex);
        }
    }
}