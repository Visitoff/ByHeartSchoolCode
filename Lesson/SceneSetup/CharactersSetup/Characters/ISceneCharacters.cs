using System.Collections.Generic;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    public interface ISceneCharacters
    {
        public List<CharacterView> charactersOnScene { get; set; }

        public void Init(List<string> charactersToCreate, List<string> charactersLocation, 
            List<List<string>> charactersAnimationIndexes);
        public void Update(List<string> charactersToCreate, List<string> charactersLocation,
            List<List<string>> charactersAnimationIndexes);
    }
}