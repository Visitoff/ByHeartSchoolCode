using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    [Serializable]
    public class CharacterView
    {
        private GameObject _sceneCharacter;
        public CharacterModel model;
        
        public CharacterView(GameObject sceneCharacter, string type, string position, List<string> characterAnimationIndexes)
        {
            model = new CharacterModel(position, type, sceneCharacter, characterAnimationIndexes);
            _sceneCharacter = sceneCharacter;
        }

        public void Speak(AudioClip clip)
        {
            var audioSource = model.audioSource;
            if(CharacterController.IsAudioSourceNull(audioSource))
                return;

            audioSource.clip = clip;
            audioSource.volume = 1f;
            audioSource.Play();
        }

        public void KeepSilent()
        {
            var audioSource = model.audioSource;
            if(CharacterController.IsAudioSourceNull(audioSource))
                return;
            
            audioSource.Stop();
        }
        
        public void Show()
        {
            _sceneCharacter.gameObject.SetActive(true);
        }

        public void Hide()
        {
            _sceneCharacter.gameObject.SetActive(false);
        }
    }
}