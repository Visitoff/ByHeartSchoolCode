using System.Collections.Generic;
using Code.Animation;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    public class CharacterModel
    {
        public string position; //Left, Right, Center
        public string skinType; //Man3, Woman4
        public AudioSource audioSource;
        public ICharacterIndexes animationStatePlayer;

        public CharacterModel(string position, string skinType, GameObject gameObject, List<string> characterAnimationIndex)
        {
            this.skinType = skinType;
            this.position = position;
            audioSource = gameObject.GetComponent<AudioSource>();
            animationStatePlayer = gameObject.GetComponent<ICharacterIndexes>();

            if (animationStatePlayer != null)
            {
                animationStatePlayer.characterIndexes = characterAnimationIndex;
            }
        }
    }
}