using Code.Animation;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    public class CharacterController
    {
        public static bool IsComponentsValid(CharacterModel model)
        {
            if (IsPositionNull(model.position) || IsTypeNull(model.skinType) ||
                IsAudioSourceNull(model.audioSource) || IsAnimationStateNull(model.animationStatePlayer))
            {
                return false;
            }

            return true;
        }

        public static bool IsPositionNull(string position)
        {
            if (!position.Equals("")) 
                return false;
            Debug.Log($"The position is NULL");
            return true;
        }
        
        public static bool IsTypeNull(string type)
        {
            if (!type.Equals("")) 
                return false;
            Debug.Log($"The type is NULL");
            return true;
        }
        
        public static bool IsAudioSourceNull(AudioSource audioSource)
        {
            if (audioSource != null) 
                return false;
            Debug.Log($"The audioSource is NULL");
            return true;
        }

        public static bool IsAnimationStateNull(ICharacterIndexes state)
        {
            if (state != null) 
                return false;
            Debug.Log($"The AnimationState is NULL");
            return true;
        }
    }
}