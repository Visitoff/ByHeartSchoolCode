using UnityEngine;

namespace Code.Lesson.SceneSetup
{
    public class InstantiatorView : MonoBehaviour
    {
        public GameObject Create(GameObject gameObject, Transform parent)
        {
            return Instantiate(gameObject, parent);
        }
    }
}