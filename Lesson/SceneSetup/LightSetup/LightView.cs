using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Lesson.SceneSetup.LightSetup
{
    [Serializable]
    public class LightView: ILightView
    {
        [SerializeField] private Light _sceneLight;
        [SerializeField] private List<LightModel> _lightsSettings;

        public void Set(string lightType)
        {
            var targetLightSettings = _lightsSettings.Find(light => light.lightType.Equals(lightType));

            if (targetLightSettings == null)
            {
                Debug.Log($"Wrong lightType = {lightType}\n{GetType()} callback");
                return;
            }
            
            _sceneLight.color = targetLightSettings.color;
            _sceneLight.intensity = targetLightSettings.intensity;
            _sceneLight.transform.eulerAngles = targetLightSettings.eulerAngles;
        }
    }
}