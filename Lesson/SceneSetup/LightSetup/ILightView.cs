namespace Code.Lesson.SceneSetup.LightSetup
{
    public interface ILightView
    {
        public void Set(string lightType);
    }
}