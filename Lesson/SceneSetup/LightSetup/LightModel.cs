using System;
using UnityEngine;

namespace Code.Lesson.SceneSetup.LightSetup
{
    [Serializable]
    public class LightModel
    {
        public string lightType; //Day, Night, Sunrise, Sunset ...
        public Color color;
        public float intensity;
        public Vector3 eulerAngles;
    }
}