using UnityEngine;

namespace Code.Lesson.SceneSetup.CameraSystem
{
    public interface ICameraSystemView
    {
        public void Init(string cameraSetName, RenderTexture cameraTexture);
        public void Update(string cameraSetIndex, RenderTexture cameraTexture);
        public void ShowCameraByPosition(string position);
    }
}