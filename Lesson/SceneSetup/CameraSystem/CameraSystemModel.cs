using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CameraSystem
{
    [Serializable]
    public class CameraSystemModel
    {
        [Tooltip("All existed camera sets in the scene")]
        public List<CameraSet> cameraSets;
    }
    
    [Serializable]
    public class CameraSet
    {
        [Tooltip("Unique camera set name. Example: '1', '2', 'First person' ...")]
        public string name;
        [Tooltip("All cameras in the set separated according to position they show")]
        public List<SidesCameras> sidesCameras;
    }

    [Serializable]
    public class SidesCameras
    {
        [Tooltip("Should be one of the Location -> uniqueSide. Example: 'Left', 'Right','Mountain' ...")]
        public string uniquePosition; 
        [Tooltip("Cameras that show the same target")]
        public List<Camera> cameras;
    }
}