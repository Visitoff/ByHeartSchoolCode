using System;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CameraSystem
{
    [Serializable]
    public class CameraSystemView: ICameraSystemView
    {
        [SerializeField] private CameraSystemModel _model;

        private int _currentCameraIndex;
        private int _previousCameraIndex;
        
        private string _currentCameraSetName;
        
        private RenderTexture _texture;
        private CameraSystemController _controller;

        private string _previousFocusPosition;
        private string _currentFocusPosition;

        public void Init(string cameraSetName, RenderTexture cameraTexture)
        {
            _controller = new CameraSystemController(_model);
            Reset(cameraTexture);
            SetCameraSet(cameraSetName);
            DisableAllSets();
            
            // main camera is enabling
            var sets = _model.cameraSets;
            var set = _model.cameraSets.Find(cameraSet => cameraSet.name.Equals(_currentCameraSetName));
            EnableCamera(
                set != null ? 
                    set.sidesCameras[0].cameras[_currentCameraIndex] : 
                    sets[0].sidesCameras[0].cameras[0], _texture
                );
        }

        public void Update(string cameraSetIndex, RenderTexture cameraTexture)
        {
            DisableSet(_currentCameraSetName);
            Reset(cameraTexture);
            SetCameraSet(cameraSetIndex);
            EnableCamera(_model.cameraSets.Find(cameraSet => cameraSet.name.Equals(_currentCameraSetName)).sidesCameras[0].cameras[_currentCameraIndex], _texture);
        }
        
        public void ShowCameraByPosition(string position)
        {
            _currentFocusPosition = position;

            _previousCameraIndex = _currentCameraIndex;
            _currentCameraIndex = GetNewCameraIndex(_currentCameraSetName, _currentFocusPosition, _currentCameraIndex);

            DisableCamera(_currentCameraSetName, _previousFocusPosition, _previousCameraIndex);
            EnableCamera(_currentCameraSetName, _currentFocusPosition, _currentCameraIndex, _texture);
        }

        #region EnableVariations

        private void EnableCamera(string setName, string position, int camera, RenderTexture texture)
        {
            var theCamera = _model.cameraSets.Find(cameraSet => cameraSet.name.Equals(setName)).sidesCameras.Find(cameras => cameras.uniquePosition.Equals(position)).cameras[camera];
            theCamera.targetTexture = texture;
            theCamera.gameObject.SetActive(true);
        }

        private void EnableCamera(Camera camera, RenderTexture texture)
        {
            camera.targetTexture = texture;
            camera.gameObject.SetActive(true);
        }

        #endregion
        
        #region DisableVariations

        private void DisableAllSets()
        {
            foreach (var set in _model.cameraSets)
            {
                foreach (var character in set.sidesCameras)
                {
                    foreach (var camera in character.cameras)
                    {
                        DisableCamera(camera);
                    } 
                }
            }
        }
        
        private void DisableSet(string setIndex)
        {
            if (_controller.IsCameraSetValid(setIndex))
            {
                foreach (var character in _model.cameraSets.Find(set => set.name.Equals(setIndex)).sidesCameras)
                {
                    foreach (var camera in character.cameras)
                    {
                        DisableCamera(camera);
                    } 
                }   
            }
        }
        
        private void DisableCamera(string setName, string position, int camera)
        {
            var theCamera = _model.cameraSets.Find(set => set.name.Equals(setName)).sidesCameras.Find(cameras => cameras.uniquePosition.Equals(position)).cameras[camera];
            theCamera.targetTexture = null;
            theCamera.gameObject.SetActive(false);
        }
        
        private void DisableCamera(Camera camera)
        {
            camera.targetTexture = null;
            camera.gameObject.SetActive(false);
        }

        #endregion
        
        private int GetNewCameraIndex(string cameraSetName, string position, int cameraIndex)
        {
            var characterCameras = _model.cameraSets.Find(cameraSet => cameraSet.name.Equals(cameraSetName)).sidesCameras.Find(pos => pos.uniquePosition.Equals(position));

            if (characterCameras == null)
            {
                characterCameras = _model.cameraSets[0].sidesCameras[0];
            }
            
            var newIndex = UnityEngine.Random.Range(0, characterCameras.cameras.Count);
            while (newIndex == cameraIndex)
            {
                newIndex = UnityEngine.Random.Range(0, characterCameras.cameras.Count);
            }

            return newIndex;
        }
        
        private void SetCameraSet(string cameraSet)
        {
            if (_controller.IsCameraSetValid(cameraSet))
            {
                _currentCameraSetName = cameraSet;
            }
            else
            {
                _currentCameraSetName = _model.cameraSets[0].name;
                Debug.Log($"Otherwise set CameraSet={_currentCameraSetName}");
            }
        }

        private void Reset(RenderTexture texture)
        {
            _texture = texture;
            _currentCameraIndex = _previousCameraIndex = 0;
            _currentCameraSetName = _model.cameraSets[0].name;
            _previousFocusPosition = _currentFocusPosition = _model.cameraSets[0].sidesCameras[0].uniquePosition;

        }
    }
}