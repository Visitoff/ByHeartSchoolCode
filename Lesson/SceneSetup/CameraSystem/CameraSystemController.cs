using UnityEngine;

namespace Code.Lesson.SceneSetup.CameraSystem
{
    public class CameraSystemController
    {
        private readonly CameraSystemModel _model;

        public CameraSystemController(CameraSystemModel model)
        {
            _model = model;
        }

        public bool IsCameraSetValid(string cameraSet)
        {
            if (_model.cameraSets.Find(camera => camera.name.Equals(cameraSet)) != null)
                return true;
            
            Debug.Log($"Unable to show wrong CameraSet = {cameraSet}\n{GetType()} callback");
            return false;
        }
    }
}