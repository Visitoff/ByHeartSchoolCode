using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.Lesson.SceneSetup.CharactersSetup
{
    public class SceneOptionsParser
    {
        public int sceneInside;
        public string light;
        public string cameraSet;
        public List<List<string>> animationCharactersIndexes;
        public List<string> skins;
        public List<string> locations;

        public SceneOptionsParser(List<string> options)
        {
            if (options == null)
            {
                sceneInside = 0;
                light = "Day";
                cameraSet = "0";
                return;
            }

            locations = new();
            skins = new();
            animationCharactersIndexes = new();
            
            Parse(options);
        }
        
        //options example:
        //CamSet=1
        //Light=Day
        //SA=Location.Skin
        //SA,SB,SS=Location.Skin
        private void Parse(List<string> options)
        {
            SceneSet(options);
            SetLight(options);
            SetCameraSet(options);
            var charactersOptions = options.FindAll(x => x.Contains("."));
            SetAnimationIndexes(charactersOptions);
            SetLocations(charactersOptions);
            SetSkins(charactersOptions);
        }

        private void SceneSet(List<string> options)
        {
            try { 
            string scene = options.Find(opt => opt.Contains("Scene"));
            this.sceneInside =Convert.ToInt32(scene.Replace("Scene=", ""));
            }
            catch
            {
                string scene = options.Find(opt => opt.Contains("Scene"));
                Debug.Log(scene);
                Debug.Log(scene.Replace("Scene=", ""));
                Debug.Log($"Scene settings not found\n{GetType()} callback");
                sceneInside = 0;
            }
        }
        
        private void SetLight(List<string> options)
        {
            try
            {
                var light = options.Find(opt => opt.Contains("Light"));
                this.light = light.Substring(light.IndexOf("=") + 1);
            }
            catch (Exception e)
            {
                Debug.Log($"Light settings not found\n{GetType()} callback");
                light = "Day";
            }
        }

        private void SetCameraSet(List<string> options)
        {
            try
            {
                var cameraSet = options.Find(opt => opt.Contains("CamSet"));
                this.cameraSet = cameraSet.Substring(cameraSet.IndexOf("=") + 1);
            }
            catch (Exception exception)
            {
                Debug.Log($"WRONG camera set input\n{GetType()} callback");
            }
        }

        private void SetLocations(List<string> charactersOptions)
        {
            try
            {
                int startIndex, endIndex;
                foreach (var option in charactersOptions)
                {
                    startIndex = option.IndexOf("=") + 1;
                    endIndex = option.IndexOf('.', startIndex);
                    locations.Add(option.Substring(startIndex, endIndex - startIndex));
                }
            }
            catch (Exception e)
            {
                Debug.Log($"WRONG LOCATIONS INPUT\n{GetType()} callback");
            }
        }
        private void SetAnimationIndexes(List<string> charactersOptions)
        {
            try
            {
                animationCharactersIndexes = charactersOptions.Select(str =>
                    str.Substring(0, str.IndexOf("=")).Split(',').ToList()).ToList();
            }
            catch (Exception e)
            {
                Debug.Log($"WRONG ANIMATION INDEXES INPUT\n{GetType()} callback");
            }
            
        }
        private void SetSkins(List<string> charactersOptions)
        {
            try
            {
                int startIndex, endIndex;
                foreach (var option in charactersOptions)
                {
                    startIndex = option.LastIndexOf('.') + 1;
                    endIndex = option.Length;
                    skins.Add(option.Substring(startIndex, endIndex - startIndex));
                }
            }
            catch (Exception e)
            {
                Debug.Log($"WRONG SKINS INPUT\n{GetType()} callback");
            }
        }
    }
}