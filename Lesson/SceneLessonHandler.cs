using System.Collections.Generic;
using ByHeartSchool.Services;
using Code.Lesson.SceneSetup;
using Code.Lesson.SceneSetup.CameraSystem;
using Code.Lesson.SceneSetup.CharactersSetup;
using Code.Lesson.SceneSetup.LightSetup;
using UnityEngine;

[RequireComponent(typeof(InstantiatorView))]
public class SceneLessonHandler : MonoBehaviour, IMediaScene
{
    [SerializeField] List<GameObject> scenes;
    [SerializeReference] private ILightView _lightView = new LightView();
    [SerializeReference] private ISceneCharacters _sceneCharacters = new SceneCharacters();
    [SerializeReference] private ICameraSystemView _cameraSystemView = new CameraSystemView();

    public void Init(List<string> options, RenderTexture texture)
    {
        ShowOptions(options);
        var parser = new SceneOptionsParser(options);
        scenes[parser.sceneInside].SetActive(true);
        _cameraSystemView.Init(parser.cameraSet, texture);
        _sceneCharacters.Init(parser.skins, parser.locations, parser.animationCharactersIndexes);
        _lightView.Set(parser.light);
    }

    public void Play(MediaSceneEvent mediaSceneEvent)
    {
        Debug.Log("TYPE " + mediaSceneEvent.type);
        string type = mediaSceneEvent.type;
        AudioClip audioClip = mediaSceneEvent.clip;
        var characters = _sceneCharacters.charactersOnScene;

        for (int i = 0; i < characters.Count; i++)
        {
            if (characters[i].model.animationStatePlayer.characterIndexes.Contains(type))
            {
                characters[i].Speak(audioClip);
                _cameraSystemView.ShowCameraByPosition(characters[i].model.position);
            }
        }
    }

    //use when another scene (with the same name) invoked with another options
    //example: Init ParkScene with Man1 Woman3
    //Invoke another time ParkScene with Man1 Woman1 (Use UpdateScene method. It uses ObjectPool) 
    public void UpdateScene(List<string> options, RenderTexture texture)
    {
        var parser = new SceneOptionsParser(options);
        foreach (GameObject previousScene in scenes) { previousScene.SetActive(false); }
        scenes[parser.sceneInside].SetActive(true);
        _cameraSystemView.Update(parser.cameraSet, texture);
        _sceneCharacters.Update(parser.skins, parser.locations, parser.animationCharactersIndexes);
        _lightView.Set(parser.light);
    }

    public void Stop()
    {
        var characters = _sceneCharacters.charactersOnScene;

        foreach (var character in characters)
        {
            character.KeepSilent();
        }
    }

    private void ShowOptions(List<string> options)
    {
        Debug.Log("OPTIONS:");
        if (options == null)
            return;
        foreach (var option in options)
        {
            Debug.Log(option);
        }
    }
}
