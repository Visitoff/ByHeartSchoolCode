﻿using ByHeartSchool.Repositories;

namespace ByHeartSchool
{
	public static class Configuration
	{
		public static EConfiguration CurrentConfiguration { get; set; }
		public static string FirestoreAppsId
		{
			get
			{
				return CurrentConfiguration switch
				{
					EConfiguration.BEASTMODE => "beastmode.pro",
					EConfiguration.LEKKERDAY => "lekker.day",
					_ => ""
				};
			}
		}
		public static string LEARNScreenId
		{
			get
			{
				return CurrentConfiguration switch
				{
					EConfiguration.BEASTMODE => "JU5CsBnoJC8tuVNn7rev",
					EConfiguration.LEKKERDAY => "ld.learn",
					_ => "",
				};
			}
		}
		public static string USEScreenId
		{
			get
			{
				return CurrentConfiguration switch
				{
					EConfiguration.BEASTMODE => "A7x2UocZhKWg5pxMnwHr",
					EConfiguration.LEKKERDAY => "ld.use",
					_ => "",
				};
			}
		}
		public static string PLAYScreenId
		{
			get
			{
				return CurrentConfiguration switch
				{
					EConfiguration.BEASTMODE => "Nb66kXFpIMIWDJasAi5o",
					EConfiguration.LEKKERDAY => "ld.game",
					_ => "",
				};
			}
		}
		public static string FEEDScreenId
		{
			get
			{
				return CurrentConfiguration switch
				{
					EConfiguration.BEASTMODE => "O1eU6lkYkPR5Eh9LfcWr",
					EConfiguration.LEKKERDAY => "ld.feed",
					_ => "",
				};
			}
		}
		public static string MYScreenId
		{
			get
			{
				return CurrentConfiguration switch
				{
					EConfiguration.BEASTMODE => "bm.my",
					EConfiguration.LEKKERDAY => "ld.my",
					_ => "",
				};
			}
		}
		public static string LANGUAGEScreenId
		{
			get
			{
				return CurrentConfiguration switch 
				{
					EConfiguration.BEASTMODE => "system.languages", 
					EConfiguration.LEKKERDAY => "system.languages.ld",
					_ => ""
				};
			}
		}
	}
}